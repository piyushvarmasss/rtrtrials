// header files
#include <GL/freeglut.h>

//global variable declarations
bool bIsFullScreen = false;

//entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Kalyani Ashok Magdum");

	initialize();


	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}
void display(void)
{
	//Function declaration
	void clock();
	
	//code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	clock();

	glutSwapBuffers();

}

void clock(void)
{
	// code
	//Outer Box
	glBegin(GL_QUADS);
	glColor3f(0.8f, 0.4f,0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glEnd();

	//Inner 
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);	
	glVertex3f(-0.4f,0.4f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glEnd();

	
	//12 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.33f, 0.0f);
	glVertex3f(-0.01f, 0.33f, 0.0f);
	glEnd();
	
	//3 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.33f, 0.01f, 0.0f);
	glVertex3f(0.39f, 0.01f, 0.0f);
	glVertex3f(0.39f, -0.01f, 0.0f);
	glVertex3f(0.33f, -0.01f, 0.0f);
	glEnd();



	//6 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.39f, 0.0f);
	glVertex3f(-0.01f, -0.39f, 0.0f);
	glEnd();
	
	//9 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.39f, 0.01f, 0.0f);
	glVertex3f(-0.33f, 0.01f, 0.0f);
	glVertex3f(-0.33f, -0.01f, 0.0f);
	glVertex3f(-0.39f, -0.01f, 0.0f);


	
	//glVertex3f(-0.39f, 0.0f, 0.0f);
	//glVertex3f(-0.35f, 0.0f, 0.0f);
	glEnd();
	
	//Minute kata
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f,0.01f,0.0f);
	glVertex3f(0.32f, 0.0f, 0.0f);
	glVertex3f(-0.05f, -0.01f, 0.0f);
	glEnd();

	//Hr kata
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.01f, 0.0f);
	glVertex3f(-0.15f, -0.25f, 0.0f);
	glVertex3f(-0.05f, -0.01f, 0.0f);
	glEnd();
	
	
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}







