void Clock(void)
{
	// code
	//Outer Box
	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.0f, 0.4f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glEnd();

	//Inner Box
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glEnd();

	// Rectangles showing Time
	//12 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.33f, 0.0f);
	glVertex3f(-0.01f, 0.33f, 0.0f);
	glEnd();

	//3 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.33f, 0.01f, 0.0f);
	glVertex3f(0.39f, 0.01f, 0.0f);
	glVertex3f(0.39f, -0.01f, 0.0f);
	glVertex3f(0.33f, -0.01f, 0.0f);
	glEnd();

	//6 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.39f, 0.0f);
	glVertex3f(-0.01f, -0.39f, 0.0f);
	glEnd();

	//9 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.39f, 0.01f, 0.0f);
	glVertex3f(-0.33f, 0.01f, 0.0f);
	glVertex3f(-0.33f, -0.01f, 0.0f);
	glVertex3f(-0.39f, -0.01f, 0.0f);
	glEnd();

	//Minute Hand
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.02f, 0.0f, 0.0f);
	glVertex3f(-0.12f, 0.28f, 0.0f);
	glVertex3f(-0.1f, 0.3f, 0.0f);
	glEnd();

	//Hour Hand
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.0f, 0.0f);
	glVertex3f(0.01f, 0.0f, 0.0f);
	glVertex3f(0.01f, 0.2f, 0.0f);
	glVertex3f(-0.01f, 0.2f, 0.0f);
	glEnd();
}