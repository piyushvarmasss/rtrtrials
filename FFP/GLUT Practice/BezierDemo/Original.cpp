// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>
#include<stdlib.h>
#include<math.h>

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

void Frame()
{
	// Outer Rectangle
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	// Inner Rectangle
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.6f, -0.6f, 0.0f);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.6f, 0.6f, 0.0f);
	glVertex3f(0.6f, -0.6f, 0.0f);
	glEnd();
}

void Man()
{
	// Man Body
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.45f, -0.6f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glVertex3f(-0.05f, -0.2f, 0.0f);
	glVertex3f(-0.45f, -0.2f, 0.0f);
	glEnd();

	// Man Shoulder Part
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * cosf(angle)), -0.2f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Man Head
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Man Left Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.05f - (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);
	
	glEnd();

	// Man Right Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (float angle = 0.0f; angle <= ((3.1515926f * 3.0f) / 4.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f - (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);


	Point p1 = { -0.15f, 0.2f, 0.0f };
	Point p2 = { -0.25, 0.1f, 0.0f };
	Point p3 = { -0.45f, 0.1f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * (1.0f - u) * u) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * (1.0f - u) * u) + (p3.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void Woman()
{
	// Woman Head Circle
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.23f * cosf(angle)), 0.1f + (0.23f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Head Left Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);

	Point p1 = { 0.48f, 0.1f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.25f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * (1.0f - u) * u) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * (1.0f - u) * u) + (p3.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Woman Head Right Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);

	Point p4 = { 0.02f, 0.1f, 0.0f };
	Point p5 = { -0.1f, -0.2f, 0.0f };
	Point p6 = { 0.25f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * (1.0f - u) * u) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * (1.0f - u) * u) + (p6.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Woman Body
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.05f, -0.6f, 0.0f);
	glVertex3f(0.45f, -0.6f, 0.0f);
	glVertex3f(0.45f, -0.2f, 0.0f);
	glVertex3f(0.05f, -0.2f, 0.0f);
	glEnd();

	// Woman Shoulder Part
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * cosf(angle)), -0.2f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Face
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Left Forehead hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.45f - (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);

	glEnd();

	// Woman Right Forehead hairs
	glBegin(GL_POLYGON);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f - (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.05f + (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);

	glEnd();
}

void HalfHeart() {
	
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f( -0.075f + 0.1f * cosf(angle), 0.1f * sinf(angle), 0.0f);
	glEnd();

	glScalef(1.5f, 1.0f, 1.0f);
	glTranslatef(-0.01f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	
	Point p1 = { -0.1f, 0.0f, 0.0f };
	Point p2 = { 0.1f, 0.1f, 0.0f };
	Point p3 = { 0.1f, -0.15f, 0.0f };
	Point p4 = { -0.1f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		
		float x = (p1.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3.x * u * u * (1.0f - u)) + (p4.x * u * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3.y * u * u * (1.0f - u)) + (p4.y * u * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void PhotoFrame() {
	Frame();
	Man();
	Woman();

	glTranslatef(0.15f, 0.4f, 0.0f);
	HalfHeart();

	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.7f, 1.0f, 1.0f);
	glTranslatef(0.3f, 0.0f, 0.0f);
	HalfHeart();
}

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;
float x = 0.0f;
float y = 0.0f;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// FunctionDeclaration
	void gift (void);
	void confitti(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//PhotoFrame();
	gift();
	confitti();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}

void gift(void)
{
	// Code of gift
	glBegin(GL_QUADS);

	// Code of Box
	glColor3f(0.5f, 0.8f, 0.0f);
	glVertex3f(-0.75f, -0.87f, 0.0f);
	glVertex3f(-0.45f, -0.87f, 0.0f);
	glVertex3f(-0.45f, -0.67f, 0.0f);
	glVertex3f(-0.75f, -0.67f, 0.0f);

	// Code of Box Lid
	glVertex3f(-0.77f, -0.73f, 0.0f);
	glVertex3f(-0.43f, -0.73f, 0.0f);
	glVertex3f(-0.43f, -0.67f, 0.0f);
	glVertex3f(-0.77f, -0.67f, 0.0f);

	// Code of Ribbon horizontal
	glColor3f(0.4f, 0.8f, 0.9f);
	glVertex3f(-0.75f, -0.80f, 0.0f);
	glVertex3f(-0.45f, -0.80f, 0.0f);
	glVertex3f(-0.45f, -0.77f, 0.0f);
	glVertex3f(-0.75f, -0.77f, 0.0f);

	// Code of Ribbon Vertical
	glVertex3f(-0.61f, -0.87f, 0.0f);
	glVertex3f(-0.59f, -0.87f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.61f, -0.67f, 0.0f);

	//Code of Ribbon Knot
	glVertex3f(-0.61f, -0.67f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.64f, -0.6f, 0.0f);
	glVertex3f(-0.66f, -0.63f, 0.0f);

	glVertex3f(-0.61f, -0.67f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.55f, -0.63f, 0.0f);
	glVertex3f(-0.57f, -0.6f, 0.0f);

	glEnd();

	// Code of Box Lid line
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.75f, -0.73f, 0.0f);
	glVertex3f(-0.45f, -0.73f, 0.0f);
	glEnd();
}

void confitti(){
	
	// Function Declarations
	void triangle1(float x, float y);
	void triangle2(float x, float y);
	void triangle3(float x, float y);
	void triangle4(float x, float y);
	void triangle5(float x, float y);
	void Rectangle1(float x, float y);
	void Rectangle2(float x, float y);
	void Rectangle3(float x, float y);

	triangle1(-0.8f, 0.8f);
	triangle2(-0.9f, 0.9f);
	triangle3(-0.8f, 0.8f);
	triangle4(-0.8f, 0.8f);
	triangle5(-0.9f, 0.9f);
	Rectangle1(-0.7f, 0.7f);
	Rectangle2(-0.7f, 0.6f);
	Rectangle3(-0.6f, 0.8f);

	triangle1(-0.7f, 0.7f);
	triangle2(-0.8f, 0.8f);
	triangle3(-0.7f, 0.9f);
	triangle4(-0.7f, 0.7f);
	triangle5(-0.8f, 0.6f);
	Rectangle1(-0.6f, 0.6f);
	Rectangle2(-0.6f, 0.7f);
	Rectangle3(-0.5f, 0.8f);

	triangle1(0.0f, 0.8f);
	triangle2(-0.2f, 0.9f);
	triangle3(-0.3f, 0.8f);
	triangle4(-0.5f, 0.8f);
	triangle5(-0.6f, 0.9f);
	Rectangle1(-0.7f, 0.7f);
	Rectangle2(-0.48f, 0.9f);
	Rectangle3(-0.35f, 0.6f);

	triangle1(0.05f, 0.7f);
	triangle2(-0.25f, 0.8f);
	triangle3(-0.35f, 0.6f);
	triangle4(-0.55f, 0.7f);
	triangle5(-0.65f, 0.8f);
	Rectangle1(-0.75f, 0.6f);
	Rectangle2(-0.48f, 0.8f);
	Rectangle3(-0.35f, 0.6f);

	triangle1(0.0f, 0.8f);
	triangle2(0.2f, 0.9f);
	triangle3(0.3f, 0.8f);
	triangle4(0.5f, 0.8f);
	triangle5(0.6f, 0.9f);
	Rectangle1(0.7f, 0.7f);
	Rectangle2(0.48f, 0.9f);
	Rectangle3(0.35f, 0.6f);

	triangle1(0.1f, 0.8f);
	triangle2(0.25f, 0.9f);
	triangle3(0.33f, 0.8f);
	triangle4(0.3f, 0.8f);
	triangle5(0.4f, 0.9f);
	Rectangle1(0.6f, 0.7f);
	Rectangle2(0.3f, 0.9f);
	Rectangle3(0.1f, 0.6f);

	triangle1(0.1f, 0.7f);
	triangle2(0.25f, 0.8f);
	triangle3(0.33f, 0.6f);
	triangle4(0.3f, 0.7f);
	triangle5(0.4f, 0.6f);
	Rectangle1(0.6f, 0.8f);
	Rectangle2(0.3f, 0.9f);
	Rectangle3(0.1f, 0.6f);


	triangle1(-0.4f, 0.7f);
	triangle2(-0.2f, 0.8f);
	triangle3(-0.2f, 0.9f);
	triangle4(-0.4f, 0.7f);
	triangle5(-0.6f, 0.6f);
	Rectangle1(-0.1f, 0.6f);
	Rectangle2(-0.2f, 0.7f);
	Rectangle3(-0.3f, 0.8f);

	triangle1(0.8f, 0.8f);
	triangle2(0.9f, 0.9f);
	triangle3(0.8f, 0.8f);
	triangle4(0.8f, 0.8f);
	triangle5(0.9f, 0.9f);
	Rectangle1(0.7f, 0.7f);
	Rectangle2(0.7f, 0.7f);
	Rectangle3(0.6f, 0.7f);

	triangle1(0.6f, 0.7f);
	triangle2(0.7f, 0.6f);
	triangle3(0.8f, 0.6f);
	triangle4(0.6f, 0.7f);
	triangle5(0.9f, 0.8f);
	Rectangle1(0.5f, 0.8f);
	Rectangle2(0.4f, 0.9f);
	Rectangle3(0.3f, 0.6f);

}

void triangle1(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(x+0.6f, x+0.2f, 0.9f);
		glVertex3f(x, y, 0.0f);
		glVertex3f(x + 0.05f, y + 0.05f, 0.0f);
		glVertex3f(x, y + 0.1f, 0.0f);
	glEnd();
}

void triangle2(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(y+0.5f, y+1.0f, 1.0f);
		glVertex3f(x - 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x - 0.1f, y - 0.1f, 0.0f);
		glVertex3f(x - 0.05f, y - 0.15f, 0.0f);
	glEnd();
}

void triangle3(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(x+0.6f, y+1.0f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x + 0.07f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.05f, 0.0f);
	glEnd();
}

void triangle4(float x, float y) {


	glBegin(GL_TRIANGLES);
		glColor3f(y+0.8f, x+0.4f, 1.0f);
		glVertex3f(x - 0.1f, y + 0.0f, 0.0f);
		glVertex3f(x - 0.13f, y + 0.03f, 0.0f);
		glVertex3f(x - 0.16f, y + 0.0f, 0.0f);
	glEnd();
}

void triangle5(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(y+0.9f, y+1.0f, 0.8f);
		glVertex3f(x - 0.08f, y - 0.2f, 0.0f);
		glVertex3f(x - 0.04f, y - 0.24f, 0.0f);
		glVertex3f(x - 0.0f, y - 0.2f, 0.0f);
	glEnd();
}

void Rectangle1(float x, float y) {
	glBegin(GL_QUADS);
		glColor3f(x+0.6f, x+1.0f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.05f, 0.0f);
	glEnd();
}

void Rectangle2(float x, float y) {
	glBegin(GL_QUADS);
	glColor3f(y+0.5f, x+1.0f, 1.0f);
	glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
	glVertex3f(x + 0.05f, y - 0.07f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.07f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.05f, 0.0f);
	glEnd();
}

void Rectangle3(float x, float y) {
	glBegin(GL_QUADS);
	glColor3f(x+0.6f, y+0.2f, 0.9f);
	glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
	glVertex3f(x + 0.05f, y - 0.09f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.09f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.05f, 0.0f);
	glEnd();
}