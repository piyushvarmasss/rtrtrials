// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>
#include<stdlib.h>
#include<math.h>

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

float y = 0.2f;
void display(void)
{

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 1.0f, 1.0f);
		
		// p1 = -0.5f, 0.0f, 0.0f
		// p2 = 0.0f, 0.5f, 0.0f
		// p3 = 0.5f, 0.0f, 0.0f

		for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
			float x = (-0.5f * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (-0.15f * 3.0f * (1.0f - u) * (1.0f - u) * u) + (0.15f * 2.0f * (1.0f - u) * u * u) + (0.5f * u * u * u);
			float y = (0.0f * (1.0f - u) * (1.0f - u) * (1.0f -u)) + (0.5f * 3.0f * (1.0f - u) * (1.0f - u) * u) + (-0.5f * 2.0f * (1.0f -u) * u * u)  + (0.0f * u * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

	glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}


