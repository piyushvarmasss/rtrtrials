
// Header Files
#include<GL/freeglut.h>
#include<math.h>

// Macro Declaration
#define PNV_PI 3.1415926

// Structure Declaration
typedef struct Point {
	float x; 
	float y;
	float z;
} Point;

// Global Variable Declarations
bool bIsFullScreen = false;

// Entry-point function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Rupali & Ashish BirthDay");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Function Declarations
	void PNV_Ocean(void);
	void PNV_HotAirBalloon(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	PNV_Ocean();
	PNV_HotAirBalloon();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
	 	case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}

void PNV_Ocean(void)
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void bezierCurve4Points(Point*, Point*, Point*, Point*);
	void BSplineCurve(int pointBase, int noOfPoints, Point * arr[])

	// Local Variables
	Point p1 = { -1.0f, 0.0f, 0.0f };     
	Point p2 = { 0.0f, 0.05f, 0.0f };
	Point p3 = { 1.0f, 0.0f, 0.0f };
	Point p4 = { 0.0f, 0.0f, 0.0f };


	// Ocean
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.8f);
		glVertex3f(-1.0f, -0.02f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glVertex3f(1.0f, -0.02f, 0.0f);
	glEnd();

	for (float y = -0.02f; y >= -0.48f; y -= 0.02f)
	{
		glBegin(GL_QUADS);
			glColor3f(0.0f, 0.0f, (0.8f - (2.0f * y)));
			glVertex3f(-1.0f, y, 0.0f);
			glVertex3f(1.0f, y, 0.0f);
			glVertex3f(1.0f, (y - 0.02f), 0.0f);
			glVertex3f(-1.0f, (y - 0.02f), 0.0f);
		glEnd();
	}

	// Bezier Curve For Shore Water

	// Ocean Shore
	glBegin(GL_QUADS);
		glColor3f(0.5f, 0.5f, 0.5f);
		glVertex3f(-1.0f, -0.5f, 0.0f);
		glVertex3f(1.0f, -0.5f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();
}

void PNV_HotAirBalloon(void) 
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void bezierCurve4Points(Point*, Point*, Point*, Point*);
	void BSplineCurve(int pointBase, int noOfPoints, Point * arr[])

	// Local Variables
	Point p1 = { -1.0f, 0.0f, 0.0f };
	Point p2 = { 0.0f, 0.05f, 0.0f };
	Point p3 = { 1.0f, 0.0f, 0.0f };
	Point p4 = { 0.0f, 0.0f, 0.0f };

	// HotAirBalloon
}

void assignPoint(Point* point, float x, float y, float z) 
{
	point->x = x;
	point->y = y;
	point->z = z;
}

void circle2D(Point* center, float radius, float sAngle, float eAngle)
{
	while (sAngle <= eAngle) 
	{
		glVertex3f(center->x + (radius * sinf((sAngle * PNV_PI) / 180.0f)), center->y + (radius * cosf(sAngle * PNV_PI) / 180.0f), 0.0f);
		sAngle += 0.1f;
	}
}

void bezierCurve3Points(Point *p1, Point *p2, Point *p3)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);

		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point *p1, Point *p2, Point *p3, Point *p4)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);

		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point *p1, Point *p2, Point *p3, Point *p4, Point *p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void BSplineCurve(int pointBase, int noOfPoints, Point* arr[]) {

	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void bezierCurve4Points(Point*, Point*, Point*, Point*);
	void bezierCurve5Points(Point*, Point*, Point*, Point*, Point*);

	for (int i = 0; i <= noOfPoints - pointBase; i++)
		switch (pointBase) {
			case 3:
				bezierCurve3Points(arr[i], arr[i + 1], arr[i + 2]);
				break;

			case 4:
				bezierCurve4Points(arr[i], arr[i + 1], arr[i + 2], arr[i + 3]);
				break;

			case 5:
				bezierCurve5Points(arr[i], arr[i+1], arr[i+2], arr[i+3], arr[i+4]);
				break;
		}	
}