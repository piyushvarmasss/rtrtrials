//Header files
#include <GL/freeglut.h>
#include <MMSystem.h>													//For PlaySound win32 API
#include<math.h>

//Global variable declarations
bool bIsFullScreen = false;

//For Animation
GLfloat happyXCoordinate = -1.0f;
GLfloat birthdayXCoordinate = 1.5f;
GLfloat kalyaniYCoordinate = -2.0f;

GLfloat ballonAngle = 0.0f;
GLfloat flame = 0.0f;

GLfloat cackeAnimation = 0.0f;

//For Baloons
float r = 0.2f, cy = 0.5f, cx = 0.5f;
int num_segments = 15;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);
	
	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Happy Birthday Kalyani RTR5:Frustum Group");

	initialize();
	glutFullScreen();
	glutReshapeFunc(resize);
	glutDisplayFunc(display);

	// glutIdleFunc(update);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}


void initialize(void)
{
	// code
	PlaySound("song.WAV", NULL, SND_ASYNC); 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	void drawHappy(void);
	void drawBirthday(void);
	void drawKalyani(void);
	void drawCake(void);

	void update(void);
	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	//Happy
	glPushMatrix();
		// glTranslatef(0.0,0.0f,0.0f);

		glTranslatef(happyXCoordinate,0.0f,0.0f);
		drawHappy();
	glPopMatrix();

	//Birthday
	glPushMatrix();
		// glTranslatef(0.05,0.0f,0.0f);

		glTranslatef(birthdayXCoordinate,0.0f,0.0f);
		drawBirthday();
	glPopMatrix();


	//Kalyani
	glPushMatrix();
		// glTranslatef(0.1f,-0.2,0.0f);
	
		glTranslatef(0.1f,kalyaniYCoordinate,0.0f);

		drawKalyani();
	glPopMatrix();

	//Cake
	glPushMatrix();
		glScalef(cackeAnimation,cackeAnimation,cackeAnimation);
		drawCake();
	glPopMatrix();


	
	update();
	glutSwapBuffers();
}

//Update Function
void update(){
		// happyYCoordinate =  happyYCoordinate - 0.1;

	if(happyXCoordinate <= 0.0f){	//-0.15
		happyXCoordinate =  happyXCoordinate + 0.0025;
		// printf("----%f\n",happyXCoordinate);	
	}else{
		if(birthdayXCoordinate > 0.05f){  //0.15
			birthdayXCoordinate = birthdayXCoordinate - 0.0045;

		}else{
			if(kalyaniYCoordinate < -0.2f){
				kalyaniYCoordinate = kalyaniYCoordinate + 0.0045;

				if(kalyaniYCoordinate >= -1.0){
					if(cackeAnimation < 1.0f){
						cackeAnimation+=0.005;
					}
				}
			}
			
		}
		
	}
	
	glutPostRedisplay();
}

//Bunch Of 3 Ballons
void bunchOfBallons(){
		void drawBallons(void);

		glScalef(0.4f, 0.6f, 0.0f);
		glRotatef(ballonAngle, -0.9f, 0.0f, 1.0f);
		glPushMatrix();
			glTranslatef(0.0f, 0.0f, 0.0f);
			drawBallons();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-0.3f, -0.2f, 0.0f);
			drawBallons();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.3f, -0.2f, 0.0f);
			drawBallons();
		glPopMatrix();

}
//ballons by @Piyush & @Dev
void drawBallons(){
	//code
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	glScalef(0.3f, 0.5f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
			for (int ii = 0; ii < num_segments; ii++)
			{	glColor3f(1.0f, 1.0f, 0.0f);
				float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle

				float x = r * cosf(theta);//calculate the x component
				float y = r * sinf(theta);//calculate the y component

				glVertex2f(x + cx, y + cy);//output vertex

			}

		glEnd();
	glPopMatrix();

	//String
	glPushMatrix();
		glColor3f(1.0f, 1.0f, 1.0f);
		glScalef(0.1f, 0.3f, 0.2f);
		glTranslatef(1.2f, 0.0f, 0.0f);

		glBegin(GL_LINES);

			glVertex2f(0.2f, 0.5f);
			glVertex2f(-0.3f, 0.0f);
			glVertex2f(-0.3f, 0.0f);
			glVertex2f(0.2f, -0.5f);

		glEnd();
	glPopMatrix();

}

//Word Happy By Piyush
void drawHappy(){
	glBegin(GL_TRIANGLES);
		// Code of H

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.68f, 0.7f, 0.0f);
		glVertex3f(-0.66f, 0.7f, 0.0f);
		glVertex3f(-0.68f, 0.6f, 0.0f);

		glVertex3f(-0.66f, 0.7f, 0.0f);
		glVertex3f(-0.68f, 0.6f, 0.0f);
		glVertex3f(-0.66f, 0.6f, 0.0f);

		glVertex3f(-0.66f, 0.67f, 0.0f);
		glVertex3f(-0.64f, 0.67f, 0.0f);
		glVertex3f(-0.66f, 0.63f, 0.0f);

		glVertex3f(-0.66f, 0.63f, 0.0f);
		glVertex3f(-0.64f, 0.63f, 0.0f);
		glVertex3f(-0.64f, 0.67f, 0.0f);

		glVertex3f(-0.64f, 0.7f, 0.0f);
		glVertex3f(-0.64f, 0.6f, 0.0f);
		glVertex3f(-0.62f, 0.7f, 0.0f);

		glVertex3f(-0.62f, 0.7f, 0.0f);
		glVertex3f(-0.62f, 0.6f, 0.0f);
		glVertex3f(-0.64f, 0.6f, 0.0f);


		// Code of A
		glColor3f(0.0f,0.4f,1.0f);
		glVertex3f(-0.58f, 0.7f, 0.0f);
		glVertex3f(-0.58f, 0.68f, 0.0f);
		glVertex3f(-0.52f, 0.7f, 0.0f);

		glVertex3f(-0.52f, 0.7f, 0.0f);
		glVertex3f(-0.52f, 0.68f, 0.0f);
		glVertex3f(-0.58f, 0.68f, 0.0f);

		glVertex3f(-0.58f, 0.68f, 0.0f);
		glVertex3f(-0.56f, 0.68f, 0.0f);
		glVertex3f(-0.58f, 0.6f, 0.0f);

		glVertex3f(-0.58f, 0.6f, 0.0f);
		glVertex3f(-0.56f, 0.68f, 0.0f);
		glVertex3f(-0.56f, 0.6f, 0.0f);

		glVertex3f(-0.54f, 0.68f, 0.0f);
		glVertex3f(-0.54f, 0.6f, 0.0f);
		glVertex3f(-0.52f, 0.68f, 0.0f);

		glVertex3f(-0.52f, 0.68f, 0.0f);
		glVertex3f(-0.54f, 0.6f, 0.0f);
		glVertex3f(-0.52f, 0.6f, 0.0f);

		glVertex3f(-0.56f, 0.66f, 0.0f);
		glVertex3f(-0.56f, 0.64f, 0.0f);
		glVertex3f(-0.54f, 0.66f, 0.0f);

		glVertex3f(-0.56f, 0.64f, 0.0f);
		glVertex3f(-0.54f, 0.66f, 0.0f);
		glVertex3f(-0.54f, 0.64f, 0.0f);


		// Code of P
		glColor3f(0.0f,0.8f,1.0f);							//blue
		glVertex3f(-0.48f, 0.7f, 0.0f);
		glVertex3f(-0.48f, 0.6f, 0.0f);
		glVertex3f(-0.46f, 0.7f, 0.0f);

		glVertex3f(-0.46f, 0.7f, 0.0f);
		glVertex3f(-0.48f, 0.6f, 0.0f);
		glVertex3f(-0.46f, 0.6f, 0.0f);

		glVertex3f(-0.44f, 0.7f, 0.0f);
		glVertex3f(-0.44f, 0.64f, 0.0f);
		glVertex3f(-0.42f, 0.7f, 0.0f);

		glVertex3f(-0.44f, 0.64f, 0.0f);
		glVertex3f(-0.42f, 0.7f, 0.0f);
		glVertex3f(-0.42f, 0.64f, 0.0f);

		glVertex3f(-0.46f, 0.7f, 0.0f);
		glVertex3f(-0.46f, 0.68f, 0.0f);
		glVertex3f(-0.44f, 0.7f, 0.0f);

		glVertex3f(-0.44f, 0.7f, 0.0f);
		glVertex3f(-0.44f, 0.68f, 0.0f);
		glVertex3f(-0.46f, 0.68f, 0.0f);

		glVertex3f(-0.46f, 0.66f, 0.0f);
		glVertex3f(-0.46f, 0.64f, 0.0f);
		glVertex3f(-0.44f, 0.66f, 0.0f);

		glVertex3f(-0.44f, 0.66f, 0.0f);
		glVertex3f(-0.44f, 0.64f, 0.0f);
		glVertex3f(-0.46f, 0.64f, 0.0f);

		// Code of P
		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.38f, 0.7f, 0.0f);
		glVertex3f(-0.38f, 0.6f, 0.0f);
		glVertex3f(-0.36f, 0.7f, 0.0f);

		glVertex3f(-0.36f, 0.7f, 0.0f);
		glVertex3f(-0.38f, 0.6f, 0.0f);
		glVertex3f(-0.36f, 0.6f, 0.0f);

		glVertex3f(-0.34f, 0.7f, 0.0f);
		glVertex3f(-0.34f, 0.64f, 0.0f);
		glVertex3f(-0.32f, 0.7f, 0.0f);

		glVertex3f(-0.34f, 0.64f, 0.0f);
		glVertex3f(-0.32f, 0.7f, 0.0f);
		glVertex3f(-0.32f, 0.64f, 0.0f);

		glVertex3f(-0.36f, 0.7f, 0.0f);
		glVertex3f(-0.36f, 0.68f, 0.0f);
		glVertex3f(-0.34f, 0.7f, 0.0f);

		glVertex3f(-0.34f, 0.7f, 0.0f);
		glVertex3f(-0.34f, 0.68f, 0.0f);
		glVertex3f(-0.36f, 0.68f, 0.0f);

		glVertex3f(-0.36f, 0.66f, 0.0f);
		glVertex3f(-0.36f, 0.64f, 0.0f);
		glVertex3f(-0.34f, 0.66f, 0.0f);

		glVertex3f(-0.34f, 0.66f, 0.0f);
		glVertex3f(-0.34f, 0.64f, 0.0f);
		glVertex3f(-0.36f, 0.64f, 0.0f);

		// Code of Y
		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(-0.28f, 0.7f, 0.0f);
		glVertex3f(-0.28f, 0.66f, 0.0f);
		glVertex3f(-0.26f, 0.7f, 0.0f);

		glVertex3f(-0.26f, 0.7f, 0.0f);
		glVertex3f(-0.28f, 0.66f, 0.0f);
		glVertex3f(-0.26f, 0.66f, 0.0f);

		glVertex3f(-0.24f, 0.7f, 0.0f);
		glVertex3f(-0.24f, 0.66f, 0.0f);
		glVertex3f(-0.22f, 0.7f, 0.0f);

		glVertex3f(-0.24f, 0.66f, 0.0f);
		glVertex3f(-0.22f, 0.7f, 0.0f);
		glVertex3f(-0.22f, 0.66f, 0.0f);

		glVertex3f(-0.28f, 0.66f, 0.0f);
		glVertex3f(-0.28f, 0.64f, 0.0f);
		glVertex3f(-0.22f, 0.66f, 0.0f);

		glVertex3f(-0.28f, 0.64f, 0.0f);
		glVertex3f(-0.22f, 0.66f, 0.0f);
		glVertex3f(-0.22f, 0.64f, 0.0f);

		glVertex3f(-0.26f, 0.64f, 0.0f);
		glVertex3f(-0.26f, 0.6f, 0.0f);
		glVertex3f(-0.24f, 0.64f, 0.0f);

		glVertex3f(-0.24f, 0.64f, 0.0f);
		glVertex3f(-0.24f, 0.6f, 0.0f);
		glVertex3f(-0.26f, 0.6f, 0.0f);

	glEnd();
}

//Word Birthday By @Aditya
void drawBirthday(){
	// Code of 'BIRTHDAY' by Aditya
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.08f, 0.7f, 0.0f);
		glVertex3f(-0.08f, 0.6f, 0.0f);
		glVertex3f(-0.06f, 0.7f, 0.0f);

		glVertex3f(-0.06f, 0.7f, 0.0f);
		glVertex3f(-0.08f, 0.6f, 0.0f);
		glVertex3f(-0.06f, 0.6f, 0.0f);

		glVertex3f(-0.04f, 0.68f, 0.0f);
		glVertex3f(-0.04f, 0.62f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);

		glVertex3f(-0.04f, 0.62f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);
		glVertex3f(-0.02f, 0.62f, 0.0f);

		glVertex3f(-0.06f, 0.7f, 0.0f);
		glVertex3f(-0.06f, 0.68f, 0.0f);
		glVertex3f(-0.03f, 0.7f, 0.0f);

		glVertex3f(-0.03f, 0.7f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);
		glVertex3f(-0.06f, 0.68f, 0.0f);

		glVertex3f(-0.06f, 0.66f, 0.0f);
		glVertex3f(-0.06f, 0.64f, 0.0f);
		glVertex3f(-0.04f, 0.66f, 0.0f);

		glVertex3f(-0.04f, 0.66f, 0.0f);
		glVertex3f(-0.04f, 0.64f, 0.0f);
		glVertex3f(-0.06f, 0.64f, 0.0f);

		glVertex3f(-0.06f, 0.62f, 0.0f);
		glVertex3f(-0.06f, 0.6f, 0.0f);
		glVertex3f(-0.02f, 0.62f, 0.0f);

		glVertex3f(-0.02f, 0.62f, 0.0f);
		glVertex3f(-0.03f, 0.6f, 0.0f);
		glVertex3f(-0.06f, 0.6f, 0.0f);
		// End of B

		// Code of I
		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(0.02f, 0.7f, 0.0f);
		glVertex3f(0.08f, 0.7f, 0.0f);
		glVertex3f(0.02f, 0.68f, 0.0f);

		glVertex3f(0.02f, 0.68f, 0.0f);
		glVertex3f(0.08f, 0.7f, 0.0f);
		glVertex3f(0.08f, 0.68f, 0.0f);

		glVertex3f(0.04f, 0.68f, 0.0f);
		glVertex3f(0.04f, 0.62f, 0.0f);
		glVertex3f(0.06f, 0.68f, 0.0f);

		glVertex3f(0.04f, 0.62f, 0.0f);
		glVertex3f(0.06f, 0.68f, 0.0f);
		glVertex3f(0.06f, 0.62f, 0.0f);

		glVertex3f(0.02f, 0.62f, 0.0f);
		glVertex3f(0.02f, 0.6f, 0.0f);
		glVertex3f(0.08f, 0.62f, 0.0f);

		glVertex3f(0.02f, 0.6f, 0.0f);
		glVertex3f(0.08f, 0.62f, 0.0f);
		glVertex3f(0.08f, 0.6f, 0.0f);
		// End of I

		// Code of R
		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.12f, 0.7f, 0.0f);
		glVertex3f(0.12f, 0.6f, 0.0f);
		glVertex3f(0.14f, 0.7f, 0.0f);

		glVertex3f(0.12f, 0.6f, 0.0f);
		glVertex3f(0.14f, 0.7f, 0.0f);
		glVertex3f(0.14f, 0.6f, 0.0f);

		glVertex3f(0.16f, 0.68f, 0.0f);
		glVertex3f(0.16f, 0.66f, 0.0f);
		glVertex3f(0.18f, 0.68f, 0.0f);

		glVertex3f(0.16f, 0.66f, 0.0f);
		glVertex3f(0.18f, 0.68f, 0.0f);
		glVertex3f(0.18f, 0.66f, 0.0f);

		glVertex3f(0.14f, 0.7f, 0.0f);
		glVertex3f(0.14f, 0.68f, 0.0f);
		glVertex3f(0.17f, 0.7f, 0.0f);

		glVertex3f(0.17f, 0.7f, 0.0f);
		glVertex3f(0.18f, 0.68f, 0.0f);
		glVertex3f(0.14f, 0.68f, 0.0f);

		glVertex3f(0.14f, 0.66f, 0.0f);
		glVertex3f(0.14f, 0.64f, 0.0f);
		glVertex3f(0.18f, 0.66f, 0.0f);

		glVertex3f(0.18f, 0.66f, 0.0f);
		glVertex3f(0.17f, 0.64f, 0.0f);
		glVertex3f(0.14f, 0.64f, 0.0f);

		glVertex3f(0.15f, 0.65f, 0.0f);
		glVertex3f(0.16f, 0.6f, 0.0f);
		glVertex3f(0.18f, 0.6f, 0.0f);
		// End of R

		// Code of T
		glColor3f(0.0f,0.8f,1.0f);							//blue
		glVertex3f(0.22f, 0.7f, 0.0f);
		glVertex3f(0.28f, 0.7f, 0.0f);
		glVertex3f(0.22f, 0.68f, 0.0f);

		glVertex3f(0.22f, 0.68f, 0.0f);
		glVertex3f(0.28f, 0.7f, 0.0f);
		glVertex3f(0.28f, 0.68f, 0.0f);

		glVertex3f(0.24f, 0.68f, 0.0f);
		glVertex3f(0.24f, 0.6f, 0.0f);
		glVertex3f(0.26f, 0.68f, 0.0f);

		glVertex3f(0.24f, 0.6f, 0.0f);
		glVertex3f(0.26f, 0.68f, 0.0f);
		glVertex3f(0.26f, 0.6f, 0.0f);
		// End of T

		// Code of H
		glColor3f(0.7f,0.4f,1.0f);						//Jambhala
		glVertex3f(0.38f, 0.7f, 0.0f);
		glVertex3f(0.36f, 0.7f, 0.0f);
		glVertex3f(0.38f, 0.6f, 0.0f);

		glVertex3f(0.36f, 0.7f, 0.0f);
		glVertex3f(0.38f, 0.6f, 0.0f);
		glVertex3f(0.36f, 0.6f, 0.0f);

		glVertex3f(0.36f, 0.67f, 0.0f);
		glVertex3f(0.34f, 0.67f, 0.0f);
		glVertex3f(0.36f, 0.63f, 0.0f);

		glVertex3f(0.36f, 0.63f, 0.0f);
		glVertex3f(0.34f, 0.63f, 0.0f);
		glVertex3f(0.34f, 0.67f, 0.0f);

		glVertex3f(0.34f, 0.7f, 0.0f);
		glVertex3f(0.34f, 0.6f, 0.0f);
		glVertex3f(0.32f, 0.7f, 0.0f);

		glVertex3f(0.32f, 0.7f, 0.0f);
		glVertex3f(0.32f, 0.6f, 0.0f);
		glVertex3f(0.34f, 0.6f, 0.0f);
		// End of H

		// Code of D
		glColor3f(0.0f,0.8f,1.0f);							//blue
		glVertex3f(0.42f, 0.7f, 0.0f);
		glVertex3f(0.42f, 0.6f, 0.0f);
		glVertex3f(0.44f, 0.7f, 0.0f);

		glVertex3f(0.44f, 0.7f, 0.0f);
		glVertex3f(0.42f, 0.6f, 0.0f);
		glVertex3f(0.44f, 0.6f, 0.0f);

		glVertex3f(0.46f, 0.68f, 0.0f);
		glVertex3f(0.46f, 0.62f, 0.0f);
		glVertex3f(0.48f, 0.68f, 0.0f);

		glVertex3f(0.46f, 0.62f, 0.0f);
		glVertex3f(0.48f, 0.68f, 0.0f);
		glVertex3f(0.48f, 0.62f, 0.0f);

		glVertex3f(0.44f, 0.7f, 0.0f);
		glVertex3f(0.44f, 0.68f, 0.0f);
		glVertex3f(0.47f, 0.7f, 0.0f);

		glVertex3f(0.47f, 0.7f, 0.0f);
		glVertex3f(0.48f, 0.68f, 0.0f);
		glVertex3f(0.44f, 0.68f, 0.0f);

		glVertex3f(0.44f, 0.62f, 0.0f);
		glVertex3f(0.44f, 0.6f, 0.0f);
		glVertex3f(0.48f, 0.62f, 0.0f);

		glVertex3f(0.48f, 0.62f, 0.0f);
		glVertex3f(0.47f, 0.6f, 0.0f);
		glVertex3f(0.44f, 0.6f, 0.0f);
		// End of D

		// Code of A
		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(0.58f, 0.7f, 0.0f);
		glVertex3f(0.58f, 0.68f, 0.0f);
		glVertex3f(0.52f, 0.7f, 0.0f);

		glVertex3f(0.52f, 0.7f, 0.0f);
		glVertex3f(0.52f, 0.68f, 0.0f);
		glVertex3f(0.58f, 0.68f, 0.0f);

		glVertex3f(0.58f, 0.68f, 0.0f);
		glVertex3f(0.56f, 0.68f, 0.0f);
		glVertex3f(0.58f, 0.6f, 0.0f);

		glVertex3f(0.58f, 0.6f, 0.0f);
		glVertex3f(0.56f, 0.68f, 0.0f);
		glVertex3f(0.56f, 0.6f, 0.0f);

		glVertex3f(0.54f, 0.68f, 0.0f);
		glVertex3f(0.54f, 0.6f, 0.0f);
		glVertex3f(0.52f, 0.68f, 0.0f);

		glVertex3f(0.52f, 0.68f, 0.0f);
		glVertex3f(0.54f, 0.6f, 0.0f);
		glVertex3f(0.52f, 0.6f, 0.0f);

		glVertex3f(0.56f, 0.66f, 0.0f);
		glVertex3f(0.56f, 0.64f, 0.0f);
		glVertex3f(0.54f, 0.66f, 0.0f);

		glVertex3f(0.56f, 0.64f, 0.0f);
		glVertex3f(0.54f, 0.66f, 0.0f);
		glVertex3f(0.54f, 0.64f, 0.0f);
		// End of A

		// Code of Y
		glColor3f(0.9f, 1.0f, 0.3f);							//Lemon Yellow
		glVertex3f(0.68f, 0.7f, 0.0f);
		glVertex3f(0.68f, 0.66f, 0.0f);
		glVertex3f(0.66f, 0.7f, 0.0f);

		glVertex3f(0.66f, 0.7f, 0.0f);
		glVertex3f(0.68f, 0.66f, 0.0f);
		glVertex3f(0.66f, 0.66f, 0.0f);

		glVertex3f(0.64f, 0.7f, 0.0f);
		glVertex3f(0.64f, 0.66f, 0.0f);
		glVertex3f(0.62f, 0.7f, 0.0f);

		glVertex3f(0.64f, 0.66f, 0.0f);
		glVertex3f(0.62f, 0.7f, 0.0f);
		glVertex3f(0.62f, 0.66f, 0.0f);

		glVertex3f(0.68f, 0.66f, 0.0f);
		glVertex3f(0.68f, 0.64f, 0.0f);
		glVertex3f(0.62f, 0.66f, 0.0f);

		glVertex3f(0.68f, 0.64f, 0.0f);
		glVertex3f(0.62f, 0.66f, 0.0f);
		glVertex3f(0.62f, 0.64f, 0.0f);

		glVertex3f(0.66f, 0.64f, 0.0f);
		glVertex3f(0.66f, 0.6f, 0.0f);
		glVertex3f(0.64f, 0.64f, 0.0f);

		glVertex3f(0.64f, 0.64f, 0.0f);
		glVertex3f(0.64f, 0.6f, 0.0f);
		glVertex3f(0.66f, 0.6f, 0.0f);
		// End of Y

	glEnd();
}

//Word Kalyani By @Piyush
void drawKalyani(){
	void bunchOfBallons(void);

	//code
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f,0.7f,0.0f);					//Gold

		glVertex3f(-0.38f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.36f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.38f, 0.4f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.36f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.36f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.38f, 0.4f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.34f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.36f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.36f, 0.44f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.36f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.34f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.32f, 0.5f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.36f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.36f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.34f, 0.4f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(-0.36f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.34f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.32f, 0.4f, 0.0f);

		// Code of A
		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.28f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.28f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.22f, 0.5f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.22f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.22f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.28f, 0.48f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.28f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.26f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.28f, 0.4f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.28f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.26f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.26f, 0.4f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.24f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.24f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.22f, 0.48f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.22f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.24f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.22f, 0.4f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala

		glVertex3f(-0.26f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.26f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.24f, 0.46f, 0.0f);

		glColor3f(0.7f,0.4f,1.0f);						//Jambhala
		glVertex3f(-0.26f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.24f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.24f, 0.44f, 0.0f);

		// Code of L
		glColor3f(0.0f,0.8f,1.0f);							//blue

		glVertex3f(-0.18f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.18f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.16f, 0.5f, 0.0f);

		glColor3f(0.0f,0.8f,1.0f);							//blue

		glVertex3f(-0.16f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.16f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.18f, 0.4f, 0.0f);

		glColor3f(0.0f,0.8f,1.0f);							//blue

		glVertex3f(-0.16f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.16f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.12f, 0.4f, 0.0f);

		glColor3f(0.0f,0.8f,1.0f);							//blue
		glVertex3f(-0.12f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.16f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.12f, 0.42f, 0.0f);

		// Code of Y
		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.08f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.08f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.06f, 0.5f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.06f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.08f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.06f, 0.46f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.04f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.04f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.5f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.04f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.46f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.08f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.08f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.46f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.08f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.02f, 0.44f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.06f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.06f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.04f, 0.44f, 0.0f);

		glColor3f(1.0f,0.2f,0.4f);								//Red
		glVertex3f(-0.04f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.04f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.06f, 0.4f, 0.0f);

		// Code of A
		glColor3f(0.0f,0.4f,1.0f);						//blue
		glVertex3f(0.02f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.02f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.08f, 0.5f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);						//blue
		glVertex3f(0.08f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.08f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.02f, 0.48f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.02f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.04f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.02f, 0.4f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.02f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.04f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.04f, 0.4f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.06f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.06f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.08f, 0.48f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.08f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.06f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.08f, 0.4f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.04f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.04f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.06f, 0.46f, 0.0f);

		glColor3f(0.0f,0.4f,1.0f);		//blue
		glVertex3f(0.04f, 0.44f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.06f, 0.46f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.06f, 0.44f, 0.0f);
		
		// Code of N
		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.12f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.12f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.14f, 0.5f, 0.0f);

		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.14f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.14f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.12f, 0.4f, 0.0f);

		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.16f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.16f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.18f, 0.5f, 0.0f);

		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.18f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.18f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.16f, 0.4f, 0.0f);

		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.14f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.14f, 0.47f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.16f, 0.4f, 0.0f);

		glColor3f(0.6f,1.0f,0.3f);							//Popti
		glVertex3f(0.16f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.16f, 0.43f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.14f, 0.5f, 0.0f);

		// Code of I
		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.22f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.22f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.28f, 0.5f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.28f, 0.5f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.28f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.22f, 0.48f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.22f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.22f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.28f, 0.42f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.28f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.28f, 0.4f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.22f, 0.4f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.24f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.24f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.26f, 0.48f, 0.0f);

		glColor3f(1.0f,0.7f,0.0f);					//Gold
		glVertex3f(0.26f, 0.48f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.26f, 0.42f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.24f, 0.42f, 0.0f);

	glEnd();

	//Ballons1
	glPushMatrix();
		glTranslatef(-0.6f,0.0f,0.0f);
		bunchOfBallons();
	glPopMatrix();

	//Ballons2
	glPushMatrix();
		glTranslatef(0.3f,0.0f,0.0f);
		bunchOfBallons();
	glPopMatrix();

}

//Cake By @Omkar Khot
void drawCake(){
	//code
	//Code for Cake
	glBegin(GL_POLYGON);
		//Bottom Polygon 
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.3f, -0.90f, 0.0f);
		glVertex3f(0.3f, -0.72f, 0.0f);
		glVertex3f(-0.3f, -0.72f, 0.0f);
		glVertex3f(-0.3f, -0.90f, 0.0f);
		
	glEnd();

	glBegin(GL_POLYGON);
		//Midddle Polygon
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.2f, -0.72f, 0.0f);
		glVertex3f(0.2f, -0.54f, 0.0f);
		glVertex3f(-0.2f, -0.54f, 0.0f);
		glVertex3f(-0.2f, -0.72f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
		//Top Polygon
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.1f, -0.54f, 0.0f);
		glVertex3f(0.1f, -0.36f, 0.0f);
		glVertex3f(-0.1f, -0.36f, 0.0f);
		glVertex3f(-0.1f, -0.54f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
		//Candle
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.008f, -0.36f, 0.0f);
		glVertex3f(0.008f, -0.26f, 0.0f);
		glVertex3f(-0.008f, -0.26f, 0.0f);
		glVertex3f(-0.008f, -0.36f, 0.0f);

	glEnd();

	//Candle Flame
	glBegin(GL_TRIANGLES);
		// up
		glColor3f(1.0f, 0.4f, 0.2f);
		glVertex3f(0.0f, -0.14f, 0.0f);

		glColor3f(0.3f, 0.0f, 0.0f);
		glVertex3f(-0.009f, -0.20f, 0.0f);
		
		glColor3f(0.3f, 0.0f, 0.0f);
		glVertex3f(0.009f, -0.20f, 0.0f);

		// Down
		glColor3f(1.0f, 0.4f, 0.2f);
		glVertex3f(0.0f, -0.26f, 0.0f);

		glColor3f(0.3f, 0.0f, 0.0f);
		glVertex3f(-0.009f, -0.20f, 0.0f);
		
		glColor3f(0.3f, 0.0f, 0.0f);
		glVertex3f(0.009f, -0.20f, 0.0f);



	glEnd();

	glBegin(GL_TRIANGLES);
		
		//Triangles inside cake Bottom polygon
		glColor3f(1.0f, 1.0f, 0.7f);
		glVertex3f(0.3f, -0.72f, 0.0f);
		glVertex3f(0.2f, -0.72f, 0.0f);
		glVertex3f(0.25f, -0.81f, 0.0f);

		glVertex3f(0.2f, -0.72f, 0.0f);
		glVertex3f(0.1f, -0.72f, 0.0f);
		glVertex3f(0.15f, -0.81f, 0.0f);

		glVertex3f(0.1f, -0.72f, 0.0f);
		glVertex3f(0.0f, -0.72f, 0.0f);
		glVertex3f(0.05f, -0.81f, 0.0f);

		glVertex3f(0.0f, -0.72f, 0.0f);
		glVertex3f(-0.1f, -0.72f, 0.0f);
		glVertex3f(-0.05f, -0.81f, 0.0f);

		glVertex3f(-0.1f, -0.72f, 0.0f);
		glVertex3f(-0.2f, -0.72f, 0.0f);
		glVertex3f(-0.15f, -0.81f, 0.0f);

		glVertex3f(-0.2f, -0.72f, 0.0f);
		glVertex3f(-0.3f, -0.72f, 0.0f);
		glVertex3f(-0.25f, -0.81f, 0.0f);

		//Triangles inside cake Middle polygon
		glColor3f(1.0f, 1.0f, 0.7f);
		glVertex3f(0.2f, -0.54f, 0.0f);
		glVertex3f(0.1f, -0.54f, 0.0f);
		glVertex3f(0.15f, -0.63f, 0.0f);

		glVertex3f(0.1f, -0.54f, 0.0f);
		glVertex3f(0.0f, -0.54f, 0.0f);
		glVertex3f(0.05f, -0.63f, 0.0f);

		glVertex3f(0.0f, -0.54f, 0.0f);
		glVertex3f(-0.1f, -0.54f, 0.0f);
		glVertex3f(-0.05f, -0.63f, 0.0f);

		glVertex3f(-0.1f, -0.54f, 0.0f);
		glVertex3f(-0.2f, -0.54f, 0.0f);
		glVertex3f(-0.15f, -0.63f, 0.0f);

		//Triangles inside cake Top polygon
		glColor3f(1.0f, 1.0f, 0.7f);
		glVertex3f(0.1f, -0.36f, 0.0f);
		glVertex3f(0.0f, -0.36f, 0.0f);
		glVertex3f(0.05f, -0.45f, 0.0f);

		glVertex3f(0.0f, -0.36f, 0.0f);
		glVertex3f(-0.1f, -0.36f, 0.0f);
		glVertex3f(-0.05f, -0.45f, 0.0f);

	glEnd();


}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key) 
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) 
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		// glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code


}
