// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>
#include<math.h>

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

void Ji() {
	// J
	// Kana1
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.1f, 0.45f, 0.0f);
	glVertex3f(-0.2f, 0.45f, 0.0f);
	glVertex3f(-0.2f, 0.4f, 0.0f);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glEnd();

	// Curves
	glBegin(GL_POLYGON);

	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.35f + 0.18f * cosf(angle), 0.36f - 0.18f * sinf(angle), 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.35f + 0.13f * cosf(angle), 0.39f - 0.13f * sinf(angle), 0.0f);

	glEnd();


	// Kana2
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.05f, 0.05f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.6f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glEnd();

	// Velanti
	glBegin(GL_POLYGON);

	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.16f * cosf(angle), 0.735f + 0.16f * sinf(angle), 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = -0.5235987f * 2.0f; angle <= 3.1415926f +(2.0f * 0.5235987f); angle += 0.01f)
		glVertex3f(-0.11f * cosf(angle), 0.735f + 0.11f * sinf(angle), 0.0f);

	glEnd();
}

void Ve() {

	// Curves
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(- 0.05f - 0.2f * sinf(angle), 0.33f - 0.2f * cosf(angle), 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.05f - 0.15f * sinf(angle), 0.33f - 0.15f * cosf(angle), 0.0f);

	glEnd();

	// Kana
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();

	// Matra
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.65f, 0.0f);
	glVertex3f(-0.25f, 0.85f, 0.0f);
	glVertex3f(-0.25f, 0.9f, 0.0f);
	glVertex3f(0.05f, 0.65f, 0.0f);
	glEnd();
}

void T() {
	// Kana
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(-0.3f, 0.05f, 0.0f);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(-0.25f, 0.4f, 0.0f);
	glEnd();
}

void M()
{
	// Kana
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.25f, 0.6f, 0.0f);
	glVertex3f(-0.25f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);
	glEnd();
	
	glBegin(GL_QUADS);
	glVertex3f(-0.25f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.22f, 0.0f);
	glVertex3f(-0.33f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.3f, 0.0f);
	glEnd();
}

void Jivet()
{
	// Line
	glBegin(GL_QUADS);
	glColor3f( 1.0f, 1.0f, 1.0f );
	glVertex3f( -0.8f, 0.6f, 0.0f );
	glVertex3f( -0.75, 0.65f, 0.0f );
	glVertex3f( 0.8f, 0.65f, 0.0f );
	glVertex3f( 0.75f, 0.6f, 0.0f );
	glEnd();

	//glTranslatef(-0.3f, 0.0f, 0.0f);
	//Ji();

	//glTranslatef(0.45f, 0.0f, 0.0f);
	//Ve();
	
	//glTranslatef(0.45f, 0.0f, 0.0f);
	//T();

	M();
}

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Jivet();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}
