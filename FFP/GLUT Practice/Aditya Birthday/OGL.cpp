// Header files
#include <GL/freeglut.h>
#include <MMSystem.h>													//For PlaySound win32 API
#include <math.h>

#define PI 3.14159265359

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

// Global Functions Declarations


void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void BSplineCurve(int pointBase, int noOfPoints, Point* arr[]) {

	for (int i = 0; i <= noOfPoints - pointBase; i++)
		switch (pointBase) {
		case 3:
			bezierCurve3Points(arr[i], arr[i + 1], arr[i + 2]);
			break;

		case 4:
			bezierCurve4Points(arr[i], arr[i + 1], arr[i + 2], arr[i + 3]);
			break;
		}
}


// Global Variables Declarations
bool bIsFullScreen = false;

float translateX = -0.9f, translateY = -2.4f;
float scaleX = 4.2f, scaleY = 4.2f, scaleZ = 4.2f;

int runFlag = 0;
int FadeInFlagOn = 0;
int FadeOutFlagOn = 0;

int scene1 = 1;
int scene2 = 0;
float alpha = 0.0f;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Frustum Group : Aditya's B'Day");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutFullScreen();

	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	PlaySound("bgm.WAV", NULL, SND_ASYNC); 

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// function declarations
	void Room(void);
	void drawQuad(void);
	void update(void);

	void WriteShlokOnBook(void);

	// codeglClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT |  GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// function calls
	if(scene1 == 1){
		glPushMatrix();
				glTranslatef(translateX,translateY,0.0f);
				glScalef(scaleX,scaleX,scaleX);
				Room();
		glPopMatrix();
	}
	else{
		glPushMatrix();
			WriteShlokOnBook();
		glPopMatrix();
	}

	glPushMatrix();
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
			drawQuad();
		glDisable(GL_BLEND);

	glPopMatrix();

	update();
	glutSwapBuffers();
}
	
void update(void)
{
	//code
	if(runFlag == 1)
	{
		if(scaleX > 1.0f){
			scaleX -= 0.0025f;
		}
		if(translateX < 0){
			translateX += 0.0025f;
		}
		if(translateY < 0){
			translateY +=0.0025f;
		}
	}

	if(FadeInFlagOn == 1){
		if(alpha < 1.0f){
			alpha += 0.01f;
		}		
	}
	if(FadeOutFlagOn == 1){
		if(alpha > 0.0f){
			alpha -= 0.005f;
		}	
	}
	glutPostRedisplay();
}

void drawQuad(void)
{
	//code
	glLoadIdentity();
 	glTranslatef(0.0f,0.0f,0.0f);
	glScalef(1.0f,1.0f,1.0f);
	glBegin(GL_QUADS);
		glColor4f(0.0f, 0.0f, 0.0f,alpha); // Width Right Rectangle : Yellow
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);	
	glEnd();

}


void Room(void)
{
	//function declarations
	void RoomInterior(void);
	void Window(void);
	void Calender(void);
	void Clock(void);
	void PhotoFrame(void);
	void Radio(void);
	void drawBookRac(void);
	void Table(void);
	void Chair(void);
	void Laptop(void);
	void PC(void);
	void ManOnChair(void);
	void CoffeeMug(void);
	void PenStand(void);

	//function calls

	glPushMatrix();
		/******** RoomInterior *******/
		glPushMatrix();
			//glTranslatef(-0.40f, 0.4f, 0.0f);
			//glScalef(0.7f, 0.7f, 0.0f);
			RoomInterior();
		glPopMatrix();


		/******** Window *******/
		glPushMatrix();
			//glTranslatef(-0.40f, 0.4f, 0.0f);
			//glScalef(0.7f, 0.7f, 0.0f);
			Window();
		glPopMatrix();


		/******** Calender *******/
		glPushMatrix();
			glTranslatef(-0.40f, 0.4f, 0.0f);
			glScalef(0.7f, 0.7f, 0.0f);
			Calender();
		glPopMatrix();
		

		/******** Clock *******/
		glPushMatrix();
			glTranslatef(0.25f, 0.8f, 0.0f);
			glScalef(0.15f, 0.25f, 0.0f);
			Clock();
		glPopMatrix();


		/******** Photo Frame *******/
		glPushMatrix();
			glTranslatef(0.0f, 0.75f, 0.0f);
			glScalef(0.1f, 0.15f, 0.0f);
			PhotoFrame();
		glPopMatrix();

		/******** Radio *******/
		glPushMatrix();
			glTranslatef(0.2f, 0.48f, 0.0f);
			glScalef(0.3f, 0.45f, 0.0f);
			Radio();
		glPopMatrix();


		/******** Book Shelf *******/
		glPushMatrix();
			glTranslatef(-0.55f, -0.7f, 0.0f);
			glScalef(0.8f, 1.2f, 0.0f);
			drawBookRac();
		glPopMatrix();

		/******** Table *******/
		glPushMatrix();
			glTranslatef(0.2f, 0.75f, 0.0f);
			glScalef(0.3f, 0.5f, 0.0f);
			Table();
		glPopMatrix();

		/******** PC *******/
		glPushMatrix();
			glTranslatef(-1.55f, 0.35f, 0.0f);
			glScalef(1.2f, 1.2f, 0.0f);
			PC();
		glPopMatrix();

		/******** Table Lamp *******/
		// glPushMatrix();
			//glTranslatef(-1.7f, 0.35f, 0.0f);
			//glScalef(1.2f, 1.2f, 0.0f);
			//PC();
		// glPopMatrix();

		/******** Man On Chair *******/
		glPushMatrix();
			// glTranslatef(-1.6f, -0.15f, 0.0f);
			// glScalef(1.5f, 1.5f, 0.0f);
			ManOnChair();
		glPopMatrix();

		/******** Coffee Mug *******/
		glPushMatrix();
			// glLoadIdentity();
			glTranslatef(0.35f, -0.2f, 0.0f);
			glScalef(0.5f, 0.4f, 0.5f);
			CoffeeMug();
		glPopMatrix();

		/******** Pen Stand *******/
		glPushMatrix();
			// glLoadIdentity();
			glTranslatef(0.4f, -0.2f, 0.0f);
			glScalef(0.4f, 0.3f, 0.5f);
			PenStand();
		glPopMatrix();
	glPopMatrix();


}



void RoomInterior(void)
{
	//code

	// front wall
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 0.8f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);
	glEnd();


	// shelf on the wall for placing radio
	glPushMatrix();
		glTranslatef(0.00f, 0.08f, 0.0f);
		//glScalef(0.6f, 0.8f, 0.0f);
		glBegin(GL_QUADS);
			glColor3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.05f, 0.30f, 0.0f);
			glVertex3f(0.35f, 0.30f, 0.0f);
			glVertex3f(0.35f, 0.27f, 0.0f);
			glVertex3f(0.05f, 0.27f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
			glColor3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.35f, 0.30f, 0.0f);
			glVertex3f(0.35f, 0.27f, 0.0f);
			glVertex3f(0.37f, 0.29f, 0.0f);
			glVertex3f(0.37f, 0.32f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
			glColor3f(0.8f, 0.6f, 0.4f);
			glVertex3f(0.35f, 0.30f, 0.0f);
			glVertex3f(0.37f, 0.32f, 0.0f);
			glVertex3f(0.07f, 0.32f, 0.0f);
			glVertex3f(0.05f, 0.30f, 0.0f);
		glEnd();
	glPopMatrix();

	// left side wall
	glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 0.7f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-0.4f, 1.0f, 0.0f);
		glVertex3f(-0.4f, -0.5f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();

	// floor
	glBegin(GL_QUADS);
		glColor3f(0.7f, 0.7f, 0.7f);
		glVertex3f(-0.4f, -0.5f, 0.0f);
		glVertex3f(1.0f, -0.5f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();

	// Alternate square tiles of two different color on the floor
	glColor3f(1.0f, 1.0f, 1.0f);

	for (int i = 0; i < 5; i++)
	{
		// 1st column
		// 1st white square tile in first column
		glBegin(GL_QUADS);
		glVertex3f((-0.8464f + (i * 0.4f)), -0.872f, 0.0f);
		glVertex3f((-0.6464f + (i * 0.4f)), -0.872f, 0.0f);
		glVertex3f((-0.8f + (i * 0.4f)), -1.0f, 0.0f);
		glVertex3f((-1.0f + (i * 0.4f)), -1.0f, 0.0f);
		glEnd();

		// 2nd white square tile in first column
		glBegin(GL_QUADS);
		glVertex3f((-0.5392f + (i * 0.4f)), -0.616f, 0.0f);
		glVertex3f((-0.3392f + (i * 0.4f)), -0.616f, 0.0f);
		glVertex3f((-0.4928f + (i * 0.4f)), -0.744f, 0.0f);
		glVertex3f((-0.6928f + (i * 0.4f)), -0.744f, 0.0f);
		glEnd();

		// 2nd column
		// 1st white square tile in second column
		glBegin(GL_QUADS);
		glVertex3f((-0.4928f + (i * 0.4f)), -0.744f, 0.0f);
		glVertex3f((-0.2928f + (i * 0.4f)), -0.744f, 0.0f);
		glVertex3f((-0.4464f + (i * 0.4f)), -0.872f, 0.0f);
		glVertex3f((-0.6464f + (i * 0.4f)), -0.872f, 0.0f);
		glEnd();

		// 2nd white square tile in second column
		glBegin(GL_QUADS);
		glVertex3f((-0.1856f + (i * 0.4f)), -0.5f, 0.0f);
		glVertex3f((0.0144f + (i * 0.4f)), -0.5f, 0.0f);
		glVertex3f((-0.1392f + (i * 0.4f)), -0.616f, 0.0f);
		glVertex3f((-0.3392f + (i * 0.4f)), -0.616f, 0.0f);
		glEnd();

	}

}

void Window(void)// Window
{
	// Window
	// Outer Box
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.85f, 0.0f);
	glVertex3f(0.9f, 0.85f, 0.0f);
	glVertex3f(0.9f, 0.05f, 0.0f);
	glVertex3f(0.4f, 0.05f, 0.0f);
	glEnd();

	// Inner Box1
	glBegin(GL_QUADS);
	glColor3f(0.7f, 1.0f, 1.0f);
	glVertex3f(0.43f, 0.82f, 0.0f);
	glVertex3f(0.635f, 0.82f, 0.0f);
	glVertex3f(0.635f, 0.08f, 0.0f);
	glVertex3f(0.43f, 0.08f, 0.0f);
	glEnd();

	// sun
	glPushMatrix();
	glTranslatef(0.50f, 0.7f, -0.10f);
	glColor3f(1.0f, 1.0f, 0.5f);
	glBegin(GL_POLYGON);
	for (float angle = 0.0f; angle <= 360.0f; angle += 0.01f)
	{
		glVertex3f(0.075f * 0.7 * cos(angle), 0.075f * sin(angle), 0.0f);
	}
	glEnd();
	glPopMatrix();

	// Birds in Inner Box1
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);

	glVertex3f(0.45f, 0.62f, -0.5f);
	glVertex3f(0.44f, 0.64f, -0.5f);
	glVertex3f(0.45f, 0.62f, -0.5f);
	glVertex3f(0.46f, 0.64f, -0.5f);

	glVertex3f(0.52f, 0.65f, -0.5f);
	glVertex3f(0.51f, 0.67f, -0.5f);
	glVertex3f(0.52f, 0.65f, -0.5f);
	glVertex3f(0.53f, 0.67f, -0.5f);

	glVertex3f(0.55f, 0.65f, -0.5f);
	glVertex3f(0.54f, 0.67f, -0.5f);
	glVertex3f(0.55f, 0.65f, -0.5f);
	glVertex3f(0.56f, 0.67f, -0.5f);

	glVertex3f(0.58f, 0.65f, -0.5f);
	glVertex3f(0.57f, 0.67f, -0.5f);
	glVertex3f(0.58f, 0.65f, -0.5f);
	glVertex3f(0.59f, 0.67f, -0.5f);

	glEnd();

	// Inner Box2
	glBegin(GL_QUADS);
	glColor3f(0.7f, 1.0f, 1.0f);
	glVertex3f(0.665f, 0.82f, 0.0f);
	glVertex3f(0.87f, 0.82f, 0.0f);
	glVertex3f(0.87f, 0.08f, 0.0f);
	glVertex3f(0.665f, 0.08f, 0.0f);
	glEnd();

	// Bottom Box1
	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.9f, 0.8f);
	glVertex3f(0.39f, 0.05f, 0.0f);
	glVertex3f(0.91f, 0.05f, 0.0f);
	glVertex3f(0.91f, 0.03f, 0.0f);
	glVertex3f(0.39f, 0.03f, 0.0f);

	// Bottom Box2
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.38f, 0.03f, 0.0f);
	glVertex3f(0.92f, 0.03f, 0.0f);
	glVertex3f(0.92f, -0.02f, 0.0f);
	glVertex3f(0.38f, -0.02f, 0.0f);
	glEnd();

	// Bottom Box3
	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.9f, 0.7f);
	glVertex3f(0.38f, -0.02f, 0.0f);
	glVertex3f(0.92f, -0.02f, 0.0f);
	glVertex3f(0.92f, -0.05f, 0.0f);
	glVertex3f(0.38f, -0.05f, 0.0f);
	glEnd();

}

void Calender(void)
{
	// Calender
	// Outer box
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.75f, 0.0f);
	glVertex3f(0.3f, 0.75f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);
	glVertex3f(0.1f, 0.3f, 0.0f);
	glEnd();

	// Top Header box
	glBegin(GL_QUADS);
	glColor3f(0.7f, 0.7f, 1.0f);
	glVertex3f(0.1f, 0.75f, 0.0f);
	glVertex3f(0.3f, 0.75f, 0.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(0.1f, 0.65f, 0.0f);
	glEnd();

	// inner box1 for pin1
	glBegin(GL_QUADS);
	glColor3f(0.8f, 1.0f, 0.4f);
	glVertex3f(0.125f, 0.74f, 0.0f);
	glVertex3f(0.145f, 0.74f, 0.0f);
	glVertex3f(0.145f, 0.71f, 0.0f);
	glVertex3f(0.125f, 0.71f, 0.0f);
	glEnd();

	// pin1 box
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.13f, 0.76f, 0.0f);
	glVertex3f(0.14f, 0.76f, 0.0f);
	glVertex3f(0.14f, 0.72f, 0.0f);
	glVertex3f(0.13f, 0.72f, 0.0f);
	glEnd();

	// inner box2 for pin2
	glBegin(GL_QUADS);
	glColor3f(0.8f, 1.0f, 0.4f);
	glVertex3f(0.255f, 0.74f, 0.0f);
	glVertex3f(0.275f, 0.74f, 0.0f);
	glVertex3f(0.275f, 0.71f, 0.0f);
	glVertex3f(0.255f, 0.71f, 0.0f);
	glEnd();

	// pin2 box
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.26f, 0.76f, 0.0f);
	glVertex3f(0.27f, 0.76f, 0.0f);
	glVertex3f(0.27f, 0.72f, 0.0f);
	glVertex3f(0.26f, 0.72f, 0.0f);
	glEnd();

	// boxes for dates
	for (int i = 0; i < 7; i++)		// value of 'i' indicates row number
	{
		for (int j = 0; j < 4; j++)		// value of 'j' indicates column number
		{
			if ((i == 5) && (j == 2))
			{
				glColor3f(1.0f, 0.0f, 0.0f);	// set the color of one random date to red, just to indicate that is Birth date
			}
			else
			{
				glColor3f(0.9f, 0.9f, 1.0f);
			}

			glBegin(GL_QUADS);
			glVertex3f((0.12f + (j * (0.025 + 0.02))), (0.63f - (i * (0.025 + 0.02))), 0.0f);
			glVertex3f((0.145f + (j * (0.025 + 0.02))), (0.63f - (i * (0.025 + 0.02))), 0.0f);
			glVertex3f((0.145f + (j * (0.025 + 0.02))), (0.605f - (i * (0.025 + 0.02))), 0.0f);
			glVertex3f((0.12f + (j * (0.025 + 0.02))), (0.605f - (i * (0.025 + 0.02))), 0.0f);
			glEnd();
		}
	}

}

void Clock(void)
{
	// code
	//Outer Box
	glBegin(GL_QUADS);
	glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glEnd();

	//Inner Box
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glEnd();

	// Rectangles showing Time
	//12 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.33f, 0.0f);
	glVertex3f(-0.01f, 0.33f, 0.0f);
	glEnd();

	//3 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.33f, 0.01f, 0.0f);
	glVertex3f(0.39f, 0.01f, 0.0f);
	glVertex3f(0.39f, -0.01f, 0.0f);
	glVertex3f(0.33f, -0.01f, 0.0f);
	glEnd();

	//6 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.39f, 0.0f);
	glVertex3f(-0.01f, -0.39f, 0.0f);
	glEnd();

	//9 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.39f, 0.01f, 0.0f);
	glVertex3f(-0.33f, 0.01f, 0.0f);
	glVertex3f(-0.33f, -0.01f, 0.0f);
	glVertex3f(-0.39f, -0.01f, 0.0f);
	glEnd();

	//Minute Hand
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.01f, 0.0f);
	glVertex3f(0.30f, 0.01f, 0.0f);
	glVertex3f(0.30f, -0.01f, 0.0f);
	glVertex3f(-0.05f, -0.01f, 0.0f);
	glEnd();

	//Hour Hand
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.02f, 0.01f, 0.0f);
	glVertex3f(-0.15f, -0.25f, 0.0f);
	glVertex3f(-0.02f, -0.01f, 0.0f);
	glEnd();


}

void PhotoFrame(void)
{
	// function declarations
	void Frame(void);
	void ManInPhoto(void);
	void Woman(void);
	void HalfHeart(void);

	// code

	// function calls
	Frame();
	ManInPhoto();

	glTranslatef(0.0f, -0.06f, 0.0f);
	glScalef(0.85f, 0.9f, 1.0f);
	Woman();
	glScalef(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, 0.1f, 0.0f);

	glTranslatef(0.15f, 0.4f, 0.0f);
	HalfHeart();

	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.7f, 1.0f, 1.0f);
	glTranslatef(0.3f, 0.0f, 0.0f);
	HalfHeart();
}

void Frame(void)
{
	// Outer Rectangle
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.8f, -0.8f, 0.0f);
	glVertex3f(-0.8f, 0.8f, 0.0f);
	glVertex3f(0.8f, 0.8f, 0.0f);
	glVertex3f(0.8f, -0.8f, 0.0f);
	glEnd();

	// Inner Rectangle_1
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.7f, 0.7f, 0.0f);
	glVertex3f(0.7f, 0.7f, 0.0f);
	glVertex3f(0.7f, -0.7f, 0.0f);
	glVertex3f(-0.7f, -0.7f, 0.0f);
	glEnd();

	// Inner Rectangle_2
	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.9f, 1.0f);
	glVertex3f(-0.6f, -0.6f, 0.0f);
	glVertex3f(-0.6f, 0.6f, 0.0f);
	glVertex3f(0.6f, 0.6f, 0.0f);
	glVertex3f(0.6f, -0.6f, 0.0f);
	glEnd();
}

void ManInPhoto(void)
{
	// Man Body
	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.0f, 0.4f);
	glVertex3f(-0.45f, -0.6f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glVertex3f(-0.05f, -0.2f, 0.0f);
	glVertex3f(-0.45f, -0.2f, 0.0f);
	glEnd();

	// Man Shoulder Part
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * cosf(angle)), -0.2f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Man Head
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Man Left Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f + (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.05f - (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);

	glEnd();

	// Man Right Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= ((3.1515926f * 3.0f) / 4.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.25f - (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);


	Point p1 = { -0.15f, 0.2f, 0.0f };
	Point p2 = { -0.25, 0.1f, 0.0f };
	Point p3 = { -0.45f, 0.1f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * (1.0f - u) * u) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * (1.0f - u) * u) + (p3.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void Woman(void)
{
	// Woman Head Circle
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.7f, 0.0f, 0.3f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.23f * cosf(angle)), 0.1f + (0.23f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Head Left Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);

	Point p1 = { 0.48f, 0.1f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.25f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * (1.0f - u) * u) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * (1.0f - u) * u) + (p3.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Woman Head Right Hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);

	Point p4 = { 0.02f, 0.1f, 0.0f };
	Point p5 = { -0.1f, -0.2f, 0.0f };
	Point p6 = { 0.25f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * (1.0f - u) * u) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * (1.0f - u) * u) + (p6.y * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Woman Body
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.05f, -0.6f, 0.0f);
	glVertex3f(0.45f, -0.6f, 0.0f);
	glVertex3f(0.45f, -0.2f, 0.0f);
	glVertex3f(0.05f, -0.2f, 0.0f);
	glEnd();

	// Woman Shoulder Part
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * cosf(angle)), -0.2f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Face
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float angle = 0.0f; angle <= 2.0f * 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * cosf(angle)), 0.1f + (0.2f * sinf(angle)), 0.0f);
	glEnd();

	// Woman Left Forehead hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f + (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.45f - (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);

	glEnd();

	// Woman Right Forehead hairs
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.25f - (0.2f * sinf(angle)), 0.1f + (0.2f * cosf(angle)), 0.0f);

	for (float angle = 0.0f; angle <= (3.1515926f / 2.0f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.05f + (0.2f * sinf(angle)), 0.3f - (0.2f * cosf(angle)), 0.0f);

	glEnd();
}

void HalfHeart(void) {

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 0.0f, 0.0f);
	for (float angle = 0.0f; angle <= 3.1515926f; angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(-0.075f + 0.1f * cosf(angle), 0.1f * sinf(angle), 0.0f);
	glEnd();

	glScalef(1.5f, 1.0f, 1.0f);
	glTranslatef(-0.01f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);

	Point p1 = { -0.1f, 0.0f, 0.0f };
	Point p2 = { 0.1f, 0.1f, 0.0f };
	Point p3 = { 0.1f, -0.15f, 0.0f };
	Point p4 = { -0.1f, -0.2f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {

		float x = (p1.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3.x * u * u * (1.0f - u)) + (p4.x * u * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3.y * u * u * (1.0f - u)) + (p4.y * u * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void Radio(void)
{
	float angle = 0;
	int i = 0;
	float radius = 0.1f;
	glBegin(GL_QUADS);

	glColor3f(0.5f, 0.5f, 0.5f); //Red -> Radio Front
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, -0.2f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.0f);
	glVertex3f(0.3f, 0.2f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0); // Width Right Rectangle : Yellow
	glVertex3f(0.3f, 0.2f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.2f);
	glVertex3f(0.3f, 0.2f, 0.2f);

	glColor3f(0.0f, 0.0f, 0.1f); //Blue -> Radio Back
	glVertex3f(0.3f, 0.2f, 0.2f);
	glVertex3f(-0.3f, 0.2f, 0.2f);
	glVertex3f(-0.3f, -0.2f, 0.2f);
	glVertex3f(0.3f, -0.2f, 0.2f);

	glColor3f(0.0f, 1.0f, 0.0f); // Width Left Rectangle : Yellow
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.2f);
	glVertex3f(-0.3f, -0.2f, 0.2f);
	glVertex3f(-0.3f, -0.2f, 0.0f);

	glColor3f(0.0f, 1.0f, 1.0f); // Width top Rectangle : Cayan
	glVertex3f(0.3f, 0.2f, 0.0f);
	glVertex3f(0.3f, 0.2f, -0.2f);
	glVertex3f(-0.3f, 0.2f, -0.2f);
	glVertex3f(-0.3f, 0.2f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.0f); // Width Bottom Rectangle : Cayan
	glVertex3f(0.3f, -0.2f, 0.0f);
	glVertex3f(0.3f, -0.2f, 0.2f);
	glVertex3f(-0.3f, -0.2f, 0.2f);
	glVertex3f(-0.3f, -0.2f, 0.0f);

	glColor3f(0.3f, 0.4f, 0.5f); // Sound Rectangle
	glVertex3f(-0.26f, 0.15f, 0.0f);
	glVertex3f(-0.26f, -0.15f, 0.0f);
	glVertex3f(-0.03f, -0.15f, 0.0f);
	glVertex3f(-0.03f, 0.15f, 0.0f);

	glColor3f(1.0f, 1.0f, 0.0f); // Channel Rectangle
	glVertex3f(0.05f, 0.15f, 0.0f);
	glVertex3f(0.05f, -0.15f, 0.0f);
	glVertex3f(0.15f, -0.15f, 0.0f);
	glVertex3f(0.15f, 0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.1f, 0.0f);  // channel Vertical line
	glVertex3f(0.12f, -0.13f, 0.0f);
	glVertex3f(0.12f, 0.13f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);   // Radio Antina
	glColor3f(0.5f, 0.5f, 0.5f);
	glVertex3f(0.27f, 0.2f, 0.0f);
	glVertex3f(0.29f, 0.2f, -0.1f);
	glVertex3f(0.07f, 0.4f, -0.1f);
	glVertex3f(0.06f, 0.4f, 0.0f);

	glEnd();

	for (float i = 0.01f; i < 0.08f; i += 0.02) { // chanel Horizontal small line mark
		//code
		glBegin(GL_LINES);
		glColor3f(0.1f, 0.1f, 0.1f);
		glVertex3f(0.05f, i, 0.0f);
		glVertex3f(0.08f, i, 0.0f);
		glEnd();

	}

	glPushMatrix();
	glTranslatef(-0.15f, 0.0f, 0.0f);
	glBegin(GL_LINES);

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)
	{
		glColor3f(0.9f, 0.7f, 0.8f);
		glVertex3f(0.0f, 0.0f, 0.0f); // Circle centre
		glColor3f(0.9f, 0.7f, 0.8f);
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);

	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	radius = 0.05f;
	glTranslatef(0.24f, 0.08f, 0.0f);
	glBegin(GL_LINES);

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)
	{
		glColor3f(0.9f, 0.8f, 0.7f);
		glVertex3f(0.0f, 0.0f, 0.0f); // Circle centre
		glColor3f(0.9f, 0.8f, 0.7f);
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);

	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	radius = 0.03f;
	glTranslatef(0.24f, -0.08f, 0.0f);
	glBegin(GL_LINES);

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)
	{
		glColor3f(0.9f, 0.8f, 0.7f);
		glVertex3f(0.0f, 0.0f, 0.0f); // Circle centre
		glColor3f(0.9f, 0.8f, 0.7f);
		glVertex3f(radius * cos(angle), radius * sin(angle), 0.0f);

	}
	glEnd();
	glPopMatrix();


}

void drawBookRac(void)
{
	//function declaration.
	void drawBook(void);

	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.5f, 0.2f); //BookRac OuterBody
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.5f, 0.1f, 0.0f);
	glVertex3f(0.5f, 0.9f, 0.0f);
	glVertex3f(0.1f, 0.9f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); //BookRac Left side patti
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.9f, 0.0f);
	glVertex3f(0.11f, 0.9f, 0.0f);
	glVertex3f(0.11f, 0.1f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); //Top draver Vrchi patti
	glVertex3f(0.1f, 0.9f, 0.0f);
	glVertex3f(0.5f, 0.9f, 0.0f);
	glVertex3f(0.5f, 0.88f, 0.0f);
	glVertex3f(0.1f, 0.88f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); //BookRac Right Weidth
	glVertex3f(0.5f, 0.1f, 0.0f);
	glVertex3f(0.6f, 0.15f, 0.0f);
	glVertex3f(0.6f, 0.95f, 0.0f);
	glVertex3f(0.5f, 0.9f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); //BookRac Top View
	glVertex3f(0.5f, 0.9f, 0.0f);
	glVertex3f(0.6f, 0.95f, 0.0f);
	glVertex3f(0.15f, 0.95f, 0.0f);
	glVertex3f(0.1f, 0.9f, 0.0f);

	/*glColor3f(0.6f, 0.2f, 0.2f);
	glVertex3f(0.1f, 0.9f, 0.0f); // Top first draver kholi
	glVertex3f(0.15f, 0.85f, -0.1f);
	glVertex3f(0.15f, 0.67f, -0.1f);
	glVertex3f(0.1f, 0.64f, 0.0f);
*/

	glColor3f(0.6f, 0.2f, 0.2f); // Lower Middle divider
	glVertex3f(0.1f, 0.45f, 0.0f);
	glVertex3f(0.5f, 0.45f, 0.0f);
	glVertex3f(0.5f, 0.42f, 0.0f);
	glVertex3f(0.1f, 0.42f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); // Bottom Draver lock last two draver divider
	glVertex3f(0.28f, 0.44f, 0.0f);
	glVertex3f(0.3f, 0.44f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glVertex3f(0.28f, 0.1f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); // Upper Second divider
	glVertex3f(0.1f, 0.67f, 0.0f);
	glVertex3f(0.5f, 0.67f, 0.0f);
	glVertex3f(0.5f, 0.65f, 0.0f);
	glVertex3f(0.1f, 0.65f, 0.0f);

	glColor3f(0.6f, 0.2f, 0.2f); // Middle draver lock open
	glVertex3f(0.3f, 0.32f, 0.0f);
	glVertex3f(0.33f, 0.32f, 0.0f);
	glVertex3f(0.33f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.1f, 0.1f, 0.1f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.6f, 0.0f, 1.0f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 0.2f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.2f, 0.2f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	drawBook();
	glEnd();
	glTranslatef(0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.5f, 0.5f, 0.5f);
	drawBook();
	glEnd();
	glTranslatef(0.0f, -0.22f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.5f, 0.5f);
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.6f, 0.6f);
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.5f);;
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.5f, 1.0f);
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.1f, 0.2f, 0.0f);
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.6f, 0.6f, 0.6f);
	drawBook();
	glEnd();
	glTranslatef(-0.05f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
	glColor3f(0.5f, 0.5f, 0.5f);
	drawBook();
	glEnd();
}

void drawBook(void)
{
	// glColor3f(0.1f, 0.4f, 0.4f); // Top Driver Book 
	glVertex3f(0.12f, 0.67f, 0.0f);
	//glVertex3f(0.25f, 0.67f, 0.0f);
	glVertex3f(0.17f, 0.69f, -0.01f);
	glVertex3f(0.17f, 0.88f, -0.01f);
	glVertex3f(0.12f, 0.86f, 0.0f);
}

void Table(void)
{
	//code
	glPushMatrix();
	// Table Up Face
	glTranslatef(0.0f, -1.8f, 0.0f);
		glBegin(GL_QUADS);
			glColor3f(0.8f, 0.6f, 0.4f);
			glVertex3f(-0.5f, -0.12f, 0.0f);
			glVertex3f(1.85f, -0.12f, 0.0f);
			glVertex3f(2.1f, 0.08f, 0.0f);
			glVertex3f(-0.15f, 0.08f, 0.0f);
		glEnd();

	// Table Side Edge
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(2.1f, 0.08f, 0.0f);
		glVertex3f(1.85f, -0.12f, 0.0f);
		glVertex3f(1.85f, -0.17f, 0.0f);
		glVertex3f(2.1f, 0.03f, 0.0f);
	glEnd();

	// Table Front Edge
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.5f, -0.12f, 0.0f);
		glVertex3f(1.85f, -0.12f, 0.0f);
		glVertex3f(1.85f, -0.17f, 0.0f);
		glVertex3f(-0.5f, -0.17f, 0.0f);
	glEnd();

	// Left - Front Leg
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.3f, -0.17f, 0.0f);
		glVertex3f(-0.35f, -0.17f, 0.0f);
		glVertex3f(-0.35f, -1.345f, 0.0f);
		glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.3f, -0.17f, 0.0f);
		glVertex3f(-0.27f, -0.17f, 0.0f);
		glVertex3f(-0.27f, -1.3f, 0.0f);
		glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	// Left - Back Leg
	glTranslatef(0.4f, -0.08f, 0.0f);
	glScalef(1.0f, 0.6f, 1.0f);
		glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.3f, -0.17f, 0.0f);
		glVertex3f(-0.35f, -0.17f, 0.0f);
		glVertex3f(-0.35f, -1.345f, 0.0f);
		glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.3f, -0.17f, 0.0f);
		glVertex3f(-0.27f, -0.17f, 0.0f);
		glVertex3f(-0.27f, -1.3f, 0.0f);
		glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	// Right - Back Leg
	glScalef(1.0f, 1.1f, 1.0f);
	glTranslatef(1.8f, 0.1f, 0.0f);
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.3f, -0.17f, 0.0f);
		glVertex3f(-0.35f, -0.22f, 0.0f);
		glVertex3f(-0.35f, -1.345f, 0.0f);
		glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.17f, 0.0f);
	glVertex3f(-0.27f, -0.145f, 0.0f);
	glVertex3f(-0.27f, -1.3f, 0.0f);
	glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	// Right - Front Leg
	glTranslatef(-0.2f, 0.02f, 0.0f);
	glScalef(1.0f, 1.52f, 0.0f);
	glBegin(GL_QUADS);
	glColor3f(0.8f, 0.6f, 0.4f);
	glVertex3f(-0.3f, -0.17f, 0.0f);
	glVertex3f(-0.35f, -0.17f, 0.0f);
	glVertex3f(-0.35f, -1.345f, 0.0f);
	glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.17f, 0.0f);
	glVertex3f(-0.27f, -0.17f, 0.0f);
	glVertex3f(-0.27f, -1.3f, 0.0f);
	glVertex3f(-0.3f, -1.345f, 0.0f);
	glEnd();

	glPushMatrix();

}

void PC(void) {

	glBegin(GL_QUADS);
	glColor3f(0.3f, 0.3f, 0.3f);
	glVertex3f(-0.35f, -0.23f, 0.0f);
	glVertex3f(0.35f, -0.23f, 0.0f);
	glVertex3f(0.35f, 0.23f, 0.0f);
	glVertex3f(-0.35f, 0.23f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.2f, 1.0f, 1.0f);
	glVertex3f(-0.33f, -0.21f, 0.0f);
	glVertex3f(0.33f, -0.21f, 0.0f);
	glVertex3f(0.33f, 0.21f, 0.0f);
	glVertex3f(-0.33f, 0.21f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.3f, 0.3f, 0.3f);
	glVertex3f(-0.03f, -0.23f, 0.0f);
	glVertex3f(0.03f, -0.23f, 0.0f);
	glVertex3f(0.03f, -0.29f, 0.0f);
	glVertex3f(-0.03f, -0.29f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.3f, 0.3f, 0.3f);
	glVertex3f(-0.13f, -0.29f, 0.0f);
	glVertex3f(0.13f, -0.29f, 0.0f);
	glVertex3f(0.13f, -0.31f, 0.0f);
	glVertex3f(-0.13f, -0.31f, 0.0f);
	glEnd();
}

void ManOnChair(void)
{
	// function declarations
	void Chair(void);
	void Man(void);

	// code
	glPushMatrix();
		// glLoadIdentity();
		glScalef(0.8f, 0.7f, 0.0f);
		glTranslatef(-2.0f, 0.5f, 0.0f);
		Man();
	glPopMatrix();

	glPushMatrix();
		// glLoadIdentity();
		// glScalef(1.0f, 1.0f, 1.0f);
		// glTranslatef(-1.7f, -0.8f, 1.0f);
		glScalef(0.8f, 0.88f, 0.5f);
		glTranslatef(0.3f, -2.8f, 1.0f);
		Chair();
	glPopMatrix();


}

void Chair(void)
{
	// glTranslatef(0.0f,-3.0f,0.0f);
	glPushMatrix();
	
		Point center = {0.0f,0.0f,0.0f};

		// Left Ball
		assignPoint(&center, -0.33f, -0.83f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.3f, 0.3f, 0.3f);
		circle2D(&center, 0.05f, 0.0f, 360.0f);
		glEnd();

		// Right Ball
		assignPoint(&center, 0.33f, -0.83f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
			circle2D(&center, 0.05f, 0.0f, 360.0f);
		glEnd();

		// Central Ball
		assignPoint(&center, 0.0f, -0.8f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
			circle2D(&center, 0.05f, 0.0f, 360.0f);
		glEnd();

		// Left Leg of chair
		glBegin(GL_QUADS);
			glColor3f(0.2f, 0.2f, 0.2f);
			glVertex3f(-0.38f, -0.8f, 0.0f);
			glVertex3f(-0.38f, -0.77f, 0.0f);
			glVertex3f(0.0f, -0.6f, 0.0f);
			glVertex3f(0.0f, -0.65f, 0.0f);
		glEnd();

		// Right Leg of chair
		glBegin(GL_QUADS);
			glVertex3f(0.38f, -0.8f, 0.0f);
			glVertex3f(0.38f, -0.77f, 0.0f);
			glVertex3f(0.0f, -0.6f, 0.0f);
			glVertex3f(0.0f, -0.65f, 0.0f);
		glEnd();

		// Central Leg of chair
		glBegin(GL_QUADS);
			glVertex3f(-0.05f, -0.63f, 0.0f);
			glVertex3f(-0.02f, -0.75f, 0.0f);
			glVertex3f(0.02f, -0.75f, 0.0f);
			glVertex3f(0.05f, -0.63f, 0.0f);
		glEnd();

		// Bar of base
		glBegin(GL_QUADS);
			glVertex3f(-0.05f, -0.63f, 0.0f);
			glVertex3f(0.05f, -0.63f, 0.0f);
			glVertex3f(0.05f, -0.3f, 0.0f);
			glVertex3f(-0.05f, -0.3f, 0.0f);
		glEnd();

		// Seat
		glBegin(GL_QUADS);
			glColor3f(0.4f, 0.1f, 0.0f);
			glVertex3f(-0.3f, -0.1f, 0.0f);
			glVertex3f(0.3f, -0.1f, 0.0f);
			glVertex3f(0.3f, -0.3f, 0.0f);
			glVertex3f(-0.3f, -0.3f, 0.0f);
		glEnd();
		// Seat Right Curve
		assignPoint(&center, 0.3f, -0.2f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
			circle2D(&center, 0.1f, 0.0f, 180.0f);
		glEnd();
		// Seat Left Curve
		assignPoint(&center, -0.3f, -0.2f, 0.0f);
		glBegin(GL_TRIANGLE_FAN);
			circle2D(&center, 0.1f, 180.0f, 360.0f);
		glEnd();

		// Support
		assignPoint(&center, 0.0f, 0.4f, 0.0f);
		glScalef(1.4f, 1.0f, 1.0f);
		glBegin(GL_TRIANGLE_FAN);
			glColor3f(0.4f, 0.1f, 0.0f);
			circle2D(&center, 0.3f, 0.0f, 360.0f);
		glEnd();

		// Bar of Support
		glBegin(GL_QUADS);
			glColor3f(0.2f, 0.2f, 0.2f);
			glVertex3f(-0.015f, -0.2f, 0.0f);
			glVertex3f(0.015f, -0.2f, 0.0f);
			glVertex3f(0.015f, 0.5f, 0.0f);
			glVertex3f(-0.015f, 0.5f, 0.0f);
		glEnd();
	glPopMatrix();
}

void Man(void) {

	glPushMatrix();
	

		Point center = { 0.0f, 0.0f, 0.0f };
		Point p1 = { 0.0f, 0.0f, 0.0f };
		Point p2 = { 0.6f, -0.2f, 0.0f };
		Point p3 = { 0.3f, 0.45f, 0.0f };
		Point p4 = { 0.5f, 0.7f, 0.0f };

		glPushMatrix();
			glPushMatrix();
				glTranslatef(0.03f, -0.1f, 0.0f);
				glScalef(0.9f, 1.2f, 1.0f);
				glBegin(GL_TRIANGLE_FAN);
					glColor3f(0.6f, 0.4f, 0.2f);
					circle2D(&center, 0.05f, 0.0f, 180.0f);
				glEnd();
			glPopMatrix();

			glPushMatrix();
				glTranslatef(-0.43f, -0.1f, 0.0f);
				glScalef(0.9f, 1.2f, 1.0f);
				glBegin(GL_TRIANGLE_FAN);
					glColor3f(0.6f, 0.4f, 0.2f);
					circle2D(&center, 0.05f, 180.0f, 360.0f);
				glEnd();
			glPopMatrix();

			glPushMatrix();
				glRotatef(125.0f, 0.0f, 0.0f, 1.0f);
				glScalef(0.7f, 0.7f, 1.0f);
				glBegin(GL_POLYGON);
					glColor3f(0.1f, 0.1f, 0.1f);
					bezierCurve4Points(&p1, &p2, &p3, &p4);
				glEnd();
			glPopMatrix();

			glPushMatrix();
				glScalef(1.0f, 1.2f, 1.0f);
				glBegin(GL_TRIANGLE_FAN);
					circle2D(&center, 0.05f, 270.0f, 451.0f);
				glEnd();
			glPopMatrix();

			glPushMatrix();
				glScalef(1.0f, 1.9f, 1.0f);
				glTranslatef(-0.2f, -0.01f, 0.0f);
				glBegin(GL_TRIANGLE_FAN);
					glColor3f(0.6f, 0.4f, 0.2f);
					circle2D(&center, 0.24f, 90.0f, 271.0f);
				glEnd();
			glPopMatrix();

			glPushMatrix();
				glScalef(1.0f, 1.8f, 1.0f);
				glTranslatef(-0.2f, 0.0f, 0.0f);
				glBegin(GL_TRIANGLE_FAN);
					glColor3f(0.1f, 0.1f, 0.1f);
					circle2D(&center, 0.24f, 90.0f, 271.0f);
				glEnd();
			glPopMatrix();

		glPopMatrix();


		// Man Body
		glPushMatrix();

			// Man Shoulder Part
			glPushMatrix();
				glScalef(1.3f, 1.4f, 1.0f);
				glTranslatef(-1.1f, 0.02f, 0.0f);

				glBegin(GL_TRIANGLE_FAN);
					//glColor3f(0.0f, 1.0f, 0.0f);
					glColor3f(0.9f, 0.5f, 0.6f);
					//glColor3f(1.0f, 0.2f, 0.9f);
					assignPoint(&center, -0.25f, -0.2f, 0.0f);
					circle2D(&center, 0.2f, 270.0f, 450.0f);
				glEnd();
			glPopMatrix();
			
			glPushMatrix();
				glScalef(1.3f, 1.0f, 1.0f);
				glTranslatef(-1.1f, 0.02f, 0.0f);
				glBegin(GL_QUADS);
					//glColor3f(0.0f, 1.0f, 0.0f);
					glColor3f(0.9f, 0.5f, 0.6f);
					//glColor3f(1.0f, 0.2f, 0.9f);
					glVertex3f(-0.45f, -0.8f, 0.0f);
					glVertex3f(-0.05f, -0.8f, 0.0f);
					glVertex3f(-0.05f, -0.2f, 0.0f);
					glVertex3f(-0.45f, -0.2f, 0.0f);
				glEnd();
			glPopMatrix();
		glPopMatrix();

		//Left Leg
		glPushMatrix();
			glTranslatef(-1.9f, -0.8f, 0.0f);
			glScalef(1.0f, 0.75f, 1.0f);
			glBegin(GL_QUADS);
				glColor3f(0.0f, 0.2f, 0.5f);
				glVertex3f(-0.07f, 0.0f, 0.0f);
				glVertex3f(-0.07f, -0.6f, 0.0f);
				glVertex3f(0.07f, -0.6f, 0.0f);
				glVertex3f(0.07f, 0.0f, 0.0f);
			glEnd();

			glTranslatef(0.25f, -0.2f, 0.0f);
			glScalef(1.0f, 2.0f, 1.0f);
			glBegin(GL_TRIANGLE_FAN);
				glColor3f(0.6f, 0.4f, 0.2f);
				assignPoint(&center, -0.25f, -0.2f, 0.0f);
				circle2D(&center, 0.05f, 90.0f, 270.0f);
			glEnd();
		glPopMatrix();

		//Right Leg
		glPushMatrix();
			glTranslatef(-1.6f, -0.8f, 0.0f);
			glScalef(1.0f, 0.75f, 1.0f);
			glBegin(GL_QUADS);
				glColor3f(0.0f, 0.2f, 0.5f);
				glVertex3f(-0.07f, 0.0f, 0.0f);
				glVertex3f(-0.07f, -0.6f, 0.0f);
				glVertex3f(0.07f, -0.6f, 0.0f);
				glVertex3f(0.07f, 0.0f, 0.0f);
			glEnd();

			glTranslatef(0.25f, -0.2f, 0.0f);
			glScalef(1.0f, 2.0f, 1.0f);
			glBegin(GL_TRIANGLE_FAN);
				glColor3f(0.6f, 0.4f, 0.2f);
				assignPoint(&center, -0.25f, -0.2f, 0.0f);
				circle2D(&center, 0.05f, 90.0f, 270.0f);
			glEnd();
		glPopMatrix();
	glPopMatrix();
	


}

void CoffeeMug(void)
{
	// coffee mug
	glPushMatrix();
		glColor3f(0.3f, 0.1f, 0.0f);
		glBegin(GL_QUADS);
			glVertex3f(0.27f, 0.4f, 0.0f);
			glVertex3f(0.45f, 0.4f, 0.0f);
			glVertex3f(0.45f, 0.05f, 0.0f);
			glVertex3f(0.27f, 0.05f, 0.0f);
		glEnd();

		// Handle top horizontal
		glBegin(GL_QUADS);
			glVertex3f(0.45f, 0.34f, 0.0f);
			glVertex3f(0.51f, 0.34f, 0.0f);
			glVertex3f(0.51f, 0.31f, 0.0f);
			glVertex3f(0.45f, 0.31f, 0.0f);
		glEnd();

		// Handle side vertical
		glBegin(GL_QUADS);
			glVertex3f(0.51f, 0.34f, 0.0f);
			glVertex3f(0.54f, 0.34f, 0.0f);
			glVertex3f(0.54f, 0.11f, 0.0f);
			glVertex3f(0.51f, 0.11f, 0.0f);
		glEnd();

		// Handle bottom horizontal
		glBegin(GL_QUADS);
			glVertex3f(0.45f, 0.14f, 0.0f);
			glVertex3f(0.51f, 0.14f, 0.0f);
			glVertex3f(0.51f, 0.11f, 0.0f);
			glVertex3f(0.45f, 0.11f, 0.0f);
		glEnd();
	glPopMatrix();

}

void PenStand(void)
{

	// Pen Stand
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.4f, 0.4f);
		glVertex3f(0.67f, 0.4f, 0.0f);
		glVertex3f(0.85f, 0.4f, 0.0f);
		glVertex3f(0.85f, 0.05f, 0.0f);
		glVertex3f(0.67f, 0.05f, 0.0f);
	glEnd();

	// Pens
	// Marker pen
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.72f, 0.6f, 0.0f);
		glVertex3f(0.76f, 0.6f, 0.0f);
		glVertex3f(0.76f, 0.4f, 0.0f);
		glVertex3f(0.72f, 0.4f, 0.0f);
	glEnd();

	glPushMatrix();
		//glTranslatef(0.1f, 0.2f,0.0f);
		//glScalef(0.5f, 0.5f, 1.0f);
		glBegin(GL_QUADS);
			glColor3f(0.0f, 0.2f, 0.6f);
			glVertex3f(0.73f, 0.6f, 0.0f);
			glVertex3f(0.75f, 0.6f, 0.0f);
			glVertex3f(0.75f, 0.45f, 0.0f);
			glVertex3f(0.73f, 0.45f, 0.0f);
		glEnd();
	glPopMatrix();

	// normal pen
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.2f, 0.5f);
		glVertex3f(0.815f, 0.6f, 0.0f);
		glVertex3f(0.845f, 0.6f, 0.0f);
		glVertex3f(0.815f, 0.4f, 0.0f);
		glVertex3f(0.785f, 0.4f, 0.0f);
	glEnd();

}

void WriteShlokOnBook(void)
{
	// Function Declaration
	//void shlokOnBook();
	void Table2();
	void TableScreen2();
	void Jivet();
	void Sharad();
	void Shatam();
	void MorPankh();
	void ShlokLines();


	// function calling

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	//glScalef(0.0f, 0.1f, 0.0f);
	Table2();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	glScalef(1.2f, 1.4f, 0.0f);
	TableScreen2();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.30f, 0.23f, 0.0f);
	glScalef(0.20f, 0.20f, 0.0f);
	Jivet();
	glPopMatrix();


	glPushMatrix();
	//glTranslatef(0.5f, 0.07f, 0.0f); // Right page sharad
	glTranslatef(-0.08f, -0.10f, 0.0f);
	glScalef(0.5f, 0.40f, 0.0f);
	Sharad();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(-0.10f, -0.32f, 0.0f);
	glScalef(0.5f, 0.40f, 0.0f);
	Shatam();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.20f, -0.15f, 0.0f);
		glScalef(0.5f, 1.0f, 1.0f);
		MorPankh();
	glPopMatrix();

	glPushMatrix(); // line j chya adhichya
	glTranslatef(-1.70f, 0.38f, 0.0f);
	glScalef(0.35f, 0.25f, 0.0f);
	ShlokLines();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.65f, 0.38f, 0.0f);
	glScalef(0.35f, 0.25f, 0.0f);
	ShlokLines();
	glPopMatrix();


	glPushMatrix(); // line M chya nantrchya
	glTranslatef(-0.8f, -0.07f, 0.0f);
	glScalef(0.35f, 0.25f, 0.0f);
	ShlokLines();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.75f, -0.07f, 0.0f);
	glScalef(0.35f, 0.25f, 0.0f);
	ShlokLines();
	glPopMatrix();
	//void TableBook();


	// Function Calling
	//shlokOnBook();
	//TableBook();
}


void TableScreen2(void)
{
	glPushMatrix();

	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.9f, 0.9f); //Book opened 
	glVertex3f(-0.5f, -0.3f, 0.0f);
	glVertex3f(0.5f, -0.3f, 0.0f);
	glVertex3f(0.5f, 0.4f, 0.0f);
	glVertex3f(-0.5f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Right Bottom weidth
	glColor3f(0.6f, 0.6f, 0.6f);
	glVertex3f(0.02f, -0.3f, 0.0f);
	glVertex3f(0.06f, -0.36f, 0.0f);
	glVertex3f(0.6f, -0.37f, 0.0f);
	glVertex3f(0.5f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.6f, 0.6f, 0.6f); //Book right weidth
	glVertex3f(0.5f, -0.3f, 0.0f);
	glVertex3f(0.6f, -0.37f, 0.0f);
	glVertex3f(0.6f, 0.38f, 0.0f);
	glVertex3f(0.5f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.6f, 0.6f, 0.6f); //Book divider
	glVertex3f(0.03f, 0.4f, 0.0f);
	glVertex3f(0.03f, -0.3f, 0.0f);
	glVertex3f(-0.02f, -0.3f, 0.0f);
	glVertex3f(-0.02f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Left Bottom weidth
	glColor3f(0.6f, 0.6f, 0.6f);
	glVertex3f(-0.01f, -0.3f, 0.0f);
	glVertex3f(-0.05f, -0.35f, 0.0f);
	glVertex3f(-0.6f, -0.37f, 0.0f);
	glVertex3f(-0.5f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Left Bottom weidth
	glColor3f(0.6f, 0.6f, 0.6f);
	glVertex3f(-0.5f, -0.3f, 0.0f);
	glVertex3f(-0.6f, -0.37f, 0.0f);
	glVertex3f(-0.6f, 0.38f, 0.0f);
	glVertex3f(-0.5f, 0.4f, 0.0f);

	glEnd();

	glPopMatrix();
}

void Table2(void)
{
	glBegin(GL_QUADS);
	glColor3f(0.8039f, 0.5216f, 0.2471f); //Book opened 
	glVertex3f(-0.8f, -0.6f, 0.4f);
	glVertex3f(0.8f, -0.6f, 0.0f);
	glVertex3f(0.8f, 0.7f, 0.0f);
	glVertex3f(-0.8f, 0.7f, 0.0f);
	glEnd();

}

void Ji() {
	// J
	// Kana1
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.1f, 0.45f, 0.0f);
	glVertex3f(-0.2f, 0.45f, 0.0f);
	glVertex3f(-0.2f, 0.4f, 0.0f);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glEnd();

	// Curves
	glBegin(GL_POLYGON);

	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.35f + 0.18f * cosf(angle), 0.36f - 0.18f * sinf(angle), 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.9f, 0.9f, 0.9f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.35f + 0.13f * cosf(angle), 0.39f - 0.13f * sinf(angle), 0.0f);

	glEnd();


	// Kana2
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.05f, 0.05f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.6f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glEnd();

	// Velanti
	glBegin(GL_POLYGON);

	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.16f * cosf(angle), 0.735f + 0.16f * sinf(angle), 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.9f, 0.9f, 0.9f);
	for (float angle = -0.5235987f * 2.0f; angle <= 3.1415926f + (2.0f * 0.5235987f); angle += 0.01f)
		glVertex3f(-0.11f * cosf(angle), 0.735f + 0.11f * sinf(angle), 0.0f);

	glEnd();
}

void Ve() {

	// Curves
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.2f, 0.5f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.05f - 0.2f * sinf(angle), 0.33f - 0.2f * cosf(angle), 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.9f, 0.9f, 0.9f);
	for (float angle = -0.5235987f; angle <= 3.1415926f + 0.5235987f; angle += 0.01f)
		glVertex3f(-0.05f - 0.15f * sinf(angle), 0.33f - 0.15f * cosf(angle), 0.0f);

	glEnd();

	// Kana
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();

	// Matra
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.0f, 0.65f, 0.0f);
	glVertex3f(-0.25f, 0.85f, 0.0f);
	glVertex3f(-0.25f, 0.9f, 0.0f);
	glVertex3f(0.05f, 0.65f, 0.0f);
	glEnd();
}

void T() {
	// Kana
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.0f, 0.05f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(-0.3f, 0.05f, 0.0f);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(-0.25f, 0.4f, 0.0f);
	glEnd();
}

void Jivet()
{
	// Line
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.8f, 0.6f, 0.0f);
	glVertex3f(-0.75, 0.65f, 0.0f);
	glVertex3f(0.8f, 0.65f, 0.0f);
	glVertex3f(0.75f, 0.6f, 0.0f);
	glEnd();

	glTranslatef(-0.3f, 0.0f, 0.0f);
	Ji();

	glTranslatef(0.45f, 0.0f, 0.0f);
	Ve();

	glTranslatef(0.45f, 0.0f, 0.0f);
	T();
}

void Sha() {


	//glTranslatef(-0.34f, 0.460f, 0.0f);
	glPushMatrix();
	// Velanti
		glPushMatrix();
			glTranslatef(-1.33f, 1.1f, 0.0f);
			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);

			for (float angle = 0.0f; angle <= 360.0f; angle += 0.01f)
				glVertex3f(0.150f * 0.5 * cosf(angle), 0.150f * sinf(angle), 0.0f);

			glEnd();


			glBegin(GL_POLYGON);
			glColor3f(0.9f, 0.9f, 0.9f);
			for (float angle = -0.0f; angle <= 360.0f; angle += 0.01f)
				glVertex3f(0.075f * 0.5 * cosf(angle), 0.075f * sinf(angle), 0.0f);

			glEnd();
		glPopMatrix();
		//glLoadIdentity();


		glPushMatrix();
			glTranslatef(-0.7f, 0.61f, 0.0f);
			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.620f, 0.4f, 0.0f);
			glVertex3f(-0.700f, 0.2f, 0.0f);
			glVertex3f(-0.670f, 0.15f, 0.0f);
			glVertex3f(-0.570f, 0.4f, 0.0f);

			glEnd();

			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.740f, 0.25f, 0.0f);
			glVertex3f(-0.700f, 0.3f, 0.0f);
			glVertex3f(-0.580f, 0.05f, 0.0f);
			glVertex3f(-0.620f, 0.0f, 0.0f);

			glEnd();

			// Kana1
			glBegin(GL_QUADS);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.500f, 0.05f, 0.0f);
			glVertex3f(-0.460f, 0.0f, 0.0f);
			glVertex3f(-0.460f, 0.6f, 0.0f);
			glVertex3f(-0.500f, 0.6f, 0.0f);
			glEnd();

		glPopMatrix();

	glPopMatrix();

}

void Sha2() {


	//glTranslatef(-0.34f, 0.460f, 0.0f);
	glPushMatrix();
	// Velanti
		glPushMatrix();
			glTranslatef(-1.33f, 1.1f, 0.0f);
			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);

			for (float angle = 0.0f; angle <= 360.0f; angle += 0.01f)
				glVertex3f(0.150f * 0.5 * cosf(angle), 0.150f * sinf(angle), 0.0f);

			glEnd();


			glBegin(GL_POLYGON);
			glColor3f(0.9f, 0.9f, 0.9f);
			for (float angle = -0.0f; angle <= 360.0f; angle += 0.01f)
				glVertex3f(0.075f * 0.5 * cosf(angle), 0.075f * sinf(angle), 0.0f);

			glEnd();
		glPopMatrix();
		//glLoadIdentity();


		glPushMatrix();
			glTranslatef(-0.7f, 0.61f, 0.0f);
			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.620f, 0.4f, 0.0f);
			glVertex3f(-0.700f, 0.2f, 0.0f);
			glVertex3f(-0.670f, 0.15f, 0.0f);
			glVertex3f(-0.570f, 0.4f, 0.0f);

			glEnd();

			glBegin(GL_POLYGON);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.740f, 0.25f, 0.0f);
			glVertex3f(-0.700f, 0.3f, 0.0f);
			glVertex3f(-0.580f, 0.05f, 0.0f);
			glVertex3f(-0.620f, 0.0f, 0.0f);

			glEnd();

			// Kana1
			glBegin(GL_QUADS);
			glColor3f(0.0f, 0.2f, 0.5f);
			glVertex3f(-0.500f, 0.05f, 0.0f);
			glVertex3f(-0.460f, 0.0f, 0.0f);
			glVertex3f(-0.460f, 0.6f, 0.0f);
			glVertex3f(-0.500f, 0.6f, 0.0f);
			glEnd();

		glPopMatrix();

	glPopMatrix();

}


void Ra() {

	//glTranslatef(-0.750f, 0.430f, 0.0f);

	// Velanti
	glPushMatrix();

		glPushMatrix();
			glTranslatef(-0.95f, 1.05f, 0.0f);
			glBegin(GL_POLYGON);
				glColor3f(0.0f, 0.2f, 0.5f);

				for (float angle = -3.14f / 2; angle <= 3.14f; angle += 0.01f)
					glVertex3f(0.150f * 0.5 * cosf(angle), 0.150f * 1.2f * sinf(angle), 0.0f);

			glEnd();


			glBegin(GL_POLYGON);
				glColor3f(0.9f, 0.9f, 0.9f);
				for (float angle = -3.14f / 2; angle <= 3.145f; angle += 0.01f)
					glVertex3f(-0.02f + 0.1f * 0.5 * cosf(angle), -0.06f + 0.1 * sinf(angle), 0.0f);

			glEnd();
			
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-0.65f, 0.61f, 0.0f);
			glBegin(GL_POLYGON);
				glColor3f(0.0f, 0.2f, 0.5f);
				glVertex3f(-0.350f, 0.30f, 0.0f);
				glVertex3f(-0.310f, 0.35f, 0.0f);
				glVertex3f(-0.190f, 0.05f, 0.0f);
				glVertex3f(-0.220f, 0.0f, 0.0f);

			glEnd();

		glPopMatrix();
	glPopMatrix();
	
}

void Da()
{
	// Curves

	//glTranslatef(-0.350f, 0.0f, 0.0f);
	glPushMatrix();
	glTranslatef(0.1f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.2f, 0.5f);
	for (float angle = -0.023f; angle <= 3.14f; angle += 0.01f)
		glVertex3f(-0.01f - 0.2f * sinf(angle), 0.33f - 0.2f * cosf(angle), 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.9f, 0.9f, 0.9f);
	for (float angle = -0.1235987f; angle <= 3.14f + 0.1f; angle += 0.01f)
		glVertex3f(-0.01f - 0.15f * sinf(angle), 0.33f - 0.15f * cosf(angle), 0.0f);

	glEnd();

	glPopMatrix();

	//glLoadIdentity();
	glPushMatrix();
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.08f, 0.02f, 0.0f);
	glVertex3f(0.11f, 0.07f, 0.0f);
	glVertex3f(0.080f, 0.25f, 0.0f);
	glVertex3f(0.045f, 0.21f, 0.0f);
	glEnd();


	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(0.05f, 0.5f, 0.0f);
	glVertex3f(0.09f, 0.5f, 0.0f);
	glVertex3f(0.09f, 0.6f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glEnd();

	glPopMatrix();




	//  2 Dots 
	glPushMatrix();
	glTranslatef(0.200f, 0.450f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.2f, 0.5f);


	for (float angle = 0.0f; angle <= 360.0f; angle += 0.01f)
		glVertex3f(0.05f * 0.5 * cosf(angle), 0.05f * sinf(angle), 0.0f);

	glEnd();

	glPopMatrix();
	//glLoadIdentity();


	glPushMatrix();
	glTranslatef(0.200f, 0.200f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.2f, 0.5f);


	for (float angle = 0.0f; angle <= 360.0f; angle += 0.01f)
		glVertex3f(0.05f * 0.5 * cosf(angle), 0.05f * sinf(angle), 0.0f);

	glEnd();

	glPopMatrix();
	//glLoadIdentity();

}

void Sharad()
{
	// Line
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.2f, 0.5f);
		glVertex3f(-0.8f, 0.6f, 0.0f);
		glVertex3f(-0.75f, 0.63f, 0.0f);
		glVertex3f(-0.1f, 0.63f, 0.0f);
		glVertex3f(-0.15f, 0.6f, 0.0f);
	glEnd();

	glPushMatrix();
		// glLoadIdentity();
	
		glScalef(0.5f, 0.5f, 1.0);
		// glTranslatef(-0.3f, 0.0f, 0.0f);
		Sha2();
	glPopMatrix();

	glPushMatrix();
		glScalef(0.25f, 0.25f, 1.0);
		glTranslatef(-0.4f, -0.6f, 0.0f);
		Ra();
	glPopMatrix();

	glPushMatrix();
			glScalef(0.25f, 0.25f, 1.0);
			glTranslatef(-1.1f, 0.0f, 0.0f);
			Da();
	glPopMatrix();
}

void Ta() {

	//glTranslatef(-0.550f, 0.0f, 0.0f);

	glPushMatrix();
	// Kana
	glTranslatef(-0.73f, 0.6f, 0.0f);
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.100f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.100f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.1f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.45f, 0.0f);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.3f, 0.4f, 0.0f);
	glVertex3f(-0.3f, 0.05f, 0.0f);
	glVertex3f(-0.25f, 0.0f, 0.0f);
	glVertex3f(-0.25f, 0.4f, 0.0f);
	glEnd();


	glPopMatrix();

}

void Ma()
{

	glPushMatrix();
	// Curves
	glTranslatef(-0.35f, 0.6f, 0.0f);

	// Kana
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.25f, 0.6f, 0.0f);
	glVertex3f(-0.25f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.6f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.1f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.3f, 0.0f);
	glVertex3f(-0.1f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.25f, 0.22f, 0.0f);
	glVertex3f(-0.2f, 0.22f, 0.0f);
	glVertex3f(-0.33f, 0.35f, 0.0f);
	glVertex3f(-0.33f, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(0.05f, -0.05f, 0.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.15f, 0.05f, 0.0f);
	glEnd();


	glPopMatrix();



}

void Lines()
{

	glTranslatef(0.7f, 0.0f, 0.0f);


	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.670f, 0.0f);
	glEnd();

	glLoadIdentity();
}

void Shatam()
{
	// Line
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.8f, 0.6f, 0.0f);
	glVertex3f(-0.75, 0.63f, 0.0f);
	glVertex3f(-0.1f, 0.63f, 0.0f);
	glVertex3f(-0.15f, 0.6f, 0.0f);
	glEnd();

	glPushMatrix();
	glScalef(0.5f, 0.5f, 1.0);
	Sha();
	glPopMatrix();

	glPushMatrix();
	glScalef(0.5f, 0.5f, 1.0);
	Ta();
	glPopMatrix();


	glPushMatrix();
	glScalef(0.5f, 0.5f, 1.0);
	Ma();
	glPopMatrix();

	//Lines();
}

void GreenHead()
{
	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.4f, 0.6f, 0.0f };
	Point p4 = { 0.5f, 0.7f, 0.0f };

	glBegin(GL_POLYGON);

	glColor3f(0.0f, 1.0f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.x * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.x * 3.0f * u * u * (1.0f - u)) + (p4.x * u * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.y * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.y * 3.0f * u * u * (1.0f - u)) + (p4.y * u * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.z * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.z * 3.0f * u * u * (1.0f - u)) + (p4.z * u * u * u);

		glVertex3f(x, y, z);
	}

	Point p5 = { 0.5f, 0.7f, 0.0f };
	Point p6 = { 0.3f, 0.4f, 0.0f };
	Point p7 = { -0.1f, 0.8f, 0.0f };
	Point p8 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p5.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.x * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.x * 3.0f * u * u * (1.0f - u)) + (p8.x * u * u * u);
		float y = (p5.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.y * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.y * 3.0f * u * u * (1.0f - u)) + (p8.y * u * u * u);
		float z = (p5.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.z * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.z * 3.0f * u * u * (1.0f - u)) + (p8.z * u * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void YellowHead()
{
	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.5f, 0.7f, 0.0f };

	glBegin(GL_POLYGON);

	glColor3f(1.0f, 1.0f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	Point p4 = { 0.5f, 0.7f, 0.0f };
	Point p5 = { -0.2f, 0.75f, 0.0f };
	Point p6 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * u * (1.0f - u)) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * u * (1.0f - u)) + (p6.y * u * u);
		float z = (p4.z * (1.0f - u) * (1.0f - u)) + (p5.z * 2.0f * u * (1.0f - u)) + (p6.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void OrangeHead() {

	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.5f, 0.7f, 0.0f };

	glScalef(0.9f, 0.9f, 1.0f);
	glBegin(GL_POLYGON);

	glColor3f(1.0f, 0.7f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	Point p4 = { 0.5f, 0.7f, 0.0f };
	Point p5 = { -0.2f, 0.75f, 0.0f };
	Point p6 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * u * (1.0f - u)) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * u * (1.0f - u)) + (p6.y * u * u);
		float z = (p4.z * (1.0f - u) * (1.0f - u)) + (p5.z * 2.0f * u * (1.0f - u)) + (p6.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void BlueHead()
{
	glBegin(GL_TRIANGLE_FAN);

	glColor3f(0.1f, 0.75f, 1.0f);

	for (float angle = 0.0f; angle <= (2.0f * 3.1415926f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f(0.2f * sinf(angle), 0.2f * cosf(angle), 0.0f);

	glEnd();
}

void DarkBlueHead()
{
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.2f, 0.0f, 1.0f);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(-0.2f * cosf(angle), -0.2f * sinf(angle), 0.0f);

	glEnd();

	glTranslatef(0.4f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(-0.2f * cosf(angle), -0.2f * sinf(angle), 0.0f);

	glEnd();

	glTranslatef(-0.203f, -0.04f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(0.4f * cosf(angle), 0.4f * sinf(angle), 0.0f);

	glEnd();
}

void Stem()
{
	Point p1 = { 0.2f, 0.3f, 0.0f };
	Point p2 = { 0.0f, 0.1f, 0.0f };
	Point p3 = { 0.0f, -1.0f, 0.0f };

	glPushMatrix();
	glRotatef(315.0f, 0.0f, 0.0f, 1.0f);
	glScalef(0.3f, 0.3f, 1.0f);
	glBegin(GL_POLYGON);

	glColor3f(0.0f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();


	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	glVertex3f(0.0f, 0.3f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}
	glPopMatrix();
	glEnd();
}

void MorPankh() {

	// 1.Stem
	// 2.GreenHead
	// 3.YellowHead
	// 4.OrangeHead
	// 5.BlueHead
	// 6.DarkBlueHead
	// 7. Ink
	void Ink(void);
	// glPushMatrix();
	// 	glScalef(2.35f, 2.35f, 1.0f);
	// 	glRotatef(125.0f,0.0f,0.0f,1.0f);
	// 	glTranslatef(0.0f, -1.0f, 0.0f);

	// 	Ink();
	// glPopMatrix();
	Stem();

	glScalef(2.0f, 2.0f, 1.0f);
	glTranslatef(-0.011f, 0.07f, 0.0f);
	glRotatef(44.0f, 0.0f, 0.0f, 1.0f);
	GreenHead();

	glTranslatef(0.06f, 0.06f, 0.0f);
	glScalef(0.6f, 0.6f, 1.0f);
	YellowHead();


	OrangeHead();

	glTranslatef(0.21f, 0.28f, 0.0f);
	glScalef(1.0f, 1.25f, 1.0f);
	BlueHead();

	glRotatef(330.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(-0.07f, 0.0f, 0.0f);
	glScalef(0.35f, 0.35f, 1.0f);
	DarkBlueHead();

	

}

void Ink() {

	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.2f, 0.5f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.008f, -0.1f, 0.0f);
		glVertex3f(0.008f, -0.1f, 0.0f);
	glEnd();
}

void ShlokLines()
{

	glTranslatef(0.7f, 0.0f, 0.0f);


	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.670f, 0.0f);
	glEnd();

	glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'R':
	case 'r':
		if(runFlag == 0)
		{	
			runFlag = 1;
		}
		else
		{
			runFlag = 0;
		}
		break;
	//Transition FadeIn
	case 'T':
	case 't':
		if(FadeInFlagOn == 0)
		{	
			FadeInFlagOn = 1;
			FadeOutFlagOn = 0;


		}
		else
		{
			FadeInFlagOn = 0;
			FadeOutFlagOn = 1;

		}
		break;

	//Transition Fadeout
	case 'O':
	case 'o':
		if(FadeOutFlagOn == 0)
		{	
			FadeOutFlagOn = 1;
			FadeInFlagOn = 0;
			scene1 = 0;
		}
		else
		{
			FadeOutFlagOn = 0;
		}
		break;

	

	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}

