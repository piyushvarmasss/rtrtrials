// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

float angle = 0.0f;

void update(void)
{
	if (angle < 360.00f)
	{
		angle = angle + 0.02f;
	}
	glutPostRedisplay();
}

void display(void)
{
	// Function Declaration
	void update(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	// glScalef(float x, float y, float z)					 // works on GL_LINES (only float x scales)
	// glTranslatef(float x, float y, float z)				 // works on GL_LINES
	// glRotatef(float angle, float x, float y, float z)	 // works on GL_LINES (angle is in degrees)

	glPushMatrix();
		
		glRotatef(angle, 0.0f, 1.0f, 0.0f);
		
		glPushMatrix();

			glTranslatef(0.0f, 0.0f, 0.0f);
			glScalef(1.0f, 1.0f, 1.0f);

			glBegin(GL_POLYGON);

			glColor3f(0.5f, 0.5f, 0.5f);
			glVertex3f(0.0f, 0.5f, 0.0f);

			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.5f);

			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.5f);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.0f, -0.5f, -0.5f);

			glEnd();

		glPopMatrix();

	glPopMatrix();

	update();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}
