// header files
#include <GL/freeglut.h>

// global variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Piyush Niranjan Varma");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}


void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

float angle = 0;

void display(void)
{
	void update(void);
	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
		
		glRotatef(angle, 0.0f, -0.5f, 0.0f);

		glBegin(GL_TRIANGLES);

		glColor3f(1.0f, 0.0f, 0.0f);	// Red
		glVertex3f(0.0f, 0.5f, 0.0f);	// A

		glColor3f(0.0f, 1.0f, 0.0f);	// Green
		glVertex3f(-0.5f, -0.5f, 0.0f);	// B 

		glColor3f(0.0f, 0.0f, 1.0f);	// Blue
		glVertex3f(0.5f, -0.5f, 0.0f);	// C

		glEnd();

	glPopMatrix();

	update();

	glutSwapBuffers();

}

void update(void) 
{
	if (angle < 360.00f) 
	{
		angle += 0.01f;
	}

	glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key) 
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) 
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}
