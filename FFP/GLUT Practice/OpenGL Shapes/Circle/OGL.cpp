// header files
#include <GL/freeglut.h>

// global variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Piyush Niranjan Varma");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

float squareRoot(float num) {

	double temp = 0, root = num / 2.0;	// Mangesh Manjare Method (accurate than any other algorithm)
	while (root != temp) {

		temp = root;
		root = (num / root + root) / 2;
	}
	return root;
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);
	
	// glColor3f(1.0f, 0.0f, 0.0f);			// Original Triangle
	// glVertex3f(0.0f, 1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, -1.0f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, -1.0f, 0.0f);


	// glColor3f(1.0f, 0.0f, 0.0f);			// vertex left corner : right angled triangle 
	// glVertex3f(-1.0f, 1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, -1.0f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, -1.0f, 0.0f);



	// glColor3f(1.0f, 0.0f, 0.0f);			// vertex right corner : right angled triangle
	// glVertex3f(1.0f, 1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, -1.0f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, -1.0f, 0.0f);


	// glColor3f(1.0f, 0.0f, 0.0f);			// Original Inverted Triangle
	// glVertex3f(0.0f, -1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, 1.0f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, 1.0f, 0.0f);



	// Coloured Star PAttern
	// glColor3f(1.0f, 0.0f, 0.0f);			// Original Triangle
	// glVertex3f(0.0f, 1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, -0.5f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, -0.5f, 0.0f);

	// glColor3f(1.0f, 0.0f, 0.0f);			// Original Inverted Triangle
	// glVertex3f(0.0f, -1.0f, 0.0f);

	// glColor3f(0.0f, 1.0f, 0.0f);
	// glVertex3f(-1.0f, 0.5f, 0.0f);

	// glColor3f(0.0f, 0.0f, 1.0f);
	// glVertex3f(1.0f, 0.5f, 0.0f);

// ____________________________________________________________________________________________
	
	// White Star Pattern
	// glColor3f(1.0f, 1.0f, 1.0f);			// Original Triangle
	// glVertex3f(0.0f, 1.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-1.0f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(1.0f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);			// Original Inverted Triangle
	// glVertex3f(0.0f, -1.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-1.0f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(1.0f, 0.5f, 0.0f);



	// White Square
	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, -0.5f, 0.0f);



	// White Pentagon
	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, -0.5f, 0.0f);




	// White Hexagon
	// glColor3f(1.0f, 1.0f, 1.0f);	// Upper Half of Hexagon
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, 0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, 0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, 0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, 0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);	// Lower Half of Hexagon
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, -0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, -0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.25f, -0.433f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.25f, -0.433f, 0.0f);



	// White Octagon
	// glColor3f(1.0f, 1.0f, 1.0f);		// Upper Half 
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.353f, 0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.353f, 0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.353f, 0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.353f, 0.353f, 0.0f);


	// glColor3f(1.0f, 1.0f, 1.0f);		// Lower Half
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.353f, -0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, -0.5f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.353f, -0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(-0.353f, -0.353f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.0f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.5f, 0.0f, 0.0f);

	// glColor3f(1.0f, 1.0f, 1.0f);
	// glVertex3f(0.353f, -0.353f, 0.0f);

	// 4 triangles using for loop
	// for (float i = -1.0f; i <= 0.5f; i+=0.5f) {

	//	float j = i + 0.5f, k = i + 0.25f;
		
	//	 glColor3f(1.0f, 1.0f, 1.0f);
	//	 glVertex3f(i, 0.0f, 0.0f);

	//	 glColor3f(1.0f, 1.0f, 1.0f);
	//	 glVertex3f(j, 0.0f, 0.0f);

	//	 glColor3f(1.0f, 1.0f, 1.0f);
	//	 glVertex3f(k, 0.5f, 0.0f);
		
	// }

	// Circular Rhombus
	// float x = 0.0f, y = 0.5f, xtemp = 0.01f, ytemp = 0.01f, xdiff = -0.1f, ydiff = -0.1f;
	// int section = 0;

	// do {

	//	glColor3f(1.0f, 0.0f, 0.0f);
	//	glVertex3f(0.0f, 0.0f, 0.0f);

	//	glColor3f(0.0f, 1.0f, 0.0f);
	//	glVertex3f(x, y, 0.0f);

	//	glColor3f(0.0f, 0.0f, 1.0f);
	//	glVertex3f(x+xtemp, y+ytemp, 0.0f);

	//	x += diff;
	//	y += ydiff;

	//	if (section == 4) {
		
	//		xdiff = 0.1f;
	//		ydiff = -0.1f;
	//		xtemp = 0.01f;
	//		ytemp = -0.01f;
	//	}
	//	if (section == 9) {
			
	
	//		xdiff = 0.1f;
	//		ydiff = 0.1f;
	//		xtemp = 0.01f;
	//		ytemp = 0.01f;
	//	}
	//	if (section == 14) {
			
	
	//		xdiff = -0.1f;
	//		ydiff = 0.1f;
	//		xtemp = -0.01f;
	//		ytemp = 0.01f;
	//	}
	
	//	section++;
	// } while (section < 20);



	 // Circular circle
	 float x = 0.0f, y = 0.5f, diff = -0.1f, xtemp = 0.01f, ytemp = 0.01f;
	 int section = 0;

	 do {

		 glColor3f(1.0f, 0.0f, 0.0f);
		 glVertex3f(0.0f, 0.0f, 0.0f);

		 glColor3f(0.0f, 1.0f, 0.0f);
		 glVertex3f(x, y, 0.0f);

		 glColor3f(0.0f, 0.0f, 1.0f);
		 glVertex3f(x + xtemp, y + ytemp, 0.0f);

		 x += diff;
		 if (section >= 4 && section <= 14)
			 y = 0 - (float)squareRoot((0.5f * 0.5f) - (x * x));
		 else
			 y = (float)squareRoot((0.5f * 0.5f) - (x * x));

		 if (section == 4) {
			 diff = 0.1f;
			 xtemp = 0.01f;
			 ytemp = -0.01f;
		 }
		 if (section == 9) {

			 diff = 0.1f;
			 xtemp = 0.01f;
			 ytemp = 0.01f;
		 }
		 if (section == 14) {

			 diff = -0.1f;
			 xtemp = -0.01f;
			 ytemp = 0.01f;
		 }
		 section++;
	 } while (section < 20);

	glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key) 
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) 
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}
