// header files
#include <GL/freeglut.h>
#include<math.h>

// global variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Happy Birthday Kalyani RTR5 2023 Frustum Group");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}


void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}


void display(void)
{
	// Baloon code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, 0.0f);
	glScalef(0.7f, 0.9f, 0.0f);

	glBegin(GL_TRIANGLE_FAN);
	float r = 0.5f;		// radius length
	float cy = 0.0f;	// y co-ordinate of origin
	float cx = 0.0f;	// x co-prdinate of origin
	int num_segments = 100;
	for (int i = 0; i < num_segments; i++)
	{
		float theta = float(i) * ((2.0f * 3.1415926f) / float(num_segments));//get the current angle

		float x = r * cosf(theta);	//calculate the x component
		float y = r * sinf(theta);	//calculate the y component

		glVertex2f(x + cx, y + cy);	//output vertex
	}

//	Circle Another Code
// 
//	for (float angle = (2.0f * 3.1415926f) / (float)num_segments; angle < (2.0f * 3.1415926f); angle += ((2.0f * 3.1415926f) / num_segments))
//		glVertex2f(cx + (r * sinf(angle)), cy + (r * cosf(angle)));

	glEnd();

/*	
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_LINES);

	glVertex2f(0.0f, 0.5f);
	glVertex2f(-0.5f, 0.0f);
	glVertex2f(-0.5f, 0.0f);
	glVertex2f(0.0f, -0.5f);
	
	glEnd();
*/
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key) 
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y) 
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}
