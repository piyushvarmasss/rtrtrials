// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

// Global Funtions
void BezierCurveThreePoint(Point p1, Point p2, Point p3, int points)
{
	float diff = 1.0f / (float)points;

	for (float u = 0.0f; u <= 1.0f; u += diff)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(x, y, z);
	}
}

void BezierCurveFourPoint(Point p1, Point p2, Point p3, Point p4, int points)
{
	float diff = 1.0f / (float)points;

	for (float u = 0.0f; u <= 1.0f; u += diff)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.x * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.x * 3.0f * u * u * (1.0f - u)) + (p4.x * u * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.y * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.y * 3.0f * u * u * (1.0f - u)) + (p4.y * u * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.z * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.z * 3.0f * u * u * (1.0f - u)) + (p4.z * u * u * u);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(x, y, z);
	}
}

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Variable Declarations
	Point p1 = {0.0f, -0.5f, 0.0f};
	Point p2 = {0.25f, 0.5f, 0.0f};
	Point p3 = {0.0f, 0.75f, 0.0f};

	Point p4 = { 0.0f, 0.0f, 0.0f };
	Point p5 = { 0.125f, 0.5f, 0.0f };
	Point p6 = { 0.375f, 0.0f, 0.0f };
	Point p7 = { 0.5f, 0.5f, 0.0f };

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

		glBegin(GL_LINE_STRIP);

		BezierCurveThreePoint(p1, p2, p3, 100);

		glEnd();


		glBegin(GL_LINE_STRIP);

		BezierCurveFourPoint(p4, p5, p6, p7, 100);

		glEnd();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}
