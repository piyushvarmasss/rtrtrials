
// Header files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.14159265359

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;

typedef struct Bezier3 {
	Point p0;
	Point p1;
	Point p2;
} Bezier3;

typedef struct Bezier4 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
} Bezier4;

typedef struct Bezier5 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
	Point p4;
} Bezier5;

// Global Variables Declarations
bool bIsFullScreen = false;

// Global Functions Declarations
void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void HexColorToFloatColor(char color[], Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {
	 
	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void ellipse2D(Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (xRadius * sinf((angle * PI) / 180.0f)), center->y + (yRadius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void bezierShape(Point curve1[], Point curve2[], Color *c) {
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (int index = 0; index < 301; index++)
		{
			glVertex3f(curve1[index].x, curve1[index].y, curve1[index].z);
			glVertex3f(curve2[index].x, curve2[index].y, curve2[index].z);
		}
	glEnd();
}

void bezier33(Bezier3 *curve1, Bezier3 *curve2, Color *c) {
	float x, y, z;
	
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
			x = (curve1->p0.x * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 2.0f * (1.0f - u) * u) + (curve1->p2.x * u * u);
			y = (curve1->p0.y * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.y * u * u);
			z = (curve1->p0.z * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.z * u * u);
			glVertex3f(x, y, z);

			x = (curve2->p0.x * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 2.0f * (1.0f - u) * u) + (curve2->p2.x * u * u);
			y = (curve2->p0.y * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 2.0f * (1.0f - u) * u) + (curve2->p2.y * u * u);
			z = (curve2->p0.z * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 2.0f * (1.0f - u) * u) + (curve2->p2.z * u * u);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier44(Bezier4 *curve1, Bezier4 *curve2, Color *c) {
	float x, y, z;
	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
		x = (curve1->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.x * 3.0f * (1.0f - u) * u * u) + (curve1->p3.x * u * u * u);
		y = (curve1->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.y * 3.0f * (1.0f - u) * u * u) + (curve1->p3.y * u * u * u);
		z = (curve1->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.z * 3.0f * (1.0f - u) * u * u) + (curve1->p3.z * u * u * u);
		glVertex3f(x, y, z);

		x = (curve2->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.x * 3.0f * (1.0f - u) * u * u) + (curve2->p3.x * u * u * u);
		y = (curve2->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.y * 3.0f * (1.0f - u) * u * u) + (curve2->p3.y * u * u * u);
		z = (curve2->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.z * 3.0f * (1.0f - u) * u * u) + (curve2->p3.z * u * u * u);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier55(Bezier5 *curve1, Bezier5 *curve2, Color *c) {
	float x, y, z;
	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
		x = (curve1->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve1->p3.x * 4.0f * (1.0f - u) * u * u * u) + (curve1->p4.x * u * u * u * u);
		y = (curve1->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve1->p3.y * 4.0f * (1.0f - u) * u * u * u) + (curve1->p4.y * u * u * u * u);
		z = (curve1->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve1->p3.z * 4.0f * (1.0f - u) * u * u * u) + (curve1->p4.z * u * u * u * u);
		glVertex3f(x, y, z);

		x = (curve2->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.x * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.x * u * u * u * u);
		y = (curve2->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.y * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.y * u * u * u * u);
		z = (curve2->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.z * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.z * u * u * u * u);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier34(Bezier3 *curve1, Bezier4 *curve2, Color *c) {
	float x, y, z;
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
			x = (curve1->p0.x * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 2.0f * (1.0f - u) * u) + (curve1->p2.x * u * u);
			y = (curve1->p0.y * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.y * u * u);
			z = (curve1->p0.z * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.z * u * u);
			glVertex3f(x, y, z);

			x = (curve2->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.x * 3.0f * (1.0f - u) * u * u) + (curve2->p3.x * u * u * u);
			y = (curve2->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.y * 3.0f * (1.0f - u) * u * u) + (curve2->p3.y * u * u * u);
			z = (curve2->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.z * 3.0f * (1.0f - u) * u * u) + (curve2->p3.z * u * u * u);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier35(Bezier3 *curve1, Bezier5 *curve2, Color *c) {
	float x, y, z;
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
			x = (curve1->p0.x * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 2.0f * (1.0f - u) * u) + (curve1->p2.x * u * u);
			y = (curve1->p0.y * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.y * u * u);
			z = (curve1->p0.z * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 2.0f * (1.0f - u) * u) + (curve1->p2.z * u * u);
			glVertex3f(x, y, z);

			x = (curve2->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.x * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.x * u * u * u * u);
			y = (curve2->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.y * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.y * u * u * u * u);
			z = (curve2->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.z * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.z * u * u * u * u);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier45(Bezier4 *curve1, Bezier5 *curve2, Color *c) {
	float x, y, z;
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f) {
			x = (curve1->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.x * 3.0f * (1.0f - u) * u * u) + (curve1->p3.x * u * u * u);
			y = (curve1->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.y * 3.0f * (1.0f - u) * u * u) + (curve1->p3.y * u * u * u);
			z = (curve1->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve1->p1.z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (curve1->p2.z * 3.0f * (1.0f - u) * u * u) + (curve1->p3.z * u * u * u);
			glVertex3f(x, y, z);

			x = (curve2->p0.x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.x * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.x * u * u * u * u);
			y = (curve2->p0.y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.y * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.y * u * u * u * u);
			z = (curve2->p0.z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (curve2->p1.z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (curve2->p2.z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (curve2->p3.z * 4.0f * (1.0f - u) * u * u * u) + (curve2->p4.z * u * u * u * u);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void BSplineCurve(int noOfPoints, Point arr[]) {

	if (noOfPoints < 3)
		return;

	if (noOfPoints % 3 == 0)
		for (int i = 0; i <= noOfPoints; i += 3)
			bezierCurve3Points(&arr[i], &arr[i+1], &arr[i+2]);
	else if(noOfPoints % 4 == 0)
		for (int i = 0; i <= noOfPoints; i += 4)
			bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i+3]);
	else if (noOfPoints % 5 == 0)
		for (int i = 0; i <= noOfPoints; i += 5)
			bezierCurve5Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3], &arr[i+4]);
	else if (noOfPoints % 3 == 1) {
		int i = 0;
		for(; i <= noOfPoints-4; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
	else if (noOfPoints % 3 == 2) {
		int i = 0;
		for (; i <= noOfPoints - 5; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
}


// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Birthday Demo : Room Interior");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// function declarations
	void Rupali_Husband_Canvas(void);
	void Tree(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Rupali_Husband_Canvas();
	//Tree();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}


void Rupali_Husband_Canvas() {

	// Function Declarations
	void Canvas(void);
	void Rupali(void);
	void Husband(void);
	void Shape(void);

	// Code
	/*
	 glPushMatrix();
		glScalef(1.2f, 1.5f, 0.0f);
		glTranslatef(-0.5f, 0.25f, 0.0f);
		Canvas();
	 glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.07f, 0.35f, 0.0f);
		glScalef(0.9f, 1.05f, 0.0f);
		Rupali();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0.0f, 0.45f, 0.0f);
		glScalef(0.8f, 0.8f, 0.0f);
		Husband();
	glPopMatrix();
	*/
	Shape();
}

void Canvas() {

	Color c;

	// Back Foot
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.68f, 0.2f, 0.0f);
		glVertex3f(0.78f, 0.2f, 0.0f);
		glVertex3f(0.94f, -0.63f, 0.0f);
		glVertex3f(0.89f, -0.62f, 0.0f);
	glEnd();

	// Left Foot
	HexColorToFloatColor("765827", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.66f, -0.72f, 0.0f);
		glVertex3f(0.61f, -0.71f, 0.0f);
	glEnd();

	// Right Foot
	glBegin(GL_QUADS);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.88f, -0.77f, 0.0f);
		glVertex3f(0.83f, -0.76f, 0.0f);
	glEnd();

	// Main Frame Of Canvas
	glBegin(GL_QUADS);
		glColor3f(0.9f, 0.9f, 0.9f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// side-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// top-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.61f, 0.26f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
	glEnd();

	// Base of Main Frame Of Canvas
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.6f, -0.35f, 0.0f);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// Side Base of Main Frame Of Canvas
	HexColorToFloatColor("3F2305", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.91f, -0.39f, 0.0f);
	glEnd();

}

void Rupali() {

	// Variable Declarations
	Color c;
	Point p[5];


	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.15f, 0.0f, 0.0f);
		glVertex3f(-0.13f, -0.25f, 0.0f);
		glVertex3f(-0.1f, -0.33f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();

	// Paint Brush Head
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.215f, 0.03f, 0.0f);
		glVertex3f(0.23f, 0.08f, 0.0f);
		glVertex3f(0.24f, 0.07f, 0.0f);
		glVertex3f(0.225f, 0.0f, 0.0f);
	glEnd();
	HexColorToFloatColor("DFA878", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], 0.23f, 0.08f, 0.0f);
		assignPoint(&p[1], 0.222f, 0.098f, 0.0f);
		assignPoint(&p[2], 0.24f, 0.1f, 0.0f);
		assignPoint(&p[3], 0.245f, 0.12f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

		assignPoint(&p[0], 0.245f, 0.12f, 0.0f);
		assignPoint(&p[1], 0.255f, 0.08f, 0.0f);
		assignPoint(&p[2], 0.24f, 0.07f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.15f, 0.0f, 0.0f);
		glVertex3f(0.13f, -0.23f, 0.0f);
		glVertex3f(0.1f, -0.25f, 0.0f);
		glVertex3f(0.1f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.1f, -0.22f, 0.0f);
		assignPoint(&p[1], 0.12f, -0.12f, 0.0f);
		assignPoint(&p[2], 0.2f, -0.02f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	
		assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
		assignPoint(&p[1], 0.17f, -0.23f, 0.0f);
		assignPoint(&p[2], 0.1f, -0.25f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);		// Hand
		assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
		assignPoint(&p[1], 0.26f, 0.01f, 0.0f);
		assignPoint(&p[2], 0.22f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.225f, 0.03f, 0.0f);
		assignPoint(&p[1], 0.19f, 0.02f, 0.0f);
		assignPoint(&p[2], 0.19f, -0.03f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Paint Brush Stack
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], 0.215f, 0.035f, 0.0f);
		assignPoint(&p[1], 0.17f, -0.2f, 0.0f);
		assignPoint(&p[2], 0.225f, 0.005f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shoulder
	HexColorToFloatColor("EA1189", &c);
	assignPoint(&p[0], 0.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(p[0].x, p[0].y, p[0].z);
		circle2D(&p[0], 0.16f, 260.0f, 460.0f);
		glVertex3f(p[0].x, p[0].y, p[0].z);
	glEnd();

	// Shirt
	glBegin(GL_QUADS);
		glVertex3f(0.1f, 0.05f, 0.0f);
		glVertex3f(-0.1f, 0.05f, 0.0f);
		glVertex3f(-0.1f, -0.3f, 0.0f);
		glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();

	// Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);

		assignPoint(&p[0], -0.13f, -0.14f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.09f, 0.0f);
		assignPoint(&p[2], -0.2f, 0.43f, 0.0f);
		assignPoint(&p[3], 0.0f, 0.45f, 0.0f);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.0f, -0.15f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	
		assignPoint(&p[0], 0.0f, 0.45f, 0.0f);
		assignPoint(&p[1], 0.2f, 0.43f, 0.0f);
		assignPoint(&p[2], 0.0f, -0.09f, 0.0f);
		assignPoint(&p[3], 0.13f, -0.14f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	glEnd();

	// Hairs Tail Curve
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.13f, -0.14f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.23f, 0.0f);
		assignPoint(&p[2], -0.13f, -0.14f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.07f, -1.1f, 0.0f);

		assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.01f, -1.16f, 0.0f);
		assignPoint(&p[2], 0.02f, -1.17f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.08f, -1.17f, 0.0f);

		assignPoint(&p[0], 0.08f, -1.17f, 0.0f);
		assignPoint(&p[1], 0.09f, -1.16f, 0.0f);
		assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.09f, -0.75f, 0.0f);

		assignPoint(&p[0], 0.01f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
		assignPoint(&p[2], 0.03f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.05f, -1.12f, 0.0f);
		assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.07f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.1f, -0.88f, 0.0f);
		assignPoint(&p[2], 0.09f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.07f, -1.1f, 0.0f);

		assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.01f, -1.16f, 0.0f);
		assignPoint(&p[2], -0.02f, -1.17f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(-0.08f, -1.17f, 0.0f);

		assignPoint(&p[0], -0.08f, -1.17f, 0.0f);
		assignPoint(&p[1], -0.09f, -1.16f, 0.0f);
		assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.09f, -0.75f, 0.0f);

		assignPoint(&p[0], -0.01f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
		assignPoint(&p[2], -0.03f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.05f, -1.12f, 0.0f);
		assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.07f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.1f, -0.88f, 0.0f);
		assignPoint(&p[2], -0.09f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock
	HexColorToFloatColor("EA1189", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.1f, -0.3f, 0.0f);

		assignPoint(&p[0], -0.1f, -0.3f, 0.0f);
		assignPoint(&p[1], -0.15f, -0.5f, 0.0f);
		assignPoint(&p[2], -0.1f, -0.75f, 0.0f); 
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.1f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.77f, 0.0f);
		assignPoint(&p[2], 0.1f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.1f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.15f, -0.5f, 0.0f);
		assignPoint(&p[2], 0.1f, -0.3f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock Ribbon
	HexColorToFloatColor("C51605", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.1f, -0.28f, 0.0f);
		glVertex3f(-0.1f, -0.28f, 0.0f);
		glVertex3f(-0.1f, -0.3f, 0.0f);
		glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();
}

void Husband() {

	// Variable Declarations
	Color c;
	Point p[5];

	// Left Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.56f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.58f, -0.75f, 0.0f);
		assignPoint(&p[2], -0.55f, -1.2f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.45f, -1.2f, 0.0f);
		glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(-0.45f, -1.2f, 0.0f);
		glVertex3f(-0.465f, -1.6f, 0.0f);
		glVertex3f(-0.535f, -1.6f, 0.0f);
		glVertex3f(-0.55f, -1.2f, 0.0f);
	glEnd();

	// Left Shoe
	glBegin(GL_POLYGON);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&p[0], -0.53f, -1.6f, 0.0f);
		assignPoint(&p[1], -0.56f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.53f, -1.67f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.47f, -1.67f, 0.0f);
		assignPoint(&p[1], -0.44f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.47f, -1.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.285f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.27f, -0.7f, 0.0f);
		assignPoint(&p[2], -0.29f, -1.2f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.39f, -1.2f, 0.0f);
		glVertex3f(-0.45f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(-0.29f, -1.2f, 0.0f);
		glVertex3f(-0.305f, -1.6f, 0.0f);
		glVertex3f(-0.375f, -1.6f, 0.0f);
		glVertex3f(-0.39f, -1.2f, 0.0f);
	glEnd();

	// Right Shoe
	glBegin(GL_POLYGON);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&p[0], -0.37f, -1.6f, 0.0f);
		assignPoint(&p[1], -0.4f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.37f, -1.67f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.31f, -1.67f, 0.0f);
		assignPoint(&p[1], -0.28f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.31f, -1.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shirt
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.425f, 0.14f, 0.0f);
		glVertex3f(-0.57f, 0.1f, 0.0f);
		assignPoint(&p[0], -0.57f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.425f, -0.64f, 0.0f);
		assignPoint(&p[2], -0.28f, -0.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.62f, -0.15f, 0.0f);
		glVertex3f(-0.62f, -0.4f, 0.0f);
		glVertex3f(-0.57f, -0.58f, 0.0f);
		glVertex3f(-0.57f, -0.15f, 0.0f);
	glEnd();

	// Left Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.57f, 0.1f, 0.0f);
		assignPoint(&p[1], -0.63f, 0.0f, 0.0f);
		assignPoint(&p[2], -0.64f, -0.15f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.57f, -0.18f, 0.0f);
	glEnd();

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.22f, -0.14f, 0.0f);
		glVertex3f(-0.14f, -0.24f, 0.0f);
		glVertex3f(-0.14f, -0.09f, 0.0f);
		glVertex3f(-0.22f, 0.02f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.14f, -0.23f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.3f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.05f, 0.04f, 0.0f);
		glVertex3f(0.0f, 0.04f, 0.0f);

		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.15f, 0.0f);
		assignPoint(&p[2], -0.14f, -0.08f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], 0.04f, 0.1f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.28f, -0.09f, 0.0f);
		glVertex3f(-0.22f, -0.15f, 0.0f);
		glVertex3f(-0.17f, -0.03f, 0.0f);
		glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	// Neck
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.38f, 0.25f, 0.0f);
		glVertex3f(-0.47f, 0.25f, 0.0f);

		assignPoint(&p[0], -0.47f, 0.12f, 0.0f);
		assignPoint(&p[1], -0.425f, 0.05f, 0.0f);
		assignPoint(&p[2], -0.38f, 0.12f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Big Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.52f, 0.1f, 0.0f);
		assignPoint(&p[2], -0.425f, 0.2f, 0.0f);
		assignPoint(&p[3], -0.33f, 0.1f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.5f, 0.0f);
		glColor3f(c.r, c.g, c.b);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Top Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.5f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.5f, 0.6f, 0.0f);
		assignPoint(&p[2], -0.3f, 0.6f, 0.0f);
		assignPoint(&p[3], -0.3f, 0.45f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.45f, 0.0f);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Side Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.505f, 0.55f, 0.0f);
		assignPoint(&p[2], -0.49f, 0.5f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
}


// Problem Arise From Here
void Shape() {


	Bezier3 curve1;
	Bezier5 curve2;
	Color c = {1.0f, 1.0f, 1.0f};

	assignPoint(&(curve1.p0), -0.5f, 0.0f, 0.0f);
	assignPoint(&(curve1.p1), 0.0f, 0.5f, 0.0f);
	assignPoint(&(curve1.p2), 0.5f, 0.0f, 0.0f);

	assignPoint(&curve2.p0, -0.5f, 0.0f, 0.0f);
	assignPoint(&curve2.p1, -0.25f, -0.5f, 0.0f);
	assignPoint(&curve2.p2, 0.0f, 1.5f, 0.0f);
	assignPoint(&curve2.p3, 0.25f, -0.5f, 0.0f);
	assignPoint(&curve2.p4, 0.5f, 0.0f, 0.0f);

	bezier35(&curve1, &curve2, &c);

}

