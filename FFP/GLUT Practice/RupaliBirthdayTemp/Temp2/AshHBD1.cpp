// header files
#include <math.h>
#include <GL/freeglut.h>

// Macros
#define PNV_PI 3.14159265359
#define PI 3.14159265359	// for Gramophone

// Structure Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

//typedef struct PointArr
//{
//	 Point p_arr;	
//} PointArr;

// global variable declarations
bool bIsFullScreen = false;

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Arif Khalil Sayyad");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Function declarations
	
	void Arif();
	//void Grass();
	//void CollectPoints(PointArr*, Point*, Point*, Point*);


	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Arif();
	//Grass();

	
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch(key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch(button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}


void Arif()
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void CollectPoints(Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);

	Point center = { 0.0f, 0.0f, 0.0f };
	Point p1 = {};
	Point p2 = {};
	Point p3 = {};

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];

	

	// tree two (right tilted)

	assignPoint(&p1, -0.8f, -0.7f, 0.0f);
	assignPoint(&p2, -0.75f, -0.1f, 0.0f);
	assignPoint(&p3, -0.35f, 0.26f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.64f, -0.7f, 0.0f);
	assignPoint(&p2, -0.66f, -0.1f, 0.0f);
	assignPoint(&p3, -0.30f, 0.25f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.7f, 0.7f, 0.7f);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// Tree  one (left tilted)

	assignPoint(&p1, -0.7f, -0.7f, 0.0f);
	assignPoint(&p2, -0.5f, 0.0f, 0.0f);
	assignPoint(&p3, -0.7f, 0.3f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.5f, -0.7f, 0.0f);
	assignPoint(&p2, -0.4f, 0.0f, 0.0f);
	assignPoint(&p3, -0.65f, 0.3f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.7f, 0.7f, 0.5);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// Tree one leaves

	// Leaf 1
	assignPoint(&p1, -0.36f, 0.26f, 0.0f);
	assignPoint(&p2, -0.48f, 0.50f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.38f, 0.24f, 0.0f);
	assignPoint(&p2, -0.50f, 0.355f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.37f, 0.25f, 0.0f);
	assignPoint(&p2, -0.50f, 0.44f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// Leaf 2

	assignPoint(&p1, -0.32f, 0.26f, 0.0f);
	assignPoint(&p2, -0.4f, 0.46f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.29f, 0.26f, 0.0f);
	assignPoint(&p2, -0.4f, 0.60f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.315f, 0.264f, 0.0f);
	assignPoint(&p2, -0.402f, 0.55f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// Leaf 3

	assignPoint(&p1, -0.28f, 0.26f, 0.0f);
	assignPoint(&p2, -0.35f, 0.55f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.27f, 0.26f, 0.0f);
	assignPoint(&p2, -0.20f, 0.43f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.275f, 0.26f, 0.0f);
	assignPoint(&p2, -0.27f, 0.44f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// 


	
	// Coconuts ( Tree one )
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.72f, 0.3f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.68f, 0.28f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.64f, 0.3f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.71f, 0.34f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.67f, 0.32f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.666f, 0.35f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	//coconuts tree 2
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.377f, 0.27f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.36f, 0.23f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.3f, 0.229f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.33f, 0.19f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.33f, 0.302f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.301f, 0.28f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	// Grass

	//Grass
	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-1.0f, -0.7f, 0.0f);
	glVertex3f(-0.98f, -0.55f, 0.0f);
	glVertex3f(-0.97f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.97f, -0.7f, 0.0f);
	glVertex3f(-0.967f, -0.44f, 0.0f);
	glVertex3f(-0.96f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.965f, -0.7f, 0.0f);
	glVertex3f(-0.95f, -0.35f, 0.0f);
	glVertex3f(-0.94f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.967f, -0.65f, 0.0f);
	glVertex3f(-0.940f, -0.35f, 0.0f);
	glVertex3f(-0.958f, -0.67f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.855f, -0.7f, 0.0f);
	glVertex3f(-0.860f, -0.36f, 0.0f);
	glVertex3f(-0.870f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.865f, -0.7f, 0.0f);
	glVertex3f(-0.880f, -0.377f, 0.0f);
	glVertex3f(-0.870f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.750f, -0.7f, 0.0f);
	glVertex3f(-0.895f, -0.32f, 0.0f);
	glVertex3f(-0.780f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.880f, -0.7f, 0.0f);
	glVertex3f(-0.930f, -0.39f, 0.0f);
	glVertex3f(-0.960f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.940f, -0.7f, 0.0f);
	glVertex3f(-0.920f, -0.35f, 0.0f);
	glVertex3f(-0.910f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.84f, -0.7f, 0.0f);
	glVertex3f(-0.8f, -0.3f, 0.0f);
	glVertex3f(-0.78f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.83f, -0.66f, 0.0f);
	glVertex3f(-0.85f, -0.3f, 0.0f);
	glVertex3f(-0.81f, -0.63f, 0.0f);
	glEnd();

	
/*
	//Star

	glBegin(GL_TRIANGLES);

	glColor3f(0.9f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glVertex3f(-0.5f, -0.2f, 0.0f);
	glVertex3f(0.5f, -0.2f, 0.0f);
	glEnd();


	glBegin(GL_TRIANGLES);
	glColor3f(0.9f, 1.0f, 0.0f);
	glVertex3f(0.0f, -0.5f, 0.0f);
	glVertex3f(-0.5f, 0.4f, 0.0f);
	glVertex3f(0.5f, 0.4f, 0.0f);
	glEnd();

	// mini start
	glBegin(GL_TRIANGLES);
	glColor3f(0.9f, 0.7f, 0.1f);
	glVertex3f(-0.2f, 0.225f, 0.0f);
	glVertex3f(0.2f, 0.225f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glEnd();


	glBegin(GL_TRIANGLES);
	glColor3f(0.9f, 0.7f, 0.1f);
	glVertex3f(-0.2f, -0.025f, 0.0f);
	glVertex3f(0.2f, -0.025f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);
	glEnd();


	// smile in star

	//lip
	glBegin(GL_LINE_STRIP);
	glColor3f(1.0f, 0.5f, 0.0f);
	assignPoint(&p1, -0.1f, 0.1f, 0.0f);
	assignPoint(&p2, 0.0f, 0.0f, 0.0f);
	assignPoint(&p3, 0.1f, 0.1f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	//lip
	glBegin(GL_LINE_STRIP);
	glColor3f(1.0f, 0.5f, 0.0f);
	assignPoint(&p1, -0.1f, 0.1f, 0.0f);
	assignPoint(&p2, 0.0f, 0.030f, 0.0f);
	assignPoint(&p3, 0.1f, 0.1f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	//eye

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.05f, 0.15f, 0.0f);
	circle2D(&center, 0.01f, 0.0f, 360.0f);
	glEnd();

	// eye
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, 0.05f, 0.15f, 0.0f);
	circle2D(&center, 0.01f, 0.0f, 360.0f);
	glEnd();
	*/	
}



void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PNV_PI) / 180.0f)), center->y + (radius * cosf((angle * PNV_PI) / 180.0f)), 0.0f);

}


void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void CollectPoints(Point* parr, Point* p1, Point* p2, Point* p3)
{
	//variable declarations
	int a = 0;
	//parr = NULL;
	
	//parr = (Point*)malloc(100 * sizeof(Point));
	for (float u = 0.0f; u <= 1.0f; u += 0.001f, a++) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);		
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		//Point pa = { x, y, z };
		parr[a].x = x;
		parr[a].y = y;
		parr[a].z = z;
	}
}



void AssignPointsCollection(Point* parr1, Point* parr2)
{
	for (int a = 0; a < 1001; a++)
	{
		glVertex3f(parr1[a].x, parr1[a].y, parr1[a].z);
		glVertex3f(parr2[a].x, parr2[a].y, parr2[a].z);
	}
}

