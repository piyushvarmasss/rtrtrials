// Header files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.14159265359

//Wave Animation variable y
float y = -0.25f;
float alpha = 0.0f;
int waveFlag = 0;

// Plane animation
 float xplane = 6.2f;

 // ballon Animation
 float x = -1.3f;

 // Boat Animation
 float fSecondScreenBoat = 4.3f;
 float fSeaScreenBoat = 4.3f;

 // Rupali Husband Hand Animation
 float timer = 0.0;

 // Global Variables Declarations
 bool bIsFullScreen = false;

 int runFlag = 0;


 float translateX = 0.0f, translateY = 0.0f;
 float scaleX = 1.0f, scaleY = 8.2f, scaleZ = 4.2f;

 float translateeX = 0.0f, translateeY = 0.0f;
 float scaleeX = 1.0f, scaleeY = 1.0f, scaleeZ = 1.0f;



// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

typedef struct Color {
	float r;
	float g; 
	float b;
} Color;

// Global Functions Declarations
void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void HexColorToFloatColor(char color[], Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void ellipse2D(Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (xRadius * sinf((angle * PI) / 180.0f)), center->y + (yRadius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void BSplineCurve(int noOfPoints, Point arr[]) {

	if (noOfPoints < 3)
		return;

	if (noOfPoints % 3 == 0)
		for (int i = 0; i <= noOfPoints; i += 3)
			bezierCurve3Points(&arr[i], &arr[i+1], &arr[i+2]);
	else if(noOfPoints % 4 == 0)
		for (int i = 0; i <= noOfPoints; i += 4)
			bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i+3]);
	else if (noOfPoints % 5 == 0)
		for (int i = 0; i <= noOfPoints; i += 5)
			bezierCurve5Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3], &arr[i+4]);
	else if (noOfPoints % 3 == 1) {
		int i = 0;
		for(; i <= noOfPoints-4; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
	else if (noOfPoints % 3 == 2) {
		int i = 0;
		for (; i <= noOfPoints - 5; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
}


void Collect5Points(Point* parr, Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	//variable declarations
	int a = 0;

	for (float u = 0.0f; u <= 1.0f; u += 0.001f, a++) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		parr[a].x = x;
		parr[a].y = y;
		parr[a].z = z;
	}
}

// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Birthday Demo : Room Interior");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

/*************************Animation
*
* 
* Update Function : Animation.
* 
* 
* 
* ********************************************/
void update(void)
{
	void Heart(void);
	void DrawText(float pos_x, float pos_y, float R, float G, float B, char* text_to_draw);
	/*
	//Wave Movement for both ashish and rupali screen
	if (waveFlag == 0)
	{
		y -= 0.0003f;
		if (y <= -0.30f)
			waveFlag = 1;
	}
	else
	{
		y += 0.0005f;
		if (y >= -0.25f)
			waveFlag = 0;
	}

	//Hot Air Baloon movement
	x += 0.002f;

	// Boat Animation for both ashish and rupali screen
	fSeaScreenBoat -= 0.01f;


	//Aeroplane Movement and happy Birthday Display  
	xplane -= 0.01f;
	if (xplane <= -1.1f)
	{
		glPushMatrix();
		glScalef(0.8f, 0.8f, 0.0f);
		glTranslatef(0.25f, 0.5f, 0.0f);
		Heart();
		glEnable(GL_LIGHTING);
		DrawText(-0.34f, 0.415f, 0.0f, 0.0f, 0.0f, "           HAPPY\n         BIRTHDAY\n           RUPALI");
		glDisable(GL_LIGHTING);
		glPopMatrix();
	}
	timer += 0.01f;
	*/


	//zooming to Canvas
	if (runFlag == 1)
	{
		if (scaleX < 6.4f) {
			scaleX += 0.0015f;
			scaleZ += 0.0015f;

		}

		if (translateX > -0.0f) {
			translateX -= 0.0015f;
		}
		if (translateY > -0.4f) {
			translateY -= 0.0015f;
		}
	}

}


/************** SeaScreen Functio for birthday seen ********************/
/****************** Second screen function for Rupali birthday seen **************/

void display(void)
{
	// function declarations
	void SecondScreen(void);
	void SeaScreen(void);
	
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
	//Function call
//	glPushMatrix();
//	SeaScreen();		//Ashish chi screen
//	glPopMatrix();


	glPushMatrix();
	SecondScreen();	//Rupali chi screen
	glPopMatrix();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}

/**********************************************
*
*	Ashish SCREEN
* 
*	Dongar, kianra, wave by Mangesh
*	Ball , Hot Air Baloon by Omkar
*	Clouds by Kalyani
*	Ashish Model Chair by Aditya
*	Tree, Star Fish by Arif
***********************************************/


/********* Ashish Birthday Screen ***************** */
void SeaScreen(void)
{
	//Function declarations
	void Dongar(void);
	void oliMati1(void);
	void oliMati2(void);
	void oliMati3(void);
	void Wave1(void);
	void Wave2(void);
	void Wave3(void);
	//void Rupali_Husband_Canvas(void);
	void Tree(void);
	void skyAndsky(void);
	void AshishScreen(void);	//Ashish model, chair purn code
	void Ball(void);
	void Ballon(void);
	void cloud1(void);
	void cloud2(void);
	void cloud3(void);
	void Boat(void);
	void Star(void);		//StarFish
	void update(void);

	// Variable Declarations
	Color c;
	
	// Kinara
	HexColorToFloatColor("C58940", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	glPushMatrix();
		skyAndsky();
	glPopMatrix();


	//Sun
	glPushMatrix();
		glTranslatef(-0.35f, 0.615f, 0.0f);
		glScalef(0.25f, 0.3f, 0.0f);
		// Draw sun (circle)
		glColor3f(1.0, 1.0, 0.0);  // Yellow color
		glBegin(GL_POLYGON);
		for (int i = 0; i < 360; i++) {
			float theta = i * PI / 180;
			glVertex2f(0.5 + 0.2 * cos(theta), 0.5 + 0.2 * sin(theta));
		}
		glEnd();

	glPopMatrix();


	/************ Clouds *****************/
	//1
	glPushMatrix();
		glTranslatef(0.1f, 0.75f, 0.0f);
		glScalef(0.280f, 0.40f, 0.0f);
		cloud1();
	glPopMatrix();
	//2
	glPushMatrix();
		glTranslatef(0.2f, 0.89f, 0.0f);
		glScalef(0.250f, 0.50f, 0.0f);
		cloud2();
	glPopMatrix();
	//3
	glPushMatrix();
		glTranslatef(-0.6f, 0.8f, 0.0f);
		glScalef(0.350f, 1.0f, 0.0f);
		cloud3();
	glPopMatrix();
	//4
	glPushMatrix();
		glTranslatef(-0.48f, 0.8f, 0.0f);
		glScalef(0.350f, 1.0f, 0.0f);
		cloud3();
	glPopMatrix();
	//5
	glPushMatrix();
		glTranslatef(0.73f, 0.75f, 0.0f);
		glScalef(0.450f, 0.450f, 0.0f);
		cloud2();
	glPopMatrix();
	//6
	glPushMatrix();
		glTranslatef(0.4f, 0.78f, 0.0f);
		glScalef(0.550f, 0.550f, 0.0f);
		cloud1();
	glPopMatrix();
	//7
	glPushMatrix();
		glTranslatef(0.9f, 0.89f, 0.0f);
		glScalef(0.350f, 0.350f, 0.0f);
		cloud3();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.89f, 0.68f, 0.0f);
		glScalef(0.550f, 0.50f, 0.0f);
		cloud1();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.89f, 0.88f, 0.0f);
		glScalef(0.350f, 0.350f, 0.0f);
		cloud2();
	glPopMatrix();



	/******* Dongar *********/
	Dongar();

	/******* Mati (kinarya jvlchi) *********/
	oliMati1();
	oliMati2();
	oliMati3();

	glPushMatrix();
		glTranslatef(0.0f, y, 0.0f);
		Wave1();
	

		glPushMatrix();
			glTranslatef(-0.54f, -0.73f, 0.0f);
			glScalef(0.06f, 0.06f, 0.0f);
			Star();
		glPopMatrix();

	
		Wave2();
	

		glPushMatrix();	//1
			glTranslatef(0.8f, -0.48f, 0.0f);
			glScalef(0.08f, 0.08f, 0.0f);
			Star();
			glPopMatrix();

	
		Wave3();
	glPopMatrix();

	//Wave movement
	
	if (waveFlag == 0)
	{
		y -= 0.0003f;
		if (y <= -0.30f)
			waveFlag = 1;
	}
	else
	{
		y += 0.0003f;
		if (y >= -0.25f)
			waveFlag = 0;
	}
	
	//update();

	/***********Hot air  Ballon ***********/
	glPushMatrix();
		glTranslatef(x, 0.7f, 0.0f);
		glScalef(0.5f, 0.5f, 0.0f);
		Ballon();
	glPopMatrix();
	x += 0.002f;
	



	/********** Boat ************/
	glPushMatrix();
		glScalef(0.15f, 0.15f, 0.0f);
		glTranslatef(fSeaScreenBoat, 0.4f, 0.0f);
		Boat();
	glPopMatrix();

	// Boat Animation
	fSeaScreenBoat -= 0.008f;
	//update();

	/************Tree **********/
	glPushMatrix();
		glTranslatef(-0.15f, -0.25f, 0.0f);
		Tree();
	glPopMatrix();

	/************Ashish Model Function Call****************/
	glPushMatrix();
		//glTranslatef(-0.130f, -0.11f, 0.0f);
		glTranslatef(-0.360f, -0.36f, 0.0f);
		AshishScreen();
	glPopMatrix();
	
	/***********Ball***************/
	glPushMatrix();
		glTranslatef(-0.3f, -0.8f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Ball();
	glPopMatrix();

	/********StarFish Function  Calls ****************/
	glPushMatrix();	//2
		glTranslatef(-0.48f, -0.87f, 0.0f);
		glScalef(0.095f, 0.095f, 0.0f);
		Star();
	glPopMatrix();
	

	glPushMatrix();
		glTranslatef(-0.98f, -1.03f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Star();
	glPopMatrix();


	glPushMatrix();
		glTranslatef(-0.88f, -1.005f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Star();
	glPopMatrix();


	glutPostRedisplay();
}




/******************************************
* 
*	Rupali Screen
*	Rupli and Husband Models by Piyush
*	Dongar, samudra,kinara by Mangesh
*	Chair by Aditya
*	Areoplane by Kalyani
*	Tree, grass, starfish by Arif
*	heart by Pratik
*	

* *******************************************/


/********* Rupali Screen *******************/
void SecondScreen(void)
{
	//Function declarations
	void Dongar(void);
	void oliMati1(void);
	void oliMati2(void);
	void oliMati3(void);
	void Wave1(void);
	void Wave2(void);
	void Wave3(void);
	void Rupali_Husband_Canvas(void);
	void Tree(void);
	void Heart();
	void DrawText(float pos_x, float pos_y, float R, float G, float B, char* text_to_draw);
	void Plane(void);
	void CanvasScreen(void);
	void Star(void);
	void Boat(void);

	// Variable Declarations
	Color c;

	/************** Kinara ****************/
	HexColorToFloatColor("C58940", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	/*************  Sea Water *************/
	//Dark shade
	glBegin(GL_QUADS); // Sea water
		glColor3f(0.0f, 0.8f, 0.9f);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
		glColor3f(0.91f, 0.40f, 0.20f);
		glVertex3f(-1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	//faint shade
	glBegin(GL_QUADS); // Sea water
		glColor3f(0.91f, 0.40f, 0.20f);
		glVertex3f(-1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
		glColor3f(0.9f, 0.6f, 0.1f);
		glVertex3f(1.0f, 0.350f, 0.0f);
		glVertex3f(-1.0f, 0.350f, 0.0f);

	glEnd();

	/**********Sky *********/
	glBegin(GL_QUADS); // SKY
		glColor3f(0.6901f, 0.08f, 0.0f);
		glVertex3f(-1.0f, 0.10f, 0.0f);	//A

		glColor3f(0.6901f, 0.14f, 0.0f);
		glVertex3f(1.0f, 0.10f, 0.0f); //B

		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C

		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D
		glEnd();
		glBegin(GL_QUADS);
		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C

		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(-1.0f, 1.0f, 0.0f);	//E

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(1.0f, 1.0f, 0.0f);	//F
	glEnd();

	/***************Sun******************/
	glPushMatrix();

		glTranslatef(0.02f, 0.215f, 0.0f);

		glBegin(GL_LINES);

			float radius = 0.07f;

			for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)
			{

				glColor3f(0.94f, 0.95f, 0.23f);
				glVertex3f(0.0f, 0.0f, 0.0f);

				float x = (radius - 0.01) * cos(angle);
				float y = (radius + 0.018f) * sin(angle);
				glColor3f(0.90f, 0.25f, 0.19f);

				glVertex3f(x, y, 0.0f);

			}
		glEnd();

	glPopMatrix();

	/************** Dongar **************/
	Dongar();

	/************ Mati (kinara javalchi mati ******************/
	oliMati1();
	oliMati2();
	oliMati3();

	/********** Star Fish Latanjavlchi posiyion ****************/
	glPushMatrix();	//1
		glTranslatef(0.48f, -0.70f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Star();
	glPopMatrix();


	/************* Waves ***************/
	glPushMatrix();
		glTranslatef(0.0f, y, 0.0f);
		Wave1();
		Wave2();
		Wave3();
	glPopMatrix();


	if (waveFlag == 0)
	{
		y -= 0.0005f;
		if (y <= -0.30f)
			waveFlag = 1;
	}
	else
	{
		y += 0.0005f;
		if (y >= -0.25f)
			waveFlag = 0;
	}

	//update();

	// Boat
	glPushMatrix();
		glScalef(0.15f, 0.15f, 0.0f);
		glTranslatef(fSecondScreenBoat, 0.0f, 0.0f);
		Boat();
	glPopMatrix();

	//Boat Movement
	//fSecondScreenBoat -= 0.002f;
	update();


	//Tree
	glPushMatrix();
		glTranslatef(-0.15f, -0.25f, 0.0f);
		Tree();
	glPopMatrix();

	/********** Rupali Function *************/
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.0f);
		Rupali_Husband_Canvas();
	glPopMatrix();

	/******************** Aeroplane******************/
	glPushMatrix();
		glScalef(0.25f, 0.25f, 0.0f);
		glTranslatef(xplane, 3.1f, 0.0f);
		Plane();
	glPopMatrix();
	xplane -= 0.005f;

	if (xplane <= -1.1f)
	{
		glPushMatrix();
			glScalef(0.8f, 0.8f, 0.0f);
			glTranslatef(0.25f, 0.5f, 0.0f);
			Heart();
			glEnable(GL_LIGHTING);
			DrawText(-0.38f, 0.4f, 0.0f, 0.0f, 0.0f, "           HAPPY\n         BIRTHDAY\n           RUPALI");
			glDisable(GL_LIGHTING);
		glPopMatrix();
	}

	

	timer += 0.01f;
	
	//update();


	glutPostRedisplay();
}



/************* Boat **************************/
void Boat(void)
{
	// Boat motha Base
	glBegin(GL_POLYGON);
		glColor3f(0.72f, 0.41f, 0.25f);
		glVertex3f(-0.85f, 0.0f, 0.0f);
		glVertex3f(-0.7f, -0.1f, 0.0f);
		glVertex3f(0.70f, -0.10f, 0.0f);
		glVertex3f(0.85f, 0.0f, 0.0f);
	glEnd();

	// Boat lahan Base
	glBegin(GL_POLYGON);
		glColor3f(0.46f, 0.32f, 0.23f);
		glVertex3f(-0.7f, -0.10f, 0.0f);
		glVertex3f(-0.5f, -0.3f, 0.0f);
		glVertex3f(0.5f, -0.3f, 0.0f);
		glVertex3f(0.70f, -0.10f, 0.0f);
	glEnd();

	//Flag
	glBegin(GL_POLYGON);
		glColor3f(0.72f, 0.72f, 0.72f);
		glVertex3f(0.0f, 0.00f, 0.0f);
		glVertex3f(0.03f, 0.0f, 0.0f);
		glVertex3f(0.03f, 0.85f, 0.0f);
		glVertex3f(0.0f, 0.85f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.03f, 0.85f, 0.0f);
		glVertex3f(0.20f, 0.77f, 0.0f);
		glVertex3f(0.03f, 0.77f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.03f, 0.77f, 0.0f);
		glVertex3f(0.20f, 0.85f, 0.0f);
		glVertex3f(0.03f, 0.85f, 0.0f);
	glEnd();

	// Boat right side
	glBegin(GL_POLYGON); 
		glColor3f(0.60f, 0.85f, 0.97f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.06f * (1.0f - u) * (1.0f - u)) + (0.20f * 2.0f * (1.0f - u) * u) + (0.06f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.70f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.06f * (1.0f - u) * (1.0f - u)) + (0.60f * 2.0f * (1.0f - u) * u) + (0.80f * u * u);
			float y = (0.70f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.80f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.06f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.15f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

	// Boat left side
	glBegin(GL_POLYGON); 
		glColor3f(0.85f, 0.93f, 0.98f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.02f * (1.0f - u) * (1.0f - u)) + (-0.02f * 2.0f * (1.0f - u) * u) + (-0.02f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.65f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.02f * (1.0f - u) * (1.0f - u)) + (-0.40f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			float y = (0.65f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.40f * 2.0f * (1.0f - u) * u) + (0.02f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();
}

/***************** Heart For Birthday Rupali Wish *************************/
void Heart()
{
	//left upper half
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.2f, 0.2f);
		for (float u = 0.0f; u <= 1.0f; u = u + 0.001f)
		{
			float x = ((-0.35f) * (1.0f - u) * (1.0f - u)) + ((-0.30f) * (2.0f) * (1.0f - u) * (u)) + ((-0.2f) * u * u);
			float y = ((0.50f) * (1.0f - u) * (1.0f - u)) + ((0.75f) * (2.0f) * (1.0f - u) * (u)) + ((0.50f) * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//right upper half
	glBegin(GL_POLYGON);

		for (float u = 0.0f; u <= 1.0f; u = u + 0.001f)
		{
			float x = ((-0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.10f) * (2.0f) * (1.0f - u) * (u)) + ((-0.05f) * u * u);
			float y = ((0.50f) * (1.0f - u) * (1.0f - u)) + ((0.75f) * (2.0f) * (1.0f - u) * (u)) + ((0.50f) * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//left lower half
	glBegin(GL_POLYGON);
		for (float u = 0.0f; u <= 1.0f; u = u + 0.001f)
		{
			float x = ((-0.35f) * (1.0f - u) * (1.0f - u)) + ((-0.36f) * (2.0f) * (1.0f - u) * (u)) + ((-0.20f) * u * u);
			float y = ((0.50f) * (1.0f - u) * (1.0f - u)) + ((0.30f) * (2.0f) * (1.0f - u) * (u)) + ((0.10f) * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//right lower half
	glBegin(GL_POLYGON);
		for (float u = 0.0f; u <= 1.0f; u = u + 0.001f)
		{
			float x = ((-0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.04f) * (2.0f) * (1.0f - u) * (u)) + ((-0.05f) * u * u);
			float y = ((0.10f) * (1.0f - u) * (1.0f - u)) + ((0.30f) * (2.0f) * (1.0f - u) * (u)) + ((0.50f) * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//downward traingle
	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.2f, 0.2f);
		glVertex3f(-0.35f, 0.50f, 0.0f);

		glColor3f(1.0f, 0.2f, 0.2f);
		glVertex3f(-0.05f, 0.50f, 0.0f);

		glColor3f(1.0f, 0.2f, 0.2f);
		glVertex3f(-0.20f, 0.10f, 0.0f);
	glEnd();
}


/************** Happy Birthday Rupali msg ********************/
void DrawText(float pos_x, float pos_y, float R, float G, float B, char* text_to_draw)	// The Design of the UNIX Operating System
{
	// variable decarations
	const unsigned char* text = reinterpret_cast<const unsigned char*>(text_to_draw);

	// code

	/*
		valid fonts :
		OpenGLUT fixed-width bitmap fonts:
			GLUT_BITMAP_9_BY_15
			GLUT_BITMAP_8_BY_13

		OpenGLUT variable width bitmap fonts:
			GLUT_BITMAP_HELVETICA_18
			LUT_BITMAP_HELVETICA_12
			GLUT_BITMAP_HELVETICA_10
			GLUT_BITMAP_TIMES_ROMAN_24
			GLUT_BITMAP_TIMES_ROMAN_10
	*/

	glRasterPos2f(pos_x, pos_y);
	glColor3f(R, G, B);
	glutBitmapString(GLUT_BITMAP_HELVETICA_18, text);
}

/************* Donger Code ************************/
void Dongar(void)
{

	// Dongar 2
	glBegin(GL_POLYGON); // Second Dongar
		glColor3f(0.40f, 0.71f, 0.44f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.50f * (1.0f - u) * (1.0f - u)) + (-0.25f * 2.0f * (1.0f - u) * u) + (-0.17f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.17f * (1.0f - u) * (1.0f - u)) + (-0.00f * 2.0f * (1.0f - u) * u) + (0.30f * u * u);
			float y = (0.40f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.30f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.50f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();



	//First 
	glBegin(GL_POLYGON); // first Dongar
		glColor3f(0.04f, 0.54f, 0.23f);

		// p1 = -1.0f, 0.10f, 0.0f
		// p2 = -0.80f, 0.77f, 0.0f
		// p3 = -0.67f, 1.450f, 0.0f

		// Dongar 1
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-1.0f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.67f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.20f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.67f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
			float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

	// Dongar 4
	glBegin(GL_POLYGON); // Fourth Dongar
		glColor3f(0.40f, 0.71f, 0.44f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.60f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.87f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.87f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (1.40f * u * u);
			float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.00f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.40f * (1.0f - u) * (1.0f - u)) + (1.40f * 2.0f * (1.0f - u) * u) + (0.60f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

	// Dongar 3
	glBegin(GL_POLYGON); // Third Dongar
		glColor3f(0.04f, 0.54f, 0.23f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.0f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.27f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
			glVertex3f(x, y, 0.0f);
		}
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.27f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (0.80f * u * u);
			float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.80f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.0f * u * u);
			float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

}



/********************* kinara latamule oli zaleli Mati Code **************************/
void oliMati1(void)
{
	//glColor3f(0.7f, 0.3f, 0.3f);

	glBegin(GL_POLYGON); // First Wave
		//glColor3f(0.0f, 0.0f, 0.9f);
		//glColor3f(0.7f, 0.3f, 0.3f);
		glColor3f(0.5725f, 0.3921f, 0.1058f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

}

void oliMati2(void)
{
	glBegin(GL_POLYGON); // Second Wave
		//glColor3f(0.0f, 0.0f, 0.9f);
		//glColor3f(0.7f, 0.3f, 0.3f);
		glColor3f(0.5725f, 0.3921f, 0.1058f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();
}

void oliMati3(void)
{
	glBegin(GL_POLYGON); // Second Wave
		//glColor3f(0.0f, 0.0f, 0.9f);
		//glColor3f(0.7f, 0.3f, 0.3f);
		glColor3f(0.5725f, 0.3921f, 0.1058f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();
}

/****************** Wave code ******************************************/

void Wave1(void)
{
	glBegin(GL_POLYGON); // First Wave
		glColor3f(1.0f, 1.0f, 1.0f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}


	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.8f, 0.9f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

}

void Wave2(void)
{
	glBegin(GL_POLYGON); // First Wave
		glColor3f(1.0f, 1.0f, 1.0f);


		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
		glColor3f(0.0f, 0.8f, 0.9f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

}

void Wave3(void)
{
	glBegin(GL_POLYGON); // First Wave
		//glColor3f(0.0f, 0.0f, 0.9f);
		glColor3f(1.0f, 1.0f, 1.0f);


		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
		glColor3f(0.0f, 0.8f, 0.9f);

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

		glColor3f(0.0f, 0.8f, 0.9f);
		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
			float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
			glVertex3f(x, y, 0.0f);
		}

	glEnd();

}

/***************** Canvas vr painting dakhvnysathi ***************************/
void CanvasScreen(void)
{
	
	//Function declarations
	void Dongar(void);
	
	void Wave1(void);
	void Wave2(void);
	void Wave3(void);
	
	void CanvasScreen(void);
	void AKJ_Chair(void);

	// Variable Declarations
	Color c;

	// Kinara
	HexColorToFloatColor("C58940", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Sea water
		glColor3f(0.0f, 0.8f, 0.9f);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
		glColor3f(0.91f, 0.40f, 0.20f);
		glVertex3f(-1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Sea water
		glColor3f(0.91f, 0.40f, 0.20f);
		glVertex3f(-1.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);
		glColor3f(0.9f, 0.6f, 0.1f);
		glVertex3f(1.0f, 0.350f, 0.0f);
		glVertex3f(-1.0f, 0.350f, 0.0f);

	glEnd();


	glBegin(GL_QUADS); // SKY
		glColor3f(0.6901f, 0.08f, 0.0f);
		glVertex3f(-1.0f, 0.10f, 0.0f);	//A

		glColor3f(0.6901f, 0.14f, 0.0f);
		glVertex3f(1.0f, 0.10f, 0.0f); //B

		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C

		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D
		glEnd();
		glBegin(GL_QUADS);
		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C

		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(-1.0f, 1.0f, 0.0f);	//E

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(1.0f, 1.0f, 0.0f);	//F
	glEnd();

	glScalef(0.75f, 0.75f, 0.0f);
	Dongar();
	
	//Sun
	glPushMatrix();
		glTranslatef(0.02f, 0.215f, 0.0f);

	
		glBegin(GL_LINES);

			float radius = 0.07f;

			for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)
			{

				glColor3f(0.94f, 0.95f, 0.23f);
				glVertex3f(0.0f, 0.0f, 0.0f);

				float x = (radius - 0.01) * cos(angle);
				float y = (radius + 0.018f) * sin(angle);
				glColor3f(0.90f, 0.25f, 0.19f);

				glVertex3f(x, y, 0.0f);

			}
		glEnd();

	glPopMatrix();

	glPushMatrix();

		glTranslatef(0.0f, -0.1f, 0.0f);
		glScalef(0.14f, 0.14f, 0.0f);
		Wave1();
		Wave2();
		Wave3();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.3f, -0.4f, 0.0f);
		glScalef(0.8f, 0.8f, 0.0f);
		AKJ_Chair();
	glPopMatrix();

	glutPostRedisplay();
}


/********************* Rupali Husband Canvas Models calls ****************************/
void Rupali_Husband_Canvas() {

	// Function Declarations
	void Canvas(void);
	void Rupali(void);
	void Husband(void);
	void AKJ_Chair(void);
	void Star(void);
	// Code
	
	/*glPushMatrix();
		glScalef(1.2f, 1.5f, 0.0f);
		glTranslatef(-0.5f, 0.25f, 0.0f);
		Canvas();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.07f, 0.35f, 0.0f);
		glScalef(0.9f, 1.05f, 0.0f);
		Rupali();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0.0f, 0.45f, 0.0f);
		glScalef(0.8f, 0.8f, 0.0f);
		Husband();
	glPopMatrix();
	*/



	/************ Canvas ****************/
	glPushMatrix();
		glScalef(0.6f, 0.72f, 0.0f);
		glTranslatef(-0.53f, -0.50f, 0.0f);
		
		Canvas();
	glPopMatrix();

	/************** Rupali Model ***************/
	glPushMatrix();
		glTranslatef(-0.029f, -0.350f, 0.0f);
		glScalef(0.5f, 0.55f, 0.0f);
		Rupali();
	glPopMatrix();

	/************** Husband Model ****************/
	glPushMatrix();
		glTranslatef(0.002f, -0.30f, 0.0f);
		glScalef(0.42f, 0.41f, 0.0f);
		Husband();
	glPopMatrix();

	/******************** Chair Code *************/
	glPushMatrix();
		glTranslatef(0.48f, -0.47f, 0.0f);
		glScalef(0.58f, 0.58f, 0.0f);
		AKJ_Chair();
	glPopMatrix();


	/***************** Star Fish ***********************/

	glPushMatrix();	//2
		glTranslatef(-0.48f, -0.87f, 0.0f);
		glScalef(0.095f, 0.095f, 0.0f);
		Star();
	glPopMatrix();


	glPushMatrix();	//3
		glTranslatef(0.38f, -0.87f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Star();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.74f, -0.97f, 0.0f);
		glScalef(0.06f, 0.06f, 0.0f);
		Star();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.88f, -1.03f, 0.0f);
		glScalef(0.08f, 0.08f, 0.0f);
		Star();
	glPopMatrix();

}

/******** Canvas ****************/
void Canvas() {

	//void CanvasScreen(void);

	Color c;

	// Back Foot
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.68f, 0.2f, 0.0f);
		glVertex3f(0.78f, 0.2f, 0.0f);
		glVertex3f(0.94f, -0.63f, 0.0f);
		glVertex3f(0.89f, -0.62f, 0.0f);
	glEnd();

	// Left Foot
	HexColorToFloatColor("765827", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.66f, -0.72f, 0.0f);
		glVertex3f(0.61f, -0.71f, 0.0f);
	glEnd();

	// Right Foot
	glBegin(GL_QUADS);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.88f, -0.77f, 0.0f);
		glVertex3f(0.83f, -0.76f, 0.0f);
	glEnd();

	// Main Frame Of Canvas
	glBegin(GL_QUADS);
		glColor3f(0.9f, 0.9f, 0.9f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// side-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// top-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.61f, 0.26f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
	glEnd();

	// Base of Main Frame Of Canvas
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
	
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.6f, -0.35f, 0.0f);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();
	


	// Side Base of Main Frame Of Canvas
	HexColorToFloatColor("3F2305", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.91f, -0.39f, 0.0f);
	glEnd();

	glPushMatrix();
	glTranslatef(0.75f, 0.04f, 0.0f);
	glScalef(0.14f, 0.16f, 0.0f);
	glRotatef(-5.0,0.70,-0.40, 0.7);
	CanvasScreen();
	glPopMatrix();

	
}

/************* Rupali Model ****************************/
void Rupali() {

	// Variable Declarations
	Color c;
	Point p[5];


	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.15f, 0.0f, 0.0f);
		glVertex3f(-0.13f, -0.25f, 0.0f);
		glVertex3f(-0.1f, -0.33f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();

	// Paint Brush Head
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.215f, 0.03f, 0.0f);
		glVertex3f(0.23f, 0.08f, 0.0f);
		glVertex3f(0.24f, 0.07f, 0.0f);
		glVertex3f(0.225f, 0.0f, 0.0f);
	glEnd();
	HexColorToFloatColor("DFA878", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], 0.23f, 0.08f, 0.0f);
	assignPoint(&p[1], 0.222f, 0.098f, 0.0f);
	assignPoint(&p[2], 0.24f, 0.1f, 0.0f);
	assignPoint(&p[3], 0.245f, 0.12f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	assignPoint(&p[0], 0.245f, 0.12f, 0.0f);
	assignPoint(&p[1], 0.255f, 0.08f, 0.0f);
	assignPoint(&p[2], 0.24f, 0.07f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(0.13f, -0.23f, 0.0f);
	glVertex3f(0.1f, -0.25f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
	assignPoint(&p[0], 0.1f, -0.22f, 0.0f);
	assignPoint(&p[1], 0.12f, -0.12f, 0.0f);
	assignPoint(&p[2], 0.2f, -0.02f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
	assignPoint(&p[1], 0.17f, -0.23f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.25f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);		// Hand
	assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
	assignPoint(&p[1], 0.26f, 0.01f, 0.0f);
	assignPoint(&p[2], 0.22f, 0.04f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.225f, 0.03f, 0.0f);
	assignPoint(&p[1], 0.19f, 0.02f, 0.0f);
	assignPoint(&p[2], 0.19f, -0.03f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Paint Brush Stack
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], 0.215f, 0.035f, 0.0f);
	assignPoint(&p[1], 0.17f, -0.2f, 0.0f);
	assignPoint(&p[2], 0.225f, 0.005f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shoulder
	HexColorToFloatColor("EA1189", &c);
	assignPoint(&p[0], 0.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(p[0].x, p[0].y, p[0].z);
	circle2D(&p[0], 0.16f, 260.0f, 460.0f);
	glVertex3f(p[0].x, p[0].y, p[0].z);
	glEnd();

	// Shirt
	glBegin(GL_QUADS);
	glVertex3f(0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();

	// Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);

	assignPoint(&p[0], -0.13f, -0.14f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.09f, 0.0f);
	assignPoint(&p[2], -0.2f, 0.43f, 0.0f);
	assignPoint(&p[3], 0.0f, 0.45f, 0.0f);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.0f, -0.15f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	assignPoint(&p[0], 0.0f, 0.45f, 0.0f);
	assignPoint(&p[1], 0.2f, 0.43f, 0.0f);
	assignPoint(&p[2], 0.0f, -0.09f, 0.0f);
	assignPoint(&p[3], 0.13f, -0.14f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	glEnd();

	// Hairs Tail Curve
	glBegin(GL_POLYGON);
	assignPoint(&p[0], 0.13f, -0.14f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.23f, 0.0f);
	assignPoint(&p[2], -0.13f, -0.14f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.07f, -1.1f, 0.0f);

	assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.01f, -1.16f, 0.0f);
	assignPoint(&p[2], 0.02f, -1.17f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	glVertex3f(0.08f, -1.17f, 0.0f);

	assignPoint(&p[0], 0.08f, -1.17f, 0.0f);
	assignPoint(&p[1], 0.09f, -1.16f, 0.0f);
	assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.09f, -0.75f, 0.0f);

	assignPoint(&p[0], 0.01f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
	assignPoint(&p[2], 0.03f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.05f, -1.12f, 0.0f);
	assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.07f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.1f, -0.88f, 0.0f);
	assignPoint(&p[2], 0.09f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.07f, -1.1f, 0.0f);

	assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.01f, -1.16f, 0.0f);
	assignPoint(&p[2], -0.02f, -1.17f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	glVertex3f(-0.08f, -1.17f, 0.0f);

	assignPoint(&p[0], -0.08f, -1.17f, 0.0f);
	assignPoint(&p[1], -0.09f, -1.16f, 0.0f);
	assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.09f, -0.75f, 0.0f);

	assignPoint(&p[0], -0.01f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
	assignPoint(&p[2], -0.03f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.05f, -1.12f, 0.0f);
	assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.07f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.1f, -0.88f, 0.0f);
	assignPoint(&p[2], -0.09f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock
	HexColorToFloatColor("EA1189", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.1f, -0.3f, 0.0f);

	assignPoint(&p[0], -0.1f, -0.3f, 0.0f);
	assignPoint(&p[1], -0.15f, -0.5f, 0.0f);
	assignPoint(&p[2], -0.1f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.1f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.77f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.1f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.15f, -0.5f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.3f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock Ribbon
	HexColorToFloatColor("C51605", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.1f, -0.28f, 0.0f);
	glVertex3f(-0.1f, -0.28f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();
}

/************** Husband Model ********************************/
void Husband() {

	// Variable Declarations
	Color c;
	Point p[5];

	// Left Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], -0.56f, -0.6f, 0.0f);
	assignPoint(&p[1], -0.58f, -0.75f, 0.0f);
	assignPoint(&p[2], -0.55f, -1.2f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glVertex3f(-0.45f, -1.2f, 0.0f);
	glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(-0.45f, -1.2f, 0.0f);
	glVertex3f(-0.465f, -1.6f, 0.0f);
	glVertex3f(-0.535f, -1.6f, 0.0f);
	glVertex3f(-0.55f, -1.2f, 0.0f);
	glEnd();

	// Left Shoe
	glBegin(GL_POLYGON);
	glColor3f(0.1f, 0.1f, 0.1f);
	assignPoint(&p[0], -0.53f, -1.6f, 0.0f);
	assignPoint(&p[1], -0.56f, -1.66f, 0.0f);
	assignPoint(&p[2], -0.53f, -1.67f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.47f, -1.67f, 0.0f);
	assignPoint(&p[1], -0.44f, -1.66f, 0.0f);
	assignPoint(&p[2], -0.47f, -1.6f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], -0.285f, -0.6f, 0.0f);
	assignPoint(&p[1], -0.27f, -0.7f, 0.0f);
	assignPoint(&p[2], -0.29f, -1.2f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glVertex3f(-0.39f, -1.2f, 0.0f);
	glVertex3f(-0.45f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(-0.29f, -1.2f, 0.0f);
	glVertex3f(-0.305f, -1.6f, 0.0f);
	glVertex3f(-0.375f, -1.6f, 0.0f);
	glVertex3f(-0.39f, -1.2f, 0.0f);
	glEnd();

	// Right Shoe
	glBegin(GL_POLYGON);
	glColor3f(0.1f, 0.1f, 0.1f);
	assignPoint(&p[0], -0.37f, -1.6f, 0.0f);
	assignPoint(&p[1], -0.4f, -1.66f, 0.0f);
	assignPoint(&p[2], -0.37f, -1.67f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.31f, -1.67f, 0.0f);
	assignPoint(&p[1], -0.28f, -1.66f, 0.0f);
	assignPoint(&p[2], -0.31f, -1.6f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shirt
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.425f, 0.14f, 0.0f);
	glVertex3f(-0.57f, 0.1f, 0.0f);
	assignPoint(&p[0], -0.57f, -0.6f, 0.0f);
	assignPoint(&p[1], -0.425f, -0.64f, 0.0f);
	assignPoint(&p[2], -0.28f, -0.6f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	if (timer < 4.5f)
	{
		// Left Hand
		HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.62f, -0.15f, 0.0f);
	glVertex3f(-0.62f, -0.4f, 0.0f);
	glVertex3f(-0.57f, -0.58f, 0.0f);
	glVertex3f(-0.57f, -0.15f, 0.0f);
	glEnd();

	// Left Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], -0.57f, 0.1f, 0.0f);
	assignPoint(&p[1], -0.63f, 0.0f, 0.0f);
	assignPoint(&p[2], -0.64f, -0.15f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glVertex3f(-0.57f, -0.18f, 0.0f);
	glEnd();

	}
	else if(timer >= 4.5f)
	{

		//New Left sleeve
		HexColorToFloatColor("6F61C0", &c);
		glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.520f, 0.12f, 0.0f);	//A
		//glVertex3f(-0.63f, 0.20f, 0.0f);	//B
		glVertex3f(-0.59f, 0.20f, 0.0f);	//B

		//	glVertex3f(-0.66f, 0.07f, 0.0f);	//C
		glVertex3f(-0.63f, 0.075f, 0.0f);	//C

		glVertex3f(-0.57f, -0.08f, 0.0f);	//D
		glEnd();

		//Vrch hat hone functions
		//vrch lower hat  //arm
		HexColorToFloatColor("EAC696", &c);
		glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.63f, 0.235f, 0.0f);
		glVertex3f(-0.595f, 0.185f, 0.0f);
		glVertex3f(-0.625f, 0.09f, 0.0f);
		glVertex3f(-0.68f, 0.25f, 0.0f);
		glEnd();

		//vrcha upper hat // forearm
		HexColorToFloatColor("EAC696", &c);
		glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.625f, 0.45f, 0.0f);
		glVertex3f(-0.595f, 0.44f, 0.0f);
		glVertex3f(-0.63f, 0.235f, 0.0f);
		glVertex3f(-0.68f, 0.25f, 0.0f);
		glEnd();

		//vrcha hatacha bot
		HexColorToFloatColor("EAC696", &c);
		glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);

		glVertex3f(-0.59f, 0.50f, 0.0f);
		glVertex3f(-0.607f, 0.443f, 0.0f);
		glVertex3f(-0.595f, 0.44f, 0.0f);

		glEnd();
	}

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.22f, -0.14f, 0.0f);
		glVertex3f(-0.14f, -0.24f, 0.0f);
		glVertex3f(-0.14f, -0.09f, 0.0f);
		glVertex3f(-0.22f, 0.02f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.14f, -0.23f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.3f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.05f, 0.04f, 0.0f);
		glVertex3f(0.0f, 0.04f, 0.0f);

		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.15f, 0.0f);
		assignPoint(&p[2], -0.14f, -0.08f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], 0.04f, 0.1f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.28f, -0.09f, 0.0f);
		glVertex3f(-0.22f, -0.15f, 0.0f);
		glVertex3f(-0.17f, -0.03f, 0.0f);
		glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	// Neck
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.38f, 0.25f, 0.0f);
		glVertex3f(-0.47f, 0.25f, 0.0f);

		assignPoint(&p[0], -0.47f, 0.12f, 0.0f);
		assignPoint(&p[1], -0.425f, 0.05f, 0.0f);
		assignPoint(&p[2], -0.38f, 0.12f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Big Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.52f, 0.1f, 0.0f);
		assignPoint(&p[2], -0.425f, 0.2f, 0.0f);
		assignPoint(&p[3], -0.33f, 0.1f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.5f, 0.0f);
		glColor3f(c.r, c.g, c.b);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Top Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.5f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.5f, 0.6f, 0.0f);
		assignPoint(&p[2], -0.3f, 0.6f, 0.0f);
		assignPoint(&p[3], -0.3f, 0.45f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.45f, 0.0f);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Side Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.505f, 0.55f, 0.0f);
		assignPoint(&p[2], -0.49f, 0.5f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
}


// Arif Code 
void Arif()
{

/*
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void CollectPoints(Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);

	Point center = { 0.0f, 0.0f, 0.0f };
	Point p1 = {};
	Point p2 = {};
	Point p3 = {};

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];



	// tree two (right tilted)

	assignPoint(&p1, -0.8f, -0.7f, 0.0f);
	assignPoint(&p2, -0.75f, -0.1f, 0.0f);
	assignPoint(&p3, -0.35f, 0.26f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.64f, -0.7f, 0.0f);
	assignPoint(&p2, -0.66f, -0.1f, 0.0f);
	assignPoint(&p3, -0.30f, 0.25f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.7f, 0.7f, 0.7f);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// Tree  one (left tilted)

	assignPoint(&p1, -0.7f, -0.7f, 0.0f);
	assignPoint(&p2, -0.5f, 0.0f, 0.0f);
	assignPoint(&p3, -0.7f, 0.3f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.5f, -0.7f, 0.0f);
	assignPoint(&p2, -0.4f, 0.0f, 0.0f);
	assignPoint(&p3, -0.65f, 0.3f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.7f, 0.7f, 0.5);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// Tree one leaves

	// Leaf 1
	assignPoint(&p1, -0.36f, 0.26f, 0.0f);
	assignPoint(&p2, -0.48f, 0.50f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.38f, 0.24f, 0.0f);
	assignPoint(&p2, -0.50f, 0.355f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.37f, 0.25f, 0.0f);
	assignPoint(&p2, -0.50f, 0.44f, 0.0f);
	assignPoint(&p3, -0.62f, 0.38f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// Leaf 2

	assignPoint(&p1, -0.32f, 0.26f, 0.0f);
	assignPoint(&p2, -0.4f, 0.46f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.29f, 0.26f, 0.0f);
	assignPoint(&p2, -0.4f, 0.60f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.315f, 0.264f, 0.0f);
	assignPoint(&p2, -0.402f, 0.55f, 0.0f);
	assignPoint(&p3, -0.55f, 0.55f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// Leaf 3

	assignPoint(&p1, -0.28f, 0.26f, 0.0f);
	assignPoint(&p2, -0.35f, 0.55f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	CollectPoints(parr_left_curve, &p1, &p2, &p3);

	assignPoint(&p1, -0.27f, 0.26f, 0.0f);
	assignPoint(&p2, -0.20f, 0.43f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	CollectPoints(parr_right_curve, &p1, &p2, &p3);

	glBegin(GL_LINES);
	glColor3f(0.6f, 1.0f, 0.1);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	assignPoint(&p1, -0.275f, 0.26f, 0.0f);
	assignPoint(&p2, -0.27f, 0.44f, 0.0f);
	assignPoint(&p3, -0.25f, 0.65f, 0.0f);
	bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// 



	// Coconuts ( Tree one )
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.72f, 0.3f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.68f, 0.28f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.64f, 0.3f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.71f, 0.34f, 0.0f);
	circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.67f, 0.32f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.666f, 0.35f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	//coconuts tree 2
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.377f, 0.27f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.36f, 0.23f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.3f, 0.229f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.33f, 0.19f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.33f, 0.302f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.301f, 0.28f, 0.0f);
	circle2D(&center, 0.021f, 0.0f, 360.0f);
	glEnd();

	// Grass

	//Grass
	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.8f, -0.7f, 0.0f);
	glVertex3f(-0.78f, -0.55f, 0.0f);
	glVertex3f(-0.77f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.77f, -0.7f, 0.0f);
	glVertex3f(-0.767f, -0.44f, 0.0f);
	glVertex3f(-0.76f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.765f, -0.7f, 0.0f);
	glVertex3f(-0.75f, -0.35f, 0.0f);
	glVertex3f(-0.74f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.767f, -0.65f, 0.0f);
	glVertex3f(-0.740f, -0.35f, 0.0f);
	glVertex3f(-0.758f, -0.67f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.655f, -0.7f, 0.0f);
	glVertex3f(-0.660f, -0.36f, 0.0f);
	glVertex3f(-0.670f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.665f, -0.7f, 0.0f);
	glVertex3f(-0.680f, -0.377f, 0.0f);
	glVertex3f(-0.670f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.550f, -0.7f, 0.0f);
	glVertex3f(-0.695f, -0.32f, 0.0f);
	glVertex3f(-0.580f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.680f, -0.7f, 0.0f);
	glVertex3f(-0.730f, -0.39f, 0.0f);
	glVertex3f(-0.760f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.740f, -0.7f, 0.0f);
	glVertex3f(-0.720f, -0.35f, 0.0f);
	glVertex3f(-0.710f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.64f, -0.7f, 0.0f);
	glVertex3f(-0.6f, -0.3f, 0.0f);
	glVertex3f(-0.58f, -0.7f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.63f, -0.66f, 0.0f);
	glVertex3f(-0.65f, -0.3f, 0.0f);
	glVertex3f(-0.61f, -0.63f, 0.0f);
	glEnd();

	//Grass
	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.7f, -0.75f, 0.0f);
	glVertex3f(-0.68f, -0.6f, 0.0f);
	glVertex3f(-0.67f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.67f, -0.75f, 0.0f);
	glVertex3f(-0.667f, -0.49f, 0.0f);
	glVertex3f(-0.66f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.665f, -0.75f, 0.0f);
	glVertex3f(-0.65f, -0.4f, 0.0f);
	glVertex3f(-0.64f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.667f, -0.7f, 0.0f);
	glVertex3f(-0.640f, -0.4f, 0.0f);
	glVertex3f(-0.658f, -0.72f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.555f, -0.75f, 0.0f);
	glVertex3f(-0.560f, -0.41f, 0.0f);
	glVertex3f(-0.570f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.565f, -0.75f, 0.0f);
	glVertex3f(-0.580f, -0.427f, 0.0f);
	glVertex3f(-0.570f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.450f, -0.75f, 0.0f);
	glVertex3f(-0.595f, -0.37f, 0.0f);
	glVertex3f(-0.480f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.580f, -0.75f, 0.0f);
	glVertex3f(-0.630f, -0.44f, 0.0f);
	glVertex3f(-0.660f, -0.35f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.640f, -0.75f, 0.0f);
	glVertex3f(-0.620f, -0.4f, 0.0f);
	glVertex3f(-0.610f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.54f, -0.75f, 0.0f);
	glVertex3f(-0.5f, -0.35f, 0.0f);
	glVertex3f(-0.48f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.53f, -0.71f, 0.0f);
	glVertex3f(-0.55f, -0.35f, 0.0f);
	glVertex3f(-0.51f, -0.68f, 0.0f);
	glEnd();

	//Grass
	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.85f, -0.75f, 0.0f);
	glVertex3f(-0.83f, -0.6f, 0.0f);
	glVertex3f(-0.82f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.82f, -0.75f, 0.0f);
	glVertex3f(-0.817f, -0.49f, 0.0f);
	glVertex3f(-0.81f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.815f, -0.75f, 0.0f);
	glVertex3f(-0.8f, -0.4f, 0.0f);
	glVertex3f(-0.79f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.817f, -0.7f, 0.0f);
	glVertex3f(-0.790f, -0.4f, 0.0f);
	glVertex3f(-0.808f, -0.72f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.705f, -0.75f, 0.0f);
	glVertex3f(-0.710f, -0.41f, 0.0f);
	glVertex3f(-0.720f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.715f, -0.75f, 0.0f);
	glVertex3f(-0.730f, -0.427f, 0.0f);
	glVertex3f(-0.720f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.60f, -0.75f, 0.0f);
	glVertex3f(-0.745f, -0.37f, 0.0f);
	glVertex3f(-0.630f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.530f, -0.75f, 0.0f);
	glVertex3f(-0.580f, -0.44f, 0.0f);
	glVertex3f(-0.610f, -0.35f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.590f, -0.75f, 0.0f);
	glVertex3f(-0.570f, -0.4f, 0.0f);
	glVertex3f(-0.560f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.49f, -0.75f, 0.0f);
	glVertex3f(-0.45f, -0.35f, 0.0f);
	glVertex3f(-0.43f, -0.75f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.4f, 0.5f, 0.0f);
	glVertex3f(-0.48f, -0.71f, 0.0f);
	glVertex3f(-0.5f, -0.35f, 0.0f);
	glVertex3f(-0.46f, -0.68f, 0.0f);
	glEnd();
	/*
		//Star

		glBegin(GL_TRIANGLES);

		glColor3f(0.9f, 1.0f, 0.0f);
		glVertex3f(0.0f, 0.7f, 0.0f);
		glVertex3f(-0.5f, -0.2f, 0.0f);
		glVertex3f(0.5f, -0.2f, 0.0f);
		glEnd();


		glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 1.0f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(-0.5f, 0.4f, 0.0f);
		glVertex3f(0.5f, 0.4f, 0.0f);
		glEnd();

		// mini start
		glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 0.7f, 0.1f);
		glVertex3f(-0.2f, 0.225f, 0.0f);
		glVertex3f(0.2f, 0.225f, 0.0f);
		glVertex3f(0.0f, -0.2f, 0.0f);
		glEnd();


		glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 0.7f, 0.1f);
		glVertex3f(-0.2f, -0.025f, 0.0f);
		glVertex3f(0.2f, -0.025f, 0.0f);
		glVertex3f(0.0f, 0.4f, 0.0f);
		glEnd();


		// smile in star

		//lip
		glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 0.5f, 0.0f);
		assignPoint(&p1, -0.1f, 0.1f, 0.0f);
		assignPoint(&p2, 0.0f, 0.0f, 0.0f);
		assignPoint(&p3, 0.1f, 0.1f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		//lip
		glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 0.5f, 0.0f);
		assignPoint(&p1, -0.1f, 0.1f, 0.0f);
		assignPoint(&p2, 0.0f, 0.030f, 0.0f);
		assignPoint(&p3, 0.1f, 0.1f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		//eye

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, -0.05f, 0.15f, 0.0f);
		circle2D(&center, 0.01f, 0.0f, 360.0f);
		glEnd();

		// eye
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, 0.05f, 0.15f, 0.0f);
		circle2D(&center, 0.01f, 0.0f, 360.0f);
		glEnd();
		*/

		// Function Declarations
void assignPoint(Point*, float, float, float);
void circle2D(Point*, float, float, float);
void bezierCurve3Points(Point*, Point*, Point*);
void CollectPoints(Point*, Point*, Point*, Point*);
void AssignPointsCollection(Point*, Point*);

Point center = { 0.0f, 0.0f, 0.0f };
Point p1 = {};
Point p2 = {};
Point p3 = {};

Point parr_left_curve[1001];
Point parr_right_curve[1001];



// tree two (right tilted)

assignPoint(&p1, -0.8f, -0.7f, 0.0f);
assignPoint(&p2, -0.75f, -0.1f, 0.0f);
assignPoint(&p3, -0.35f, 0.26f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.64f, -0.7f, 0.0f);
assignPoint(&p2, -0.66f, -0.1f, 0.0f);
assignPoint(&p3, -0.30f, 0.25f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.62f, 0.27f, 0.02);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

// Tree  one (left tilted)

assignPoint(&p1, -0.7f, -0.7f, 0.0f);
assignPoint(&p2, -0.5f, 0.0f, 0.0f);
assignPoint(&p3, -0.7f, 0.3f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.5f, -0.7f, 0.0f);
assignPoint(&p2, -0.4f, 0.0f, 0.0f);
assignPoint(&p3, -0.65f, 0.3f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.62f, 0.2f, 0.04);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

// Tree Two (right tilted) leaves

// Leaf 1
assignPoint(&p1, -0.36f, 0.26f, 0.0f);
assignPoint(&p2, -0.48f, 0.50f, 0.0f);
assignPoint(&p3, -0.62f, 0.38f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.38f, 0.24f, 0.0f);
assignPoint(&p2, -0.50f, 0.355f, 0.0f);
assignPoint(&p3, -0.62f, 0.38f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.37f, 0.25f, 0.0f);
assignPoint(&p2, -0.50f, 0.44f, 0.0f);
assignPoint(&p3, -0.62f, 0.38f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// Leaf 2

assignPoint(&p1, -0.35f, 0.26f, 0.0f);
assignPoint(&p2, -0.4f, 0.46f, 0.0f);
assignPoint(&p3, -0.55f, 0.55f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.32f, 0.26f, 0.0f);
assignPoint(&p2, -0.4f, 0.60f, 0.0f);
assignPoint(&p3, -0.55f, 0.55f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.335f, 0.264f, 0.0f);
assignPoint(&p2, -0.402f, 0.55f, 0.0f);
assignPoint(&p3, -0.55f, 0.55f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// Leaf 3
assignPoint(&p1, -0.32f, 0.26f, 0.0f);
assignPoint(&p2, -0.38f, 0.46f, 0.0f);
assignPoint(&p3, -0.324f, 0.58f, -0.5f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.30f, 0.26f, 0.0f);
assignPoint(&p2, -0.30f, 0.43f, 0.0f);
assignPoint(&p3, -0.324f, 0.58f, -0.5f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.308f, 0.26f, 0.0f);
assignPoint(&p2, -0.35f, 0.45f, 0.0f);
assignPoint(&p3, -0.324f, 0.58f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();



// leaf 4
assignPoint(&p1, -0.31f, 0.26f, 0.0f);
assignPoint(&p2, -0.26f, 0.50f, 0.0f);
assignPoint(&p3, -0.12f, 0.48f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.30f, 0.26f, 0.0f);
assignPoint(&p2, -0.24f, 0.40f, 0.0f);
assignPoint(&p3, -0.12f, 0.48f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);


glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.308f, 0.26f, 0.0f);
assignPoint(&p2, -0.26f, 0.43f, 0.0f);
assignPoint(&p3, -0.12f, 0.48f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// leaf 5

assignPoint(&p1, -0.30f, 0.26f, 0.0f);
assignPoint(&p2, -0.23f, 0.39f, 0.0f);
assignPoint(&p3, -0.12f, 0.23f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.294f, 0.25f, 0.0f);
assignPoint(&p2, -0.23f, 0.275f, 0.0f);
assignPoint(&p3, -0.12f, 0.23f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.295f, 0.26f, 0.0f);
assignPoint(&p2, -0.24f, 0.32f, 0.0f);
assignPoint(&p3, -0.12f, 0.23f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// leaf 6


assignPoint(&p1, -0.31f, 0.26f, 0.0f);
assignPoint(&p2, -0.20f, 0.24f, 0.0f);
assignPoint(&p3, -0.15f, 0.1f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.31f, 0.22f, 0.0f);
assignPoint(&p2, -0.20f, 0.20f, 0.0f);
assignPoint(&p3, -0.15f, 0.1f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.31f, 0.24f, 0.0f);
assignPoint(&p2, -0.20f, 0.21f, 0.0f);
assignPoint(&p3, -0.15f, 0.1f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();


// // Tree One (left tilted ) Leaves

// leaf 0

assignPoint(&p1, -0.65f, 0.3f, 0.0f);
assignPoint(&p2, -0.7f, 0.35f, 0.0f);
assignPoint(&p3, -0.85f, 0.19f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.65f, 0.28f, 0.0f);
assignPoint(&p2, -0.68f, 0.25f, 0.0f);
assignPoint(&p3, -0.85f, 0.19f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.65f, 0.29f, 0.0f);
assignPoint(&p2, -0.69f, 0.30f, 0.0f);
assignPoint(&p3, -0.85f, 0.19f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();


// leaf 1
glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.68f, 0.26f, 0.0f);
assignPoint(&p2, -0.73f, 0.46f, 0.0f);
assignPoint(&p3, -0.87f, 0.55f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);
glEnd();


assignPoint(&p1, -0.65f, 0.26f, 0.0f);
assignPoint(&p2, -0.73f, 0.60f, 0.0f);
assignPoint(&p3, -0.88f, 0.55f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.665f, 0.264f, 0.0f);
assignPoint(&p2, -0.732f, 0.55f, 0.0f);
assignPoint(&p3, -0.88f, 0.55f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// leaf 2
assignPoint(&p1, -0.67f, 0.27f, 0.0f);
assignPoint(&p2, -0.70f, 0.47f, 0.0f);
assignPoint(&p3, -0.714f, 0.59f, -0.5f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);

assignPoint(&p1, -0.65f, 0.27f, 0.0f);
assignPoint(&p2, -0.63f, 0.44f, 0.0f);
assignPoint(&p3, -0.714f, 0.59f, -0.5f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.66f, 0.27f, 0.0f);
assignPoint(&p2, -0.68f, 0.46f, 0.0f);
assignPoint(&p3, -0.714f, 0.59f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// leaf3
glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.66f, 0.31f, 0.0f);
assignPoint(&p2, -0.61f, 0.55f, 0.0f);
assignPoint(&p3, -0.47f, 0.53f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.65f, 0.31f, 0.0f);
assignPoint(&p2, -0.59f, 0.45f, 0.0f);
assignPoint(&p3, -0.47f, 0.53f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);
glEnd();

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.658f, 0.31f, 0.0f);
assignPoint(&p2, -0.61f, 0.48f, 0.0f);
assignPoint(&p3, -0.47f, 0.53f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();

// leaf 4

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.650f, 0.32f, 0.0f);
assignPoint(&p2, -0.58f, 0.42f, 0.0f);
assignPoint(&p3, -0.46f, 0.26f, 0.0f);
CollectPoints(parr_left_curve, &p1, &p2, &p3);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.644f, 0.27f, 0.0f);
assignPoint(&p2, -0.58f, 0.325f, 0.0f);
assignPoint(&p3, -0.46f, 0.26f, 0.0f);
CollectPoints(parr_right_curve, &p1, &p2, &p3);
glEnd();

glBegin(GL_LINES);
glColor3f(0.6f, 1.0f, 0.1);
AssignPointsCollection(parr_left_curve, parr_right_curve);
glEnd();

glBegin(GL_LINES);
glColor3f(1.0f, 0.0f, 0.0f);
assignPoint(&p1, -0.645f, 0.30f, 0.0f);
assignPoint(&p2, -0.59f, 0.36f, 0.0f);
assignPoint(&p3, -0.46f, 0.26f, 0.0f);
bezierCurve3Points(&p1, &p2, &p3);
glEnd();


// Coconuts ( Tree one )
glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.72f, 0.3f, 0.0f);
circle2D(&center, 0.02f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.68f, 0.28f, 0.0f);
circle2D(&center, 0.02f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.64f, 0.3f, 0.0f);
circle2D(&center, 0.02f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.71f, 0.34f, 0.0f);
circle2D(&center, 0.02f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.67f, 0.32f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.666f, 0.35f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();



//coconuts tree 2
glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.377f, 0.27f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.36f, 0.23f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.3f, 0.229f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.33f, 0.19f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.33f, 0.302f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.301f, 0.28f, 0.0f);
circle2D(&center, 0.021f, 0.0f, 360.0f);
glEnd();

glBegin(GL_TRIANGLE_FAN);
glColor3f(0.0f, 0.7f, 0.01f);
assignPoint(&center, -0.32f, 0.26f, 0.0f);
circle2D(&center, 0.02f, 0.0f, 360.0f);
glEnd();



//Grass
glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.8f, -0.7f, 0.0f);
glVertex3f(-0.78f, -0.55f, 0.0f);
glVertex3f(-0.77f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.77f, -0.7f, 0.0f);
glVertex3f(-0.767f, -0.44f, 0.0f);
glVertex3f(-0.76f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.765f, -0.7f, 0.0f);
glVertex3f(-0.75f, -0.35f, 0.0f);
glVertex3f(-0.74f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.767f, -0.65f, 0.0f);
glVertex3f(-0.740f, -0.35f, 0.0f);
glVertex3f(-0.758f, -0.67f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.655f, -0.7f, 0.0f);
glVertex3f(-0.660f, -0.36f, 0.0f);
glVertex3f(-0.670f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.665f, -0.7f, 0.0f);
glVertex3f(-0.680f, -0.377f, 0.0f);
glVertex3f(-0.670f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.550f, -0.7f, 0.0f);
glVertex3f(-0.695f, -0.32f, 0.0f);
glVertex3f(-0.580f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.680f, -0.7f, 0.0f);
glVertex3f(-0.730f, -0.39f, 0.0f);
glVertex3f(-0.760f, -0.3f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.740f, -0.7f, 0.0f);
glVertex3f(-0.720f, -0.35f, 0.0f);
glVertex3f(-0.710f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.64f, -0.7f, 0.0f);
glVertex3f(-0.6f, -0.3f, 0.0f);
glVertex3f(-0.58f, -0.7f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.63f, -0.66f, 0.0f);
glVertex3f(-0.65f, -0.3f, 0.0f);
glVertex3f(-0.61f, -0.63f, 0.0f);
glEnd();

//Grass
glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.7f, -0.75f, 0.0f);
glVertex3f(-0.68f, -0.6f, 0.0f);
glVertex3f(-0.67f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.67f, -0.75f, 0.0f);
glVertex3f(-0.667f, -0.49f, 0.0f);
glVertex3f(-0.66f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.665f, -0.75f, 0.0f);
glVertex3f(-0.65f, -0.4f, 0.0f);
glVertex3f(-0.64f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.667f, -0.7f, 0.0f);
glVertex3f(-0.640f, -0.4f, 0.0f);
glVertex3f(-0.658f, -0.72f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.555f, -0.75f, 0.0f);
glVertex3f(-0.560f, -0.41f, 0.0f);
glVertex3f(-0.570f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.565f, -0.75f, 0.0f);
glVertex3f(-0.580f, -0.427f, 0.0f);
glVertex3f(-0.570f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.450f, -0.75f, 0.0f);
glVertex3f(-0.595f, -0.37f, 0.0f);
glVertex3f(-0.480f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.580f, -0.75f, 0.0f);
glVertex3f(-0.630f, -0.44f, 0.0f);
glVertex3f(-0.660f, -0.35f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.640f, -0.75f, 0.0f);
glVertex3f(-0.620f, -0.4f, 0.0f);
glVertex3f(-0.610f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.54f, -0.75f, 0.0f);
glVertex3f(-0.5f, -0.35f, 0.0f);
glVertex3f(-0.48f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.53f, -0.71f, 0.0f);
glVertex3f(-0.55f, -0.35f, 0.0f);
glVertex3f(-0.51f, -0.68f, 0.0f);
glEnd();

//Grass
glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.85f, -0.75f, 0.0f);
glVertex3f(-0.83f, -0.6f, 0.0f);
glVertex3f(-0.82f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.82f, -0.75f, 0.0f);
glVertex3f(-0.817f, -0.49f, 0.0f);
glVertex3f(-0.81f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.815f, -0.75f, 0.0f);
glVertex3f(-0.8f, -0.4f, 0.0f);
glVertex3f(-0.79f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.817f, -0.7f, 0.0f);
glVertex3f(-0.790f, -0.4f, 0.0f);
glVertex3f(-0.808f, -0.72f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.705f, -0.75f, 0.0f);
glVertex3f(-0.710f, -0.41f, 0.0f);
glVertex3f(-0.720f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.715f, -0.75f, 0.0f);
glVertex3f(-0.730f, -0.427f, 0.0f);
glVertex3f(-0.720f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.60f, -0.75f, 0.0f);
glVertex3f(-0.745f, -0.37f, 0.0f);
glVertex3f(-0.630f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.530f, -0.75f, 0.0f);
glVertex3f(-0.580f, -0.44f, 0.0f);
glVertex3f(-0.610f, -0.35f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.590f, -0.75f, 0.0f);
glVertex3f(-0.570f, -0.4f, 0.0f);
glVertex3f(-0.560f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.49f, -0.75f, 0.0f);
glVertex3f(-0.45f, -0.35f, 0.0f);
glVertex3f(-0.43f, -0.75f, 0.0f);
glEnd();

glBegin(GL_TRIANGLES);
glColor3f(0.4f, 0.5f, 0.0f);
glVertex3f(-0.48f, -0.71f, 0.0f);
glVertex3f(-0.5f, -0.35f, 0.0f);
glVertex3f(-0.46f, -0.68f, 0.0f);
glEnd();

}


void CollectPoints(Point* parr, Point* p1, Point* p2, Point* p3)
{
	//variable declarations
	int a = 0;
	//parr = NULL;

	//parr = (Point*)malloc(100 * sizeof(Point));
	for (float u = 0.0f; u <= 1.0f; u += 0.001f, a++) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		//Point pa = { x, y, z };
		parr[a].x = x;
		parr[a].y = y;
		parr[a].z = z;
	}
}

void AssignPointsCollection(Point* parr1, Point* parr2)
{
	for (int a = 0; a < 1001; a++)
	{
		glVertex3f(parr1[a].x, parr1[a].y, parr1[a].z);
		glVertex3f(parr2[a].x, parr2[a].y, parr2[a].z);
	}
}

/******************* Areoplane ************************************/
void Plane(void)
{
	Point center;
	// polygon 1
				// 1
	//Rectangle body
	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f, 0.0f, 0.0f);
		//clockwise move
		//body
		glVertex3f(0.1f, 0.35f, 0.0f); //A
		glVertex3f(0.4f, 0.35f, 0.0f); //B
		glVertex3f(0.4f, 0.0f, 0.0f); //C
		glVertex3f(0.1f, 0.0f, 0.0f); //D
	glEnd();

	//Triangle
	glBegin(GL_POLYGON);								// 1
		glColor3f(1.0f, 0.0f, 0.0f);
		//clockwise move

		glVertex3f(0.4f, 0.35f, 0.0f); //B
		glVertex3f(0.8f, 0.35f, 0.0f); //E
		glVertex3f(0.4f, 0.0f, 0.0f); //C
	glEnd();

	//back curve
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.0f, 0.0f);

		// p1 = 0.8,0.35
		//p2 = 0.6, 0.02
		//p3 = 0.4,0.0

		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (0.4f * (1.0f - u) * (1.0f - u)) + (0.6f * 2.0f * (1.0f - u) * u) + (0.8f * u * u);
			float y = (0.0f * (1.0f - u) * (0.02f - u)) + (0.02f * 2.0f * (1.0f - u) * u) + (0.35f * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();
	//end of body 


	//smile
	glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);

		for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
		{
			float x = (0.10f * (1.0f - u) * (1.0f - u)) + (0.16f * 2.0f * (1.0f - u) * u) + (0.20f * u * u);
			float y = (0.11f * (1.0f - u) * (0.02f - u)) + (0.09f * 2.0f * (1.0f - u) * u) + (0.13f * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//fan
	glTranslatef(0.0f, 0.0f, 0.0f);
	//fan ubha rect
	glBegin(GL_POLYGON);								// 1
		glColor3f(0.9f, 0.9f, 0.9f);
		//clockwise move	
		glVertex3f(0.0550f, 0.450f, 0.0f); //a
		glVertex3f(0.0650f, 0.450f, 0.0f); //b
		glVertex3f(0.0650f, -0.035f, 0.0f); //c
		glVertex3f(0.0550f, -0.035f, 0.0f); //d

		//glVertex3f(0.035f, -0.015f, 0.0f); //x
	glEnd();

	//curve
	
	glBegin(GL_POLYGON);
		glColor3f(0.9f, 0.9f, 0.9f);

		for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
		{
			float x = (0.065f * (1.0f - u) * (1.0f - u)) + (0.0870f * 2.0f * (1.0f - u) * u) + (0.065f * u * u);
			float y = (-0.0350f * (1.0f - u) * (0.02f - u)) + (0.15f * 2.0f * (1.0f - u) * u) + (0.450f * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//joining rect
	glBegin(GL_POLYGON);								// 1
		glColor3f(0.0f, 0.9f, 0.9f);
		//clockwise move

		glVertex3f(0.0755f, 0.240f, 0.0f); //a
		glVertex3f(0.10f, 0.240f, 0.0f); //b
		glVertex3f(0.10f, 0.198f, 0.0f); //c
		glVertex3f(0.0755f, 0.198f, 0.0f); //d

		//glVertex3f(0.035f, -0.015f, 0.0f); //x
	glEnd();

	//End of fan

	//khalch quad
	glBegin(GL_POLYGON);								// 1
		glColor3f(1.0f, 0.0f, 0.0f);
		//clockwise move
		glVertex3f(0.31f, 0.015f, 0.0f); //x
		glVertex3f(0.465f, 0.015f, 0.0f); //w
		glVertex3f(0.28f, -0.23f, 0.0f); //z
		//glVertex3f(0.28f, -0.2f, 0.0f); //y

	glEnd();


	//Curve
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.0f, 0.0f);

		// p1 = 0.3,-0.2			0.3f, -0.25f, 0.0f
		//p2 = 0.25,-0.15
		//p3 = 0.1,-0.2				0.432f, 0.0f, 0.0f

		for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
		{
			float x = (0.452f * (1.0f - u) * (1.0f - u)) + (0.42f * 2.0f * (1.0f - u) * u) + (0.28f * u * u);
			float y = (0.014f * (1.0f - u) * (0.02f - u)) + (-0.190f * 2.0f * (1.0f - u) * u) + (-0.23f * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	//End of khalch wing
	//Vrch elipse
	center = { 0.75f, 0.50f, 0.0f };
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 0.0f, 0.0f);
		ellipse2D(&center, 0.08f, 0.190f, 0.0f, 360.0f);
		glEnd();
	
		//middle SEAT 
		center = { 0.52f, 0.34f, 0.0f };
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 0.0f);
		circle2D(&center, 0.034f, 0.0f, 360.0f);
	glEnd();
	

	//MOTHA Eye	
	glPushMatrix();
		center = { 0.165f, 0.28f, 0.0f };
		glBegin(GL_TRIANGLE_FAN);
			glColor3f(1.0f, 1.0f, 1.0f);
			circle2D(&center, 0.028f, 0.0f, 360.0f);
		glEnd();

		//lahan Eye
		center = { 0.153f, 0.28f, 0.0f };
		glBegin(GL_TRIANGLE_FAN);
			glColor3f(0.0f, 0.0f, 0.0f);
			circle2D(&center, 0.022f, 0.0f, 360.0f);
		glEnd();
	glPopMatrix();

	//glPopMatrix();
	//vrch rect top support

	glBegin(GL_POLYGON);								// 1
		glColor3f(1.0f, 0.0f, 0.0f);
	

		//clockwise move
		glVertex3f(0.31f, 0.70f, 0.0f); //a
		glVertex3f(0.45f, 0.64f, 0.0f); //b
		glVertex3f(0.43f, 0.62f, 0.0f); //c
		glVertex3f(0.32f, 0.66f, 0.0f); //d

		glEnd();

		// khalcha rect bottom support

		glBegin(GL_POLYGON);								// 1
		glColor3f(1.0f, 0.4f, 1.0f);
		//clockwise move
		glVertex3f(0.32f, 0.100f, 0.0f); //a
		glVertex3f(0.45f, 0.061f, 0.0f); //b
		glVertex3f(0.445f, 0.0605f, 0.0f); //c
		glVertex3f(0.31f, 0.060f, 0.0f); //d
	glEnd();

	// First ubhi line for pankhe

	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(0.335f, 0.655f, 0.0f); //a
		glVertex3f(0.342f, 0.655f, 0.0f); //b
		glVertex3f(0.333f, 0.093f, 0.0f); //c
		glVertex3f(0.325f, 0.10f, 0.0f); //d
	glEnd();

	// Second ubhi line for pankhe
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.420f, 0.625f, 0.0f); //a
		glVertex3f(0.427f, 0.62f, 0.0f); //b
		glVertex3f(0.410f, 0.0705f, 0.0f); //c
		glVertex3f(0.401f, 0.0705f, 0.0f); //d
	glEnd();

	// Tirki ubhi line for pankhe
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.337f, 0.64f, 0.0f); //a
		glVertex3f(0.344f, 0.64f, 0.0f); //b
		glVertex3f(0.411f, 0.0705f, 0.0f); //c
		glVertex3f(0.400f, 0.0705f, 0.0f); //d
	glEnd();


	//Chak motha

	center = { 0.28f, -0.23f, 0.0f };
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.5f);
		circle2D(&center, 0.068f, 0.0f, 360.0f);
	glEnd();

	//Chak lahan

	center = { 0.28f, -0.23f, 0.0f };
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.0f, 0.5f);
		circle2D(&center, 0.028f, 0.0f, 360.0f);
	glEnd();


}

// For colouring effect
void skyAndsky(void) {

	glBegin(GL_QUADS); // Sea water
	glColor3f(0.0f, 0.8f, 0.9f);
	glVertex3f(1.0f, -0.60f, 0.0f);
	glVertex3f(-1.0f, -0.60f, 0.0f);

	glColor3f(0.0f, 0.8f, 0.9f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 0.8f, 0.9f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //sea water
	glColor3f(0.0f, 0.8f, 0.9f);
	glVertex3f(-1.0f, -0.20f, 0.0f);
	glVertex3f(1.0f, -0.20f, 0.0f);

	glColor3f(0.1f, 0.45f, 1.0f);
	glVertex3f(1.0f, 0.35f, 0.0f);
	glVertex3f(-1.0f, 0.35f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // SKY
	glColor3f(0.3f, 0.75f, 0.95f);
	glVertex3f(-1.0f, 0.10f, 0.0f);
	glVertex3f(1.0f, 0.10f, 0.0f);

	glColor3f(0.67f, 0.860f, 0.95f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();

}

//Ashish Model Code by Aditya
void AshishScreen(void)
{
	// function declaration
	void AKJ_Ashish_On_Beach(void);

	// code
	glPushMatrix();
		glScalef(0.8f, 0.8f, 0.0f);
		glTranslatef(0.5f, 0.13f, 0.0f);
		AKJ_Ashish_On_Beach();
	glPopMatrix();
}

/************** Ashish model, chiar, laptop function call ****************/
void AKJ_Ashish_On_Beach(void)
{
	// function declaration
	void AKJ_Ashish(void);
	void AKJ_Chair(void);
	void AKJ_Laptop(void);

	// code
	glPushMatrix();
		AKJ_Chair();
	glPopMatrix();

	glPushMatrix();
		AKJ_Ashish();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.28f, 0.45f, 0.0f);
		glScalef(1.6f, 1.8f, 0.0f);
		AKJ_Laptop();
	glPopMatrix();


}

/**************** Ashish Model ***********************/

void AKJ_Ashish(void)
{
	// function declaration
	void AKJ_upperBody(void);
	void AKJ_lowerBody(void);
	void AKJ_leg1(void);
	void AKJ_leg2(void);
	void AKJ_hand1(void);
	void AKJ_hand2(void);
	void AKJ_face(void);

	// code
	
	glPushMatrix();
		AKJ_upperBody();
	glPopMatrix();

	glPushMatrix();
		AKJ_lowerBody();
	glPopMatrix();


	glPushMatrix();
		AKJ_leg1();
	 glPopMatrix();

	glPushMatrix();
		AKJ_leg2();
	glPopMatrix();

	glPushMatrix();
		AKJ_hand1();
	glPopMatrix();

	glPushMatrix();
		AKJ_hand2();
	glPopMatrix();

	glPushMatrix();
		AKJ_face();
	glPopMatrix();
}
//Upper Body Function
void AKJ_upperBody(void)
{
	// code
	// code of upper body
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.79f, -0.35f, 0.0f);
		glVertex3f(0.73f, -0.38f, 0.0f);
		glVertex3f(0.68f, -0.41f, 0.0f);
		glVertex3f(0.59f, -0.60f, 0.0f);
		glVertex3f(0.68f, -0.68f, 0.0f);
		glVertex3f(0.83f, -0.365f, 0.0f);
	glEnd();
}

//Lower Body
void AKJ_lowerBody(void)
{
	// code
	// code of lower body
	glBegin(GL_POLYGON);
		glColor3f(0.3f, 0.9f, 0.9f);
		glVertex3f(0.59f, -0.60f, 0.0f);
		glVertex3f(0.46f, -0.55f, 0.0f);
		glVertex3f(0.46f, -0.60f, 0.0f);
		glVertex3f(0.59f, -0.60f, 0.0f);
		glVertex3f(0.68f, -0.68f, 0.0f);
		glVertex3f(0.59f, -0.70f, 0.0f);
		glVertex3f(0.45f, -0.66f, 0.0f);
		glVertex3f(0.45f, -0.60f, 0.0f);
		glVertex3f(0.46f, -0.60f, 0.0f);
	glEnd();

	// Shorts Fold line
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.46f, -0.60f, 0.0f);
		glVertex3f(0.59f, -0.60f, 0.0f);
	glEnd();
}

//Leg 1
void AKJ_leg1(void)
{
	// function declarations
	void assignPoint(Point*, float, float, float);
	void Collect5Points(Point*, Point*, Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);


	// variable declaration
	Point p1 = { 0.46f, -0.56f, 0.0f };
	Point p2 = { 0.41f, -0.55f, 0.0f };
	Point p3 = { 0.35f, -0.52f, 0.0f };
	Point p4 = { 0.34f, -0.60f, 0.0f };
	Point p5 = { 0.27f, -0.79f, 0.0f };

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];

	// code
	// Leg 2
	Collect5Points(parr_left_curve, &p1, &p2, &p3, &p3, &p5);
	assignPoint(&p1, 0.46f, -0.60f, 0.0f);
	assignPoint(&p2, 0.39f, -0.63f, 0.0f);
	assignPoint(&p3, 0.35f, -0.58f, 0.0f);
	assignPoint(&p4, 0.41f, -0.72f, 0.0f);
	assignPoint(&p5, 0.29f, -0.80f, 0.0f);
	Collect5Points(parr_right_curve, &p1, &p2, &p3, &p3, &p5);

	glBegin(GL_LINES);
		glColor3f(1.0f, 0.8f, 0.5f);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// foot
	glBegin(GL_QUADS);
		glVertex3f(0.27f, -0.79f, 0.0f);
		glVertex3f(0.24f, -0.80f, 0.0f);
		glVertex3f(0.25f, -0.815f, 0.0f);
		glVertex3f(0.29f, -0.81f, 0.0f);
	glEnd();

}

void AKJ_leg2(void)
{
	// function declarations
	void assignPoint(Point*, float, float, float);
	void Collect5Points(Point*, Point*, Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);
	void bezierCurve5Points(Point*, Point*, Point*, Point*, Point*);

	// variable declaration
	Point p1, p2, p3, p4, p5;

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];

	// code
	// Leg 1
	assignPoint(&p1, 0.45f, -0.60f, 0.0f);
	assignPoint(&p2, 0.40f, -0.62f, 0.0f);
	assignPoint(&p3, 0.34f, -0.58f, 0.0f);
	assignPoint(&p4, 0.30f, -0.70f, 0.0f);
	assignPoint(&p5, 0.29f, -0.81f, 0.0f);
	Collect5Points(parr_left_curve, &p1, &p2, &p3, &p3, &p5);

	assignPoint(&p1, 0.45f, -0.66f, 0.0f);
	assignPoint(&p2, 0.40f, -0.69f, 0.0f);
	assignPoint(&p3, 0.36f, -0.63f, 0.0f);
	assignPoint(&p4, 0.40f, -0.72f, 0.0f);
	assignPoint(&p5, 0.32f, -0.83f, 0.0f);
	Collect5Points(parr_right_curve, &p1, &p2, &p3, &p3, &p5);

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.8f, 0.5f);
	AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

	// Legs Partition
	glBegin(GL_LINE_STRIP);
		glColor3f(0.0f, 0.0f, 0.0f);
		assignPoint(&p1, 0.46f, -0.60f, 0.0f);
		assignPoint(&p2, 0.43f, -0.61f, 0.0f);
		assignPoint(&p3, 0.30f, -0.50f, 0.0f);
		assignPoint(&p4, 0.32f, -0.72f, 0.0f);
		assignPoint(&p5, 0.29f, -0.80f, 0.0f);
		bezierCurve5Points(&p1, &p2, &p3, &p4, &p5);
	glEnd();

	// foot
	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(0.29f, -0.80f, 0.0f);
		glVertex3f(0.25f, -0.81f, 0.0f);
		glVertex3f(0.26f, -0.825f, 0.0f);
		glVertex3f(0.315f, -0.828f, 0.0f);
	glEnd();
}


//Hand
void AKJ_hand1(void)
{
	// function declarations
	void assignPoint(Point*, float, float, float);
	void Collect5Points(Point*, Point*, Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);

	// variable declaration
	Point p1, p2, p3, p4, p5;

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];

	// code
	// hand 1
	assignPoint(&p1, 0.68f, -0.41f, 0.0f);
	assignPoint(&p2, 0.66f, -0.41f, 0.0f);
	assignPoint(&p3, 0.58f, -0.55f, 0.0f);
	assignPoint(&p4, 0.54f, -0.50f, 0.0f);
	assignPoint(&p5, 0.52f, -0.51f, 0.0f);
	Collect5Points(parr_left_curve, &p1, &p2, &p3, &p3, &p5);

	assignPoint(&p1, 0.66f, -0.45f, 0.0f);
	assignPoint(&p2, 0.63f, -0.53f, 0.0f);
	assignPoint(&p3, 0.58f, -0.55f, 0.0f);
	assignPoint(&p4, 0.54f, -0.52f, 0.0f);
	assignPoint(&p5, 0.52f, -0.53f, 0.0f);
	Collect5Points(parr_right_curve, &p1, &p2, &p3, &p3, &p5);

	glBegin(GL_LINES);
		glColor3f(1.0f, 0.8f, 0.5f);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

}

void AKJ_hand2(void)
{
	// function declarations
	void assignPoint(Point*, float, float, float);
	void Collect5Points(Point*, Point*, Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);
	void bezierCurve5Points(Point*, Point*, Point*, Point*, Point*);

	// variable declaration
	Point p1, p2, p3, p4, p5;

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];

	// code
	// hand 2
	assignPoint(&p1, 0.78f, -0.41f, 0.0f);
	assignPoint(&p2, 0.70f, -0.50f, 0.0f);
	assignPoint(&p3, 0.62f, -0.65f, 0.0f);
	assignPoint(&p4, 0.58f, -0.50f, 0.0f);
	assignPoint(&p5, 0.53f, -0.58f, 0.0f);
	Collect5Points(parr_left_curve, &p1, &p2, &p3, &p3, &p5);

	assignPoint(&p1, 0.80f, -0.44f, 0.0f);
	assignPoint(&p2, 0.73f, -0.53f, 0.0f);
	assignPoint(&p3, 0.65f, -0.68f, 0.0f);
	assignPoint(&p4, 0.60f, -0.55f, 0.0f);
	assignPoint(&p5, 0.55f, -0.61f, 0.0f);
	Collect5Points(parr_right_curve, &p1, &p2, &p3, &p3, &p5);

	glBegin(GL_LINES);
		glColor3f(1.0f, 0.8f, 0.5f);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
	glEnd();

}

/*********** Face **************/
//Face relayed all function call 

void AKJ_face(void)
{
	void AKJ_neck(void);
	void AKJ_face_base(void);
	void AKJ_hairs(void);
	void AKJ_nose(void);
	void AKJ_goggle(void);
	void AKJ_ear(void);
	void AKJ_mouth(void);
	
	// face
	glPushMatrix();
		AKJ_face_base();
	glPopMatrix();

	// Goggle
	glPushMatrix();
		AKJ_goggle();
	glPopMatrix();

	// Ear
	glPushMatrix();
		AKJ_ear();
	glPopMatrix();


	// neck												// Distoy
	glPushMatrix();
		AKJ_neck();
	glPopMatrix();

	

	// hairs											// distoy
	glPushMatrix();
		AKJ_hairs();
	glPopMatrix();

	// nose												// distoy
	glPushMatrix();
		AKJ_nose();
	glPopMatrix();

	
	// Mouth											// distoy
	glPushMatrix();
	AKJ_mouth();
	glPopMatrix();

}

//Neck
void AKJ_neck(void)
{
	// code
	glBegin(GL_QUADS);
		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(0.81f, -0.32f, 0.0f);
		glVertex3f(0.79f, -0.35f, 0.0f);
		glVertex3f(0.83f, -0.365f, 0.0f);
		glVertex3f(0.84f, -0.34f, 0.0f);
	glEnd();

}

//Face BAse
void AKJ_face_base(void)
{
	// function declaration
	void ellipse2D(Point*, float, float, float, float);
	void assignPoint(Point*, float, float, float);

	// variable declaration
	Point center;

	// code
	assignPoint(&center, 0.83f, -0.25f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);			// Trianglewala
		glColor3f(1.0f, 0.8f, 0.5f);
		//ellipse2D(&center, 0.0f, 360.0f, 0.045f, 0.095f);
		ellipse2D(&center, 0.045f, 0.095f, 0.0f, 360.0f);

	glEnd();
	//Point* center, float xRadius, float yRadius, float sAngle, float eAngle)
}

// Hair Function , yamadhe back hair function la call kela ahe 
void AKJ_hairs(void)
{
	// function declaration
	void bezierCurve3Points(Point*, Point*, Point*);
	void assignPoint(Point*, float, float, float);
	void AKJ_back_hairs(void);

	// variable declaration
	Point p1, p2, p3;

	// code
	glBegin(GL_POLYGON);
		glColor3f(0.6f, 0.3f, 0.0f);
		assignPoint(&p1, 0.76f, -0.14f, 0.0f);
		assignPoint(&p2, 0.80f, -0.28f, 0.0f);
		assignPoint(&p3, 0.88f, -0.16f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	//Back Hair Call
	AKJ_back_hairs();

}

// Back Hair
void AKJ_back_hairs(void)
{
	// function declaration
	void bezierCurve4Points(Point*, Point*, Point*, Point*);
	void assignPoint(Point*, float, float, float);

	// variable declaration
	Point p1, p2, p3, p4;

	// code
	glBegin(GL_POLYGON);
		glColor3f(0.6f, 0.3f, 0.0f);
		assignPoint(&p1, 0.88f, -0.16f, 0.0f);
		assignPoint(&p2, 0.84f, -0.21f, 0.0f);
		assignPoint(&p3, 0.84f, -0.25f, 0.0f);
		assignPoint(&p4, 0.878f, -0.272f, 0.0f);
		bezierCurve4Points(&p1, &p2, &p3, &p4);

	glEnd();
}

void AKJ_nose(void)
{
	// function declaration
	void bezierCurve3Points(Point*, Point*, Point*);
	void assignPoint(Point*, float, float, float);

	// variable declaration
	Point p1, p2, p3;

	// code
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.8f, 0.5f);
		assignPoint(&p1, 0.80f, -0.26f, 0.0f);
		assignPoint(&p2, 0.775f, -0.294f, 0.0f);
		assignPoint(&p3, 0.798f, -0.295f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	glBegin(GL_LINE_STRIP);
		glColor3f(0.4f, 0.2f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();


}

//Goggle 
void AKJ_goggle(void)
{
	// function declarations
	void ellipse2D(Point*, float, float, float, float);
	void assignPoint(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);

	// variable declaration
	Point center;
	Point p1, p2, p3;

	// code
	// left circle
	assignPoint(&center, 0.82f, -0.26f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);		// Trianglewala
		glColor3f(0.9f, 0.3f, 0.3f);
		ellipse2D(&center,0.013f, 0.019f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
		glColor3f(0.3f, 0.9f, 0.3f);
		ellipse2D(&center,0.0132, 0.0192, 0.0f, 360.0f);
		ellipse2D(&center, 0.0135, 0.0195, 0.0f, 360.0f);
	glEnd();

	// goggle arm
	glBegin(GL_LINE_STRIP);
		glColor3f(0.3f, 0.9f, 0.3f);
		assignPoint(&p1, 0.833f, -0.26f, 0.0f);
		assignPoint(&p2, 0.87f, -0.265f, 0.0f);
		assignPoint(&p3, 0.85f, -0.28f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	// right circle
	assignPoint(&center, 0.785f, -0.255f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);				// Trianglewala
		glColor3f(0.9f, 0.3f, 0.3f);
		ellipse2D(&center, 0.009f, 0.019f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
		glColor3f(0.3f, 0.9f, 0.3f);
		ellipse2D(&center, 0.0092f, 0.0192f, 0.0f, 360.0f);
		ellipse2D(&center, 0.0095f, 0.0195f, 0.0f, 360.0f);
	glEnd();

	// bridge
	glBegin(GL_LINES);
		glColor3f(0.3f, 0.9f, 0.3f);
		glVertex3f(0.794f, -0.255f, 0.0f);
		glVertex3f(0.81f, -0.255f, 0.0f);
	glEnd();
}

void AKJ_ear(void)
{
	// function declarations
	void assignPoint(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);

	// variable declaration
	Point center;
	Point p1, p2, p3;

	// code
	glBegin(GL_LINE_STRIP);
		glColor3f(0.4f, 0.2f, 0.0f);
		assignPoint(&p1, 0.847f, -0.262f, 0.0f);
		assignPoint(&p2, 0.865f, -0.265f, 0.0f);
		assignPoint(&p3, 0.847f, -0.295f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();
}

void AKJ_mouth(void)
{
	// function declaration
	void bezierCurve3Points(Point*, Point*, Point*);
	void assignPoint(Point*, float, float, float);

	// variable declaration
	Point p1, p2, p3;

	// code
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 1.0f);
		assignPoint(&p1, 0.795f, -0.305f, 0.0f);
		assignPoint(&p2, 0.805f, -0.34f, 0.0f);
		assignPoint(&p3, 0.815f, -0.305f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();
}

void AKJ_Chair(void)
{
	// code
	glPushMatrix();

		glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 0.0f);

			// Chair horizontal line
			glVertex3f(0.45f, -0.65f, 0.0f);
			glVertex3f(0.44f, -0.67f, 0.0f);
			glVertex3f(0.85f, -0.80f, 0.0f);
			glVertex3f(0.87f, -0.78f, 0.0f);

			// Chair diagonal line
			glVertex3f(0.87f, -0.30f, 0.0f);
			glVertex3f(0.57f, -0.90f, 0.0f);
			glVertex3f(0.59f, -0.91f, 0.0f);
			glVertex3f(0.89f, -0.29f, 0.0f);

			// Chair Back
			glVertex3f(0.77f, -0.25f, 0.0f);
			glVertex3f(0.56f, -0.69f, 0.0f);
			glVertex3f(0.68f, -0.72f, 0.0f);
			glVertex3f(0.89f, -0.29f, 0.0f);

			// Chair Inner Back
			glColor3f(1.0f, 0.6f, 0.3f);
			glVertex3f(0.78f, -0.27f, 0.0f);
			glVertex3f(0.58f, -0.67f, 0.0f);
			glVertex3f(0.66f, -0.70f, 0.0f);
			glVertex3f(0.87f, -0.30f, 0.0f);

			// Chair front leg
			glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(0.54f, -0.68f, 0.0f);
			glVertex3f(0.45f, -0.85f, 0.0f);
			glVertex3f(0.47f, -0.86f, 0.0f);
			glVertex3f(0.56f, -0.69f, 0.0f);

			// Chair back leg
			glVertex3f(0.67f, -0.73f, 0.0f);
			glVertex3f(0.70f, -0.87f, 0.0f);
			glVertex3f(0.72f, -0.89f, 0.0f);
			glVertex3f(0.69f, -0.74f, 0.0f);


		glEnd();
	glPopMatrix();
}

void AKJ_Laptop(void)
{
	// function declarations
	void ellipse2D(Point*, float, float, float, float);
	void assignPoint(Point*, float, float, float);

	// variable declaration
	Point center;

	// code
	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.8f, 0.9f);

	// Laptop back
	glVertex3f(0.50f, -0.45f, 0.0f);
	glVertex3f(0.46f, -0.55f, 0.0f);
	glVertex3f(0.53f, -0.57f, 0.0f);
	glVertex3f(0.57f, -0.47f, 0.0f);

	// Laptop base
	glVertex3f(0.46f, -0.55f, 0.0f);
	glVertex3f(0.49f, -0.59f, 0.0f);
	glVertex3f(0.55f, -0.61f, 0.0f);
	glVertex3f(0.53f, -0.57f, 0.0f);


	glEnd();

	// Laptop Fold line
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.46f, -0.55f, 0.0f);
	glVertex3f(0.53f, -0.57f, 0.0f);
	glEnd();

	// Laptop Logo
	
	assignPoint(&center, 0.51f, -0.51f, 0.0f);
	glBegin(GL_POLYGON);							// Trianglewala
	glColor3f(0.0f, 0.1f, 0.1f);
	ellipse2D(&center, 0.0085f, 0.014f, 0.0f, 360.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.1f, 0.1f);
	ellipse2D(&center, 0.013, 0.005, 0.0f, 360.0f);
	ellipse2D(&center, 0.0135f, 0.0055f, 0.0f, 360.0f);

	glEnd();
}
/************** Ball ***********************/
void Ball(void)
{

	// variable declaration

	int i;
	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat radius = 0.5f;

	int triangleAmount = 60;

	GLfloat twicePi = 2.0f * PI;
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);


	glVertex2f(x, y);
	for (i = 0; i <= triangleAmount; i++)
	{
		glVertex2f(x + (radius * cos(i * twicePi / triangleAmount)), y + ((radius * 2) * sin(i * twicePi / triangleAmount)));

	}

	glEnd();

	// 1
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.096f, -0.016f, 0.0f);
		glVertex3f(0.144f, -0.378f, 0.0f);
		glVertex3f(0.32f, -0.345f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.096f, -0.016f, 0.0f);
		glVertex3f(0.32f, -0.345f, 0.0f);
		glVertex3f(0.398f, 0.003f, 0.0f);
		glVertex3f(0.25f, 0.23f, 0.0f);
	glEnd();


	// 2
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.129f, 0.12f, 0.0f);
		glVertex3f(-0.178f, 0.488f, 0.0f);
		glVertex3f(-0.356f, 0.44f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.129f, 0.12f, 0.0f);
		glVertex3f(-0.356f, 0.44f, 0.0f);
		glVertex3f(-0.41f, 0.072f, 0.0f);
		glVertex3f(-0.278f, -0.14f, 0.0f);
	glEnd();


	// 3
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.196f, 0.624f, 0.0f);
		glVertex3f(0.280f, 0.739f, 0.0f);
		glVertex3f(0.142f, 0.936f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.196f, 0.624f, 0.0f);
		glVertex3f(0.142f, 0.936f, 0.0f);
		glVertex3f(-0.029f, 0.926f, 0.0f);
		glVertex3f(-0.004f, 0.758f, 0.0f);
	glEnd();

	// 4
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.02f, -0.65f, 0.0f);
		glVertex3f(0.015f, -0.816f, 0.0f);
		glVertex3f(-0.088f, -0.866f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.02f, -0.65f, 0.0f);
		glVertex3f(-0.225f, -0.515f, 0.0f);
		glVertex3f(-0.294f, -0.636f, 0.0f);
		glVertex3f(-0.234f, -0.771f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.088f, -0.866f, 0.0f);
		glVertex3f(-0.02f, -0.65f, 0.0f);
		glVertex3f(-0.234f, -0.771f, 0.0f);
	glEnd();

	///////////
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.096f, -0.016f);
		glVertex2f(-0.129f, 0.12f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.25f, 0.23f);
		glVertex2f(0.196f, 0.624f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.004f, 0.758f);
		glVertex2f(-0.178f, 0.488f);
	glEnd();


	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.144f, -0.378f);
		glVertex2f(-0.02f, -0.65f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.225f, -0.515f);
		glVertex2f(-0.278f, -0.14f);
	glEnd();

	//////////////
	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.32f, -0.345f);
		glVertex2f(0.40f, -0.589f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.088f, -0.866f);
		glVertex2f(-0.05f, -0.99f);
	glEnd();


	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.015f, -0.816f);
		glVertex2f(0.2f, -0.92f);
	glEnd();


	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.398f, 0.003f);
		glVertex2f(0.49f, 0.18f);
	glEnd();


	glBegin(GL_LINES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex2f(0.398f, 0.0f);
		glVertex2f(0.5f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.280f, 0.739f);
		glVertex2f(0.380f, 0.656f);
	glEnd();


	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.142f, 0.936f);
		glVertex2f(0.142f, 0.956f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.029f, 0.926f);
		glVertex2f(-0.124f, 0.970f);
	glEnd();



	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.356f, 0.44f);
		glVertex2f(-0.403f, 0.598f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.41f, 0.072f);
		glVertex2f(-0.491f, -0.15f);
	glEnd();



	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.294f, -0.636f);
		glVertex2f(-0.394f, -0.615f);
	glEnd();

	glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex2f(-0.234f, -0.771f);
		glVertex2f(-0.24f, -0.88f);
	glEnd();
}
/*********** Hot Air Baloon ****************/
void Ballon(void)
{

	// Function Declarations
	void DrawText(float pos_x, float pos_y, float R, float G, float B, char* text_to_draw);

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 0.0f, 0.0f);
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (0.0f * (1.0f - u) * (1.0f - u)) + (0.42f * 2.0f * (1.0f - u) * u) + (0.03f * u * u);
		float y = (-0.24 * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 0.0f, 0.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (-0.42f * 2.0f * (1.0f - u) * u) + (-0.03f * u * u);
			float y = (-0.24f * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();


	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (0.38f * 2.0f * (1.0f - u) * u) + (0.03f * u * u);
			float y = (-0.24 * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (-0.38f * 2.0f * (1.0f - u) * u) + (-0.03f * u * u);
			float y = (-0.24f * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();


	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 0.0f, 0.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.03f * u * u);
			float y = (-0.24 * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 0.0f, 0.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.03f * u * u);
			float y = (-0.24f * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (0.15f * 2.0f * (1.0f - u) * u) + (0.03f * u * u);
			float y = (-0.24 * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.0f * (1.0f - u) * (1.0f - u)) + (-0.15f * 2.0f * (1.0f - u) * u) + (-0.03f * u * u);
			float y = (-0.24f * (1.0f - u) * (1.0f - u)) + (0.44f * 2.0f * (1.0f - u) * u) + (0.544f * u * u);

			glVertex3f(x, y, 0.0f);
		}
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.03f, 0.544f, 0.0f);
		glVertex3f(-0.03f, 0.544f, 0.0f);
		glVertex3f(0.0f, -0.24f, 0.0f);
	glEnd();



	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.0f, 0.0f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (-0.08f * (1.0f - u) * (1.0f - u)) + (0.0f * 2.0f * (1.0f - u) * u) + (0.08f * u * u);
			float y = (-0.10f * (1.0f - u) * (1.0f - u)) + (-0.13f * 2.0f * (1.0f - u) * u) + (-0.10f * u * u);

			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.08f * (1.0f - u) * (1.0f - u)) + (0.07f * 2.0f * (1.0f - u) * u) + (0.06f * u * u);
			float y = (-0.10f * (1.0f - u) * (1.0f - u)) + (-0.14f * 2.0f * (1.0f - u) * u) + (-0.24f * u * u);

			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (-0.08f * (1.0f - u) * (1.0f - u)) + (-0.07f * 2.0f * (1.0f - u) * u) + (-0.06f * u * u);
			float y = (-0.10f * (1.0f - u) * (1.0f - u)) + (-0.14f * 2.0f * (1.0f - u) * u) + (-0.24f * u * u);

			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (-0.06f * (1.0f - u) * (1.0f - u)) + (0.0f * 2.0f * (1.0f - u) * u) + (0.06f * u * u);
			float y = (-0.24f * (1.0f - u) * (1.0f - u)) + (-0.26f * 2.0f * (1.0f - u) * u) + (-0.24f * u * u);

			glVertex3f(x, y, 0.0f);
		}

	glEnd();


	// RODS
	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(-0.06f, -0.24f, 0.0f);
		glVertex3f(-0.055f, -0.24f, 0.0f);
		glVertex3f(-0.055f, -0.37f, 0.0f);
		glVertex3f(-0.06f, -0.37f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(0.06f, -0.24f, 0.0f);
		glVertex3f(0.055f, -0.24f, 0.0f);
		glVertex3f(0.055f, -0.37f, 0.0f);
		glVertex3f(0.06f, -0.37f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(0.035f, -0.245f, 0.0f);
		glVertex3f(0.030f, -0.245f, 0.0f);
		glVertex3f(0.030f, -0.37f, 0.0f);
		glVertex3f(0.035f, -0.37f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(-0.035f, -0.245f, 0.0f);
		glVertex3f(-0.030f, -0.245f, 0.0f);
		glVertex3f(-0.030f, -0.37f, 0.0f);
		glVertex3f(-0.035f, -0.37f, 0.0f);
	glEnd();


	// Base Area

	glBegin(GL_POLYGON);
		glColor3f(0.95f, 0.73f, 0.29f);
		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (-0.07f * (1.0f - u) * (1.0f - u)) + (0.0f * 2.0f * (1.0f - u) * u) + (0.07f * u * u);
			float y = (-0.37f * (1.0f - u) * (1.0f - u)) + (-0.37f * 2.0f * (1.0f - u) * u) + (-0.37f * u * u);

			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.07f * (1.0f - u) * (1.0f - u)) + (0.08f * 2.0f * (1.0f - u) * u) + (0.07f * u * u);
			float y = (-0.37f * (1.0f - u) * (1.0f - u)) + (-0.39f * 2.0f * (1.0f - u) * u) + (-0.41f * u * u);

			glVertex3f(x, y, 0.0f);
		}


		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (0.07f * (1.0f - u) * (1.0f - u)) + (0.0f * 2.0f * (1.0f - u) * u) + (-0.07f * u * u);
			float y = (-0.41f * (1.0f - u) * (1.0f - u)) + (-0.41f * 2.0f * (1.0f - u) * u) + (-0.41f * u * u);

			glVertex3f(x, y, 0.0f);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.01f)
		{
			float x = (-0.07f * (1.0f - u) * (1.0f - u)) + (-0.08f * 2.0f * (1.0f - u) * u) + (-0.07f * u * u);
			float y = (-0.41f * (1.0f - u) * (1.0f - u)) + (-0.39f * 2.0f * (1.0f - u) * u) + (-0.37f * u * u);

			glVertex3f(x, y, 0.0f);
		}

	glEnd();


	glBegin(GL_POLYGON);
		glColor3f(0.78f, 0.55f, 0.20f);
		glVertex3f(-0.07f, -0.41f, 0.0f);
		glVertex3f(0.07f, -0.41f, 0.0f);
		glVertex3f(0.07f, -0.42f, 0.0f);
		glVertex3f(-0.07f, -0.42f, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
		glColor3f(0.95f, 0.73f, 0.29f);
		glVertex3f(-0.07f, -0.42f, 0.0f);
		glVertex3f(0.07f, -0.42f, 0.0f);
		glVertex3f(0.05f, -0.52f, 0.0f);
		glVertex3f(-0.05f, -0.52f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(-0.05f, -0.52f, 0.0f);
		glVertex3f(-0.04f, -0.52f, 0.0f);
		glVertex3f(-0.19f, -0.72f, 0.0f);
		glVertex3f(-0.2f, -0.72f, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
		glColor3f(0.40f, 0.40f, 0.40f);
		glVertex3f(0.05f, -0.52f, 0.0f);
		glVertex3f(0.04f, -0.52f, 0.0f);
		glVertex3f(0.19f, -0.72f, 0.0f);
		glVertex3f(0.2f, -0.72f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.95f, 0.73f, 0.29f);
		glVertex3f(-0.3f, -0.72f, 0.0f);
		glVertex3f(0.3f, -0.72f, 0.0f);
		glVertex3f(0.3f, -1.0f, 0.0f);
		glVertex3f(-0.3f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(0.01f, 0.80f, 0.89f);
		glVertex3f(-0.29f, -0.74f, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.29f, -0.74f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.29f, -0.98f, 0.0f);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.29f, -0.98f, 0.0f);
	glEnd();


	glPushMatrix();
		glEnable(GL_LIGHTING);
		//glScalef(0.75f, 0.75f, 0.0f);
		DrawText(-0.36f, -0.84f, 0.0f, 0.0f, 1.0f, "     HAPPY  BIRTHDAY \n               ASHISH");
		glDisable(GL_LIGHTING);
	glPopMatrix();


}
//Cloud Functions
void cloud1(void)
{
	//circle1
	glBegin(GL_LINES);
	//
		float radius = 0.1f;


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = radius * cos(angle);
			float y = (radius - 0.02) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
		glEnd();


	////circle2
	glTranslatef(0.124f, 0.10f, 0.0f);
	glBegin(GL_LINES);

		radius = 0.125f;


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)	// 2
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = radius * cos(angle);
			float y = (radius - 0.05) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
		glEnd();

	// Circle 3

	glTranslatef(0.08f, -0.08f, 0.0f);

	glBegin(GL_LINES);


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = radius * cos(angle);
			float y = (radius - 0.05) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
	glEnd();
}

void cloud2(void)
{
	//cloud2.1
	//glTranslatef(0.124f, -0.1f, 0.0f);
	float radius = 0.045;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.01) * cos(angle);
		float y = (radius + 0.012) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//cloud 2.2
	glTranslatef(0.124f, 0.011f, 0.0f);
	radius = 0.055;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.05) * cos(angle);
		float y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//circle3
	glTranslatef(-0.003f, -0.065f, 0.0f);
	radius = 0.035;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.053f) * cos(angle);
		float y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


}

void cloud3(void)
{

	//glTranslatef(-0.003f, -0.065f, 0.0f);
	float radius = 0.1;
	glBegin(GL_LINES);

		//circle 3.1
		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = (radius)*cos(angle);
			float y = (radius - 0.14) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
	glEnd();


	//circle 3.2

	glTranslatef(0.128f, 0.048f, 0.0f);
	glBegin(GL_LINES);


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = (radius + 0.01) * cos(angle);
			float y = (radius - 0.14) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
	glEnd();


	//circle 3.3
	glTranslatef(0.16f, -0.012f, 0.0f);
	glBegin(GL_LINES);


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = (radius)*cos(angle);
			float y = (radius - 0.14) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
	glEnd();


	//circle 3.4
	glTranslatef(-0.11f, -0.05f, 0.0f);
	glBegin(GL_LINES);


		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = (radius)*cos(angle);
			float y = (radius - 0.14) * sin(angle);
			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(x, y, 0.0f);

		}
	glEnd();

}

/************** Star Fish **************************/
void Star(void)
{

	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve3Points(Point*, Point*, Point*);
	void CollectPoints(Point*, Point*, Point*, Point*);
	void AssignPointsCollection(Point*, Point*);

	Point center = { 0.0f, 0.0f, 0.0f };
	Point p1 = {};
	Point p2 = {};
	Point p3 = {};

	Point parr_left_curve[1001];
	Point parr_right_curve[1001];



	// Start with five notches

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 1.0f, 0.0f);
		glVertex3f(-0.73f, 0.53f, 0.0f);
		glVertex3f(-0.61f, 0.8f, 0.0f);
		glVertex3f(-0.3f, 0.82f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 1.0f, 0.0f);
		glVertex3f(-0.654f, 0.7f, 0.0f);
		glVertex3f(-0.55f, 0.95f, 0.0f);
		glVertex3f(-0.40f, 0.51f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 1.0f, 0.0f);
		glVertex3f(-0.654f, 0.7f, 0.0f);
		glVertex3f(-0.79f, 0.8f, 0.0f);
		glVertex3f(-0.61f, 0.8f, 0.0f);
	glEnd();

	//small star

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 0.7f, 0.1f);
		glVertex3f(-0.653f, 0.62f, 0.0f);
		glVertex3f(-0.58f, 0.76f, 0.0f);
		glVertex3f(-0.43f, 0.77f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 0.7f, 0.1f);
		glVertex3f(-0.61f, 0.72f, 0.0f);
		glVertex3f(-0.56f, 0.85f, 0.0f);
		glVertex3f(-0.485f, 0.64f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glColor3f(0.9f, 0.7f, 0.1f);
		glVertex3f(-0.60f, 0.71f, 0.0f);
		glVertex3f(-0.67f, 0.76f, 0.0f);
		glVertex3f(-0.59f, 0.765f, 0.0f);
	glEnd();

	// smile in start

	//lip
	glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 0.5f, 0.0f);
		assignPoint(&p1, -0.595f, 0.72f, 0.0f);
		assignPoint(&p2, -0.565f, 0.67f, 0.0f);
		assignPoint(&p3, -0.535f, 0.72f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();

	//lip
	glBegin(GL_LINE_STRIP);
		glColor3f(1.0f, 0.5f, 0.0f);
		assignPoint(&p1, -0.595f, 0.72f, 0.0f);
		assignPoint(&p2, -0.565f, 0.69f, 0.0f);
		assignPoint(&p3, -0.535f, 0.72f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
	glEnd();


	//eye

	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, -0.582f, 0.75f, 0.0f);
		circle2D(&center, 0.008f, 0.0f, 360.0f);
	glEnd();

	// eye
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, -0.542f, 0.75f, 0.0f);
		circle2D(&center, 0.008f, 0.0f, 360.0f);
	glEnd();

}

/***************** Tree and Grass ***********************/

void Tree(void)
{
	
		// Function Declarations
		void assignPoint(Point*, float, float, float);
		void circle2D(Point*, float, float, float);
		void bezierCurve3Points(Point*, Point*, Point*);
		void CollectPoints(Point*, Point*, Point*, Point*);
		void AssignPointsCollection(Point*, Point*);

		Point center = { 0.0f, 0.0f, 0.0f };
		Point p1 = {};
		Point p2 = {};
		Point p3 = {};

		Point parr_left_curve[1001];
		Point parr_right_curve[1001];



		// tree two (right tilted)

		assignPoint(&p1, -0.8f, -0.7f, 0.0f);
		assignPoint(&p2, -0.75f, -0.1f, 0.0f);
		assignPoint(&p3, -0.35f, 0.26f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.64f, -0.7f, 0.0f);
		assignPoint(&p2, -0.66f, -0.1f, 0.0f);
		assignPoint(&p3, -0.30f, 0.25f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.62f, 0.27f, 0.02);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		// Tree  one (left tilted)

		assignPoint(&p1, -0.7f, -0.7f, 0.0f);
		assignPoint(&p2, -0.5f, 0.0f, 0.0f);
		assignPoint(&p3, -0.7f, 0.3f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.5f, -0.7f, 0.0f);
		assignPoint(&p2, -0.4f, 0.0f, 0.0f);
		assignPoint(&p3, -0.65f, 0.3f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.62f, 0.2f, 0.04);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		// Tree Two (right tilted) leaves

		// Leaf 1
		assignPoint(&p1, -0.36f, 0.26f, 0.0f);
		assignPoint(&p2, -0.48f, 0.50f, 0.0f);
		assignPoint(&p3, -0.62f, 0.38f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.38f, 0.24f, 0.0f);
		assignPoint(&p2, -0.50f, 0.355f, 0.0f);
		assignPoint(&p3, -0.62f, 0.38f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.37f, 0.25f, 0.0f);
		assignPoint(&p2, -0.50f, 0.44f, 0.0f);
		assignPoint(&p3, -0.62f, 0.38f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// Leaf 2

		assignPoint(&p1, -0.35f, 0.26f, 0.0f);
		assignPoint(&p2, -0.4f, 0.46f, 0.0f);
		assignPoint(&p3, -0.55f, 0.55f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.32f, 0.26f, 0.0f);
		assignPoint(&p2, -0.4f, 0.60f, 0.0f);
		assignPoint(&p3, -0.55f, 0.55f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.335f, 0.264f, 0.0f);
		assignPoint(&p2, -0.402f, 0.55f, 0.0f);
		assignPoint(&p3, -0.55f, 0.55f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// Leaf 3
		assignPoint(&p1, -0.32f, 0.26f, 0.0f);
		assignPoint(&p2, -0.38f, 0.46f, 0.0f);
		assignPoint(&p3, -0.324f, 0.58f, -0.5f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.30f, 0.26f, 0.0f);
		assignPoint(&p2, -0.30f, 0.43f, 0.0f);
		assignPoint(&p3, -0.324f, 0.58f, -0.5f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.308f, 0.26f, 0.0f);
		assignPoint(&p2, -0.35f, 0.45f, 0.0f);
		assignPoint(&p3, -0.324f, 0.58f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();



		// leaf 4
		assignPoint(&p1, -0.31f, 0.26f, 0.0f);
		assignPoint(&p2, -0.26f, 0.50f, 0.0f);
		assignPoint(&p3, -0.12f, 0.48f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.30f, 0.26f, 0.0f);
		assignPoint(&p2, -0.24f, 0.40f, 0.0f);
		assignPoint(&p3, -0.12f, 0.48f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);


		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.308f, 0.26f, 0.0f);
		assignPoint(&p2, -0.26f, 0.43f, 0.0f);
		assignPoint(&p3, -0.12f, 0.48f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// leaf 5

		assignPoint(&p1, -0.30f, 0.26f, 0.0f);
		assignPoint(&p2, -0.23f, 0.39f, 0.0f);
		assignPoint(&p3, -0.12f, 0.23f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.294f, 0.25f, 0.0f);
		assignPoint(&p2, -0.23f, 0.275f, 0.0f);
		assignPoint(&p3, -0.12f, 0.23f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.295f, 0.26f, 0.0f);
		assignPoint(&p2, -0.24f, 0.32f, 0.0f);
		assignPoint(&p3, -0.12f, 0.23f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// leaf 6


		assignPoint(&p1, -0.31f, 0.26f, 0.0f);
		assignPoint(&p2, -0.20f, 0.24f, 0.0f);
		assignPoint(&p3, -0.15f, 0.1f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.31f, 0.22f, 0.0f);
		assignPoint(&p2, -0.20f, 0.20f, 0.0f);
		assignPoint(&p3, -0.15f, 0.1f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.31f, 0.24f, 0.0f);
		assignPoint(&p2, -0.20f, 0.21f, 0.0f);
		assignPoint(&p3, -0.15f, 0.1f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();


		// // Tree One (left tilted ) Leaves

		// leaf 0

		assignPoint(&p1, -0.65f, 0.3f, 0.0f);
		assignPoint(&p2, -0.7f, 0.35f, 0.0f);
		assignPoint(&p3, -0.85f, 0.19f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.65f, 0.28f, 0.0f);
		assignPoint(&p2, -0.68f, 0.25f, 0.0f);
		assignPoint(&p3, -0.85f, 0.19f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.65f, 0.29f, 0.0f);
		assignPoint(&p2, -0.69f, 0.30f, 0.0f);
		assignPoint(&p3, -0.85f, 0.19f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();


		// leaf 1
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.68f, 0.26f, 0.0f);
		assignPoint(&p2, -0.73f, 0.46f, 0.0f);
		assignPoint(&p3, -0.87f, 0.55f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);
		glEnd();


		assignPoint(&p1, -0.65f, 0.26f, 0.0f);
		assignPoint(&p2, -0.73f, 0.60f, 0.0f);
		assignPoint(&p3, -0.88f, 0.55f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.665f, 0.264f, 0.0f);
		assignPoint(&p2, -0.732f, 0.55f, 0.0f);
		assignPoint(&p3, -0.88f, 0.55f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// leaf 2
		assignPoint(&p1, -0.67f, 0.27f, 0.0f);
		assignPoint(&p2, -0.70f, 0.47f, 0.0f);
		assignPoint(&p3, -0.714f, 0.59f, -0.5f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);

		assignPoint(&p1, -0.65f, 0.27f, 0.0f);
		assignPoint(&p2, -0.63f, 0.44f, 0.0f);
		assignPoint(&p3, -0.714f, 0.59f, -0.5f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.66f, 0.27f, 0.0f);
		assignPoint(&p2, -0.68f, 0.46f, 0.0f);
		assignPoint(&p3, -0.714f, 0.59f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// leaf3
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.66f, 0.31f, 0.0f);
		assignPoint(&p2, -0.61f, 0.55f, 0.0f);
		assignPoint(&p3, -0.47f, 0.53f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.65f, 0.31f, 0.0f);
		assignPoint(&p2, -0.59f, 0.45f, 0.0f);
		assignPoint(&p3, -0.47f, 0.53f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.658f, 0.31f, 0.0f);
		assignPoint(&p2, -0.61f, 0.48f, 0.0f);
		assignPoint(&p3, -0.47f, 0.53f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();

		// leaf 4

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.650f, 0.32f, 0.0f);
		assignPoint(&p2, -0.58f, 0.42f, 0.0f);
		assignPoint(&p3, -0.46f, 0.26f, 0.0f);
		CollectPoints(parr_left_curve, &p1, &p2, &p3);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.644f, 0.27f, 0.0f);
		assignPoint(&p2, -0.58f, 0.325f, 0.0f);
		assignPoint(&p3, -0.46f, 0.26f, 0.0f);
		CollectPoints(parr_right_curve, &p1, &p2, &p3);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(0.6f, 1.0f, 0.1);
		AssignPointsCollection(parr_left_curve, parr_right_curve);
		glEnd();

		glBegin(GL_LINES);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p1, -0.645f, 0.30f, 0.0f);
		assignPoint(&p2, -0.59f, 0.36f, 0.0f);
		assignPoint(&p3, -0.46f, 0.26f, 0.0f);
		bezierCurve3Points(&p1, &p2, &p3);
		glEnd();


		// Coconuts ( Tree one )
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.72f, 0.3f, 0.0f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.68f, 0.28f, 0.0f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.64f, 0.3f, 0.0f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.71f, 0.34f, 0.0f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.67f, 0.32f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.666f, 0.35f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();



		//coconuts tree 2
		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.377f, 0.27f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.36f, 0.23f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.3f, 0.229f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.33f, 0.19f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.33f, 0.302f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.301f, 0.28f, 0.0f);
		circle2D(&center, 0.021f, 0.0f, 360.0f);
		glEnd();

		glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.0f, 0.7f, 0.01f);
		assignPoint(&center, -0.32f, 0.26f, 0.0f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
		glEnd();



		//Grass
		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.8f, -0.7f, 0.0f);
		glVertex3f(-0.78f, -0.55f, 0.0f);
		glVertex3f(-0.77f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.77f, -0.7f, 0.0f);
		glVertex3f(-0.767f, -0.44f, 0.0f);
		glVertex3f(-0.76f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.765f, -0.7f, 0.0f);
		glVertex3f(-0.75f, -0.35f, 0.0f);
		glVertex3f(-0.74f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.767f, -0.65f, 0.0f);
		glVertex3f(-0.740f, -0.35f, 0.0f);
		glVertex3f(-0.758f, -0.67f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.655f, -0.7f, 0.0f);
		glVertex3f(-0.660f, -0.36f, 0.0f);
		glVertex3f(-0.670f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.665f, -0.7f, 0.0f);
		glVertex3f(-0.680f, -0.377f, 0.0f);
		glVertex3f(-0.670f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.550f, -0.7f, 0.0f);
		glVertex3f(-0.695f, -0.32f, 0.0f);
		glVertex3f(-0.580f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.680f, -0.7f, 0.0f);
		glVertex3f(-0.730f, -0.39f, 0.0f);
		glVertex3f(-0.760f, -0.3f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.740f, -0.7f, 0.0f);
		glVertex3f(-0.720f, -0.35f, 0.0f);
		glVertex3f(-0.710f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.64f, -0.7f, 0.0f);
		glVertex3f(-0.6f, -0.3f, 0.0f);
		glVertex3f(-0.58f, -0.7f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.63f, -0.66f, 0.0f);
		glVertex3f(-0.65f, -0.3f, 0.0f);
		glVertex3f(-0.61f, -0.63f, 0.0f);
		glEnd();

		//Grass
		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.7f, -0.75f, 0.0f);
		glVertex3f(-0.68f, -0.6f, 0.0f);
		glVertex3f(-0.67f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.67f, -0.75f, 0.0f);
		glVertex3f(-0.667f, -0.49f, 0.0f);
		glVertex3f(-0.66f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.665f, -0.75f, 0.0f);
		glVertex3f(-0.65f, -0.4f, 0.0f);
		glVertex3f(-0.64f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.667f, -0.7f, 0.0f);
		glVertex3f(-0.640f, -0.4f, 0.0f);
		glVertex3f(-0.658f, -0.72f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.555f, -0.75f, 0.0f);
		glVertex3f(-0.560f, -0.41f, 0.0f);
		glVertex3f(-0.570f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.565f, -0.75f, 0.0f);
		glVertex3f(-0.580f, -0.427f, 0.0f);
		glVertex3f(-0.570f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.450f, -0.75f, 0.0f);
		glVertex3f(-0.595f, -0.37f, 0.0f);
		glVertex3f(-0.480f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.580f, -0.75f, 0.0f);
		glVertex3f(-0.630f, -0.44f, 0.0f);
		glVertex3f(-0.660f, -0.35f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.640f, -0.75f, 0.0f);
		glVertex3f(-0.620f, -0.4f, 0.0f);
		glVertex3f(-0.610f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.54f, -0.75f, 0.0f);
		glVertex3f(-0.5f, -0.35f, 0.0f);
		glVertex3f(-0.48f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.53f, -0.71f, 0.0f);
		glVertex3f(-0.55f, -0.35f, 0.0f);
		glVertex3f(-0.51f, -0.68f, 0.0f);
		glEnd();

		//Grass
		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.85f, -0.75f, 0.0f);
		glVertex3f(-0.83f, -0.6f, 0.0f);
		glVertex3f(-0.82f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.82f, -0.75f, 0.0f);
		glVertex3f(-0.817f, -0.49f, 0.0f);
		glVertex3f(-0.81f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.815f, -0.75f, 0.0f);
		glVertex3f(-0.8f, -0.4f, 0.0f);
		glVertex3f(-0.79f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.817f, -0.7f, 0.0f);
		glVertex3f(-0.790f, -0.4f, 0.0f);
		glVertex3f(-0.808f, -0.72f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.705f, -0.75f, 0.0f);
		glVertex3f(-0.710f, -0.41f, 0.0f);
		glVertex3f(-0.720f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.715f, -0.75f, 0.0f);
		glVertex3f(-0.730f, -0.427f, 0.0f);
		glVertex3f(-0.720f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.60f, -0.75f, 0.0f);
		glVertex3f(-0.745f, -0.37f, 0.0f);
		glVertex3f(-0.630f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.530f, -0.75f, 0.0f);
		glVertex3f(-0.580f, -0.44f, 0.0f);
		glVertex3f(-0.610f, -0.35f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.590f, -0.75f, 0.0f);
		glVertex3f(-0.570f, -0.4f, 0.0f);
		glVertex3f(-0.560f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.49f, -0.75f, 0.0f);
		glVertex3f(-0.45f, -0.35f, 0.0f);
		glVertex3f(-0.43f, -0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		glColor3f(0.4f, 0.5f, 0.0f);
		glVertex3f(-0.48f, -0.71f, 0.0f);
		glVertex3f(-0.5f, -0.35f, 0.0f);
		glVertex3f(-0.46f, -0.68f, 0.0f);
		glEnd();
}
