// Header files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.14159265359

//Wave Animation variable y
float y = -0.25f;
float alpha = 0.0f;

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

typedef struct Color {
	float r;
	float g; 
	float b;
} Color;

// Global Functions Declarations
void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void HexColorToFloatColor(char color[], Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void ellipse2D(Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (xRadius * sinf((angle * PI) / 180.0f)), center->y + (yRadius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void BSplineCurve(int noOfPoints, Point arr[]) {

	if (noOfPoints < 3)
		return;

	if (noOfPoints % 3 == 0)
		for (int i = 0; i <= noOfPoints; i += 3)
			bezierCurve3Points(&arr[i], &arr[i+1], &arr[i+2]);
	else if(noOfPoints % 4 == 0)
		for (int i = 0; i <= noOfPoints; i += 4)
			bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i+3]);
	else if (noOfPoints % 5 == 0)
		for (int i = 0; i <= noOfPoints; i += 5)
			bezierCurve5Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3], &arr[i+4]);
	else if (noOfPoints % 3 == 1) {
		int i = 0;
		for(; i <= noOfPoints-4; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
	else if (noOfPoints % 3 == 2) {
		int i = 0;
		for (; i <= noOfPoints - 5; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
}


// Global Variables Declarations
bool bIsFullScreen = false;


// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Birthday Demo : Room Interior");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void Dongar(void)
{

	// Dongar 2
	glBegin(GL_POLYGON); // Second Dongar
	glColor3f(0.40f, 0.71f, 0.44f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.50f * (1.0f - u) * (1.0f - u)) + (-0.25f * 2.0f * (1.0f - u) * u) + (-0.17f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.17f * (1.0f - u) * (1.0f - u)) + (-0.00f * 2.0f * (1.0f - u) * u) + (0.30f * u * u);
		float y = (0.40f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.30f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.50f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();




	glBegin(GL_POLYGON); // first Dongar
	glColor3f(0.04f, 0.54f, 0.23f);

	// p1 = -1.0f, 0.10f, 0.0f
	// p2 = -0.80f, 0.77f, 0.0f
	// p3 = -0.67f, 1.450f, 0.0f

	// Dongar 1
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-1.0f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.67f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.20f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.67f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Dongar 4
	glBegin(GL_POLYGON); // Fourth Dongar
	glColor3f(0.40f, 0.71f, 0.44f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.60f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.87f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.87f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (1.40f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.00f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.40f * (1.0f - u) * (1.0f - u)) + (1.40f * 2.0f * (1.0f - u) * u) + (0.60f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Dongar 3
	glBegin(GL_POLYGON); // Third Dongar
	glColor3f(0.04f, 0.54f, 0.23f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.0f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.27f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.27f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (0.80f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.80f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.0f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void oliMati1(void)
{
	//glColor3f(0.7f, 0.3f, 0.3f);

	glBegin(GL_POLYGON); // First Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	//glColor3f(0.7f, 0.3f, 0.3f);
	glColor3f(0.5725f, 0.3921f, 0.1058f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void oliMati2(void)
{
	glBegin(GL_POLYGON); // Second Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	//glColor3f(0.7f, 0.3f, 0.3f);
	glColor3f(0.5725f, 0.3921f, 0.1058f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}
void oliMati3(void)
{
	glBegin(GL_POLYGON); // Second Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	//glColor3f(0.7f, 0.3f, 0.3f);
	glColor3f(0.5725f, 0.3921f, 0.1058f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void Wave1(void)
{


	glBegin(GL_POLYGON); // First Wave
	glColor3f(1.0f, 1.0f, 1.0f);
		
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.8f, 0.9f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void Wave2(void)
{
	glBegin(GL_POLYGON); // First Wave
	glColor3f(1.0f, 1.0f, 1.0f);


	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
	glColor3f(0.0f, 0.8f, 0.9f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}
void Wave3(void)
{
	glBegin(GL_POLYGON); // First Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(1.0f, 1.0f, 1.0f);


	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
	glColor3f(0.0f, 0.8f, 0.9f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void TryLearn(void)
{
	/*glColor3f(1.0f, 1.0f, 1.0f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-1.0f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	*/
	/*for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (-0.33f * (1.0f - u) * (1.0f - u)) + (-0.33f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	*/
	//Circle
	//glBegin(GL_LINES);
	float x, y;
	float radius = 0.50f;

	glColor3f(0.0f, 0.8f, 0.9f);
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glRotatef(60, 0.1f, 0.0f, 0.0f);


	//Top Circle
	x = (float)radius * cos(359 * PI / 180.0f);
	y = (float)radius * sin(359 * PI / 180.0f);
	for (int j = 0; j < 180; j++)
	{
		glVertex2f(x, y);
		x = (float)radius * cos(j * PI / 180.0f);
		y = (float)radius * sin(j * PI / 180.0f);
		glVertex2f(x, y);
	}
	glEnd();

}

void display(void)
{
	// function declarations
	void Rupali_Husband_Canvas(void);
	void SecondScreen(void);
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Rupali_Husband_Canvas();
	SecondScreen();
	
	glutSwapBuffers();
}


void SecondScreen(void)
{
	//Function declarations
	void Dongar(void);
	void oliMati1(void);
	void oliMati2(void);
	void oliMati3(void);
	void Wave1(void);
	void Wave2(void);
	void Wave3(void);
	void Rupali_Husband_Canvas(void);

	// Variable Declarations
	Color c;

	// Kinara
	HexColorToFloatColor("C58940", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Sea water
		glColor3f(0.0f, 0.8f, 0.9f);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
		glColor3f(0.91f, 0.40f, 0.20f);
		glVertex3f(-1.0f, 0.350f, 0.0f);
		glVertex3f(1.0f, 0.350f, 0.0f);
	glEnd();


	glBegin(GL_QUADS); // SKY
		glColor3f(0.6901f, 0.08f, 0.0f);
		glVertex3f(-1.0f, 0.10f, 0.0f);	//A

		glColor3f(0.6901f, 0.14f, 0.0f);
		glVertex3f(1.0f, 0.10f, 0.0f); //B

		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C
	
		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.9607f, 0.450f, 0.04f);
		glVertex3f(1.0f, 0.5f, 0.0f);	//C

		glColor3f(0.9647f, 0.6550f, 0.04f);
		glVertex3f(-1.0f, 0.5f, 0.0f);	//D

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(-1.0f, 1.0f, 0.0f);	//E

		glColor3f(1.0f, 0.8f, 0.4f);
		glVertex3f(1.0f, 1.0f, 0.0f);	//F
	glEnd();

	//Sun
	glPushMatrix();

		glTranslatef(0.02f, 0.215f, 0.0f);

		glBegin(GL_LINES);

		float outerradius = 0.1f;

		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		
		{

			glColor3f(0.96f, 0.78f, 0.39f);
			glVertex3f(0.0f, 0.0f, 0.0f);

			float x = (outerradius - 0.01) * cos(angle);
			float y = (outerradius + 0.018) * sin(angle);
			glColor3f(0.96f, 0.78f, 0.39f);

			glVertex3f(x, y, 0.0f);

		}
		glEnd();
		glBegin(GL_LINES);

			float radius = 0.07f;

			for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)	
			{
		
				glColor3f(0.94f, 0.95f, 0.23f);
				glVertex3f(0.0f, 0.0f, 0.0f);

				float x = (radius - 0.01) * cos(angle);
				float y = (radius+0.018f)*sin(angle);
				glColor3f(0.90f, 0.25f, 0.19f);

				glVertex3f(x, y, 0.0f);

			}
		glEnd();

	

	glPopMatrix();

	// Function Call
	Dongar();
	oliMati1();
	oliMati2();
	oliMati3();

	
	
	glPushMatrix();
	
	glTranslatef(0.0f, y, 0.0f);

	Wave1();
	Wave2();
	Wave3();
	glPopMatrix();

	if (y >= -0.30f)
	{
		y -= 0.0002f;
	}
	else
	{
		y += 0.05f;
	}

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	Rupali_Husband_Canvas();
	glPopMatrix();

	glutPostRedisplay();
}


void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}


void Rupali_Husband_Canvas() {

	// Function Declarations
	void Canvas(void);
	void Rupali(void);
	void Husband(void);

	// Code
	
	/*glPushMatrix();
		glScalef(1.2f, 1.5f, 0.0f);
		glTranslatef(-0.5f, 0.25f, 0.0f);
		Canvas();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.07f, 0.35f, 0.0f);
		glScalef(0.9f, 1.05f, 0.0f);
		Rupali();
	glPopMatrix();
	
	glPushMatrix();
		glTranslatef(0.0f, 0.45f, 0.0f);
		glScalef(0.8f, 0.8f, 0.0f);
		Husband();
	glPopMatrix();
	*/




	glPushMatrix();
	glScalef(0.6f, 0.72f, 0.0f);
	glTranslatef(0.3f, -0.50f, 0.0f);
	Canvas();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.42f, -0.311f, 0.0f);
	glScalef(0.5f, 0.55f, 0.0f);
	Rupali();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.452f, -0.26f, 0.0f);
	glScalef(0.42f, 0.41f, 0.0f);
	Husband();
	glPopMatrix();





}

void Canvas() {

	Color c;

	// Back Foot
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.68f, 0.2f, 0.0f);
		glVertex3f(0.78f, 0.2f, 0.0f);
		glVertex3f(0.94f, -0.63f, 0.0f);
		glVertex3f(0.89f, -0.62f, 0.0f);
	glEnd();

	// Left Foot
	HexColorToFloatColor("765827", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.66f, -0.72f, 0.0f);
		glVertex3f(0.61f, -0.71f, 0.0f);
	glEnd();

	// Right Foot
	glBegin(GL_QUADS);
		glVertex3f(0.71f, 0.2633f, 0.0f);
		glVertex3f(0.81f, 0.2466f, 0.0f);
		glVertex3f(0.88f, -0.77f, 0.0f);
		glVertex3f(0.83f, -0.76f, 0.0f);
	glEnd();

	// Main Frame Of Canvas
	glBegin(GL_QUADS);
		glColor3f(0.9f, 0.9f, 0.9f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// side-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// top-width of main frame
	glBegin(GL_QUADS);
		glColor3f(0.75f, 0.75f, 0.75f);
		glVertex3f(0.9f, 0.2f, 0.0f);
		glVertex3f(0.6f, 0.25f, 0.0f);
		glVertex3f(0.61f, 0.26f, 0.0f);
		glVertex3f(0.91f, 0.21f, 0.0f);
	glEnd();

	// Base of Main Frame Of Canvas
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.6f, -0.3f, 0.0f);
		glVertex3f(0.6f, -0.35f, 0.0f);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
	glEnd();

	// Side Base of Main Frame Of Canvas
	HexColorToFloatColor("3F2305", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.9f, -0.4f, 0.0f);
		glVertex3f(0.9f, -0.35f, 0.0f);
		glVertex3f(0.91f, -0.34f, 0.0f);
		glVertex3f(0.91f, -0.39f, 0.0f);
	glEnd();

}

/*void Rupali() {

	// Variable Declarations
	Color c;
	Point p[5];


	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.15f, 0.0f, 0.0f);
		glVertex3f(-0.13f, -0.25f, 0.0f);
		glVertex3f(-0.1f, -0.33f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();

	// Right Hand
	glBegin(GL_QUADS);
		glVertex3f(0.15f, 0.0f, 0.0f);
		glVertex3f(0.13f, -0.23f, 0.0f);
		glVertex3f(0.1f, -0.25f, 0.0f);
		glVertex3f(0.1f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.1f, -0.22f, 0.0f);
		assignPoint(&p[1], 0.2f, 0.05f, 0.0f);
		assignPoint(&p[2], 0.25f, -0.02f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.23f, -0.05f, 0.0f);
	
		assignPoint(&p[0], 0.25f, -0.02f, 0.0f);
		assignPoint(&p[1], 0.2f, 0.0f, 0.0f);
		assignPoint(&p[2], 0.15f, -0.3f, 0.0f);
		assignPoint(&p[3], 0.1f, -0.25f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	glEnd();

	// Paint Slate
	assignPoint(&p[0], 0.25f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(0.8f, 0.8f, 0.8f);
		ellipse2D(&p[0], 0.08f, 0.07f, 180.0f, 360.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.25f, 0.07f, 0.0f);
		assignPoint(&p[1], 0.32f, 0.04f, 0.0f);
		assignPoint(&p[2], 0.22f, 0.01f, 0.0f);
		assignPoint(&p[3], 0.37f, -0.02f, 0.0f);
		assignPoint(&p[4], 0.25f, -0.07f, 0.0f);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Colors On Paint Slate
	assignPoint(&p[0], 0.25f, 0.04f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(1.0f, 0.0f, 0.0f);
		ellipse2D(&p[0], 0.02f, 0.015f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p[0], 0.2f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 1.0f, 0.0f);
		ellipse2D(&p[0], 0.02f, 0.015f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p[0], 0.25f, -0.04f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 1.0f);
		ellipse2D(&p[0], 0.02f, 0.015f, 0.0f, 360.0f);
	glEnd();

	// Shoulder
	HexColorToFloatColor("EA1189", &c);
	assignPoint(&p[0], 0.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(p[0].x, p[0].y, p[0].z);
		circle2D(&p[0], 0.16f, 260.0f, 460.0f);
		glVertex3f(p[0].x, p[0].y, p[0].z);
	glEnd();

	// Shirt
	glBegin(GL_QUADS);
		glVertex3f(0.1f, 0.05f, 0.0f);
		glVertex3f(-0.1f, 0.05f, 0.0f);
		glVertex3f(-0.1f, -0.3f, 0.0f);
		glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();

	// Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);

		assignPoint(&p[0], -0.13f, -0.14f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.09f, 0.0f);
		assignPoint(&p[2], -0.2f, 0.43f, 0.0f);
		assignPoint(&p[3], 0.0f, 0.45f, 0.0f);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.0f, -0.15f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	
		assignPoint(&p[0], 0.0f, 0.45f, 0.0f);
		assignPoint(&p[1], 0.2f, 0.43f, 0.0f);
		assignPoint(&p[2], 0.0f, -0.09f, 0.0f);
		assignPoint(&p[3], 0.13f, -0.14f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	glEnd();

	// Hairs Tail Curve
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.13f, -0.14f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.23f, 0.0f);
		assignPoint(&p[2], -0.13f, -0.14f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.07f, -1.1f, 0.0f);

		assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.01f, -1.16f, 0.0f);
		assignPoint(&p[2], 0.02f, -1.17f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.08f, -1.17f, 0.0f);

		assignPoint(&p[0], 0.08f, -1.17f, 0.0f);
		assignPoint(&p[1], 0.09f, -1.16f, 0.0f);
		assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.09f, -0.75f, 0.0f);

		assignPoint(&p[0], 0.01f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
		assignPoint(&p[2], 0.03f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.05f, -1.12f, 0.0f);
		assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.07f, -1.1f, 0.0f);
		assignPoint(&p[1], 0.1f, -0.88f, 0.0f);
		assignPoint(&p[2], 0.09f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.07f, -1.1f, 0.0f);

		assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.01f, -1.16f, 0.0f);
		assignPoint(&p[2], -0.02f, -1.17f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(-0.08f, -1.17f, 0.0f);

		assignPoint(&p[0], -0.08f, -1.17f, 0.0f);
		assignPoint(&p[1], -0.09f, -1.16f, 0.0f);
		assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.09f, -0.75f, 0.0f);

		assignPoint(&p[0], -0.01f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
		assignPoint(&p[2], -0.03f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.05f, -1.12f, 0.0f);
		assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.07f, -1.1f, 0.0f);
		assignPoint(&p[1], -0.1f, -0.88f, 0.0f);
		assignPoint(&p[2], -0.09f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock
	HexColorToFloatColor("EA1189", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.1f, -0.3f, 0.0f);

		assignPoint(&p[0], -0.1f, -0.3f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.5f, 0.0f);
		assignPoint(&p[2], -0.1f, -0.75f, 0.0f); 
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.1f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.0f, -0.77f, 0.0f);
		assignPoint(&p[2], 0.1f, -0.75f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], 0.1f, -0.75f, 0.0f);
		assignPoint(&p[1], 0.13f, -0.5f, 0.0f);
		assignPoint(&p[2], 0.1f, -0.3f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

}*/


void Rupali() {

	// Variable Declarations
	Color c;
	Point p[5];


	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.13f, -0.25f, 0.0f);
	glVertex3f(-0.1f, -0.33f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();

	// Paint Brush Head
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.215f, 0.03f, 0.0f);
	glVertex3f(0.23f, 0.08f, 0.0f);
	glVertex3f(0.24f, 0.07f, 0.0f);
	glVertex3f(0.225f, 0.0f, 0.0f);
	glEnd();
	HexColorToFloatColor("DFA878", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], 0.23f, 0.08f, 0.0f);
	assignPoint(&p[1], 0.222f, 0.098f, 0.0f);
	assignPoint(&p[2], 0.24f, 0.1f, 0.0f);
	assignPoint(&p[3], 0.245f, 0.12f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	assignPoint(&p[0], 0.245f, 0.12f, 0.0f);
	assignPoint(&p[1], 0.255f, 0.08f, 0.0f);
	assignPoint(&p[2], 0.24f, 0.07f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(0.13f, -0.23f, 0.0f);
	glVertex3f(0.1f, -0.25f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
	assignPoint(&p[0], 0.1f, -0.22f, 0.0f);
	assignPoint(&p[1], 0.12f, -0.12f, 0.0f);
	assignPoint(&p[2], 0.2f, -0.02f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
	assignPoint(&p[1], 0.17f, -0.23f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.25f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);		// Hand
	assignPoint(&p[0], 0.22f, -0.06f, 0.0f);
	assignPoint(&p[1], 0.26f, 0.01f, 0.0f);
	assignPoint(&p[2], 0.22f, 0.04f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.225f, 0.03f, 0.0f);
	assignPoint(&p[1], 0.19f, 0.02f, 0.0f);
	assignPoint(&p[2], 0.19f, -0.03f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Paint Brush Stack
	HexColorToFloatColor("6C3428", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	assignPoint(&p[0], 0.215f, 0.035f, 0.0f);
	assignPoint(&p[1], 0.17f, -0.2f, 0.0f);
	assignPoint(&p[2], 0.225f, 0.005f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shoulder
	HexColorToFloatColor("EA1189", &c);
	assignPoint(&p[0], 0.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(p[0].x, p[0].y, p[0].z);
	circle2D(&p[0], 0.16f, 260.0f, 460.0f);
	glVertex3f(p[0].x, p[0].y, p[0].z);
	glEnd();

	// Shirt
	glBegin(GL_QUADS);
	glVertex3f(0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();

	// Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);

	assignPoint(&p[0], -0.13f, -0.14f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.09f, 0.0f);
	assignPoint(&p[2], -0.2f, 0.43f, 0.0f);
	assignPoint(&p[3], 0.0f, 0.45f, 0.0f);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.0f, -0.15f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	assignPoint(&p[0], 0.0f, 0.45f, 0.0f);
	assignPoint(&p[1], 0.2f, 0.43f, 0.0f);
	assignPoint(&p[2], 0.0f, -0.09f, 0.0f);
	assignPoint(&p[3], 0.13f, -0.14f, 0.0f);
	bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);
	glEnd();

	// Hairs Tail Curve
	glBegin(GL_POLYGON);
	assignPoint(&p[0], 0.13f, -0.14f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.23f, 0.0f);
	assignPoint(&p[2], -0.13f, -0.14f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.07f, -1.1f, 0.0f);

	assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.01f, -1.16f, 0.0f);
	assignPoint(&p[2], 0.02f, -1.17f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	glVertex3f(0.08f, -1.17f, 0.0f);

	assignPoint(&p[0], 0.08f, -1.17f, 0.0f);
	assignPoint(&p[1], 0.09f, -1.16f, 0.0f);
	assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Left Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.09f, -0.75f, 0.0f);

	assignPoint(&p[0], 0.01f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
	assignPoint(&p[2], 0.03f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.05f, -1.12f, 0.0f);
	assignPoint(&p[2], 0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.07f, -1.1f, 0.0f);
	assignPoint(&p[1], 0.1f, -0.88f, 0.0f);
	assignPoint(&p[2], 0.09f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Shoe
	HexColorToFloatColor("FF0060", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.07f, -1.1f, 0.0f);

	assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.01f, -1.16f, 0.0f);
	assignPoint(&p[2], -0.02f, -1.17f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	glVertex3f(-0.08f, -1.17f, 0.0f);

	assignPoint(&p[0], -0.08f, -1.17f, 0.0f);
	assignPoint(&p[1], -0.09f, -1.16f, 0.0f);
	assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(-0.09f, -0.75f, 0.0f);

	assignPoint(&p[0], -0.01f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.88f, 0.0f);
	assignPoint(&p[2], -0.03f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.03f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.05f, -1.12f, 0.0f);
	assignPoint(&p[2], -0.07f, -1.1f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.07f, -1.1f, 0.0f);
	assignPoint(&p[1], -0.1f, -0.88f, 0.0f);
	assignPoint(&p[2], -0.09f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock
	HexColorToFloatColor("EA1189", &c);
	glBegin(GL_POLYGON);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.1f, -0.3f, 0.0f);

	assignPoint(&p[0], -0.1f, -0.3f, 0.0f);
	assignPoint(&p[1], -0.15f, -0.5f, 0.0f);
	assignPoint(&p[2], -0.1f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], -0.1f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.0f, -0.77f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.75f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);

	assignPoint(&p[0], 0.1f, -0.75f, 0.0f);
	assignPoint(&p[1], 0.15f, -0.5f, 0.0f);
	assignPoint(&p[2], 0.1f, -0.3f, 0.0f);
	bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Frock Ribbon
	HexColorToFloatColor("C51605", &c);
	glBegin(GL_QUADS);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.1f, -0.28f, 0.0f);
	glVertex3f(-0.1f, -0.28f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(0.1f, -0.3f, 0.0f);
	glEnd();
}

void Husband() {

	// Variable Declarations
	Color c;
	Point p[5];

	// Left Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.56f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.58f, -0.75f, 0.0f);
		assignPoint(&p[2], -0.55f, -1.2f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.45f, -1.2f, 0.0f);
		glVertex3f(-0.4f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(-0.45f, -1.2f, 0.0f);
		glVertex3f(-0.465f, -1.6f, 0.0f);
		glVertex3f(-0.535f, -1.6f, 0.0f);
		glVertex3f(-0.55f, -1.2f, 0.0f);
	glEnd();

	// Left Shoe
	glBegin(GL_POLYGON);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&p[0], -0.53f, -1.6f, 0.0f);
		assignPoint(&p[1], -0.56f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.53f, -1.67f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.47f, -1.67f, 0.0f);
		assignPoint(&p[1], -0.44f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.47f, -1.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Leg
	HexColorToFloatColor("4477CE", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.285f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.27f, -0.7f, 0.0f);
		assignPoint(&p[2], -0.29f, -1.2f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.39f, -1.2f, 0.0f);
		glVertex3f(-0.45f, -0.6f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(-0.29f, -1.2f, 0.0f);
		glVertex3f(-0.305f, -1.6f, 0.0f);
		glVertex3f(-0.375f, -1.6f, 0.0f);
		glVertex3f(-0.39f, -1.2f, 0.0f);
	glEnd();

	// Right Shoe
	glBegin(GL_POLYGON);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&p[0], -0.37f, -1.6f, 0.0f);
		assignPoint(&p[1], -0.4f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.37f, -1.67f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		assignPoint(&p[0], -0.31f, -1.67f, 0.0f);
		assignPoint(&p[1], -0.28f, -1.66f, 0.0f);
		assignPoint(&p[2], -0.31f, -1.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Shirt
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.425f, 0.14f, 0.0f);
		glVertex3f(-0.57f, 0.1f, 0.0f);
		assignPoint(&p[0], -0.57f, -0.6f, 0.0f);
		assignPoint(&p[1], -0.425f, -0.64f, 0.0f);
		assignPoint(&p[2], -0.28f, -0.6f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	// Left Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.62f, -0.15f, 0.0f);
		glVertex3f(-0.62f, -0.4f, 0.0f);
		glVertex3f(-0.57f, -0.58f, 0.0f);
		glVertex3f(-0.57f, -0.15f, 0.0f);
	glEnd();

	// Left Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.57f, 0.1f, 0.0f);
		assignPoint(&p[1], -0.63f, 0.0f, 0.0f);
		assignPoint(&p[2], -0.64f, -0.15f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
		glVertex3f(-0.57f, -0.18f, 0.0f);
	glEnd();

	// Right Hand
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_QUADS);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.22f, -0.14f, 0.0f);
		glVertex3f(-0.14f, -0.24f, 0.0f);
		glVertex3f(-0.14f, -0.09f, 0.0f);
		glVertex3f(-0.22f, 0.02f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.14f, -0.23f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.3f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

		glVertex3f(0.05f, 0.04f, 0.0f);
		glVertex3f(0.0f, 0.04f, 0.0f);

		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], -0.13f, -0.15f, 0.0f);
		assignPoint(&p[2], -0.14f, -0.08f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
	glBegin(GL_POLYGON);
		assignPoint(&p[0], 0.0f, 0.04f, 0.0f);
		assignPoint(&p[1], 0.04f, 0.1f, 0.0f);
		assignPoint(&p[2], 0.05f, 0.04f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Right Sleeve
	HexColorToFloatColor("6F61C0", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.28f, -0.09f, 0.0f);
		glVertex3f(-0.22f, -0.15f, 0.0f);
		glVertex3f(-0.17f, -0.03f, 0.0f);
		glVertex3f(-0.28f, 0.1f, 0.0f);
	glEnd();

	// Neck
	HexColorToFloatColor("EAC696", &c);
	glBegin(GL_POLYGON);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.38f, 0.25f, 0.0f);
		glVertex3f(-0.47f, 0.25f, 0.0f);

		assignPoint(&p[0], -0.47f, 0.12f, 0.0f);
		assignPoint(&p[1], -0.425f, 0.05f, 0.0f);
		assignPoint(&p[2], -0.38f, 0.12f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();

	// Big Hairs
	HexColorToFloatColor("65451F", &c);
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.52f, 0.1f, 0.0f);
		assignPoint(&p[2], -0.425f, 0.2f, 0.0f);
		assignPoint(&p[3], -0.33f, 0.1f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.5f, 0.0f);
		glColor3f(c.r, c.g, c.b);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Top Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.5f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.5f, 0.6f, 0.0f);
		assignPoint(&p[2], -0.3f, 0.6f, 0.0f);
		assignPoint(&p[3], -0.3f, 0.45f, 0.0f);
		assignPoint(&p[4], -0.33f, 0.45f, 0.0f);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);
	glEnd();

	// Side Hairs
	glBegin(GL_POLYGON);
		assignPoint(&p[0], -0.52f, 0.5f, 0.0f);
		assignPoint(&p[1], -0.505f, 0.55f, 0.0f);
		assignPoint(&p[2], -0.49f, 0.5f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);
	glEnd();
}

