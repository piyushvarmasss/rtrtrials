// header files
#include <GL/freeglut.h>
#include <math.h>
//global variable declarations
bool bIsFullScreen = false;

//entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First RTR5 Program : Kalyani Ashok Magdum");

	initialize();


	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	//code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// code
	void middlSeat(void);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// polygon 1
				// 1
	//Rectangle body
	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f,0.0f,0.0f);
	//clockwise move
	//body
	glVertex3f(0.1f,0.35f,0.0f); //A
	glVertex3f(0.4f, 0.35f, 0.0f); //B
	glVertex3f(0.4f, 0.0f, 0.0f); //C
	glVertex3f(0.1f, 0.0f, 0.0f); //D

	glEnd();

	
	//Triangle
	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f, 0.0f, 0.0f);
	//clockwise move

	glVertex3f(0.4f, 0.35f, 0.0f); //B
	glVertex3f(0.8f, 0.35f, 0.0f); //E
	glVertex3f(0.4f, 0.0f, 0.0f); //C
	glEnd();

	//back curve
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 0.0f, 0.0f);

	// p1 = 0.8,0.35
	//p2 = 0.6, 0.02
	//p3 = 0.4,0.0

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.4f * (1.0f - u) * (1.0f - u)) + (0.6f * 2.0f * (1.0f - u) * u) + (0.8f * u * u);
		float y = (0.0f * (1.0f - u) * (0.02f - u)) + (0.02f * 2.0f * (1.0f - u) * u) + (0.35f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();
	//end of body 


	//smile
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = (0.10f * (1.0f - u) * (1.0f - u)) + (0.16f * 2.0f * (1.0f - u) * u) + (0.20f * u * u);
		float y = (0.11f * (1.0f - u) * (0.02f - u)) + (0.09f * 2.0f * (1.0f - u) * u) + (0.13f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//fan
	glTranslatef(0.0f, 0.0f, 0.0f);
	//fan ubha rect
	glBegin(GL_POLYGON);								// 1
	glColor3f(0.9f,0.9f, 0.9f);
	//clockwise move	
	glVertex3f(0.0550f, 0.450f, 0.0f); //a
	glVertex3f(0.0650f, 0.450f, 0.0f); //b
	glVertex3f(0.0650f, -0.035f, 0.0f); //c
	glVertex3f(0.0550f, -0.035f, 0.0f); //d

	//glVertex3f(0.035f, -0.015f, 0.0f); //x
	glEnd();

	//curve
	//p3 : 0.0550, 0.450
	//p2 : 0.0750, 0.15
	//p1 : 0.0550, -0.0350
	glBegin(GL_POLYGON);
	glColor3f(0.9f, 0.9f, 0.9f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = (0.065f * (1.0f - u) * (1.0f - u)) + (0.0870f * 2.0f * (1.0f - u) * u) + (0.065f * u * u);
		float y = (-0.0350f * (1.0f - u) * (0.02f - u)) + (0.15f * 2.0f * (1.0f - u) * u) + (0.450f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//joining rect
	glBegin(GL_POLYGON);								// 1
	glColor3f(0.0f, 0.9f, 0.9f);
	//clockwise move

	glVertex3f(0.0755f, 0.240f, 0.0f); //a
	glVertex3f(0.10f, 0.240f, 0.0f); //b
	glVertex3f(0.10f, 0.198f, 0.0f); //c
	glVertex3f(0.0755f, 0.198f, 0.0f); //d

	//glVertex3f(0.035f, -0.015f, 0.0f); //x
	glEnd();

	//End of fan
	 
	//khalch quad
	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f, 0.0f, 0.0f);
	//clockwise move
	glVertex3f(0.31f, 0.015f, 0.0f); //x
	glVertex3f(0.465f, 0.015f, 0.0f); //w
	glVertex3f(0.28f, -0.23f, 0.0f); //z
	//glVertex3f(0.28f, -0.2f, 0.0f); //y

	glEnd();
	
	/*
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);

	// p1 = 0.3,-0.2
	//p2 = 0.25,-0.15
	//p3 = 0.1,-0.2
	/*
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = (0.34f * (1.0f - u) * (1.0f - u)) + (0.35f * 2.0f * (1.0f - u) * u) + (0.28f * u * u);
		float y = (0.0f * (1.0f - u) * (0.02f - u)) + (-0.15f * 2.0f * (1.0f - u) * u) + (-0.2f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();
	*/
	
	//Curve
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 0.0f, 0.0f);

	// p1 = 0.3,-0.2			0.3f, -0.25f, 0.0f
	//p2 = 0.25,-0.15
	//p3 = 0.1,-0.2				0.432f, 0.0f, 0.0f

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = (0.452f * (1.0f - u) * (1.0f - u)) + (0.42f * 2.0f * (1.0f - u) * u) + (0.28f * u * u);
		float y = (0.014f * (1.0f - u) * (0.02f - u)) + (-0.190f * 2.0f * (1.0f - u) * u) + (-0.23f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//End of khalch wing

	//eye	

	//Vrch elipse
	glTranslatef(0.76f, 0.46f, 0.0f);

	glBegin(GL_LINES);


  float radius = 0.15f;


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.1) * cos(angle);
		float y = (radius + 0.005 ) * sin(angle);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();	
	

	
	//middle SEAT 
	//-0.045
	
	glTranslatef(-0.29f, -0.115f, 0.0f);
	//middlSeat();
	glBegin(GL_LINES);

	 radius = 0.07f;


	for (float angle = 180.0f; angle <= 270.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.04) * cos(angle);
		float y = (radius)*sin(angle);
		glColor3f(1.0f, 1.0f, 0.0f);

		glVertex3f(x, y, 0.0f);

	}
	glEnd();




	
	/*
	
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = (0.4f * (1.0f - u) * (1.0f - u)) + (0.2f * 2.0f * (1.0f - u) * u) + (0.35f * u * u);
		float y = (0.35f * (1.0f - u) * (0.02f - u)) + (0.35f * 2.0f * (1.0f - u) * u) + (0.25f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();
	*/ 


	//Chak motha
	
	//glTranslatef(-0.110f, -0.55f, 0.0f);
	glTranslatef(-0.20f, -0.52f, 0.0f);

	glBegin(GL_LINES);


	radius = 0.15f;


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.095 ) * cos(angle);
		float y = (radius - 0.07) * sin(angle);
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//chak lahn
	
	glTranslatef(0.0041f, 0.0085f, 0.0f);

	glBegin(GL_LINES);
	radius = 0.15;

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.12) * cos(angle);
		float y = (radius - 0.11) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();
	
	

	//MOTHA Eye
	//glPushMatrix();
	//glTranslatef(-0.10f, 0.45f, 0.0f);

	glTranslatef(-0.10f, 0.43f, 0.0f);

	glBegin(GL_LINES);
	radius = 0.15;

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.125) * cos(angle);
		float y = (radius - 0.113) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();
	
	//lahan eye
//	glTranslatef(-0.007f, 0.0007f, 0.0f);
	glTranslatef(-0.012f, 0.0007f, 0.0f);

	glBegin(GL_LINES);
	radius = 0.15;

	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius - 0.132) * cos(angle);
		float y = (radius - 0.117) * sin(angle);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//glPopMatrix();
	//vrch rect top support
	
	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f, 0.0f, 0.0f);
	//clockwise move
	glVertex3f(0.09f, 0.38f, 0.0f); //a
	glVertex3f(0.23f, 0.32f, 0.0f); //b
	glVertex3f(0.21f, 0.30f, 0.0f); //c
	glVertex3f(0.10f, 0.34f, 0.0f); //d

	glEnd();

	// khalcha rect bottom support

	glBegin(GL_POLYGON);								// 1
	glColor3f(1.0f, 0.4f, 1.0f);
	//clockwise move
	glVertex3f(0.10f, -0.19f, 0.0f); //a
	glVertex3f(0.23f, -0.21f, 0.0f); //b
	glVertex3f(0.225f, -0.215f, 0.0f); //c
	glVertex3f(0.09f, -0.23f, 0.0f); //d

	glEnd();
	/* 
	// Lining 1
	glBegin(GL_LINES);								// 1
	glColor3f(0.0f, 0.0f, 0.0f);
	//clockwise move
	glVertex3f(0.10f, -0.19f, 0.0f); //a
	glVertex3f(0.23f, -0.21f, 0.0f); //b

	*/
	// First ubhi line for pankhe
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);
	//clockwise move
	glVertex3f(0.115f, 0.34f, 0.0f); //a
	glVertex3f(0.122f, 0.34f, 0.0f); //b
	glVertex3f(0.113f, -0.19f, 0.0f); //c
	glVertex3f(0.105f, -0.19f, 0.0f); //d
	glEnd();

	// Second ubhi line for pankhe
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);
	//clockwise move
	glVertex3f(0.200f, 0.31f, 0.0f); //a
	glVertex3f(0.207f, 0.31f, 0.0f); //b
	glVertex3f(0.190f, -0.210f, 0.0f); //c
	glVertex3f(0.181f, -0.210f, 0.0f); //d
	glEnd();

	// Tirki ubhi line for pankhe
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);
	//clockwise move
	glVertex3f(0.117f, 0.34f, 0.0f); //a
	glVertex3f(0.124f, 0.34f, 0.0f); //b
	glVertex3f(0.191f, -0.210f, 0.0f); //c
	glVertex3f(0.180f, -0.210f, 0.0f); //d
	glEnd();

	
	 
	glutSwapBuffers();
}
void middlSeat()
{
	
}
void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{

}







