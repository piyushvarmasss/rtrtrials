// Header Files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.141592
//Global Variable declarations
bool bIsFullScreen = false;



//Wave Animation variable y
float y = 0.0f;
float alpha = 0.0f;


//Entry-Point function
int main(int argc, char* argv[])
{
	//Function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);

	glutInitWindowPosition(100, 200);

	glutCreateWindow("My First RTR5 Program : Mangesh Kondiba Kachare");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	// Code 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

//Cloud Functions
void cloud1(void)
{
	//circle1
	glBegin(GL_LINES);
	//
	float radius = 0.1f;


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 1
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = radius * cos(angle);
		float y = (radius - 0.02) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


	////circle2
	glTranslatef(0.124f, 0.10f, 0.0f);
	glBegin(GL_LINES);

	radius = 0.125f;


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)	// 2
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = radius * cos(angle);
		float y = (radius - 0.05) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	// Circle 3

	glTranslatef(0.08f, -0.08f, 0.0f);

	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = radius * cos(angle);
		float y = (radius - 0.05) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();
}

void cloud2(void)
{
	//cloud2.1
	//glTranslatef(0.124f, -0.1f, 0.0f);
	float radius = 0.045;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.01) * cos(angle);
		float y = (radius + 0.012) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//cloud 2.2
	glTranslatef(0.124f, 0.011f, 0.0f);
	radius = 0.055;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.05) * cos(angle);
		float y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

	//circle3
	glTranslatef(-0.003f, -0.065f, 0.0f);
	radius = 0.035;
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.053f) * cos(angle);
		float y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


	/*glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.1f)
	{
		float x = -0.01f * (1.0f - u) * (1.0f - u) + (-0.02f * 2.0f * (-1.0f - u) * u) + 0.0f * u * u;

		float y = 0.0f * (1.0f - u) * (1.0f - u) + (0.011f * 2.0f * (1.0f - u) * u) + 0.01 * u * u;

		glVertex3f(x, y, 0.0f);
	}

	glEnd();*/



}

void cloud3(void)
{

	//glTranslatef(-0.003f, -0.065f, 0.0f);
	float radius = 0.1;
	glBegin(GL_LINES);

	//circle 3.1
	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius)*cos(angle);
		float y = (radius - 0.14) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


	//circle 3.2

	glTranslatef(0.128f, 0.048f, 0.0f);
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius + 0.01) * cos(angle);
		float y = (radius - 0.14) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


	//circle 3.3
	glTranslatef(0.16f, -0.012f, 0.0f);
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius)*cos(angle);
		float y = (radius - 0.14) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();


	//circle 3.4
	glTranslatef(-0.11f, -0.05f, 0.0f);
	glBegin(GL_LINES);


	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.01f)		// 3
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		float x = (radius)*cos(angle);
		float y = (radius - 0.14) * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();

}

void Dongar(void)
{

// Dongar 2
	glBegin(GL_POLYGON); // Second Dongar
	glColor3f(0.40f, 0.71f, 0.44f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.50f * (1.0f - u) * (1.0f - u)) + (-0.25f * 2.0f * (1.0f - u) * u) + (-0.17f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.17f * (1.0f - u) * (1.0f - u)) + (-0.00f * 2.0f * (1.0f - u) * u) + (0.30f * u * u);
		float y = (0.40f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.30f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.50f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	glBegin(GL_POLYGON); // first Dongar
	glColor3f(0.04f, 0.54f, 0.23f);

	// p1 = -1.0f, 0.10f, 0.0f
	// p2 = -0.80f, 0.77f, 0.0f
	// p3 = -0.67f, 1.450f, 0.0f

	// Dongar 1
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-1.0f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.67f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.20f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.67f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.05f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	
	glEnd();

	// Dongar 4
	glBegin(GL_POLYGON); // Fourth Dongar
	glColor3f(0.40f, 0.71f, 0.44f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.60f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.87f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.87f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (1.40f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.00f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.40f * (1.0f - u) * (1.0f - u)) + (1.40f * 2.0f * (1.0f - u) * u) + (0.60f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

	// Dongar 3
	glBegin(GL_POLYGON); // Third Dongar
	glColor3f(0.04f, 0.54f, 0.23f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.0f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (0.27f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.40f * 2.0f * (1.0f - u) * u) + (0.50f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.27f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (0.80f * u * u);
		float y = (0.50f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.80f * (1.0f - u) * (1.0f - u)) + (0.80f * 2.0f * (1.0f - u) * u) + (0.0f * u * u);
		float y = (0.10f * (1.0f - u) * (1.0f - u)) + (0.10f * 2.0f * (1.0f - u) * u) + (0.10f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void oliMati1(void)
{
	//glColor3f(0.7f, 0.3f, 0.3f);

	glBegin(GL_POLYGON); // First Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(0.7f, 0.3f, 0.3f);
	
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
		
}

void oliMati2(void)
{
	glBegin(GL_POLYGON); // Second Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(0.7f, 0.3f, 0.3f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}
void oliMati3(void)
{
	glBegin(GL_POLYGON); // Second Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(0.7f, 0.3f, 0.3f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.80f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.60f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.60f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();
}

void Wave1(void)
{


	glBegin(GL_POLYGON); // First Wave
	glColor3f(1.0f, 1.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON); 
	glColor3f(0.04f, 0.53f, 0.85f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-1.0f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void Wave2(void)
{
	glBegin(GL_POLYGON); // First Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(1.0f, 1.0f, 1.0f);


	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
	glColor3f(0.04f, 0.53f, 0.85f);
	//glColor3f(1.0f, 1.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (0.50f * (1.0f - u) * (1.0f - u)) + (0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}
void Wave3(void)
{
	glBegin(GL_POLYGON); // First Wave
	//glColor3f(0.0f, 0.0f, 0.9f);
	glColor3f(1.0f, 1.0f, 1.0f);


	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}


	glEnd();

	glBegin(GL_POLYGON); // First Wave
	glColor3f(0.04f, 0.53f, 0.85f);
	//glColor3f(1.0f, 1.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.10f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.50f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glColor3f(0.0f, 0.8f, 0.9f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (1.20f * (1.0f - u) * (1.0f - u)) + (1.20f * 2.0f * (1.0f - u) * u) + (0.40f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.30f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}

	glEnd();

}

void TryLearn(void)
{
	/*glColor3f(1.0f, 1.0f, 1.0f);
	for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-1.0f * (1.0f - u) * (1.0f - u)) + (-0.60f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (-0.30f * (1.0f - u) * (1.0f - u)) + (-0.55f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	*/
	/*for (float u = 0.0f; u <= 1.0f; u += 0.001f)
	{
		float x = (-0.20f * (1.0f - u) * (1.0f - u)) + (-0.20f * 2.0f * (1.0f - u) * u) + (-0.20f * u * u);
		float y = (-0.33f * (1.0f - u) * (1.0f - u)) + (-0.33f * 2.0f * (1.0f - u) * u) + (-0.30f * u * u);
		glVertex3f(x, y, 0.0f);
	}
	*/
	//Circle
	//glBegin(GL_LINES);
	float x, y;
	float radius = 0.50f;

	glColor3f(0.0f, 0.8f, 0.9f);
	glBegin(GL_QUADS);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glRotatef(60, 0.1f, 0.0f, 0.0f);


	//Top Circle
	x = (float)radius * cos(359 * PI / 180.0f);
	y = (float)radius * sin(359 * PI / 180.0f);
	for (int j = 0; j < 180; j++)
	{
		glVertex2f(x, y);
		x = (float)radius * cos(j * PI / 180.0f);
		y = (float)radius * sin(j * PI / 180.0f);
		glVertex2f(x, y);
	}
	glEnd();

}

void display(void)
{


	// Code

	//Function declarations
	void FirstScreen(void);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Function call
	FirstScreen();

	glutSwapBuffers();
}

void FirstScreen(void)
{
	//Function declarations
	void Dongar(void);
	void oliMati1(void);
	void oliMati2(void);
	void oliMati3(void);
	void Wave1(void);
	void Wave2(void);
	void Wave3(void);

	//clouds 
	void cloud1(void);
	void cloud2(void);
	void cloud3(void);

	// Kinara
	glBegin(GL_QUADS);
	glColor3f(0.7f, 0.6f, 0.5f);
	glVertex3f(1.0f, -0.60f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Sea water
	glColor3f(0.0f, 0.8f, 0.9f);
	//glVertex3f(1.0f, -0.60f, 0.0f);
	//glVertex3f(-1.0f, -0.60f, 0.0f);
	//glVertex3f(-1.0f, 0.10f, 0.0f);
	//glVertex3f(1.0f, 0.10f, 0.0f);
	//glEnd();

	glVertex3f(1.0f, -0.60f, 0.0f);
	glVertex3f(-1.0f, -0.60f, 0.0f);
	glVertex3f(-1.0f, 0.350f, 0.0f);
	glVertex3f(1.0f, 0.350f, 0.0f);
	glEnd();


	glBegin(GL_QUADS); // SKY
	glColor3f(0.1f, 0.50f, 0.8f);
	glVertex3f(-1.0f, 0.10f, 0.0f);
	glVertex3f(1.0f, 0.10f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();


	// Function Call
	Dongar();
	oliMati1();
	oliMati2();
	oliMati3();


	//Cloud calls
	//1
	glPushMatrix();
	glTranslatef(0.1f, 0.75f, 0.0f);
	glScalef(0.280f, 0.280f, 0.0f);
	cloud1();
	glPopMatrix();
	//2
	glPushMatrix();
	glTranslatef(0.2f, 0.89f, 0.0f);
	glScalef(0.250f, 0.250f, 0.0f);
	cloud2();
	glPopMatrix();
	//3
	glPushMatrix();
	glTranslatef(-0.6f, 0.8f, 0.0f);
	glScalef(0.350f, 0.250f, 0.0f);
	cloud3();
	glPopMatrix();
	//4
	glPushMatrix();
	glTranslatef(-0.48f, 0.8f, 0.0f);
	glScalef(0.350f, 0.250f, 0.0f);
	cloud3();
	glPopMatrix();
	//5
	glPushMatrix();
	glTranslatef(0.73f, 0.75f, 0.0f);
	glScalef(0.450f, 0.450f, 0.0f);
	cloud2();
	glPopMatrix();
	//6
	glPushMatrix();
	glTranslatef(0.4f, 0.78f, 0.0f);
	glScalef(0.550f, 0.550f, 0.0f);
	cloud1();
	glPopMatrix();
	//7
	glPushMatrix();
	glTranslatef(0.9f, 0.89f, 0.0f);
	glScalef(0.350f, 0.350f, 0.0f);
	cloud3();
	glPopMatrix();


	glPushMatrix();
	glTranslatef(-0.89f, 0.68f, 0.0f);
	glScalef(0.550f, 0.550f, 0.0f);
	cloud1();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.89f, 0.88f, 0.0f);
	glScalef(0.350f, 0.350f, 0.0f);
	cloud2();
	glPopMatrix();


	glTranslatef(0.0f, y, 0.0f);
	Wave1();
	Wave2();
	Wave3();
	if (y >= -0.30f)
	{
		y -= 0.002f;

	}
	else
	{
		y += 0.2f;

	}

	glutPostRedisplay();


}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}

}

void uninitialize(void)
{
	// Code
}

