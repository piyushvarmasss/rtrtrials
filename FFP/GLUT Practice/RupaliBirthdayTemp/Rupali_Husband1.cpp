
// Header files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.14159265359
#define curve3Points(u, p0, p1, p2, axis) (p0.##axis * (1.0f - u) * (1.0f - u)) + (p1.##axis * 2.0f * (1.0f - u) * u) + (p2.##axis * u * u)
#define curve4Points(u, p0, p1, p2, p3, axis) (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 3.0f * (1.0f - u) * u * u) + (p3.##axis * u * u * u)
#define curve5Points(u, p0, p1, p2, p3, p4, axis)  (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p3.##axis * 4.0f * (1.0f - u) * u * u * u) + (p4.##axis * u * u * u * u);

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;

typedef struct Bezier3 {
	Point p0;
	Point p1;
	Point p2;
} Bezier3;

typedef struct Bezier4 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
} Bezier4;

typedef struct Bezier5 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
	Point p4;
} Bezier5;

// Global Variables Declarations
bool bIsFullScreen = false;

// Global Functions Declarations
void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void HexColorToFloatColor(char color[], Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {
	 
	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void ellipse2D(Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (xRadius * sinf((angle * PI) / 180.0f)), center->y + (yRadius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void bezierShape(Point curve1[], Point curve2[], Color *c) {
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (int index = 0; index < 301; index++)
		{
			glVertex3f(curve1[index].x, curve1[index].y, curve1[index].z);
			glVertex3f(curve2[index].x, curve2[index].y, curve2[index].z);
		}
	glEnd();
}


// Problem to all below Functions until main function
void bezier33(Bezier3 *curve1, Bezier3 *curve2, Color *c) {
	
	float x, y, z;
	
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, x);
			y = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, y);
			z = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier44(Bezier4 *curve1, Bezier4 *curve2, Color *c) {

	float x, y, z;
	
	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier55(Bezier5 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, x);
		y = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, y);
		z = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, z);
		glVertex3f(x, y, z);

		x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier34(Bezier3 *curve1, Bezier4 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
			y = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
			z = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier35(Bezier3 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
			y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
			z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier45(Bezier4 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
			y = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
			z = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
			glVertex3f(x, y, z);

			x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
			y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
			z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void BSplineCurve(int noOfPoints, Point arr[]) {

	if (noOfPoints < 3)
		return;

	if (noOfPoints % 3 == 0)
		for (int i = 0; i <= noOfPoints; i += 3)
			bezierCurve3Points(&arr[i], &arr[i+1], &arr[i+2]);
	else if(noOfPoints % 4 == 0)
		for (int i = 0; i <= noOfPoints; i += 4)
			bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i+3]);
	else if (noOfPoints % 5 == 0)
		for (int i = 0; i <= noOfPoints; i += 5)
			bezierCurve5Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3], &arr[i+4]);
	else if (noOfPoints % 3 == 1) {
		int i = 0;
		for(; i <= noOfPoints-4; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
	else if (noOfPoints % 3 == 2) {
		int i = 0;
		for (; i <= noOfPoints - 5; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
}


// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Birthday Demo : Room Interior");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// function declarations
	void AshishModel();

	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}

void AshishModel() {

	// function declarations
	void Chair();
	void Ashish();
	void Laptop();

}

void Chair() {



	//glBegin();
}

void Ashish() {


}
