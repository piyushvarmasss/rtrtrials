
// Header files
#include <GL/freeglut.h>
#include<math.h>

// Macros
#define PNV_PI 3.14159265359

// Structure Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

// Global Variables Declarations
bool bIsFullScreen = false;


// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1400, 750);
	glutInitWindowPosition(50, 20);
	glutCreateWindow("Birthday Demo : Room Interior");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}


void display(void)
{
	// function declarations
	void FirstScreen(void);
	void SecondScreen(void);
	void ThirdScreen(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// function calls
	FirstScreen();
	SecondScreen();
	ThirdScreen();

	glutSwapBuffers();
}

void FirstScreen(void)
{
	// code
}

void SecondScreen(void)
{
	// function declarations
	void RoomStructure(void);
	void PNV_Table(void);
	void PNV_ManOnChair(void);

	// function calls

	//******** RoomInterior *******
	glPushMatrix();
		//glTranslatef(-0.40f, 0.4f, 0.0f);
		//glScalef(0.7f, 0.7f, 0.0f);
		RoomStructure();
	glPopMatrix();

	glPushMatrix();
		PNV_Table();
	glPopMatrix();

	glPushMatrix();
		PNV_ManOnChair();
	glPopMatrix();
}

void RoomStructure(void)
{

	//code

	// front wall
	glBegin(GL_QUADS);
		glColor3f(0.6f, 1.0f, 0.8f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(0.6f, 1.0f, 0.0f);
		glVertex3f(0.6f, -0.6f, 0.0f);
		glVertex3f(-1.0f, -0.6f, 0.0f);
	glEnd();

	// right side wall
	glBegin(GL_QUADS);
		glColor3f(0.6f, 1.0f, 0.7f);
		glVertex3f(0.6f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(0.6f, -0.6f, 0.0f);
	glEnd();

	// floor
	glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 0.9f);
		glVertex3f(-1.0f, -0.6f, 0.0f);
		glVertex3f(0.6f, -0.6f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();

	// vertical lines on the floor (for vertical edges of tiles)
	glColor3f(0.7f, 0.7f, 0.7f);

	for (int i = 0; i < 11; i++)
	{
		glBegin(GL_LINES);
			glVertex3f((0.6f - (0.2 * i)), -0.6f, 0.0f);
			glVertex3f((1.0f - (0.2 * i)), -1.0f, 0.0f);
		glEnd();
	}

	// horizontal lines on the floor (for horizontal edges of tiles)

	for (int i = 0; i < 6; i++)
	{
		glBegin(GL_LINES);
			glVertex3f(-1.0f, (-0.6f - (0.1 * i)), 0.0f);
			glVertex3f((0.6f + (0.1 * i)), (-0.6f - (0.1 * i)), 0.0f);
		glEnd();
	}
}

void PNV_Table(void)
{
	// Function Declarations
	void PNV_PC(void);
	void PNV_Mobile(void);

	//code

	// Left Leg with Drawers
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.7f, -0.23f, 0.0f);	// A
		glVertex3f(-0.5f, -0.23f, 0.0f);	// B
		glVertex3f(-0.5f, -0.7f, 0.0f);		// C
		glVertex3f(-0.7f, -0.7f, 0.0f);		// D
	glEnd();
	
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.8f, -0.13f, 0.0f);	// A
		glVertex3f(-0.7f, -0.23f, 0.0f);	// B
		glVertex3f(-0.7f, -0.7f, 0.0f);		// C
		glVertex3f(-0.8f, -0.6f, 0.0f);		// D
	glEnd();

	// Drawer 1
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.69f, -0.24f, 0.0f);	// A
		glVertex3f(-0.51f, -0.24f, 0.0f);	// B
		glVertex3f(-0.51f, -0.36f, 0.0f);	// C
		glVertex3f(-0.69f, -0.36f, 0.0f);	// D
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.4f, 0.1f, 0.0f);
		for (float sAngle = 0.0f, eAngle = 360.0f; sAngle <= eAngle; sAngle += 1.0f)
			glVertex3f(-0.6f + (0.02f * sinf((sAngle * PNV_PI) / 180.0f)), -0.3f + (0.02f * cosf((sAngle * PNV_PI) / 180.0f)), 0.0f);
	glEnd();

	// Drawer 2
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.69f, -0.38f, 0.0f);	// A
		glVertex3f(-0.51f, -0.38f, 0.0f);	// B
		glVertex3f(-0.51f, -0.5f, 0.0f);	// C
		glVertex3f(-0.69f, -0.5f, 0.0f);	// D
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.4f, 0.1f, 0.0f);
		for (float sAngle = 0.0f, eAngle = 360.0f; sAngle <= eAngle; sAngle += 1.0f)
			glVertex3f(-0.6f + (0.02f * sinf((sAngle * PNV_PI) / 180.0f)), -0.45f + (0.02f * cosf((sAngle * PNV_PI) / 180.0f)), 0.0f);
	glEnd();

	// Drawer 3
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.69f, -0.52f, 0.0f);	// A
		glVertex3f(-0.51f, -0.52f, 0.0f);	// B
		glVertex3f(-0.51f, -0.64f, 0.0f);	// C
		glVertex3f(-0.69f, -0.64f, 0.0f);	// D
	glEnd();
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.4f, 0.1f, 0.0f);
		for (float sAngle = 0.0f, eAngle = 360.0f; sAngle <= eAngle; sAngle += 1.0f)
			glVertex3f(-0.6f + (0.02f * sinf((sAngle * PNV_PI) / 180.0f)), -0.58f + (0.02f * cosf((sAngle * PNV_PI) / 180.0f)), 0.0f);
	glEnd();


	// Right - Front Leg
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(0.08f, -0.23f, 0.0f);	// A
		glVertex3f(0.1f, -0.23f, 0.0f);		// B
		glVertex3f(0.1f, -0.7f, 0.0f);		// C
		glVertex3f(0.08f, -0.7f, 0.0f);		// D
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(0.07f, -0.23f, 0.0f);	// A
		glVertex3f(0.08f, -0.23f, 0.0f);	// B
		glVertex3f(0.08f, -0.7f, 0.0f);		// C
		glVertex3f(0.07f, -0.69f, 0.0f);	// D
	glEnd();


	// Right - Back Leg
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.01f, -0.13f, 0.0f);	// A
		glVertex3f(0.01f, -0.13f, 0.0f);	// B
		glVertex3f(0.01f, -0.61f, 0.0f);	// C
		glVertex3f(-0.01f, -0.61f, 0.0f);	// D
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.02f, -0.13f, 0.0f);	// A
		glVertex3f(-0.01f, -0.13f, 0.0f);	// B
		glVertex3f(-0.01f, -0.61f, 0.0f);	// C
		glVertex3f(-0.02f, -0.6f, 0.0f);	// D
	glEnd();
	

	// Table Up Face
	glBegin(GL_QUADS);
		glColor3f(0.8f, 0.6f, 0.4f);
		glVertex3f(-0.88f, -0.1f, 0.0f);	// A
		glVertex3f(0.03f, -0.1f, 0.0f);		// B
		glVertex3f(0.2f, -0.2f, 0.0f);		// C
		glVertex3f(-0.8f, -0.2f, 0.0f);		// D
	glEnd();

	// Table Side Edge
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.88f, -0.1f, 0.0f);	// A
		glVertex3f(-0.76f, -0.2f, 0.0f);	// B
		glVertex3f(-0.76f, -0.23f, 0.0f);	// C
		glVertex3f(-0.88f, -0.13f, 0.0f);	// D
	glEnd();


	// Table Front Edge
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.76f, -0.2f, 0.0f);	// A
		glVertex3f(0.2f, -0.2f, 0.0f);		// B
		glVertex3f(0.2f, -0.23f, 0.0f);		// C
		glVertex3f(-0.76f, -0.23f, 0.0f);	// D
	glEnd();

	PNV_PC();
	PNV_Mobile();
}

void PNV_PC()
{
	// Outer Rectangle
	glBegin(GL_QUADS);
		glColor3f(0.3f, 0.3f, 0.3f);
		glVertex3f(-0.6f, -0.05f, 0.0f);
		glVertex3f(-0.2f, -0.05f, 0.0f);
		glVertex3f(-0.2f, 0.31f, 0.0f);
		glVertex3f(-0.6f, 0.31f, 0.0f);
	glEnd();

	// Inner Rectangle
	glBegin(GL_QUADS);
		glColor3f(0.2f, 1.0f, 1.0f);
		glVertex3f(-0.59f, -0.03f, 0.0f);
		glVertex3f(-0.21f, -0.03f, 0.0f);
		glVertex3f(-0.21f, 0.29f, 0.0f);
		glVertex3f(-0.59f, 0.29f, 0.0f);
	glEnd();

	// Handle
	glBegin(GL_QUADS);
		glColor3f(0.3f, 0.3f, 0.3f);
		glVertex3f(-0.43f, -0.05f, 0.0f);
		glVertex3f(-0.37f, -0.05f, 0.0f);
		glVertex3f(-0.37f, -0.11f, 0.0f);
		glVertex3f(-0.43f, -0.11f, 0.0f);
	glEnd();

	// Base
	glBegin(GL_QUADS);
		glColor3f(0.3f, 0.3f, 0.3f);
		glVertex3f(-0.53f, -0.11f, 0.0f);
		glVertex3f(-0.27f, -0.11f, 0.0f);
		glVertex3f(-0.27f, -0.13f, 0.0f);
		glVertex3f(-0.53f, -0.13f, 0.0f);
	glEnd();
}

void PNV_Mobile() 
{
	// Function Declarations
	void circle2D(Point*, float, float, float);

	// Local Variable
	Point center = {-0.7f, -0.085f, 0.0f};

	// Outer Rectangle
	glBegin(GL_QUADS);
		glColor3f(0.3f, 0.3f, 0.3f);
		glVertex3f(-0.74f, -0.1f, 0.0f);
		glVertex3f(-0.66f, -0.1f, 0.0f);
		glVertex3f(-0.66f, 0.16f, 0.0f);
		glVertex3f(-0.74f, 0.16f, 0.0f);
	glEnd();

	// Inner Rectangle
	glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.735f, -0.07f, 0.0f);
		glVertex3f(-0.665f, -0.07f, 0.0f);
		glVertex3f(-0.665f, 0.14f, 0.0f);
		glVertex3f(-0.735f, 0.14f, 0.0f);
	glEnd();

	// Start Button
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.7f, 0.7f, 0.7f);
		circle2D(&center, 0.01f, 0.0f, 360.0f);
	glEnd();

	// Mobile Line
	glBegin(GL_LINES);
		glColor3f(0.7f, 0.7f, 0.7f);
		glVertex3f(-0.72f, 0.15f, 0.0f);
		glVertex3f(-0.68f, 0.15f, 0.0f);
	glEnd();
}

void PNV_ManOnChair()
{
	// Fucntion Declaration
	void PNV_Man();
	void PNV_Chair(void);

	PNV_Man();
	PNV_Chair();
}

void PNV_Man() 
{
	// Function Declarations
	void PNV_Head(void);
	void PNV_Body(void);
	
	
	glPushMatrix();
		glScalef(0.5f, 0.5f, 1.0f);
		glTranslatef(-0.35f, 0.3f, 0.0f);
		PNV_Head();
	glPopMatrix();
	
	glPushMatrix();
		PNV_Body();	
	glPopMatrix();
}

void PNV_Head() 
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);
	void bezierCurve4Points(Point*, Point*, Point*, Point*);
	void bezierCurve3Points(Point*, Point*, Point*);

	Point center = { 0.0f, 0.0f, 0.0f };
	Point p1 = { 0.2f, 0.0f, 0.0f };
	Point p2 = { 0.1f, 0.4f, 0.0f };
	Point p3 = { -0.1f, 0.0f, 0.0f };
	Point p4 = { -0.2f, 0.05f, 0.0f };

	// Right Ear
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, 0.22f, -0.1f, 0.0f);
		circle2D(&center, 0.05f, 0.0f, 180.0f);
	glEnd();

	// Left Ear
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&center, -0.12f, -0.1f, 0.0f);
		circle2D(&center, 0.05f, 180.0f, 360.0f);
	glEnd();

	// Big Hair
	glBegin(GL_POLYGON);
		glColor3f(0.1f, 0.1f, 0.1f);
		bezierCurve4Points(&p1, &p2, &p3, &p4);
		assignPoint(&p2, -0.05f, -0.1, 0.0f);
		bezierCurve3Points(&p4, &p2, &p1);
	glEnd();

	// Small Hair
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&center, 0.2f, 0.0f, 0.0f);
		circle2D(&center, 0.05f, 270.0f, 451.0f);
	glEnd();

	// Head Skin
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.6f, 0.4f, 0.2f);
		assignPoint(&p1, 0.23f, 0.0f, 0.0f);
		assignPoint(&p2, 0.23f, -0.6f, 0.0f);
		assignPoint(&p3, -0.13f, -0.6f, 0.0f);
		assignPoint(&p4, -0.13f, 0.0f, 0.0f);
		bezierCurve4Points(&p1, &p2, &p3, &p4);
	glEnd();

	// Hairs
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.1f, 0.1f, 0.1f);
		assignPoint(&p1, 0.23f, 0.0f, 0.0f);
		assignPoint(&p2, 0.22f, -0.55f, 0.0f);
		assignPoint(&p3, -0.12f, -0.55f, 0.0f);
		assignPoint(&p4, -0.13f, 0.0f, 0.0f);
		bezierCurve4Points(&p1, &p2, &p3, &p4);
	glEnd();
}

void PNV_Body()
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);

	// Variable Declarations
	Point center = { 0.0f, 0.0f, 0.0f };

	// Man Body
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.25f, -0.4f, 0.0f);
	glVertex3f(-0.05f, -0.4f, 0.0f);
	glVertex3f(-0.05f, -0.15f, 0.0f);
	glVertex3f(-0.25f, -0.15f, 0.0f);
	glEnd();

	// Man Shoulder Part
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0f, 1.0f, 0.0f);
	assignPoint(&center, -0.15f, -0.15f, 0.0f);
	circle2D(&center, 0.1f, 270.0f, 450.0f);
	glEnd();

	// Right Leg
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.12f, -0.45f, 0.0f);
	glVertex3f(-0.12f, -0.68f, 0.0f);
	glVertex3f(-0.06f, -0.68f, 0.0f);
	glVertex3f(-0.06f, -0.45f, 0.0f);
	glEnd();

	// Right Toe
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.09f, -0.68f, 0.0f);
	circle2D(&center, 0.02f, 90.0f, 270.0f);
	glEnd();

	// Left Leg
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f(-0.24f, -0.45f, 0.0f);
	glVertex3f(-0.24f, -0.68f, 0.0f);
	glVertex3f(-0.18f, -0.68f, 0.0f);
	glVertex3f(-0.18f, -0.45f, 0.0f);
	glEnd();

	// Left Toe
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.6f, 0.4f, 0.2f);
	assignPoint(&center, -0.21f, -0.68f, 0.0f);
	circle2D(&center, 0.02f, 90.0f, 270.0f);
	glEnd();
}

void PNV_Chair()
{
	// Function Declarations
	void assignPoint(Point*, float, float, float);
	void circle2D(Point*, float, float, float);

	// Variable Declarations
	Point center;

	// Left Leg of chair
	glBegin(GL_QUADS);
		glColor3f(0.2f, 0.2f, 0.2f);
		glVertex3f(-0.28f, -0.75f, 0.0f);
		glVertex3f(-0.28f, -0.73f, 0.0f);
		glVertex3f(-0.15f, -0.66f, 0.0f);
		glVertex3f(-0.15f, -0.68f, 0.0f);
	glEnd();
	// Right Leg of chair
	glBegin(GL_QUADS);
		glVertex3f(-0.02f, -0.75f, 0.0f);
		glVertex3f(-0.02f, -0.73f, 0.0f);
		glVertex3f(-0.15f, -0.66f, 0.0f);
		glVertex3f(-0.15f, -0.68f, 0.0f);
	glEnd();
	// Central Leg of chair
	glBegin(GL_QUADS);
		glVertex3f(-0.17f, -0.68f, 0.0f);
		glVertex3f(-0.16f, -0.72f, 0.0f);
		glVertex3f(-0.14f, -0.72f, 0.0f);
		glVertex3f(-0.13f, -0.68f, 0.0f);
	glEnd();

	// Left Ball
	assignPoint(&center, -0.25f, -0.75f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.3f, 0.3f, 0.3f);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();
	// Right Ball
	assignPoint(&center, -0.05f, -0.75f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();
	// Central Ball
	assignPoint(&center, -0.15f, -0.72f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		circle2D(&center, 0.02f, 0.0f, 360.0f);
	glEnd();
	

	// Bar of base
	glBegin(GL_QUADS);
		glVertex3f(-0.165f, -0.67f, 0.0f);
		glVertex3f(-0.165f, -0.47f, 0.0f);
		glVertex3f(-0.135f, -0.47f, 0.0f);
		glVertex3f(-0.135f, -0.67f, 0.0f);
	glEnd();

	// Seat
	glBegin(GL_QUADS);
		glColor3f(0.4f, 0.1f, 0.0f);
		glVertex3f(-0.25f, -0.4f, 0.0f);
		glVertex3f(-0.05f, -0.4f, 0.0f);
		glVertex3f(-0.05f, -0.47f, 0.0f);
		glVertex3f(-0.25f, -0.47f, 0.0f);
	glEnd();
	// Seat Right Curve
	assignPoint(&center, -0.05f, -0.435f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		circle2D(&center, 0.035f, 0.0f, 180.0f);
	glEnd();
	// Seat Left Curve
	assignPoint(&center, -0.25f, -0.435f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		circle2D(&center, 0.035f, 180.0f, 360.0f);
	glEnd();

	// Support										// glScalef() is compulsory to make ellipse
	assignPoint(&center, -0.15f, -0.2f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(0.4f, 0.1f, 0.0f);
		circle2D(&center, 0.12f, 0.0f, 360.0f);
	glEnd();

	// Bar of Support
	glBegin(GL_QUADS);
		glColor3f(0.2f, 0.2f, 0.2f);
		glVertex3f(-0.155f, -0.42f, 0.0f);
		glVertex3f(-0.145f, -0.42f, 0.0f);
		glVertex3f(-0.145f, -0.2f, 0.0f);
		glVertex3f(-0.155f, -0.2f, 0.0f);
	glEnd();
}

void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PNV_PI) / 180.0f)), center->y + (radius * cosf((angle * PNV_PI) / 180.0f)), 0.0f);

}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void ThirdScreen(void)
{
	// code
}
