// Header Files
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.141592
//Global Variable declarations
bool bIsFullScreen = false;

//Entry-Point function
int main(int argc, char* argv[])
{
	//Function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);

	glutInitWindowPosition(100, 200);

	glutCreateWindow("My First RTR5 Program : Mangesh Kondiba Kachare");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	// Code 
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	 // Kinara
	glBegin(GL_QUADS);
		glColor3f(0.1f, 0.1f, 0.1f);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Sea water
		glColor3f(0.0f, 0.8f, 0.9f);
		glVertex3f(1.0f, -0.60f, 0.0f);
		glVertex3f(-1.0f, -0.60f, 0.0f);
		glVertex3f(-1.0f, 0.40f, 0.0f);
		glVertex3f(1.0f, 0.40f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // SKY
	glColor3f(0.1f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 0.40f, 0.0f);
		glVertex3f(1.0f, 0.40f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
		glColor3f(1.0f, 1.0f, 1.0f);

		// p1 = -0.5f,0.0f, 0.0.0f
		// p2 = 0.0f, 0.5f, 0.0f
		// p3 = 0.05, 0.0f, 0.0f


		for (float u = 0.0f; u <= 1.0f; u += 0.001f)
		{
			float x = (-0.5f * (1.0f - u) * (1.0f - u)) + (0.0f * 2.0f * (1.0f - u) * u) + (0.5f * u * u);
			float y = (0.0f * (1.0f - u) * (1.0f - u)) + (0.5f * 2.0f * (1.0f - u) * u) + (0.0f * u * u);
			glVertex3f(x, y, 0.0f);
		}
	glEnd();
	
	glutSwapBuffers();
}

void TryLearn(void)
{

	//Circle
	//glBegin(GL_LINES);
	float x, y;
	float radius = 0.50f;

	glColor3f(0.0f, 0.8f, 0.9f);
	glBegin(GL_QUADS);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glRotatef(60, 0.1f, 0.0f, 0.0f);


	//Top Circle
	x = (float)radius * cos(359 * PI / 180.0f);
	y = (float)radius * sin(359 * PI / 180.0f);
	for (int j = 0; j < 180; j++)
	{
		glVertex2f(x, y);
		x = (float)radius * cos(j * PI / 180.0f);
		y = (float)radius * sin(j * PI / 180.0f);
		glVertex2f(x, y);
	}
	glEnd();

}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}

}

void uninitialize(void)
{
	// Code
}

