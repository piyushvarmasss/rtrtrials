// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>
#include<stdlib.h>
#include<math.h>

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

float y = 0.2f;
void display(void)
{
	// FunctionDeclaration
	void gift (float, float, float);
	void confitti(void);
	void update(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gift(0.5f, 0.8f, 0.0f);

	glTranslatef(1.2f, 0.0f, 0.0f);
	gift(1.0f, 0.5f, 0.5f);


	glTranslatef(-1.15f, y, 0.0f);
	confitti();

	update();
	glutSwapBuffers();
}

void update(void) {
	
	if (y >= -2.0f)
		y -= 0.0001f;

	glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}

void gift(float r, float g, float b)
{
	// Code of gift
	glBegin(GL_QUADS);

	// Code of Box
	glColor3f(r, g, b);
	glVertex3f(-0.75f, -0.87f, 0.0f);
	glVertex3f(-0.45f, -0.87f, 0.0f);
	glVertex3f(-0.45f, -0.67f, 0.0f);
	glVertex3f(-0.75f, -0.67f, 0.0f);

	// Code of Box Lid
	glVertex3f(-0.77f, -0.73f, 0.0f);
	glVertex3f(-0.43f, -0.73f, 0.0f);
	glVertex3f(-0.43f, -0.67f, 0.0f);
	glVertex3f(-0.77f, -0.67f, 0.0f);

	// Code of Ribbon horizontal
	glColor3f(0.4f, 0.8f, 0.9f);
	glVertex3f(-0.75f, -0.80f, 0.0f);
	glVertex3f(-0.45f, -0.80f, 0.0f);
	glVertex3f(-0.45f, -0.77f, 0.0f);
	glVertex3f(-0.75f, -0.77f, 0.0f);

	// Code of Ribbon Vertical
	glVertex3f(-0.61f, -0.87f, 0.0f);
	glVertex3f(-0.59f, -0.87f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.61f, -0.67f, 0.0f);

	//Code of Ribbon Knot
	glVertex3f(-0.61f, -0.67f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.64f, -0.6f, 0.0f);
	glVertex3f(-0.66f, -0.63f, 0.0f);

	glVertex3f(-0.61f, -0.67f, 0.0f);
	glVertex3f(-0.59f, -0.67f, 0.0f);
	glVertex3f(-0.55f, -0.63f, 0.0f);
	glVertex3f(-0.57f, -0.6f, 0.0f);

	glEnd();

	// Code of Box Lid line
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.75f, -0.73f, 0.0f);
	glVertex3f(-0.45f, -0.73f, 0.0f);
	glEnd();
}

void confitti(){
	
	// Function Declarations
	void triangle1(float x, float y);
	void triangle2(float x, float y);
	void triangle3(float x, float y);
	void triangle4(float x, float y);
	void triangle5(float x, float y);
	void Rectangle1(float x, float y);
	void Rectangle2(float x, float y);
	void Rectangle3(float x, float y);

	triangle1(-0.8f, 0.8f);
	triangle2(-0.9f, 0.9f);
	triangle3(-0.8f, 0.8f);
	triangle4(-0.8f, 0.8f);
	triangle5(-0.9f, 0.9f);
	Rectangle1(-0.7f, 0.7f);
	Rectangle2(-0.7f, 0.6f);
	Rectangle3(-0.6f, 0.8f);

	glTranslatef(0.1f, 0.0f, 0.0f);
	triangle1(-0.7f, 0.7f);
	triangle2(-0.8f, 0.8f);
	triangle3(-0.7f, 0.9f);
	triangle4(-0.7f, 0.7f);
	triangle5(-0.8f, 0.6f);
	Rectangle1(-0.6f, 0.6f);
	Rectangle2(-0.6f, 0.7f);
	Rectangle3(-0.5f, 0.8f);

	glTranslatef(-0.1f, 0.0f, 0.0f);
	triangle1(0.0f, 0.8f);
	triangle2(-0.2f, 0.9f);
	triangle3(-0.3f, 0.8f);
	triangle4(-0.5f, 0.8f);
	triangle5(-0.6f, 0.9f);
	Rectangle1(-0.7f, 0.7f);
	Rectangle2(-0.48f, 0.9f);
	Rectangle3(-0.35f, 0.6f);

	glTranslatef(0.1f, 0.0f, 0.0f);
	triangle1(0.05f, 0.7f);
	triangle2(-0.25f, 0.8f);
	triangle3(-0.35f, 0.6f);
	triangle4(-0.55f, 0.7f);
	triangle5(-0.65f, 0.8f);
	Rectangle1(-0.75f, 0.6f);
	Rectangle2(-0.48f, 0.8f);
	Rectangle3(-0.35f, 0.6f);

	glTranslatef(-0.1f, 0.0f, 0.0f);
	triangle1(0.0f, 0.8f);
	triangle2(0.2f, 0.9f);
	triangle3(0.3f, 0.8f);
	triangle4(0.5f, 0.8f);
	triangle5(0.6f, 0.9f);
	Rectangle1(0.7f, 0.7f);
	Rectangle2(0.48f, 0.9f);
	Rectangle3(0.35f, 0.6f);

	glTranslatef(0.1f, 0.0f, 0.0f);
	triangle1(0.1f, 0.8f);
	triangle2(0.25f, 0.9f);
	triangle3(0.33f, 0.8f);
	triangle4(0.3f, 0.8f);
	triangle5(0.4f, 0.9f);
	Rectangle1(0.6f, 0.7f);
	Rectangle2(0.3f, 0.9f);
	Rectangle3(0.1f, 0.6f);

	glTranslatef(-0.1f, 0.0f, 0.0f);
	triangle1(0.1f, 0.7f);
	triangle2(0.25f, 0.8f);
	triangle3(0.33f, 0.6f);
	triangle4(0.3f, 0.7f);
	triangle5(0.4f, 0.6f);
	Rectangle1(0.6f, 0.8f);
	Rectangle2(0.3f, 0.9f);
	Rectangle3(0.1f, 0.6f);

	glTranslatef(0.1f, 0.0f, 0.0f);
	triangle1(-0.4f, 0.7f);
	triangle2(-0.2f, 0.8f);
	triangle3(-0.2f, 0.9f);
	triangle4(-0.4f, 0.7f);
	triangle5(-0.6f, 0.6f);
	Rectangle1(-0.1f, 0.6f);
	Rectangle2(-0.2f, 0.7f);
	Rectangle3(-0.3f, 0.8f);

	glTranslatef(-0.1f, 0.0f, 0.0f);
	triangle1(0.8f, 0.8f);
	triangle2(0.9f, 0.9f);
	triangle3(0.8f, 0.8f);
	triangle4(0.8f, 0.8f);
	triangle5(0.9f, 0.9f);
	Rectangle1(0.7f, 0.7f);
	Rectangle2(0.7f, 0.7f);
	Rectangle3(0.6f, 0.7f);

	glTranslatef(0.1f, 0.0f, 0.0f);
	triangle1(0.6f, 0.7f);
	triangle2(0.7f, 0.6f);
	triangle3(0.8f, 0.6f);
	triangle4(0.6f, 0.7f);
	triangle5(0.9f, 0.8f);
	Rectangle1(0.5f, 0.8f);
	Rectangle2(0.4f, 0.9f);
	Rectangle3(0.3f, 0.6f);

}

void triangle1(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(x+0.6f, x+0.2f, 0.9f);
		glVertex3f(x, y, 0.0f);
		glVertex3f(x + 0.05f, y + 0.05f, 0.0f);
		glVertex3f(x, y + 0.1f, 0.0f);
	glEnd();
}

void triangle2(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(y+0.5f, y+1.0f, 1.0f);
		glVertex3f(x - 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x - 0.1f, y - 0.1f, 0.0f);
		glVertex3f(x - 0.05f, y - 0.15f, 0.0f);
	glEnd();
}

void triangle3(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(x+0.6f, y+1.0f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x + 0.07f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.05f, 0.0f);
	glEnd();
}

void triangle4(float x, float y) {


	glBegin(GL_TRIANGLES);
		glColor3f(y+0.8f, x+0.4f, 1.0f);
		glVertex3f(x - 0.1f, y + 0.0f, 0.0f);
		glVertex3f(x - 0.13f, y + 0.03f, 0.0f);
		glVertex3f(x - 0.16f, y + 0.0f, 0.0f);
	glEnd();
}

void triangle5(float x, float y) {

	glBegin(GL_TRIANGLES);
		glColor3f(y+0.9f, y+1.0f, 0.8f);
		glVertex3f(x - 0.08f, y - 0.2f, 0.0f);
		glVertex3f(x - 0.04f, y - 0.24f, 0.0f);
		glVertex3f(x - 0.0f, y - 0.2f, 0.0f);
	glEnd();
}

void Rectangle1(float x, float y) {
	glBegin(GL_QUADS);
		glColor3f(x+0.6f, x+1.0f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
		glVertex3f(x + 0.05f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.07f, 0.0f);
		glVertex3f(x + 0.09f, y - 0.05f, 0.0f);
	glEnd();
}

void Rectangle2(float x, float y) {
	glBegin(GL_QUADS);
	glColor3f(y+0.5f, x+1.0f, 1.0f);
	glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
	glVertex3f(x + 0.05f, y - 0.07f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.07f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.05f, 0.0f);
	glEnd();
}

void Rectangle3(float x, float y) {
	glBegin(GL_QUADS);
	glColor3f(x+0.6f, y+0.2f, 0.9f);
	glVertex3f(x + 0.05f, y - 0.05f, 0.0f);
	glVertex3f(x + 0.05f, y - 0.09f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.09f, 0.0f);
	glVertex3f(x + 0.07f, y - 0.05f, 0.0f);
	glEnd();
}