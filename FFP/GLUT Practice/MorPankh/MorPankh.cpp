// Header File (With Path C:\freeglut\include\GL\freeglut.h)
#include <GL/freeglut.h>
#include<math.h>

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

void GreenHead() 
{
	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.4f, 0.6f, 0.0f };
	Point p4 = { 0.5f, 0.7f, 0.0f };

	glBegin(GL_POLYGON);

	glColor3f( 0.0f, 1.0f, 0.0f );
	
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.x * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.x * 3.0f * u * u * (1.0f - u)) + (p4.x * u * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.y * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.y * 3.0f * u * u * (1.0f - u)) + (p4.y * u * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2.z * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p3.z * 3.0f * u * u * (1.0f - u)) + (p4.z * u * u * u);

		glVertex3f(x, y, z);
	}

	Point p5 = { 0.5f, 0.7f, 0.0f };
	Point p6 = { 0.3f, 0.4f, 0.0f };
	Point p7 = { -0.1f, 0.8f, 0.0f };
	Point p8 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p5.x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.x * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.x * 3.0f * u * u * (1.0f - u)) + (p8.x * u * u * u);
		float y = (p5.y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.y * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.y * 3.0f * u * u * (1.0f - u)) + (p8.y * u * u * u);
		float z = (p5.z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p6.z * 3.0f * u * (1.0f - u) * (1.0f - u)) + (p7.z * 3.0f * u * u * (1.0f - u)) + (p8.z * u * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void YellowHead()
{
	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.5f, 0.7f, 0.0f };

	glBegin(GL_POLYGON);

	glColor3f( 1.0f, 1.0f, 0.0f );

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	Point p4 = { 0.5f, 0.7f, 0.0f };
	Point p5 = { -0.2f, 0.75f, 0.0f };
	Point p6 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * u * (1.0f - u)) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * u * (1.0f - u)) + (p6.y * u * u);
		float z = (p4.z * (1.0f - u) * (1.0f - u)) + (p5.z * 2.0f * u * (1.0f - u)) + (p6.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void OrangeHead() {

	Point p1 = { 0.0f, 0.0f, 0.0f };
	Point p2 = { 0.6f, -0.2f, 0.0f };
	Point p3 = { 0.5f, 0.7f, 0.0f };

	glScalef(0.9f, 0.9f, 1.0f);
	glBegin(GL_POLYGON);

	glColor3f(1.0f, 0.7f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	Point p4 = { 0.5f, 0.7f, 0.0f };
	Point p5 = { -0.2f, 0.75f, 0.0f };
	Point p6 = { 0.0f, 0.0f, 0.0f };

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p4.x * (1.0f - u) * (1.0f - u)) + (p5.x * 2.0f * u * (1.0f - u)) + (p6.x * u * u);
		float y = (p4.y * (1.0f - u) * (1.0f - u)) + (p5.y * 2.0f * u * (1.0f - u)) + (p6.y * u * u);
		float z = (p4.z * (1.0f - u) * (1.0f - u)) + (p5.z * 2.0f * u * (1.0f - u)) + (p6.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();
}

void BlueHead()
{
	glBegin(GL_TRIANGLE_FAN);

	glColor3f(0.1f, 0.75f, 1.0f);

	for (float angle = 0.0f; angle <= (2.0f * 3.1415926f); angle += ((2.0f * 3.1415926f) / 100.0f))
		glVertex3f( 0.2f * sinf(angle), 0.2f * cosf(angle), 0.0f);

	glEnd();
}

void DarkBlueHead()
{
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.2f, 0.0f, 1.0f);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(-0.2f * cosf(angle), -0.2f * sinf(angle), 0.0f);

	glEnd();

	glTranslatef(0.4f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(-0.2f * cosf(angle), -0.2f * sinf(angle), 0.0f);

	glEnd();

	glTranslatef(-0.203f, -0.04f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	for (float angle = 0.0f; angle <= 3.1415926f; angle += ((2.0f * 3.1415926f) / 100.00f))
		glVertex3f(0.4f * cosf(angle), 0.4f * sinf(angle), 0.0f);

	glEnd();
}


void Stem()
{
	Point p1 = { 0.2f, 0.3f, 0.0f };
	Point p2 = { 0.0f, 0.1f, 0.0f };
	Point p3 = { 0.0f, -1.0f, 0.0f };
	
	glPushMatrix();
	glRotatef(315.0f, 0.0f, 0.0f, 1.0f);
	glScalef(0.3f, 0.3f, 1.0f);
	glBegin(GL_POLYGON);

	glColor3f(0.0f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}

	glEnd();


	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	glVertex3f(0.0f, 0.3f, 0.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1.x * (1.0f - u) * (1.0f - u)) + (p2.x * 2.0f * u * (1.0f - u)) + (p3.x * u * u);
		float y = (p1.y * (1.0f - u) * (1.0f - u)) + (p2.y * 2.0f * u * (1.0f - u)) + (p3.y * u * u);
		float z = (p1.z * (1.0f - u) * (1.0f - u)) + (p2.z * 2.0f * u * (1.0f - u)) + (p3.z * u * u);

		glVertex3f(x, y, z);
	}
	glPopMatrix();
	glEnd();
}

void MorPankh() {
	
	// 1.Stem
	// 2.GreenHead
	// 3.YellowHead
	// 4.OrangeHead
	// 5.BlueHead
	// 6.DarkBlueHead

	Stem();

	glScalef(2.0f, 2.0f, 1.0f);
	glTranslatef(-0.011f, 0.07f, 0.0f);
	glRotatef(44.0f, 0.0f, 0.0f, 1.0f);
	GreenHead();

	glTranslatef( 0.06f, 0.06f, 0.0f );
	glScalef(0.6f, 0.6f, 1.0f);
	YellowHead();


	OrangeHead();

	glTranslatef( 0.21f, 0.28f, 0.0f );
	glScalef( 1.0f, 1.25f, 1.0f );
	BlueHead();

	glRotatef( 330.0f, 0.0f, 0.0f, 1.0f );
	glTranslatef( -0.07f, 0.0f, 0.0f );
	glScalef( 0.35f, 0.35f, 1.0f );
	DarkBlueHead();
}

void Ink() {

	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.2f, 0.5f);
	glVertex3f( 0.0f, 0.0f, 0.0f );
	glVertex3f( -0.008f, -0.1f, 0.0f );
	glVertex3f( 0.008f, -0.1f, 0.0f );
	glEnd();
}

// Global Variable Declaration (Hungarian Notation)
bool bIsFullScreen = false;

// Entry - Point Function
int main(int argc, char* argv[])
{
	// Function Declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// Code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GL_POINTS : Behaviour");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// Code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// Code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glScalef(0.6f , 1.0f, 1.0f);
	MorPankh();
	glPopMatrix();

	glRotatef(135.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.22f, 0.0f);
	Ink();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// Code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;
		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// Code
	switch (button)
	{
		case GLUT_RIGHT_BUTTON:
			glutLeaveMainLoop();
			break;
		default:
			break;
	}
}

void uninitialize(void)
{
	// Code
}
