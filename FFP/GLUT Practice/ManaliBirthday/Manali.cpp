
// Header files
#include <GL/freeglut.h>
#include <MMSystem.h>	
#include <math.h>

#define PI 3.14159265359
#define curve3Points(u, p0, p1, p2, axis) (p0.##axis * (1.0f - u) * (1.0f - u)) + (p1.##axis * 2.0f * (1.0f - u) * u) + (p2.##axis * u * u)
#define curve4Points(u, p0, p1, p2, p3, axis) (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 3.0f * (1.0f - u) * u * u) + (p3.##axis * u * u * u)
#define curve5Points(u, p0, p1, p2, p3, p4, axis)  (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p3.##axis * 4.0f * (1.0f - u) * u * u * u) + (p4.##axis * u * u * u * u);

// Struct Declaration
typedef struct Point {
	float x;
	float y;
	float z;
} Point;

typedef struct Color {
	float r;
	float g;
	float b;
} Color;

typedef struct Bezier3 {
	Point p0;
	Point p1;
	Point p2;
} Bezier3;

typedef struct Bezier4 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
} Bezier4;

typedef struct Bezier5 {
	Point p0;
	Point p1;
	Point p2;
	Point p3;
	Point p4;
} Bezier5;

// Global Variables Declarations
bool bIsFullScreen = false;

float snowfallx = 1.0f, snowfally = 1.0f;
float HappyFlag = 0.0f, ManaliFlag = 0.0f;
int start = 0;

// Global Functions Declarations
void assignPoint(Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void HexColorToFloatColor(char color[], Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

}

void circle2D(Point* center, float radius, float sAngle, float eAngle) {
	 
	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (radius * sinf((angle * PI) / 180.0f)), center->y + (radius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void ellipse2D(Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	for (float angle = sAngle; angle <= eAngle; angle += 1.0f)
		glVertex3f(center->x + (xRadius * sinf((angle * PI) / 180.0f)), center->y + (yRadius * cosf((angle * PI) / 180.0f)), 0.0f);

}

void bezierCurve3Points(Point* p1, Point* p2, Point* p3) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u)) + (p2->x * 2.0f * (1.0f - u) * u) + (p3->x * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u)) + (p2->y * 2.0f * (1.0f - u) * u) + (p3->y * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u)) + (p2->z * 2.0f * (1.0f - u) * u) + (p3->z * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve4Points(Point* p1, Point* p2, Point* p3, Point* p4) {

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->x * 3.0f * (1.0f - u) * u * u) + (p4->x * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->y * 3.0f * (1.0f - u) * u * u) + (p4->y * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p3->z * 3.0f * (1.0f - u) * u * u) + (p4->z * u * u * u);
		glVertex3f(x, y, z);
	}
}

void bezierCurve5Points(Point* p1, Point* p2, Point* p3, Point* p4, Point* p5)
{
	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		float x = (p1->x * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->x * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->x * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->x * 4.0f * (1.0f - u) * u * u * u) + (p5->x * u * u * u * u);
		float y = (p1->y * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->y * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->y * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->y * 4.0f * (1.0f - u) * u * u * u) + (p5->y * u * u * u * u);
		float z = (p1->z * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p2->z * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p3->z * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p4->z * 4.0f * (1.0f - u) * u * u * u) + (p5->z * u * u * u * u);

		glVertex3f(x, y, z);
	}
}

void bezier33(Bezier3 *curve1, Bezier3 *curve2, Color *c) {
	
	float x, y, z;
	
	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, x);
			y = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, y);
			z = curve3Points(u, curve2->p0, curve2->p1, curve2->p2, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier44(Bezier4 *curve1, Bezier4 *curve2, Color *c) {

	float x, y, z;
	
	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier55(Bezier5 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
	glColor3f(c->r, c->g, c->b);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, x);
		y = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, y);
		z = curve5Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, z);
		glVertex3f(x, y, z);

		x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void bezier34(Bezier3 *curve1, Bezier4 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
			y = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
			z = curve4Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier35(Bezier3 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, x);
			y = curve3Points(u, curve1->p0, curve1->p1, curve1->p2, y);
			z = curve3Points(u, curve1->p0, curve1->p2, curve1->p2, z);
			glVertex3f(x, y, z);

			x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
			y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
			z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void bezier45(Bezier4 *curve1, Bezier5 *curve2, Color *c) {

	float x, y, z;

	glBegin(GL_LINES);
		glColor3f(c->r, c->g, c->b);
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
			x = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
			y = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
			z = curve4Points(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
			glVertex3f(x, y, z);

			x = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
			y = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
			z = curve5Points(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
			glVertex3f(x, y, z);
		}
	glEnd();
}

void BSplineCurve(int noOfPoints, Point arr[]) {

	if (noOfPoints < 3)
		return;

	if (noOfPoints % 3 == 0)
		for (int i = 0; i <= noOfPoints; i += 3)
			bezierCurve3Points(&arr[i], &arr[i+1], &arr[i+2]);
	else if(noOfPoints % 4 == 0)
		for (int i = 0; i <= noOfPoints; i += 4)
			bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i+3]);
	else if (noOfPoints % 5 == 0)
		for (int i = 0; i <= noOfPoints; i += 5)
			bezierCurve5Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3], &arr[i+4]);
	else if (noOfPoints % 3 == 1) {
		int i = 0;
		for(; i <= noOfPoints-4; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
	else if (noOfPoints % 3 == 2) {
		int i = 0;
		for (; i <= noOfPoints - 5; i += 3)
			bezierCurve3Points(&arr[i], &arr[i + 1], &arr[i + 2]);
		bezierCurve4Points(&arr[i], &arr[i + 1], &arr[i + 2], &arr[i + 3]);
	}
}


// entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowSize(1200, 700);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Happy Birthday Manali");

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	PlaySound("song.WAV", NULL, SND_ASYNC);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// function declarations
	void Screen(void);
	void Happy(void);
	void Birthday(void);
	void Manali(void);
	void Snowfall(void);


	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	Screen();

	glPushMatrix();
		Snowfall();
	glPopMatrix();

	glPushMatrix();

		glScalef(0.8f, 0.8f, 1.0f);
		glTranslatef(0.2f, 0.5f, 0.0f);
		Happy();
		Birthday();
	glPopMatrix();

	glPushMatrix();
		glScalef(1.0f, 1.0f, 1.0f);
		glTranslatef(-0.25f, 0.1f, 0.0f);
		Manali();
	glPopMatrix();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
		case 27:
			glutLeaveMainLoop();
			break;
			
		case 'S':
		case 's':
			start = 1;
			break;

		case 'F':
		case 'f':
			if (bIsFullScreen == false)
			{
				glutFullScreen();
				bIsFullScreen = true;
			}
			else
			{
				glutLeaveFullScreen();
				bIsFullScreen = false;
			}
			break;

		default:
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
}


void Screen() {

	// Function Declarations
	void Snowman(void);
	void Lamppost(void);
	void Bench(void);
	void ManaliModel(void);

	Point p[5];
	Color c;
	Bezier4 curve1, curve2;

	// Sky
	glBegin(GL_QUADS);

		HexColorToFloatColor("071952", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		
		HexColorToFloatColor("78C1F3", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(1.0f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	// Moon
	glBegin(GL_POLYGON);

		c.r = c.g = c.b = 1.0f;
		assignPoint(&(curve1.p0), 0.9f, 0.9f, 0.0f);
		assignPoint(&(curve1.p1), 0.97f, 0.87f, 0.0f);
		assignPoint(&(curve1.p2), 0.94f, 0.78f, 0.0f);
		assignPoint(&(curve1.p3), 0.88f, 0.8f, 0.0f);

		assignPoint(&(curve2.p0), 0.9f, 0.9f, 0.0f);
		assignPoint(&(curve2.p1), 0.93f, 0.87f, 0.0f);
		assignPoint(&(curve2.p2), 0.92f, 0.8f, 0.0f);
		assignPoint(&(curve2.p3), 0.88f, 0.8f, 0.0f);

		bezier44(&curve1, &curve2, &c);

	glEnd();

	// Mountains
	HexColorToFloatColor("A0BFE0", &c);
	glBegin(GL_POLYGON);
		
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p[0], -0.3f, -0.4f, 0.0f);
		assignPoint(&p[1], 0.2f, 0.9f, 0.0f);
		assignPoint(&p[2], 0.7f, 0.9f, 0.0f);
		assignPoint(&p[3], 1.2f, -0.4f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	glEnd();

	// Path Quad
	HexColorToFloatColor("A1C2F1", &c);
	glBegin(GL_QUADS);
		
		glColor3f(c.r, c.g, c.b);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.4f, 0.0f);
		glVertex3f(1.0f, -0.4f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

	// Path Width
	HexColorToFloatColor("7895CB", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-1.0f, -0.35f, 0.0f);
		glVertex3f(-1.0f, -0.4f, 0.0f);
		glVertex3f(1.0f, -0.4f, 0.0f);
		glVertex3f(1.0f, -0.35f, 0.0f);

	glEnd();

	// Lower Snow
	glBegin(GL_POLYGON);
		
		glColor3f(0.9f, 1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -0.9f, 0.0f);

		assignPoint(&p[0], -1.0f, -0.9f, 0.0f);
		assignPoint(&p[1], -0.3f, -0.6f, 0.0f);
		assignPoint(&p[2], 0.0f, -1.0f, 0.0f);
		assignPoint(&p[3], 0.5f, -0.7f, 0.0f);
		assignPoint(&p[4], 1.0f, -0.9f, 0.0f);
		bezierCurve5Points(&p[0], &p[1], &p[2], &p[3], &p[4]);

	glEnd();

	// Upper Snow
	glBegin(GL_POLYGON);

		glColor3f(0.9f, 1.0f, 1.0f);
		glVertex3f(-1.0f, -0.35f, 0.0f);
		glVertex3f(-1.0f, 0.2f, 0.0f);
		
		assignPoint(&p[0], -1.0f, 0.4f, 0.0f);
		assignPoint(&p[1], -0.5f, 0.6f, 0.0f);
		assignPoint(&p[2], 0.3f, -0.3f, 0.0f);
		assignPoint(&p[3], 1.0f, -0.1f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

		glVertex3f(1.0f, -0.35f, 0.0f);

	glEnd();

	// Lamp Post
	Lamppost();

	// Snowman
	glPushMatrix();
		
		glTranslatef(0.3f, -0.1f, 0.0f);
		glScalef(0.7f, 0.7f, 1.0f);
		Snowman();

	glPopMatrix();

	// Bench
	glPushMatrix();
		
		glTranslatef(0.1f, -0.1f, 0.0f);
		glScalef(0.8f, 0.8f, 1.0f);
		Bench();
	
	glPopMatrix();

	// Manali
	glPushMatrix();
	
		glTranslatef(0.05f, -0.02f, 0.0f);
		ManaliModel();

	glPopMatrix();
}

void Lamppost() {

	Point p;
	Color c;
	Bezier3 curve1, curve2;

	HexColorToFloatColor("102C57", &c);
	glBegin(GL_QUADS);
		
		glColor3f(c.r, c.g, c.b);
		glVertex3f( -0.82f, -0.42f, 0.0f);
		glVertex3f( -0.8f, -0.36f, 0.0f);
		glVertex3f( -0.7f, -0.36f, 0.0f);
		glVertex3f( -0.68f, -0.42f, 0.0f);

	glEnd();

	HexColorToFloatColor("4F709C", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.79f, -0.36f, 0.0f);
		glVertex3f(-0.79f, -0.33f, 0.0f);
		glVertex3f(-0.71f, -0.33f, 0.0f);
		glVertex3f(-0.71f, -0.36f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glVertex3f(-0.78f, -0.33f, 0.0f);
		glVertex3f(-0.78f, -0.32f, 0.0f);
		glVertex3f(-0.72f, -0.32f, 0.0f);
		glVertex3f(-0.72f, -0.33f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glVertex3f(-0.775f, -0.33f, 0.0f);
		glVertex3f(-0.758f, -0.2f, 0.0f);
		glVertex3f(-0.742f, -0.2f, 0.0f);
		glVertex3f(-0.725f, -0.33f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

		glVertex3f(-0.758f, -0.2f, 0.0f);
		glVertex3f(-0.758f, 0.55f, 0.0f);
		glVertex3f(-0.742f, 0.55f, 0.0f);
		glVertex3f(-0.742f, -0.2f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

		assignPoint(&(curve1.p0), -0.758f, 0.55f, 0.0f);
		assignPoint(&(curve1.p1), -0.705f, 0.825f, 0.0f);
		assignPoint(&(curve1.p2), -0.652f, 0.55f, 0.0f);

		assignPoint(&(curve2.p0), -0.742f, 0.55f, 0.0f);
		assignPoint(&(curve2.p1), -0.705f, 0.745f, 0.0f);
		assignPoint(&(curve2.p2), -0.668f, 0.55f, 0.0f);

		bezier33(&curve1, &curve2, &c);

	glEnd();

	glBegin(GL_POLYGON);

		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-0.663f, 0.5f, 0.0f);
		assignPoint(&p, -0.66f, 0.48f, 0.0f);
		circle2D(&p, 0.04f, 60.0f, 300.0f);

	glEnd();

	glBegin(GL_POLYGON);
	
		glColor3f(c.r, c.g, c.b);
		assignPoint(&p, -0.66f, 0.5f, 0.0f);
		ellipse2D(&p, 0.05f, 0.05f, 270.0f, 450.0f);

	glEnd();

	glBegin(GL_POLYGON);

		glColor3f(1.0f, 1.0f, 0.7f);
		assignPoint(&p, -0.66f, -0.5f, 0.0f);
		ellipse2D(&p, 0.29f, 0.05f, 0.0f, 360.0f);

	glEnd();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glBegin(GL_POLYGON);
		
		glColor4f(1.0f, 1.0f, 0.0f, 0.3f);
		glVertex3f(-0.7f, 0.48f, 0.0f);
		glVertex3f(-0.62f, 0.48f, 0.0f);
		glVertex3f(-0.37f, -0.5f, 0.0f);
		glVertex3f(-0.95f, -0.5f, 0.0f);

	glEnd();
}

void Snowman() {

	// Variable Declarations
	Point p[5];
	Bezier3 curve1, curve2;
	Color c;

	// Body
	glBegin(GL_TRIANGLE_FAN);

		glColor3f(0.98f, 1.0f, 1.0f);
		assignPoint(&p[0], 0.75f, 0.0f, 0.0f);
		ellipse2D(&p[0], 0.17f, 0.3f, 0.0f, 360.0f);
	
	glEnd();

	// Button1
	glBegin(GL_TRIANGLE_FAN);

		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.78f, 0.1f, 0.0f);
		ellipse2D(&p[0], 0.02f, 0.03f, 0.0f, 360.0f);

	glEnd();

	// Button2
	glBegin(GL_TRIANGLE_FAN);

		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.78f, 0.0f, 0.0f);
		ellipse2D(&p[0], 0.02f, 0.03f, 0.0f, 360.0f);

	glEnd();

	// Button3
	glBegin(GL_TRIANGLE_FAN);

		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.78f, -0.1f, 0.0f);
		ellipse2D(&p[0], 0.02f, 0.03f, 0.0f, 360.0f);

	glEnd();

	// Muffler
	glBegin(GL_POLYGON);

	c.r = 1.0f;
	c.g = 0.0f;
	c.b = 0.0f;
	assignPoint(&(curve1.p0), 0.7f, 0.24f, 0.0f);
	assignPoint(&(curve1.p1), 0.7f, 0.16f, 0.0f);
	assignPoint(&(curve1.p2), 0.65f, 0.08f, 0.0f);

	assignPoint(&(curve2.p0), 0.67f, 0.24f, 0.0f);
	assignPoint(&(curve2.p1), 0.67f, 0.17f, 0.0f);
	assignPoint(&(curve2.p2), 0.62f, 0.1f, 0.0f);

	bezier33(&curve1, &curve2, &c);

	glEnd();

	glBegin(GL_POLYGON);
		
		HexColorToFloatColor("BB2525", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.75f, 0.4f, 0.0f);
		assignPoint(&p[0], 0.75f, 0.4f, 0.0f);
		ellipse2D(&p[0], 0.13f, 0.23f, 131.0f, 229.0f);

	glEnd();

	// Hat Back
	glBegin(GL_POLYGON);

		glColor3f(0.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.75f, 0.48f, 0.0f);
		ellipse2D(&p[0], 0.09f, 0.05f, 90.0f, 270.0f);

	glEnd();
	
	// Head
	glBegin(GL_TRIANGLE_FAN);

		glColor3f(0.98f, 1.0f, 1.0f);
		assignPoint(&p[0], 0.75f, 0.38f, 0.0f);
		ellipse2D(&p[0], 0.1f, 0.14f, 0.0f, 360.0f);

	glEnd();

	// Hat
	glBegin(GL_QUADS);
		
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.8f, 0.48f, 0.0f);
		glVertex3f(0.7f, 0.48f, 0.0f);
		glVertex3f(0.695f, 0.68f, 0.0f);
		glVertex3f(0.805f, 0.68f, 0.0f);

	glEnd();
	glBegin(GL_POLYGON);

		glVertex3f(0.75f, 0.48f, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.75f, 0.48f, 0.0f);
		ellipse2D(&p[0], 0.09f, 0.07f, 320.0f, 400.0f);

	glEnd();
	glBegin(GL_POLYGON);

		glColor3f(0.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.75f, 0.48f, 0.0f);
		ellipse2D(&p[0], 0.09f, 0.05f, 270.0f, 450.0f);

	glEnd();
	glBegin(GL_POLYGON);

		glColor3f(0.0f, 0.0f, 0.0f);
		assignPoint(&p[0], 0.75f, 0.68f, 0.0f);
		ellipse2D(&p[0], 0.055f, 0.02f, 270.0f, 450.0f);

	glEnd();

	// Nose
	glBegin(GL_TRIANGLES);

		glColor3f(1.0f, 0.5f, 0.0f);
		glVertex3f(0.63f, 0.3f, 0.0f);
		glVertex3f(0.71f, 0.37f, 0.0f);
		glVertex3f(0.73f, 0.34f, 0.0f);

	glEnd();
	glBegin(GL_POLYGON);
		
		assignPoint(&p[0], 0.71f, 0.37f, 0.0f);
		assignPoint(&p[1], 0.74f, 0.365f, 0.0f);
		assignPoint(&p[2], 0.73f, 0.34f, 0.0f);
		bezierCurve3Points(&p[0], &p[1], &p[2]);

	glEnd();

	// Left Eye
	glBegin(GL_POLYGON);
		
		assignPoint(&p[0], 0.77f, 0.43f, 0.0f);
		glColor3f(0.1f, 0.1f, 0.1f);
		ellipse2D(&p[0], 0.015f, 0.02f, 0.0f, 360.0f);

	glEnd();
	glBegin(GL_POLYGON);

		assignPoint(&p[0], 0.779f, 0.439f, 0.0f);
		glColor3f(0.9f, 0.9f, 0.9f);
		ellipse2D(&p[0], 0.005f, 0.008f, 0.0f, 360.0f);

	glEnd();


	// Right Eye
	glBegin(GL_POLYGON);

		assignPoint(&p[0], 0.7f, 0.43f, 0.0f);
		glColor3f(0.1f, 0.1f, 0.1f);
		ellipse2D(&p[0], 0.015f, 0.02f, 0.0f, 360.0f);

	glEnd();
	glBegin(GL_POLYGON);

		assignPoint(&p[0], 0.709f, 0.439f, 0.0f);
		glColor3f(0.9f, 0.9f, 0.9f);
		ellipse2D(&p[0], 0.005f, 0.008f, 0.0f, 360.0f);

	glEnd();

	// Base
	glBegin(GL_POLYGON);

		glColor3f(0.9f, 1.0f, 1.0f);
		assignPoint(&p[0], 0.62f, -0.31f, 0.0f);
		assignPoint(&p[1], 0.71f, -0.23f, 0.0f);
		assignPoint(&p[2], 0.8f, -0.23f, 0.0f);
		assignPoint(&p[3], 0.89f, -0.31f, 0.0f);
		bezierCurve4Points(&p[0], &p[1], &p[2], &p[3]);

	glEnd();

	// Smile
	glBegin(GL_POLYGON);

		c.r = 0.1f;
		c.g = 0.1f;
		c.b = 0.1f;

		assignPoint(&(curve1.p0), 0.69f, 0.31f, 0.0f);
		assignPoint(&(curve1.p1), 0.73f, 0.25f, 0.0f);
		assignPoint(&(curve1.p2), 0.77f, 0.31f, 0.0f);

		assignPoint(&(curve2.p0), 0.69f, 0.32f, 0.0f);
		assignPoint(&(curve2.p1), 0.73f, 0.26f, 0.0f);
		assignPoint(&(curve2.p2), 0.77f, 0.32f, 0.0f);

		bezier33(&curve1, &curve2, &c);

	glEnd();

}

void Bench() {

	Color c;

	// Left Base
	HexColorToFloatColor("4F709C", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.17f, -0.49f, 0.0f);
		glVertex3f(-0.15f, -0.49f, 0.0f);
		glVertex3f(-0.1f, -0.42f, 0.0f);
		glVertex3f(-0.12f, -0.42f, 0.0f);
		
	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.16f, -0.48f, 0.0f);
		glVertex3f(-0.11f, -0.43f, 0.0f);
		glVertex3f(-0.11f, -0.3f, 0.0f);
		glVertex3f(-0.16f, -0.3f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.13f, -0.3f, 0.0f);
		glVertex3f(-0.11f, -0.3f, 0.0f);
		glVertex3f(-0.08f, 0.0f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);

	glEnd();

	// Right Base
	HexColorToFloatColor("4F709C", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.37f, -0.49f, 0.0f);
		glVertex3f(0.35f, -0.49f, 0.0f);
		glVertex3f(0.3f, -0.42f, 0.0f);
		glVertex3f(0.32f, -0.42f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.36f, -0.48f, 0.0f);
		glVertex3f(0.31f, -0.43f, 0.0f);
		glVertex3f(0.31f, -0.3f, 0.0f);
		glVertex3f(0.36f, -0.3f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.33f, -0.3f, 0.0f);
		glVertex3f(0.31f, -0.3f, 0.0f);
		glVertex3f(0.28f, 0.0f, 0.0f);
		glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();

	// Connect
	HexColorToFloatColor("102C57", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.13f, -0.38f, 0.0f);
		glVertex3f(-0.13f, -0.36f, 0.0f);
		glVertex3f(0.33f, -0.36f, 0.0f);
		glVertex3f(0.33f, -0.38f, 0.0f);

	glEnd();

	// Wood Sit
	HexColorToFloatColor("765827", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.33f, -0.3f, 0.0f);
		glVertex3f(0.52f, -0.3f, 0.0f);
		glVertex3f(0.52f, -0.29f, 0.0f);
		glVertex3f(-0.33f, -0.29f, 0.0f);

	glEnd();
	
	HexColorToFloatColor("C8AE7D", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.3f, -0.25f, 0.0f);
		glVertex3f(0.49f, -0.25f, 0.0f);
		glVertex3f(0.52f, -0.29f, 0.0f);
		glVertex3f(-0.33f, -0.29f, 0.0f);

	glEnd();

	// Back Support
	HexColorToFloatColor("C8AE7D", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.28f, -0.05f, 0.0f);
		glVertex3f(0.47f, -0.05f, 0.0f);
		glVertex3f(0.465f, 0.0f, 0.0f);
		glVertex3f(-0.275f, 0.0f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.285f, -0.07f, 0.0f);
		glVertex3f(0.475f, -0.07f, 0.0f);
		glVertex3f(0.48f, -0.12f, 0.0f);
		glVertex3f(-0.29f, -0.12f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(-0.295f, -0.14f, 0.0f);
		glVertex3f(0.485f, -0.14f, 0.0f);
		glVertex3f(0.49f, -0.19f, 0.0f);
		glVertex3f(-0.3f, -0.19f, 0.0f);

	glEnd();
}

void ManaliModel() {

	// Vraiable Declarations
	Color c;
	Bezier4 curve1, curve3;
	Bezier5 curve2;
	Bezier3 curve4, curve5;

	// Left Foot
	assignPoint(&(curve1.p0), 0.12f, -0.49f, 0.0f);
	assignPoint(&(curve1.p1), 0.13f, -0.53f, 0.0f);
	assignPoint(&(curve1.p2), 0.12f, -0.57f, 0.0f);
	assignPoint(&(curve1.p3), 0.1f, -0.58f, 0.0f);

	assignPoint(&(curve2.p0), 0.12f, -0.49f, 0.0f);
	assignPoint(&(curve2.p1), 0.07f, -0.50f, 0.0f);
	assignPoint(&(curve2.p2), 0.12f, -0.53f, 0.0f);
	assignPoint(&(curve2.p3), 0.05f, -0.56f, 0.0f);
	assignPoint(&(curve2.p4), 0.1f, -0.58f, 0.0f);

	HexColorToFloatColor("765827", &c);
	bezier45(&curve1, &curve2, &c);

	glBegin(GL_QUADS);

		glVertex3f(0.098f, -0.52f, 0.0f);
		glVertex3f(0.122f, -0.52f, 0.0f);
		glVertex3f(0.125f, -0.45f, 0.0f);
		glVertex3f(0.095f, -0.45f, 0.0f);
		
	glEnd();

	glBegin(GL_QUADS);
		
		HexColorToFloatColor("C8AE7D", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.092f, -0.45f, 0.0f);
		glVertex3f(0.128f, -0.45f, 0.0f);
		glVertex3f(0.128f, -0.43f, 0.0f);
		glVertex3f(0.092f, -0.43f, 0.0f);
		
	glEnd();

	glBegin(GL_QUADS);

		HexColorToFloatColor("060047", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.095f, -0.43f, 0.0f);
		glVertex3f(0.125f, -0.43f, 0.0f);
		glVertex3f(0.135f, -0.29f, 0.0f);
		glVertex3f(0.09f, -0.29f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
		
		glVertex3f(0.09F, -0.295F, 0.0F);
		glVertex3f(0.15f, -0.295f, 0.0f);
		glVertex3f(0.15f, -0.22f, 0.0f);
		glVertex3f(0.09f, -0.22f, 0.0f);

	glEnd();
	

	// Head
	HexColorToFloatColor("080202", &c);
	
	assignPoint(&(curve2.p0), 0.02f, 0.0f, 0.0f);
	assignPoint(&(curve2.p1), 0.07f, 0.06f, 0.0f);
	assignPoint(&(curve2.p2), -0.02f, 0.08f, 0.0f);
	assignPoint(&(curve2.p3), 0.05f, 0.11f, 0.0f);
	assignPoint(&(curve2.p4), 0.05f, 0.18f, 0.0f);

	assignPoint(&(curve4.p0), 0.155f, 0.0f, 0.0f);
	assignPoint(&(curve4.p1), 0.13f, 0.1f, 0.0f);
	assignPoint(&(curve4.p2), 0.135f, 0.18f, 0.0f);

	bezier35(&curve4, &curve2, &c);

	// Neck
	HexColorToFloatColor("F0B86E", &c);
	assignPoint(&(curve4.p0), 0.06f, 0.0f, 0.0f);
	assignPoint(&(curve4.p1), 0.08f, 0.05f, 0.0f);
	assignPoint(&(curve4.p2), 0.06f, 0.1f, 0.0f);

	assignPoint(&(curve5.p0), 0.12f, 0.0f, 0.0f);
	assignPoint(&(curve5.p1), 0.1f, 0.05f, 0.0f);
	assignPoint(&(curve5.p2), 0.12f, 0.1f, 0.0f);

	bezier33(&curve4, &curve5, &c);

	// Right Ear
	glBegin(GL_POLYGON);
		
		assignPoint(&(curve1.p0), 0.052f, 0.14f, 0.0f);
		ellipse2D(&(curve1.p0), 0.008f, 0.025f, 0.0f, 360.0f);

	glEnd();

	// Left Ear
	glBegin(GL_POLYGON);

		assignPoint(&(curve1.p0), 0.128f, 0.14f, 0.0f);
		ellipse2D(&(curve1.p0), 0.008f, 0.025f, 0.0f, 360.0f);

	glEnd();

	// Face
	glBegin(GL_POLYGON);

		HexColorToFloatColor("FFC26F", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.09f, 0.16f, 0.0f);
		ellipse2D(&(curve1.p0), 0.04f, 0.1f, 90.0f, 270.0f);
			
	glEnd();

	glBegin(GL_POLYGON);

		HexColorToFloatColor("FFC26F", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.09f, 0.16f, 0.0f);
		ellipse2D(&(curve1.p0), 0.04f, 0.03f, 270.0f, 450.0f);

	glEnd();

	assignPoint(&(curve4.p0), 0.045f, 0.16f, 0.0f);
	assignPoint(&(curve4.p1), 0.09f, 0.22f, 0.0f);
	assignPoint(&(curve4.p2), 0.135f, 0.16f, 0.0f);

	assignPoint(&(curve5.p0), 0.04f, 0.19f, 0.0f);
	assignPoint(&(curve5.p1), 0.09f, 0.25f, 0.0f);
	assignPoint(&(curve5.p2), 0.14f, 0.19f, 0.0f);

	HexColorToFloatColor("0C356A", &c);
	bezier33(&curve4, &curve5, &c);

	glBegin(GL_POLYGON);

		HexColorToFloatColor("0C356A", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.09f, 0.27f, 0.0f);
		ellipse2D(&(curve1.p0), 0.02f, 0.03f, 0.0f, 360.0f);

	glEnd();

	assignPoint(&(curve4.p0), 0.045f, 0.2f, 0.0f);
	assignPoint(&(curve4.p1), 0.09f, 0.24f, 0.0f);
	assignPoint(&(curve4.p2), 0.135f, 0.2f, 0.0f);

	assignPoint(&(curve5.p0), 0.055f, 0.24f, 0.0f);
	assignPoint(&(curve5.p1), 0.09f, 0.3f, 0.0f);
	assignPoint(&(curve5.p2), 0.125f, 0.24f, 0.0f);

	HexColorToFloatColor("279EFF", &c);
	bezier33(&curve4, &curve5, &c);

	// Right Eye
	glBegin(GL_LINE_STRIP);
	
		HexColorToFloatColor("080202", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve4.p0), 0.08f, 0.16f, 0.0f);
		assignPoint(&(curve4.p1), 0.07f, 0.163f, 0.0f);
		assignPoint(&(curve4.p2), 0.06f, 0.157f, 0.0f);
		bezierCurve3Points(&(curve4.p0), &(curve4. p1), &(curve4.p2));

	glEnd();

	glBegin(GL_POLYGON);
		
		assignPoint(&(curve1.p0), 0.07f, 0.14f, 0.0f);
		ellipse2D(&(curve1.p0), 0.005f, 0.008f, 0.0f, 360.0f);

	glEnd();

	glBegin(GL_POLYGON);

		glColor3f(1.0f, 1.0f, 1.0f);
		assignPoint(&(curve1.p0), 0.07f, 0.136f, 0.0f);
		ellipse2D(&(curve1.p0), 0.002f, 0.004f, 0.0f, 360.0f);

	glEnd();

	// Left Eye
	glBegin(GL_LINE_STRIP);

		HexColorToFloatColor("080202", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve4.p0), 0.1f, 0.16f, 0.0f);
		assignPoint(&(curve4.p1), 0.11f, 0.163f, 0.0f);
		assignPoint(&(curve4.p2), 0.12f, 0.157f, 0.0f);
		bezierCurve3Points(&(curve4.p0), &(curve4.p1), &(curve4.p2));

	glEnd();

	glBegin(GL_POLYGON);

		assignPoint(&(curve1.p0), 0.11f, 0.14f, 0.0f);
		ellipse2D(&(curve1.p0), 0.005f, 0.008f, 0.0f, 360.0f);

	glEnd();

	glBegin(GL_POLYGON);

		glColor3f(1.0f, 1.0f, 1.0f);
		assignPoint(&(curve1.p0), 0.11f, 0.136f, 0.0f);
		ellipse2D(&(curve1.p0), 0.002f, 0.004f, 0.0f, 360.0f);

	glEnd();

	// Smile
	glBegin(GL_LINE_STRIP);

		HexColorToFloatColor("FF8DC7", &c);
		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve4.p0), 0.08f, 0.09f, 0.0f);
		assignPoint(&(curve4.p1), 0.09f, 0.082f, 0.0f);
		assignPoint(&(curve4.p2), 0.1f, 0.09f, 0.0f);
		bezierCurve3Points(&(curve4.p0), &(curve4.p1), &(curve4.p2));

	glEnd();


	// Shirt
	HexColorToFloatColor("FF6969", &c);
	assignPoint(&(curve1.p0), 0.019f, -0.22f, 0.0f);
	assignPoint(&(curve1.p1), 0.015f, -0.21f, 0.0f);
	assignPoint(&(curve1.p2), 0.045f, -0.1f, 0.0f);
	assignPoint(&(curve1.p3), 0.02f, 0.0f, 0.0f);

	assignPoint(&(curve3.p0), 0.161f, -0.22f, 0.0f);
	assignPoint(&(curve3.p1), 0.165f, -0.21f, 0.0f);
	assignPoint(&(curve3.p2), 0.135f, -0.1f, 0.0f);
	assignPoint(&(curve3.p3), 0.16f, 0.0f, 0.0f);

	bezier44(&curve1, &curve3, &c);

	glBegin(GL_QUADS);
		
		glVertex3f(0.07f, 0.0f, 0.0f);
		glVertex3f(0.11f, 0.0f, 0.0f);
		glVertex3f(0.11f, 0.05f, 0.0f);
		glVertex3f(0.07f, 0.05f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);

		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.09f, 0.0f, 0.0f);
		ellipse2D(&(curve1.p0), 0.067f, 0.035f, 270.0f, 450.0f);

	glEnd();

	// Left Arm
	HexColorToFloatColor("FF9F9F", &c);
	assignPoint(&(curve4.p0), 0.16f, 0.0f, 0.0f);
	assignPoint(&(curve4.p1), 0.14f, -0.05f, 0.0f);
	assignPoint(&(curve4.p2), 0.15f, -0.12f, 0.0f);

	assignPoint(&(curve5.p0), 0.16f, 0.0f, 0.0f);
	assignPoint(&(curve5.p1), 0.188f, -0.05f, 0.0f);
	assignPoint(&(curve5.p2), 0.178f, -0.12f, 0.0f);
	bezier33(&curve4, &curve5, &c);

	glBegin(GL_QUADS);

		glVertex3f( 0.178f, -0.12f, 0.0f);
		glVertex3f( 0.155f, -0.2f, 0.0f);
		glVertex3f( 0.13f, -0.183f, 0.0f);
		glVertex3f( 0.15f, -0.12f, 0.0f);

	glEnd();


	// Right Arm
	HexColorToFloatColor("FF9F9F", &c);
	assignPoint(&(curve4.p0), 0.02f, 0.0f, 0.0f);
	assignPoint(&(curve4.p1), 0.04f, -0.05f, 0.0f);
	assignPoint(&(curve4.p2), 0.03f, -0.12f, 0.0f);

	assignPoint(&(curve5.p0), 0.02f, 0.0f, 0.0f);
	assignPoint(&(curve5.p1), -0.008f, -0.05f, 0.0f);
	assignPoint(&(curve5.p2), 0.00f, -0.12f, 0.0f);
	bezier33(&curve4, &curve5, &c);

	glBegin(GL_QUADS);

		glVertex3f(0.00f, -0.12f, 0.0f);
		glVertex3f(0.026f, -0.21f, 0.0f);
		glVertex3f(0.047f, -0.181f, 0.0f);
		glVertex3f(0.03f, -0.12f, 0.0f);

	glEnd();
	

	HexColorToFloatColor("445069", &c);
	glLineWidth(2);
	glBegin(GL_LINE_STRIP);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.09f, -0.21f, 0.0f);
		glVertex3f(0.09f, 0.05f, 0.0f);

	glEnd();

	glBegin(GL_LINE_STRIP);

		assignPoint(&(curve4.p0), 0.16f, -0.21f, 0.0f);
		assignPoint(&(curve4.p1), 0.12f, -0.18f, 0.0f);
		assignPoint(&(curve4.p2), 0.12f, -0.15f, 0.0f);
		bezierCurve3Points(&(curve4.p0), &(curve4.p1), &(curve4.p2));

	glEnd();


	glBegin(GL_LINE_STRIP);

		assignPoint(&(curve4.p0), 0.02f, -0.21f, 0.0f);
		assignPoint(&(curve4.p1), 0.06f, -0.18f, 0.0f);
		assignPoint(&(curve4.p2), 0.06f, -0.15f, 0.0f);
		bezierCurve3Points(&(curve4.p0), &(curve4.p1), &(curve4.p2));

	glEnd();


	HexColorToFloatColor("060047", &c);
	glBegin(GL_POLYGON);

		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.15f, -0.255f, 0.0f);
		ellipse2D(&(curve1.p0), 0.02f, 0.035f, 0.0f, 180.0f);
		
	glEnd();


	glBegin(GL_POLYGON);

		assignPoint(&(curve1.p0), 0.09f, -0.22f, 0.0f);
		ellipse2D(&(curve1.p0), 0.06f, 0.015f, 270.0f, 450.0f);

	glEnd();

	HexColorToFloatColor("0002A1", &c);
	glBegin(GL_QUADS);
		
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.09f, -0.295f, 0.0f);
		glVertex3f(0.03f, -0.295f, 0.0f);
		glVertex3f(0.03f, -0.22f, 0.0f);
		glVertex3f(0.09f, -0.22f, 0.0f);
		
	glEnd();

	HexColorToFloatColor("0002A1", &c);
	glBegin(GL_POLYGON);

		glColor3f(c.r, c.g, c.b);
		assignPoint(&(curve1.p0), 0.03f, -0.255f, 0.0f);
		ellipse2D(&(curve1.p0), 0.02f, 0.035f, 180.0f, 360.0f);

	glEnd();

	assignPoint(&(curve1.p0), 0.05f, -0.22f, 0.0f);
	assignPoint(&(curve1.p1), 0.11f, -0.22f, 0.0f);
	assignPoint(&(curve1.p2), 0.15f, -0.18f, 0.0f);
	assignPoint(&(curve1.p3), 0.178f, -0.29f, 0.0f);

	assignPoint(&(curve3.p0), 0.09f, -0.28f, 0.0f);
	assignPoint(&(curve3.p1), 0.1f, -0.24f, 0.0f);
	assignPoint(&(curve3.p2), 0.13f, -0.27f, 0.0f);
	assignPoint(&(curve3.p3), 0.14f, -0.3f, 0.0f);

	bezier44(&curve1, &curve3, &c);

	glBegin(GL_QUADS);

		glVertex3f(0.177f, -0.29f, 0.0f);
		glVertex3f(0.14f, -0.3f, 0.0f);
		glVertex3f(0.157f, -0.385f, 0.0f);
		glVertex3f(0.185f, -0.365f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		HexColorToFloatColor("C8AE7D", &c);
		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.153f, -0.385f, 0.0f);
		glVertex3f(0.189f, -0.365f, 0.0f);
		glVertex3f(0.194f, -0.383f, 0.0f);
		glVertex3f(0.159f, -0.403f, 0.0f);

	glEnd();

	HexColorToFloatColor("765827", &c);
	glBegin(GL_QUADS);

		glColor3f(c.r, c.g, c.b);
		glVertex3f(0.19f, -0.383f, 0.0f);
		glVertex3f(0.163f, -0.403f, 0.0f);
		glVertex3f(0.18f, -0.473f, 0.0f);
		glVertex3f(0.2f, -0.453f, 0.0f);

	glEnd();

	assignPoint(&(curve2.p0), 0.19f, -0.45f, 0.0f);
	assignPoint(&(curve2.p1), 0.24f, -0.46f, 0.0f);
	assignPoint(&(curve2.p2), 0.17f, -0.47f, 0.0f);
	assignPoint(&(curve2.p3), 0.27f, -0.49f, 0.0f);
	assignPoint(&(curve2.p4), 0.21f, -0.53f, 0.0f);

	assignPoint(&(curve1.p0), 0.19f, -0.45f, 0.0f);
	assignPoint(&(curve1.p1), 0.175f, -0.47f, 0.0f);
	assignPoint(&(curve1.p2), 0.175f, -0.51f, 0.0f);
	assignPoint(&(curve1.p3), 0.21f, -0.53f, 0.0f);


	HexColorToFloatColor("765827", &c);
	bezier45(&curve1, &curve2, &c);

}


void Snowfall() {

	Point p;

	assignPoint(&p, snowfallx, snowfally, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx-0.8f, snowfally-0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 1.5f, snowfally - 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();


	assignPoint(&p, snowfallx + 0.2f, snowfally - 0.8f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();


	assignPoint(&p, snowfallx - 0.5f, snowfally - 0.8f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.3f, snowfally - 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.5f, snowfally + 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.8f, snowfally + 0.8f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 1.5f, snowfally + 0.8f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();


	assignPoint(&p, snowfallx - 1.5f, snowfally + 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx + 0.5f, snowfally + 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx + 1.0f, snowfally - 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.5f, snowfally - 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.4f, snowfally + 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.7f, snowfally - 0.1f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.6f, snowfally + 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx + 0.8f, snowfally + 0.4f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.2f, snowfally + 0.9f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.5f, snowfally + 0.2f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.8f, snowfally - 0.5f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx + 0.1f, snowfally + 0.6f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0f, 1.0f, 1.0f);
	circle2D(&p, 0.005f, 0.0f, 360.0f);
	glEnd();

	assignPoint(&p, snowfallx - 0.9f, snowfally + 0.8f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
		glColor3f(1.0f, 1.0f, 1.0f);
		circle2D(&p, 0.008f, 0.0f, 360.0f);
	glEnd();


	if (snowfally < -0.5f) {
		snowfally = 1.0f;
		snowfallx = 1.0f;
	}
	snowfally -= 0.001f;
	snowfallx -= 0.0005f;

	glutPostRedisplay();
}


void Happy() {

	if (start == 1 && HappyFlag < 1.0f)
		HappyFlag += 0.0005f;

	glBegin(GL_TRIANGLES);
	// Code of H

	glColor4f(1.0f, 0.2f, 0.4f, HappyFlag);								//Red
	glVertex3f(-0.68f, 0.7f, 0.0f);
	glVertex3f(-0.66f, 0.7f, 0.0f);
	glVertex3f(-0.68f, 0.6f, 0.0f);

	glVertex3f(-0.66f, 0.7f, 0.0f);
	glVertex3f(-0.68f, 0.6f, 0.0f);
	glVertex3f(-0.66f, 0.6f, 0.0f);

	glVertex3f(-0.66f, 0.67f, 0.0f);
	glVertex3f(-0.64f, 0.67f, 0.0f);
	glVertex3f(-0.66f, 0.63f, 0.0f);

	glVertex3f(-0.66f, 0.63f, 0.0f);
	glVertex3f(-0.64f, 0.63f, 0.0f);
	glVertex3f(-0.64f, 0.67f, 0.0f);

	glVertex3f(-0.64f, 0.7f, 0.0f);
	glVertex3f(-0.64f, 0.6f, 0.0f);
	glVertex3f(-0.62f, 0.7f, 0.0f);

	glVertex3f(-0.62f, 0.7f, 0.0f);
	glVertex3f(-0.62f, 0.6f, 0.0f);
	glVertex3f(-0.64f, 0.6f, 0.0f);


	// Code of A
	glColor4f(0.0f, 0.4f, 1.0f, HappyFlag);
	glVertex3f(-0.58f, 0.7f, 0.0f);
	glVertex3f(-0.58f, 0.68f, 0.0f);
	glVertex3f(-0.52f, 0.7f, 0.0f);

	glVertex3f(-0.52f, 0.7f, 0.0f);
	glVertex3f(-0.52f, 0.68f, 0.0f);
	glVertex3f(-0.58f, 0.68f, 0.0f);

	glVertex3f(-0.58f, 0.68f, 0.0f);
	glVertex3f(-0.56f, 0.68f, 0.0f);
	glVertex3f(-0.58f, 0.6f, 0.0f);

	glVertex3f(-0.58f, 0.6f, 0.0f);
	glVertex3f(-0.56f, 0.68f, 0.0f);
	glVertex3f(-0.56f, 0.6f, 0.0f);

	glVertex3f(-0.54f, 0.68f, 0.0f);
	glVertex3f(-0.54f, 0.6f, 0.0f);
	glVertex3f(-0.52f, 0.68f, 0.0f);

	glVertex3f(-0.52f, 0.68f, 0.0f);
	glVertex3f(-0.54f, 0.6f, 0.0f);
	glVertex3f(-0.52f, 0.6f, 0.0f);

	glVertex3f(-0.56f, 0.66f, 0.0f);
	glVertex3f(-0.56f, 0.64f, 0.0f);
	glVertex3f(-0.54f, 0.66f, 0.0f);

	glVertex3f(-0.56f, 0.64f, 0.0f);
	glVertex3f(-0.54f, 0.66f, 0.0f);
	glVertex3f(-0.54f, 0.64f, 0.0f);


	// Code of P
	glColor4f(0.0f, 0.8f, 1.0f, HappyFlag);							//blue
	glVertex3f(-0.48f, 0.7f, 0.0f);
	glVertex3f(-0.48f, 0.6f, 0.0f);
	glVertex3f(-0.46f, 0.7f, 0.0f);

	glVertex3f(-0.46f, 0.7f, 0.0f);
	glVertex3f(-0.48f, 0.6f, 0.0f);
	glVertex3f(-0.46f, 0.6f, 0.0f);

	glVertex3f(-0.44f, 0.7f, 0.0f);
	glVertex3f(-0.44f, 0.64f, 0.0f);
	glVertex3f(-0.42f, 0.7f, 0.0f);

	glVertex3f(-0.44f, 0.64f, 0.0f);
	glVertex3f(-0.42f, 0.7f, 0.0f);
	glVertex3f(-0.42f, 0.64f, 0.0f);

	glVertex3f(-0.46f, 0.7f, 0.0f);
	glVertex3f(-0.46f, 0.68f, 0.0f);
	glVertex3f(-0.44f, 0.7f, 0.0f);

	glVertex3f(-0.44f, 0.7f, 0.0f);
	glVertex3f(-0.44f, 0.68f, 0.0f);
	glVertex3f(-0.46f, 0.68f, 0.0f);

	glVertex3f(-0.46f, 0.66f, 0.0f);
	glVertex3f(-0.46f, 0.64f, 0.0f);
	glVertex3f(-0.44f, 0.66f, 0.0f);

	glVertex3f(-0.44f, 0.66f, 0.0f);
	glVertex3f(-0.44f, 0.64f, 0.0f);
	glVertex3f(-0.46f, 0.64f, 0.0f);

	// Code of P
	glColor4f(1.0f, 0.7f, 0.0f, HappyFlag);					//Gold
	glVertex3f(-0.38f, 0.7f, 0.0f);
	glVertex3f(-0.38f, 0.6f, 0.0f);
	glVertex3f(-0.36f, 0.7f, 0.0f);

	glVertex3f(-0.36f, 0.7f, 0.0f);
	glVertex3f(-0.38f, 0.6f, 0.0f);
	glVertex3f(-0.36f, 0.6f, 0.0f);

	glVertex3f(-0.34f, 0.7f, 0.0f);
	glVertex3f(-0.34f, 0.64f, 0.0f);
	glVertex3f(-0.32f, 0.7f, 0.0f);

	glVertex3f(-0.34f, 0.64f, 0.0f);
	glVertex3f(-0.32f, 0.7f, 0.0f);
	glVertex3f(-0.32f, 0.64f, 0.0f);

	glVertex3f(-0.36f, 0.7f, 0.0f);
	glVertex3f(-0.36f, 0.68f, 0.0f);
	glVertex3f(-0.34f, 0.7f, 0.0f);

	glVertex3f(-0.34f, 0.7f, 0.0f);
	glVertex3f(-0.34f, 0.68f, 0.0f);
	glVertex3f(-0.36f, 0.68f, 0.0f);

	glVertex3f(-0.36f, 0.66f, 0.0f);
	glVertex3f(-0.36f, 0.64f, 0.0f);
	glVertex3f(-0.34f, 0.66f, 0.0f);

	glVertex3f(-0.34f, 0.66f, 0.0f);
	glVertex3f(-0.34f, 0.64f, 0.0f);
	glVertex3f(-0.36f, 0.64f, 0.0f);

	// Code of Y
	glColor4f(0.6f, 1.0f, 0.3f, HappyFlag);							//Popti
	glVertex3f(-0.28f, 0.7f, 0.0f);
	glVertex3f(-0.28f, 0.66f, 0.0f);
	glVertex3f(-0.26f, 0.7f, 0.0f);

	glVertex3f(-0.26f, 0.7f, 0.0f);
	glVertex3f(-0.28f, 0.66f, 0.0f);
	glVertex3f(-0.26f, 0.66f, 0.0f);

	glVertex3f(-0.24f, 0.7f, 0.0f);
	glVertex3f(-0.24f, 0.66f, 0.0f);
	glVertex3f(-0.22f, 0.7f, 0.0f);

	glVertex3f(-0.24f, 0.66f, 0.0f);
	glVertex3f(-0.22f, 0.7f, 0.0f);
	glVertex3f(-0.22f, 0.66f, 0.0f);

	glVertex3f(-0.28f, 0.66f, 0.0f);
	glVertex3f(-0.28f, 0.64f, 0.0f);
	glVertex3f(-0.22f, 0.66f, 0.0f);

	glVertex3f(-0.28f, 0.64f, 0.0f);
	glVertex3f(-0.22f, 0.66f, 0.0f);
	glVertex3f(-0.22f, 0.64f, 0.0f);

	glVertex3f(-0.26f, 0.64f, 0.0f);
	glVertex3f(-0.26f, 0.6f, 0.0f);
	glVertex3f(-0.24f, 0.64f, 0.0f);

	glVertex3f(-0.24f, 0.64f, 0.0f);
	glVertex3f(-0.24f, 0.6f, 0.0f);
	glVertex3f(-0.26f, 0.6f, 0.0f);

	glEnd();
}

void Birthday() {

	glBegin(GL_TRIANGLES);
	glColor4f(1.0f, 0.7f, 0.0f, HappyFlag);					//Gold
	glVertex3f(-0.08f, 0.7f, 0.0f);
	glVertex3f(-0.08f, 0.6f, 0.0f);
	glVertex3f(-0.06f, 0.7f, 0.0f);

	glVertex3f(-0.06f, 0.7f, 0.0f);
	glVertex3f(-0.08f, 0.6f, 0.0f);
	glVertex3f(-0.06f, 0.6f, 0.0f);

	glVertex3f(-0.04f, 0.68f, 0.0f);
	glVertex3f(-0.04f, 0.62f, 0.0f);
	glVertex3f(-0.02f, 0.68f, 0.0f);

	glVertex3f(-0.04f, 0.62f, 0.0f);
	glVertex3f(-0.02f, 0.68f, 0.0f);
	glVertex3f(-0.02f, 0.62f, 0.0f);

	glVertex3f(-0.06f, 0.7f, 0.0f);
	glVertex3f(-0.06f, 0.68f, 0.0f);
	glVertex3f(-0.03f, 0.7f, 0.0f);

	glVertex3f(-0.03f, 0.7f, 0.0f);
	glVertex3f(-0.02f, 0.68f, 0.0f);
	glVertex3f(-0.06f, 0.68f, 0.0f);

	glVertex3f(-0.06f, 0.66f, 0.0f);
	glVertex3f(-0.06f, 0.64f, 0.0f);
	glVertex3f(-0.04f, 0.66f, 0.0f);

	glVertex3f(-0.04f, 0.66f, 0.0f);
	glVertex3f(-0.04f, 0.64f, 0.0f);
	glVertex3f(-0.06f, 0.64f, 0.0f);

	glVertex3f(-0.06f, 0.62f, 0.0f);
	glVertex3f(-0.06f, 0.6f, 0.0f);
	glVertex3f(-0.02f, 0.62f, 0.0f);

	glVertex3f(-0.02f, 0.62f, 0.0f);
	glVertex3f(-0.03f, 0.6f, 0.0f);
	glVertex3f(-0.06f, 0.6f, 0.0f);
	// End of B

	// Code of I
	glColor4f(1.0f, 0.2f, 0.4f, HappyFlag);								//Red
	glVertex3f(0.02f, 0.7f, 0.0f);
	glVertex3f(0.08f, 0.7f, 0.0f);
	glVertex3f(0.02f, 0.68f, 0.0f);

	glVertex3f(0.02f, 0.68f, 0.0f);
	glVertex3f(0.08f, 0.7f, 0.0f);
	glVertex3f(0.08f, 0.68f, 0.0f);

	glVertex3f(0.04f, 0.68f, 0.0f);
	glVertex3f(0.04f, 0.62f, 0.0f);
	glVertex3f(0.06f, 0.68f, 0.0f);

	glVertex3f(0.04f, 0.62f, 0.0f);
	glVertex3f(0.06f, 0.68f, 0.0f);
	glVertex3f(0.06f, 0.62f, 0.0f);

	glVertex3f(0.02f, 0.62f, 0.0f);
	glVertex3f(0.02f, 0.6f, 0.0f);
	glVertex3f(0.08f, 0.62f, 0.0f);

	glVertex3f(0.02f, 0.6f, 0.0f);
	glVertex3f(0.08f, 0.62f, 0.0f);
	glVertex3f(0.08f, 0.6f, 0.0f);
	// End of I

	// Code of R
	glColor4f(0.6f, 1.0f, 0.3f, HappyFlag);							//Popti
	glVertex3f(0.12f, 0.7f, 0.0f);
	glVertex3f(0.12f, 0.6f, 0.0f);
	glVertex3f(0.14f, 0.7f, 0.0f);

	glVertex3f(0.12f, 0.6f, 0.0f);
	glVertex3f(0.14f, 0.7f, 0.0f);
	glVertex3f(0.14f, 0.6f, 0.0f);

	glVertex3f(0.16f, 0.68f, 0.0f);
	glVertex3f(0.16f, 0.66f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);

	glVertex3f(0.16f, 0.66f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);
	glVertex3f(0.18f, 0.66f, 0.0f);

	glVertex3f(0.14f, 0.7f, 0.0f);
	glVertex3f(0.14f, 0.68f, 0.0f);
	glVertex3f(0.17f, 0.7f, 0.0f);

	glVertex3f(0.17f, 0.7f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);
	glVertex3f(0.14f, 0.68f, 0.0f);

	glVertex3f(0.14f, 0.66f, 0.0f);
	glVertex3f(0.14f, 0.64f, 0.0f);
	glVertex3f(0.18f, 0.66f, 0.0f);

	glVertex3f(0.18f, 0.66f, 0.0f);
	glVertex3f(0.17f, 0.64f, 0.0f);
	glVertex3f(0.14f, 0.64f, 0.0f);

	glVertex3f(0.15f, 0.65f, 0.0f);
	glVertex3f(0.16f, 0.6f, 0.0f);
	glVertex3f(0.18f, 0.6f, 0.0f);
	// End of R

	// Code of T
	glColor4f(0.0f, 0.8f, 1.0f, HappyFlag);							//blue
	glVertex3f(0.22f, 0.7f, 0.0f);
	glVertex3f(0.28f, 0.7f, 0.0f);
	glVertex3f(0.22f, 0.68f, 0.0f);

	glVertex3f(0.22f, 0.68f, 0.0f);
	glVertex3f(0.28f, 0.7f, 0.0f);
	glVertex3f(0.28f, 0.68f, 0.0f);

	glVertex3f(0.24f, 0.68f, 0.0f);
	glVertex3f(0.24f, 0.6f, 0.0f);
	glVertex3f(0.26f, 0.68f, 0.0f);

	glVertex3f(0.24f, 0.6f, 0.0f);
	glVertex3f(0.26f, 0.68f, 0.0f);
	glVertex3f(0.26f, 0.6f, 0.0f);
	// End of T

	// Code of H
	glColor4f(0.7f, 0.4f, 1.0f, HappyFlag);						//Jambhala
	glVertex3f(0.38f, 0.7f, 0.0f);
	glVertex3f(0.36f, 0.7f, 0.0f);
	glVertex3f(0.38f, 0.6f, 0.0f);

	glVertex3f(0.36f, 0.7f, 0.0f);
	glVertex3f(0.38f, 0.6f, 0.0f);
	glVertex3f(0.36f, 0.6f, 0.0f);

	glVertex3f(0.36f, 0.67f, 0.0f);
	glVertex3f(0.34f, 0.67f, 0.0f);
	glVertex3f(0.36f, 0.63f, 0.0f);

	glVertex3f(0.36f, 0.63f, 0.0f);
	glVertex3f(0.34f, 0.63f, 0.0f);
	glVertex3f(0.34f, 0.67f, 0.0f);

	glVertex3f(0.34f, 0.7f, 0.0f);
	glVertex3f(0.34f, 0.6f, 0.0f);
	glVertex3f(0.32f, 0.7f, 0.0f);

	glVertex3f(0.32f, 0.7f, 0.0f);
	glVertex3f(0.32f, 0.6f, 0.0f);
	glVertex3f(0.34f, 0.6f, 0.0f);
	// End of H

	// Code of D
	glColor4f(0.0f, 0.8f, 1.0f, HappyFlag);							//blue
	glVertex3f(0.42f, 0.7f, 0.0f);
	glVertex3f(0.42f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.7f, 0.0f);

	glVertex3f(0.44f, 0.7f, 0.0f);
	glVertex3f(0.42f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);

	glVertex3f(0.46f, 0.68f, 0.0f);
	glVertex3f(0.46f, 0.62f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);

	glVertex3f(0.46f, 0.62f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);
	glVertex3f(0.48f, 0.62f, 0.0f);

	glVertex3f(0.44f, 0.7f, 0.0f);
	glVertex3f(0.44f, 0.68f, 0.0f);
	glVertex3f(0.47f, 0.7f, 0.0f);

	glVertex3f(0.47f, 0.7f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);
	glVertex3f(0.44f, 0.68f, 0.0f);

	glVertex3f(0.44f, 0.62f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);
	glVertex3f(0.48f, 0.62f, 0.0f);

	glVertex3f(0.48f, 0.62f, 0.0f);
	glVertex3f(0.47f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);
	// End of D

	// Code of A
	glColor4f(1.0f, 0.2f, 0.4f, HappyFlag);								//Red
	glVertex3f(0.58f, 0.7f, 0.0f);
	glVertex3f(0.58f, 0.68f, 0.0f);
	glVertex3f(0.52f, 0.7f, 0.0f);

	glVertex3f(0.52f, 0.7f, 0.0f);
	glVertex3f(0.52f, 0.68f, 0.0f);
	glVertex3f(0.58f, 0.68f, 0.0f);

	glVertex3f(0.58f, 0.68f, 0.0f);
	glVertex3f(0.56f, 0.68f, 0.0f);
	glVertex3f(0.58f, 0.6f, 0.0f);

	glVertex3f(0.58f, 0.6f, 0.0f);
	glVertex3f(0.56f, 0.68f, 0.0f);
	glVertex3f(0.56f, 0.6f, 0.0f);

	glVertex3f(0.54f, 0.68f, 0.0f);
	glVertex3f(0.54f, 0.6f, 0.0f);
	glVertex3f(0.52f, 0.68f, 0.0f);

	glVertex3f(0.52f, 0.68f, 0.0f);
	glVertex3f(0.54f, 0.6f, 0.0f);
	glVertex3f(0.52f, 0.6f, 0.0f);

	glVertex3f(0.56f, 0.66f, 0.0f);
	glVertex3f(0.56f, 0.64f, 0.0f);
	glVertex3f(0.54f, 0.66f, 0.0f);

	glVertex3f(0.56f, 0.64f, 0.0f);
	glVertex3f(0.54f, 0.66f, 0.0f);
	glVertex3f(0.54f, 0.64f, 0.0f);
	// End of A

	// Code of Y
	glColor4f(0.9f, 1.0f, 0.3f, HappyFlag);							//Lemon Yellow
	glVertex3f(0.68f, 0.7f, 0.0f);
	glVertex3f(0.68f, 0.66f, 0.0f);
	glVertex3f(0.66f, 0.7f, 0.0f);

	glVertex3f(0.66f, 0.7f, 0.0f);
	glVertex3f(0.68f, 0.66f, 0.0f);
	glVertex3f(0.66f, 0.66f, 0.0f);

	glVertex3f(0.64f, 0.7f, 0.0f);
	glVertex3f(0.64f, 0.66f, 0.0f);
	glVertex3f(0.62f, 0.7f, 0.0f);

	glVertex3f(0.64f, 0.66f, 0.0f);
	glVertex3f(0.62f, 0.7f, 0.0f);
	glVertex3f(0.62f, 0.66f, 0.0f);

	glVertex3f(0.68f, 0.66f, 0.0f);
	glVertex3f(0.68f, 0.64f, 0.0f);
	glVertex3f(0.62f, 0.66f, 0.0f);

	glVertex3f(0.68f, 0.64f, 0.0f);
	glVertex3f(0.62f, 0.66f, 0.0f);
	glVertex3f(0.62f, 0.64f, 0.0f);

	glVertex3f(0.66f, 0.64f, 0.0f);
	glVertex3f(0.66f, 0.6f, 0.0f);
	glVertex3f(0.64f, 0.64f, 0.0f);

	glVertex3f(0.64f, 0.64f, 0.0f);
	glVertex3f(0.64f, 0.6f, 0.0f);
	glVertex3f(0.66f, 0.6f, 0.0f);
	// End of Y

	glEnd();
}

void Manali() {

	// Function Declarations
	void R(void);
	void A(void);
	void D(void);
	void H(void);

	if (HappyFlag >= 1.0f && ManaliFlag < 0.9f)
		ManaliFlag += 0.0003f;

	glPushMatrix();
		R();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.3f, 0.0f, 0.0f);
		A();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.1f, 0.0f, 0.0f);
		D();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.1f, 0.0f, 0.0f);
		H();
	glPopMatrix();

	glPushMatrix();
		A();
	glPopMatrix();

}

void R() {

	glBegin(GL_TRIANGLES);

	// Code of R
	glColor4f(0.0f, 0.8f, 1.0f, ManaliFlag);
	glVertex3f(0.12f, 0.7f, 0.0f);
	glVertex3f(0.12f, 0.6f, 0.0f);
	glVertex3f(0.14f, 0.7f, 0.0f);

	glVertex3f(0.12f, 0.6f, 0.0f);
	glVertex3f(0.14f, 0.7f, 0.0f);
	glVertex3f(0.14f, 0.6f, 0.0f);

	glVertex3f(0.16f, 0.68f, 0.0f);
	glVertex3f(0.16f, 0.66f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);

	glVertex3f(0.16f, 0.66f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);
	glVertex3f(0.18f, 0.66f, 0.0f);

	glVertex3f(0.14f, 0.7f, 0.0f);
	glVertex3f(0.14f, 0.68f, 0.0f);
	glVertex3f(0.17f, 0.7f, 0.0f);

	glVertex3f(0.17f, 0.7f, 0.0f);
	glVertex3f(0.18f, 0.68f, 0.0f);
	glVertex3f(0.14f, 0.68f, 0.0f);

	glVertex3f(0.14f, 0.66f, 0.0f);
	glVertex3f(0.14f, 0.64f, 0.0f);
	glVertex3f(0.18f, 0.66f, 0.0f);

	glVertex3f(0.18f, 0.66f, 0.0f);
	glVertex3f(0.17f, 0.64f, 0.0f);
	glVertex3f(0.14f, 0.64f, 0.0f);

	glVertex3f(0.15f, 0.65f, 0.0f);
	glVertex3f(0.16f, 0.6f, 0.0f);
	glVertex3f(0.18f, 0.6f, 0.0f);
	// End of R

	glEnd();
}

void A() {

	glBegin(GL_TRIANGLES);

		// Code of A
		glColor4f(0.0f, 0.8f, 1.0f, ManaliFlag);
		glVertex3f(0.58f, 0.7f, 0.0f);
		glVertex3f(0.58f, 0.68f, 0.0f);
		glVertex3f(0.52f, 0.7f, 0.0f);

		glVertex3f(0.52f, 0.7f, 0.0f);
		glVertex3f(0.52f, 0.68f, 0.0f);
		glVertex3f(0.58f, 0.68f, 0.0f);

		glVertex3f(0.58f, 0.68f, 0.0f);
		glVertex3f(0.56f, 0.68f, 0.0f);
		glVertex3f(0.58f, 0.6f, 0.0f);

		glVertex3f(0.58f, 0.6f, 0.0f);
		glVertex3f(0.56f, 0.68f, 0.0f);
		glVertex3f(0.56f, 0.6f, 0.0f);

		glVertex3f(0.54f, 0.68f, 0.0f);
		glVertex3f(0.54f, 0.6f, 0.0f);
		glVertex3f(0.52f, 0.68f, 0.0f);

		glVertex3f(0.52f, 0.68f, 0.0f);
		glVertex3f(0.54f, 0.6f, 0.0f);
		glVertex3f(0.52f, 0.6f, 0.0f);

		glVertex3f(0.56f, 0.66f, 0.0f);
		glVertex3f(0.56f, 0.64f, 0.0f);
		glVertex3f(0.54f, 0.66f, 0.0f);

		glVertex3f(0.56f, 0.64f, 0.0f);
		glVertex3f(0.54f, 0.66f, 0.0f);
		glVertex3f(0.54f, 0.64f, 0.0f);
		// End of A

	glEnd();
}

void D() {

	glBegin(GL_TRIANGLES);


	// Code of D
	glColor4f(0.0f, 0.8f, 1.0f, ManaliFlag);
	glVertex3f(0.42f, 0.7f, 0.0f);
	glVertex3f(0.42f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.7f, 0.0f);

	glVertex3f(0.44f, 0.7f, 0.0f);
	glVertex3f(0.42f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);

	glVertex3f(0.46f, 0.68f, 0.0f);
	glVertex3f(0.46f, 0.62f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);

	glVertex3f(0.46f, 0.62f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);
	glVertex3f(0.48f, 0.62f, 0.0f);

	glVertex3f(0.44f, 0.7f, 0.0f);
	glVertex3f(0.44f, 0.68f, 0.0f);
	glVertex3f(0.47f, 0.7f, 0.0f);

	glVertex3f(0.47f, 0.7f, 0.0f);
	glVertex3f(0.48f, 0.68f, 0.0f);
	glVertex3f(0.44f, 0.68f, 0.0f);

	glVertex3f(0.44f, 0.62f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);
	glVertex3f(0.48f, 0.62f, 0.0f);

	glVertex3f(0.48f, 0.62f, 0.0f);
	glVertex3f(0.47f, 0.6f, 0.0f);
	glVertex3f(0.44f, 0.6f, 0.0f);
	// End of D

	glEnd();
}

void H() {

	glBegin(GL_TRIANGLES);

	// Code of H
	glColor4f(0.0f, 0.8f, 1.0f, ManaliFlag);
	glVertex3f(0.38f, 0.7f, 0.0f);
	glVertex3f(0.36f, 0.7f, 0.0f);
	glVertex3f(0.38f, 0.6f, 0.0f);

	glVertex3f(0.36f, 0.7f, 0.0f);
	glVertex3f(0.38f, 0.6f, 0.0f);
	glVertex3f(0.36f, 0.6f, 0.0f);

	glVertex3f(0.36f, 0.67f, 0.0f);
	glVertex3f(0.34f, 0.67f, 0.0f);
	glVertex3f(0.36f, 0.63f, 0.0f);

	glVertex3f(0.36f, 0.63f, 0.0f);
	glVertex3f(0.34f, 0.63f, 0.0f);
	glVertex3f(0.34f, 0.67f, 0.0f);

	glVertex3f(0.34f, 0.7f, 0.0f);
	glVertex3f(0.34f, 0.6f, 0.0f);
	glVertex3f(0.32f, 0.7f, 0.0f);

	glVertex3f(0.32f, 0.7f, 0.0f);
	glVertex3f(0.32f, 0.6f, 0.0f);
	glVertex3f(0.34f, 0.6f, 0.0f);
	// End of H

	glEnd();
}