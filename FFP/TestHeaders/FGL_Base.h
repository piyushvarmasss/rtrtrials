
// FGL_Base.h header is used to provide basic structures and functions used by Frustum Group.

typedef struct FGL_Point {
	float x;
	float y;
	float z;
} FGL_Point;

typedef struct FGL_Color {
	float r;
	float g;
	float b;
	float a;
} FGL_Color;

typedef struct FGL_3Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
} FGL_3Points;

typedef struct FGL_4Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
} FGL_4Points;

typedef struct FGL_5Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
	FGL_Point p4;
} FGL_5Points;


void FGL_ColorToFloat(char color[], FGL_Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

	c->a = 1.0f;
}

void FGL_PointAssign(FGL_Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

void FGL_ColorAssign(FGL_Color* color, float r, float g, float b, float a) {
	color->r = r;
	color->g = g;
	color->b = b;
	color->a = a;
}