
// FGL_Curves.h is related to any curve used by Frustum Group. 

#define FGL_Bezier3(u, p0, p1, p2, axis) (p0.##axis * (1.0f - u) * (1.0f - u)) + (p1.##axis * 2.0f * (1.0f - u) * u) + (p2.##axis * u * u)
#define FGL_Bezier4(u, p0, p1, p2, p3, axis) (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 3.0f * (1.0f - u) * u * u) + (p3.##axis * u * u * u)
#define FGL_Bezier5(u, p0, p1, p2, p3, p4, axis)  (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p3.##axis * 4.0f * (1.0f - u) * u * u * u) + (p4.##axis * u * u * u * u);


void FGL_BezierCurve3(FGL_3Points *curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, x);
		y = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, y);
		z = FGL_Bezier3(u, curve->p0, curve->p2, curve->p2, z);
		glVertex3f(x, y, z);
	}
}

void FGL_BezierCurve4(FGL_4Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, x);
		y = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, y);
		z = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, z);
		glVertex3f(x, y, z);
	}
}

void FGL_BezierCurve5(FGL_5Points* curve){

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		x = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, x);
		y = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, y); 
		z = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, z);

		glVertex3f(x, y, z);
	}
}

void FGL_BezierShape33(FGL_3Points* curve1, FGL_3Points* curve2, FGL_Color* c) {

	float x = 0.0f; 
	float y = 0.0f; 
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, x);
		y = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, y);
		z = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void FGL_BezierShape44(FGL_4Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f; 
	float y = 0.0f; 
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void FGL_BezierShape55(FGL_5Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f; 
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, x);
		y = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, y);
		z = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void FGL_BezierShape34(FGL_3Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void FGL_BezierShape35(FGL_3Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

void FGL_BezierShape45(FGL_4Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// BSpline Function to implement

