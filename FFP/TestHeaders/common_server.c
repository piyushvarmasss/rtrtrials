
#include <windows.h> //For WIN32
#include <stdio.h>	//For FILE IO
#include <stdlib.h> //For exit 

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

// FGL.c file is used for basic operations of FGL OpenGL Group.


/*********************** Basic **********************************/

// Function : FGL_ColorToFloat() 
// Used to : convert String of hexadecimal color into equivalent color values into given pointer to FGL_Color object. 
// Parameters : 
// char color[] - String of 6 hexadecimal characters indicates color code.
// FGL_Color* c - Pointer to object of color which stores the respective color code string.
// Returns : void (nothing).
void FGL_ColorToFloat(char color[], FGL_Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

	c->a = 1.0f;
}

// Function : FGL_PointAssign() 
// Used to : assigns given float values of x, y and z in respective co-ordinates into given pointer to FGL_Point object.
// Parameters : 
// FGL_Point* point - Pointer to object of point which stores the given coordinate values.
// float x - indicates given x coordinate and stored in given pointer to FGL_Point. 
// float y - indicates given y coordinate and stored in given pointer to FGL_Point. 
// float z - indicates given z coordinate and stored in given pointer to FGL_Point. 
// Returns : void (nothing). 
void FGL_PointAssign(FGL_Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

// Function : FGL_ColorAssign() 
// Used to : assigns given float values of red, green and blue in respective members into given pointer to FGL_Color object.
// Parameters : 
// FGL_Color* color - Pointer to object of color which stores the given color values.
// float r - indicates given red color and stored in given pointer to FGL_Color. 
// float g - indicates given green color and stored in given pointer to FGL_Color. 
// float b - indicates given blue color and stored in given pointer to FGL_Color.  
// float a - indicates given alpha color and stored in given pointer to FGL_Color. 
// // Returns : void (nothing). 
void FGL_ColorAssign(FGL_Color* color, float r, float g, float b, float a) {
	color->r = r;
	color->g = g;
	color->b = b;
	color->a = a;
}



/*********************** Mathematics **********************************/

// Function : FGL_Sin()
// Used to : calculate Sine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Sin value of given angle).
double FGL_Sin(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Sine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Sine180(angle));
	}
}

// Function : FGL_Cos()
// Used to : calculate Cosine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Cos value of given angle).
double FGL_Cos(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Cosine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Cosine180(angle));
	}
}

// Function : FGL_SquareRoot()
// Used to : calculates square root of any number using Bakhshali method.
// Parameters : 
// double num - indicates a number of which square-root to be calculated.
// Returns : double (square-root of given number).
double FGL_SquareRoot(double num) {

	double temp = num / 2.0;
	double a, b;

	int itr = 4;
	while (--itr) {

		a = (num - FGL_Square(temp)) / (2.0 * temp);
		b = (temp + a);
		temp = b - (FGL_Square(a) / (2 * b));
	}

	return temp;
}

// Function : FGL_Power()
// Used to : calculates any integer power of any number.
// Parameters : 
// double num - indicates a number of which power to be calculated.
// int deg - indicates which power of number to be calculated.
// Returns : double (given power of given number).
double FGL_Power(double num, int deg) {

	if (deg == 0) {

		if (num == 0) {

			return 0;
		}

		return 1;
	}
	else if (deg == 1) {

		return num;
	}
	else {

		double ret = num;

		while (--deg) {

			ret *= num;
		}

		return (deg < 0 ? (1.0 / ret) : ret);
	}
}

// Function : FGL_Factorial()
// Used to : calculates factorial of given number.
// Parameters : 
// int num - indicates a number of which factorial to be calculated.
// Returns : long long int (factorial of given number).
long long int FGL_Factorial(int num) {

	long long int ret = num;
	while (--num > 1)
		ret *= num;

	return ret;
}

// Function : FGL_Ceil()
// Used to : calculates ceil integer value of any number.
// Parameters : 
// double num - indicates a number of which ceil to be calculated.
// Returns : int (ceil integer of given number).
int FGL_Ceil(double num) {

	if (num > (int)num)
		num = (int)num + 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}

// Function : FGL_Floor()
// Used to : calculates floor integer value of any number.
// Parameters : 
// double num - indicates a number of which floor to be calculated.
// Returns : int (floor integer of given number).
int FGL_Floor(double num) {

	if (num < (int)num)
		num = (int)num - 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}

// Function : FGL_CeilBase()
// Used to : calculates ceil integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which ceil to be calculated of given base.
// double base - indicates a base.
// Returns : int (ceil integer of given number with given base).
int FGL_CeilBase(double num, double base) {

	num = FGL_Ceil(num / base);
	return FGL_Ceil(num * base);
}

// Function : FGL_Floor()
// Used to : calculates floor integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which floor to be calculated of given base.
// double base - indicates a base.
// Returns : int (floor integer of given number with given base).
int FGL_FloorBase(double num, double base) {

	num = FGL_Floor(num / base);
	return FGL_Floor(num * base);
}



/*********************** Shapes **********************************/

// Function : FGL_Circle()
// Used to : draw 2D circle of given radius bound with start and end angle. 
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Circle.
// float radius - indicates radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Circle(FGL_Point* center, float radius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (radius * FGL_Cos(sAngle)), center->y + (radius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}

// Function : FGL_Ellipse()
// Used to : draw 2D ellipse of given two radius bound with start and end angle.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Ellipse.
// float xRadius - indicates Xaxis radius of circle to be drawn. 
// float yRadius - indicates Yaxis radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Ellipse(FGL_Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (xRadius * FGL_Cos(sAngle)), center->y + (yRadius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}

// Function : FGL_Sphere()
// Used to : draw sphere of given radius.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Sphere.
// FGL_Color* color1 - Pointer to object of color which indicates one color in sphere.
// FGL_Color* color2 - Pointer to object of color which indicates another color in sphere.
// float radius - indicates Xaxis radius of circle to be drawn. 
// Returns : void (nothing).
void FGL_Sphere(FGL_Point* center, FGL_Color* color1, FGL_Color* color2, float radius) {

	int i = 0;
	float angle = 0.0f;
	float z = center->z;

	while (angle <= 90.0f) {

		glBegin(GL_POLYGON);

		if (i % 2)
			glColor4f(color1->r, color1->g, color1->b, color1->a);
		else
			glColor4f(color2->r, color2->g, color2->b, color2->a);

		center->z = z + (FGL_Cos(angle) * radius);
		FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

		center->z = z - (FGL_Cos(angle) * radius);
		FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

		glEnd();

		angle += 0.1f;
		i++;
	}
}



/*********************** Curves **********************************/

// Function : FGL_BezierCurve3()
// Used to : draw bezier curve of 3 points.
// Parameters : 
// FGL_3Points* curve - Pointer to object of 3 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve3(FGL_3Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, x);
		y = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, y);
		z = FGL_Bezier3(u, curve->p0, curve->p2, curve->p2, z);
		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierCurve4()
// Used to : draw bezier curve of 4 points.
// Parameters : 
// FGL_4Points* curve - Pointer to object of 4 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd();
void FGL_BezierCurve4(FGL_4Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, x);
		y = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, y);
		z = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, z);
		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierCurve5()
// Used to : draw bezier curve of 5 points.
// Parameters : 
// FGL_5Points* curve - Pointer to object of 5 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve5(FGL_5Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		x = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, x);
		y = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, y);
		z = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, z);

		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierShape33()
// Used to : draw a shape enclosed in between 2 bezier curves of 3 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_3Points* curve2 - Pointer to object of 3 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape33(FGL_3Points* curve1, FGL_3Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, x);
		y = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, y);
		z = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape44()
// Used to : draw a shape enclosed in between 2 bezier curves of 4 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape44(FGL_4Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape55()
// Used to : draw a shape enclosed in between 2 bezier curves of 5 points.
// Parameters : 
// FGL_5Points* curve1 - Pointer to object of 5 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape55(FGL_5Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, x);
		y = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, y);
		z = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape34()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 4 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape34(FGL_3Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape35()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 5 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape35(FGL_3Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape45()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 4 points and other of 5 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape45(FGL_4Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

