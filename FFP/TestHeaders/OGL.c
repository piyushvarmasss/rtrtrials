
// Common Header Files
#include<stdio.h>			// File IO
#include<stdlib.h>			// exit()
#include <windows.h>		// WIN32 SDK
#include <string.h>
#include "OGL.h"	

// OpenGL Header Files
#include<gl/GL.h>			// C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl
#include<GL/glu.h>

#include "FGL_Base.h"
#include "FGL_Math.h"
#include "FGL_Shapes.h"
#include "FGL_Curves.h"

// Macros
#define PNV_WIN_WIDTH 800
#define PNV_WIN_HEIGHT 600

// Link with OpenGL Library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


// Global Variable Declarations
FILE* PNV_gFILE = NULL;

HWND PNV_ghwnd = NULL;
BOOL PNV_gbActive = FALSE;
DWORD PNV_dwStyle = 0;
WINDOWPLACEMENT PNV_wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL PNV_gbFullscreen = FALSE;



// OpenGL related Global Variables
HDC PNV_ghdc = NULL;					// Global Handle Device Context  (used when any function do not have window handle)
HGLRC PNV_ghrc = NULL;					// Global Handle Rendering Context	(used for render the window from overall program)


GLfloat angle = 0.0f;
GLfloat x = 0.0f;
GLfloat y = 0.0f;
GLfloat z = 0.0f;
GLfloat inc = 0.0f;
FGL_Point points[20000];

int initialFlag = 0;

int num = 0;

int curveFlag = 0;

int index1 = 0;

int delay = 0;

// Entry-Point Function
int WINAPI WinMain(HINSTANCE PNV_hInstance, HINSTANCE PNV_hPrevInstance, LPSTR PNV_lpszCmdLine, int PNV_iCmdShow)
{
	// Function Declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local Variable Declarations
	WNDCLASSEX PNV_wndclass;
	HWND PNV_hwnd;
	MSG PNV_msg;
	TCHAR PNV_szAppName[] = TEXT("PNVWindow");
	BOOL PNV_bDone = FALSE;
	int PNV_iResult = 0;

	int PNV_SW = GetSystemMetrics(SM_CXSCREEN);
	int PNV_SH = GetSystemMetrics(SM_CYSCREEN);
	int PNV_xCoordinate = ((PNV_SW / 2) - (PNV_WIN_WIDTH / 2));
	int PNV_yCoordinate = ((PNV_SH / 2) - (PNV_WIN_HEIGHT / 2));
	
	// Code
	PNV_gFILE = fopen("log.txt", "w");
	if (PNV_gFILE == NULL)
	{
		MessageBox(NULL, TEXT("Program cannot open log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(PNV_gFILE, "Program started successfully.\n");

	// WNDCLASSEX Initilization 

	PNV_wndclass.cbSize = sizeof(WNDCLASSEX);
	PNV_wndclass.style = CS_HREDRAW | CS_VREDRAW;
	PNV_wndclass.cbClsExtra = 0;
	PNV_wndclass.cbWndExtra = 0;
	PNV_wndclass.lpfnWndProc = WndProc;
	PNV_wndclass.hInstance = PNV_hInstance;
	PNV_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	PNV_wndclass.hIcon = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));
	PNV_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	PNV_wndclass.lpszClassName = PNV_szAppName;
	PNV_wndclass.lpszMenuName = NULL;
	PNV_wndclass.hIconSm = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));


	// Register WNDCLASSEX
	RegisterClassEx(&PNV_wndclass);


	// Create Window								// glutCreateWindow
	PNV_hwnd = CreateWindowEx(WS_EX_APPWINDOW,			// to above of taskbar for fullscreen
						PNV_szAppName,
						TEXT("Piyush Niranjan Varma"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						PNV_xCoordinate,				// glutWindowPosition 1st Parameter
						PNV_yCoordinate,				// glutWindowPosition 2nd Parameter
						PNV_WIN_WIDTH,					// glutWindowSize 1st Parameter
						PNV_WIN_HEIGHT,					// glutWindowSize 2nd Parameter
						NULL,
						NULL,
						PNV_hInstance,
						NULL);

	PNV_ghwnd = PNV_hwnd;

	// Initialization
	PNV_iResult = initialize();
	if (PNV_iResult != 0)
	{
		MessageBox(PNV_hwnd, TEXT("initialize() Failed!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(PNV_hwnd);
	}

	// Show The Window
	ShowWindow(PNV_hwnd, PNV_iCmdShow);
	SetForegroundWindow(PNV_hwnd);
	SetFocus(PNV_hwnd);

	// Game Loop					// glutMainLoop()
	while (PNV_bDone == FALSE)
	{
		if (PeekMessage(&PNV_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (PNV_msg.message == WM_QUIT)
				PNV_bDone = TRUE;
			else
			{
				TranslateMessage(&PNV_msg);
				DispatchMessage(&PNV_msg);
			}
		}
		else
		{
			if (PNV_gbActive == TRUE)
			{
				// Render
				display();

				// Update
				update();
			}
		}
	}

	// Uninitialization
	uninitialize();						// glutCloseFunc(uninitialize)

	return((int)PNV_msg.wParam);
}


// CALLBACK Function
LRESULT CALLBACK WndProc(HWND PNV_hwnd, UINT PNV_iMsg, WPARAM PNV_wParam, LPARAM PNV_lParam)
{
	// Function Declarations
	void ToggleFullscreen( void );
	void resize(int, int);

	// Code
	switch (PNV_iMsg)
	{
		case WM_SETFOCUS:
			PNV_gbActive = TRUE;
			break;

		case WM_KILLFOCUS:
			PNV_gbActive = FALSE;
			break;

		case WM_SIZE:
			resize(LOWORD(PNV_lParam), HIWORD(PNV_lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_KEYDOWN:
			switch (LOWORD(PNV_wParam))
			{
			case VK_ESCAPE:
				fprintf(PNV_gFILE, "Program ended successfully.\n");
				fclose(PNV_gFILE);
				PNV_gFILE = NULL;
				DestroyWindow(PNV_hwnd);
				break;
			}
			break;

		case WM_CHAR:
			switch (LOWORD(PNV_wParam))
			{
				case 'F':
				case 'f':
					if (PNV_gbFullscreen == FALSE)
					{
						ToggleFullscreen();
						PNV_gbFullscreen = TRUE;
						fprintf(PNV_gFILE, "Program entered Fullscreen.\n");
					}
					else
					{
						ToggleFullscreen();
						PNV_gbFullscreen = FALSE;
						fprintf(PNV_gFILE, "Program ended Fullscreen.\n");
					}
					break;

				case 'C':
				case 'c':
					if (curveFlag == 0) {
						curveFlag = 1;
					}
					else {
						curveFlag = 0;
					}
					break;
			}
			break;

		case WM_RBUTTONDOWN:									// glutMouseFunc(mouse)
			DestroyWindow(PNV_hwnd);
			break;

		case WM_CLOSE:
			DestroyWindow(PNV_hwnd);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return(DefWindowProc(PNV_hwnd, PNV_iMsg, PNV_wParam, PNV_lParam));
}


void ToggleFullscreen(void)
{
	// Local Variable Declarations
	MONITORINFO PNV_mi = { sizeof(MONITORINFO) };

	// Code
	if (PNV_gbFullscreen == FALSE)
	{
		PNV_dwStyle = GetWindowLong(PNV_ghwnd, GWL_STYLE);

		if (PNV_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(PNV_ghwnd, &PNV_wpPrev) && GetMonitorInfo(MonitorFromWindow(PNV_ghwnd, MONITORINFOF_PRIMARY), &PNV_mi))
			{
				SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(PNV_ghwnd, HWND_TOP, PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.top, PNV_mi.rcMonitor.right - PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.bottom - PNV_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				// HWND_TOP ~ WS_OVERLAPPED, rc ~ RECT, SWP_FRAMECHANGED ~ WM_NCCALCSIZE msg
			}
		}

		ShowCursor(FALSE);
	}
	else {
		SetWindowPlacement(PNV_ghwnd, &PNV_wpPrev);
		SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(PNV_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		// SetWindowPos has greater priority than SetWindowPlacement and SetWindowStyle for Z-Order
		ShowCursor(TRUE);
	}
}


int initialize(void)
{
	// Local Variables
	PIXELFORMATDESCRIPTOR PNV_pfd;
	int PNV_iPixelFormatIndex = 0;

	// Code

	// Initialize pfd
	ZeroMemory(&PNV_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	PNV_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	PNV_pfd.nVersion = 1;
	PNV_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	PNV_pfd.iPixelType = PFD_TYPE_RGBA;
	PNV_pfd.cColorBits = 32;
	PNV_pfd.cRedBits = 8;
	PNV_pfd.cGreenBits = 8;
	PNV_pfd.cBlueBits = 8;
	PNV_pfd.cAlphaBits = 8;
	PNV_pfd.cDepthBits = 32;

	// Get the DC (in OpenGL Global Variables)
	PNV_ghdc = GetDC(PNV_ghwnd);

	if (PNV_ghdc == NULL)
	{
		fprintf(PNV_gFILE, "GetDC Failed!\n");
		return(-1);
	}

	// Initialize iPixelFormatIndex
	PNV_iPixelFormatIndex = ChoosePixelFormat(PNV_ghdc, &PNV_pfd);				// Must be non Zero Positive

	if (PNV_iPixelFormatIndex == 0)
	{
		fprintf(PNV_gFILE, "ChoosePixelFormat function failed!\n");
		return(-2);
	}

	// Set Obtained Pixel Format
	if (SetPixelFormat(PNV_ghdc, PNV_iPixelFormatIndex, &PNV_pfd) == FALSE)
	{
		fprintf(PNV_gFILE, "SetPixelFormat funtion failed!\n");
		return(-3);
	}

	// Tell WGL (Bridging API from Window to OpenGL i.e. Windows Graphics Library) to give OpenGL compatible context from this device context
	PNV_ghrc = wglCreateContext(PNV_ghdc);

	if (PNV_ghrc == NULL)
	{
		fprintf(PNV_gFILE, "wglCreateContext function failed!\n");
		return(-4);
	}

	// Make Rendering Context Current
	if (wglMakeCurrent(PNV_ghdc, PNV_ghrc) == FALSE)
	{
		fprintf(PNV_gFILE, "wglMakeCurrent function failed!\n");
		return(-5);
	}

	glShadeModel(GL_SMOOTH);					
	glClearDepth(1.0f);					
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);				
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Set the Clear Color of Window to Blue (OpenGL Function)
	// Here OpenGL starts
	/// glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);						

	// Warm Up Resize
	resize(PNV_WIN_WIDTH, PNV_WIN_HEIGHT);

	return(0);
}


void resize(int PNV_width, int PNV_height)
{
	// Code
	if (PNV_height <= 0)
		PNV_height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)PNV_width, (GLsizei)PNV_height);					// Like Binacular (Part of window to view)
	gluPerspective(45.0f, (GLfloat)PNV_width / (GLfloat)PNV_height, 0.1f, 100.0f);
}


void display(void)
{	
	// Function Declarations
	void Pyramid(void);

	// Variable Declarations
	FGL_Point center;

	FGL_3Points curve1;
	FGL_4Points curve2;

	// Gives Color to Window (given form glClearColor in initialize) 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Temp
	//glTranslatef(0.0f, 0.0f, -25.0f);
	//glRotatef(90.0f, 0.0f, 1.0f, 0.0f);

	FGL_PointAssign(&curve1.p0, 3.0f, 8.0f, 7.0f);
	FGL_PointAssign(&curve1.p1, 3.0f, 1.5f, 7.0f);
	FGL_PointAssign(&curve1.p2, 0.0f, 1.0f, 2.0f);

	FGL_PointAssign(&curve2.p0, 0.0f, 1.0f, 2.0f);
	FGL_PointAssign(&curve2.p1, -3.2f, 0.7f, -3.0f);
	FGL_PointAssign(&curve2.p2, 3.2f, 0.3f, -3.0f);
	FGL_PointAssign(&curve2.p3, 0.0f, 0.0f, -8.0f);

	if (initialFlag == 0) {

		int i = 0;
		for (float u = 0.0f; u <= 1.0f; u += 0.0001f, i++) {

			points[i].x = FGL_Bezier3(u, curve1.p0, curve1.p1, curve1.p2, x);
			points[i].y = FGL_Bezier3(u, curve1.p0, curve1.p1, curve1.p2, y);
			points[i].z = FGL_Bezier3(u, curve1.p0, curve1.p1, curve1.p2, z);
		}

		for (float u = 0.0f; u <= 1.0f; u += 0.0001f, i++) {

			points[i].x = FGL_Bezier4(u, curve2.p0, curve2.p1, curve2.p2, curve2.p3, x);
			points[i].y = FGL_Bezier4(u, curve2.p0, curve2.p1, curve2.p2, curve2.p3, y);
			points[i].z = FGL_Bezier4(u, curve2.p0, curve2.p1, curve2.p2, curve2.p3, z);
		}
		initialFlag = 1;
	}
	
	
	gluLookAt(points[index1].x, points[index1].y, points[index1].z, 0.0f, 0.0f, -8.0f, 0.0f, points[index1].y + 1.0f, 0.0f);
	//gluLookAt(points[index1].x, points[index1].y, points[index1].z, 0.0f, 0.0f, -8.0f, points[index1].x, points[index1].y + 1.0f, points[index1].z);

	if (curveFlag == 0) {

			glBegin(GL_LINE_STRIP);

				glColor3f(1.0f, 1.0f, 1.0f);

				for (int i = 0; i < 20000; i++)
					glVertex3f(points[i].x, points[i].y, points[i].z);

			glEnd();
	}

	// Pyramid Code

	glPushMatrix();

		glTranslatef(-1.5f, 0.0f, 0.0f);
		Pyramid();

	glPopMatrix();


	glPushMatrix();

		glTranslatef(1.5f, 0.0f, -2.0f);
		Pyramid();

	glPopMatrix();


	glPushMatrix();

		glTranslatef(-1.5f, 0.0f, -4.0f);
		Pyramid();

	glPopMatrix();


	glPushMatrix();

		glTranslatef(1.5f, 0.0f, -6.0f);
		Pyramid();

	glPopMatrix();

	SwapBuffers(PNV_ghdc);
}


void update(void)
{
	delay = delay + 1;
	if (index1 < 19999)
		index1 = index1 + 1;
}


void uninitialize(void)
{
		// Function Declarations
		void ToggleFullScreen(void);

		// Code
		// If Application is exitting in full-screen
		if (PNV_gbFullscreen == TRUE)
		{
			ToggleFullscreen();
			PNV_gbFullscreen = FALSE;
		}

		// Make the hdc as current DC
		if (wglGetCurrentContext() == PNV_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}

		// Delete Rendering Context
		if (PNV_ghrc)
		{
			wglDeleteContext(PNV_ghrc);
			PNV_ghrc = NULL;
		}

		// Release the hdc
		if (PNV_ghdc)
		{
			ReleaseDC(PNV_ghwnd, PNV_ghdc);
			PNV_ghdc = NULL;
		}

		// Destroy Window
		if (PNV_ghwnd)
		{
			DestroyWindow(PNV_ghwnd);
			PNV_ghwnd = NULL;
		}

		// Close the log file
		if (PNV_gFILE)
		{
			fprintf(PNV_gFILE, "Program ended successfully.\n");
			fclose(PNV_gFILE);
			PNV_gFILE = NULL;
		}

}


void Pyramid() {

	glBegin(GL_TRIANGLES);

		// Front Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);

		// Right Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		// Back Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		// Left Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);

		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
}
