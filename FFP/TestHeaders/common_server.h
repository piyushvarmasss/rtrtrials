
#if !defined(FGL_HDR)
// FGL.h header is used for basic operations of FGL OpenGL Group.



/*********************** Basic **********************************/

// Object Like Macro : FGL_HDR
// Used to : prevent inclusion of header multiple times.
#define FGL_HDR 1

// Structure : FGL_Point
// Contains : Three float members
// Used to : indicate a vertex of OpenGL. 
// x indicates  X-coordinate
// y indicates  Y-coordinate
// z indicates  Z-coordinate
typedef struct FGL_Point {
	float x;
	float y;
	float z;
} FGL_Point;


// Structure : FGL_Color
// Contains : Four float members
// Used to : indicate a color of OpenGL.
// r indicates  red color 
// g indicates  green color
// b indicates  blue color
// a indicates  alpha 
typedef struct FGL_Color {
	float r;
	float g;
	float b;
	float a;
} FGL_Color;


// Structure : FGL_3Points 
// Contains : Three FGL_Point objects
// Used to : indicate a collection of three vertices used for curves in OpenGL. 
// p0 indicates  first vertex 
// p1 indicates  second vertex
// p2 indicates  third vertex
typedef struct FGL_3Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
} FGL_3Points;


// Structure : FGL_4Points 
// Contains : Four FGL_Point objects
// Used to : indicate a collection of four vertices used for curves in OpenGL. 
// p0 indicates  first vertex 
// p1 indicates  second vertex
// p2 indicates  third vertex
// p3 indicates  fourth vertex
typedef struct FGL_4Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
} FGL_4Points;


// Structure : FGL_5Points 
// Contains : Five FGL_Point objects
// Used to : indicate a collection of five vertices used for curves in OpenGL. 
// p0 indicates  first vertex 
// p1 indicates  second vertex
// p2 indicates  third vertex
// p3 indicates  fourth vertex
// p4 indicates  fifth vertex
typedef struct FGL_5Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
	FGL_Point p4;
} FGL_5Points;



// Function : FGL_ColorToFloat() 
// Used to : convert String of hexadecimal color into equivalent color values into given pointer to FGL_Color object. 
// Parameters : 
// char color[] - String of 6 hexadecimal characters indicates color code.
// FGL_Color* c - Pointer to object of color which stores the respective color code string.
// Returns : void (nothing).
void FGL_ColorToFloat(char color[], FGL_Color* c);


// Function : FGL_PointAssign() 
// Used to : assigns given float values of x, y and z in respective co-ordinates into given pointer to FGL_Point object.
// Parameters : 
// FGL_Point* point - Pointer to object of point which stores the given coordinate values.
// float x - indicates given x coordinate and stored in given pointer to FGL_Point. 
// float y - indicates given y coordinate and stored in given pointer to FGL_Point. 
// float z - indicates given z coordinate and stored in given pointer to FGL_Point. 
// Returns : void (nothing).
void FGL_PointAssign(FGL_Point* point, float x, float y, float z);


// Function : FGL_ColorAssign() 
// Used to : assigns given float values of red, green and blue in respective members into given pointer to FGL_Color object.
// Parameters : 
// FGL_Color* color - Pointer to object of color which stores the given color values.
// float r - indicates given red color and stored in given pointer to FGL_Color. 
// float g - indicates given green color and stored in given pointer to FGL_Color. 
// float b - indicates given blue color and stored in given pointer to FGL_Color.  
// float a - indicates given alpha color and stored in given pointer to FGL_Color. 
// // Returns : void (nothing). 
void FGL_ColorAssign(FGL_Color* color, float r, float g, float b, float a);



/*********************** Mathematics **********************************/


// Object Like Macro : FGL_Pi
// Used to : standard pi value.
#define FGL_Pi 3.14159265359


// Function like Macro : FGL_Radian
// Used to : convert any degree into radian.
#define FGL_Radian(degree)  ((degree * FGL_Pi) / 180.00)


// Function like Macro : FGL_Square
// Used to : calculate square of any number hard-codedly.
#define FGL_Square(num)   (num * num)
// Function like Macro : FGL_Cube
// Used to : calculate cube of any number hard-codedly.
#define FGL_Cube(num)     (num * num * num)
// Function like Macro : FGL_Quad
// Used to : calculate fourth power of any number hard-codedly.
#define FGL_Quad(num)     (num * num * num * num)
// Function like Macro : FGL_Penta
// Used to : calculate fifth power of any number hard-codedly.
#define FGL_Penta(num)    (num * num * num * num * num)
// Function like Macro : FGL_Hexa
// Used to : calculate sixth power of any number hard-codedly.
#define FGL_Hexa(num)     (num * num * num * num * num * num)
// Function like Macro : FGL_Hepta
// Used to : calculate seventh power of any number hard-codedly.
#define FGL_Hepta(num)    (num * num * num * num * num * num * num)
// Function like Macro : FGL_Octa
// Used to : calculate eighth power of any number hard-codedly.
#define FGL_Octa(num)     (num * num * num * num * num * num * num * num)
// Function like Macro : FGL_Nona
// Used to : calculate nineth power of any number hard-codedly.
#define FGL_Nona(num)     (num * num * num * num * num * num * num * num * num)
// Function like Macro : FGL_Deca
// Used to : calculate tenth power of any number hard-codedly.
#define FGL_Deca(num)     (num * num * num * num * num * num * num * num * num * num)
// Function like Macro : FGL_Hedeca
// Used to : calculate eleventh power of any number hard-codedly.
#define FGL_Hedeca(num)   (num * num * num * num * num * num * num * num * num * num * num)
// Function like Macro : FGL_Dodeca
// Used to : calculate twelveth power of any number hard-codedly.
#define FGL_Dodeca(num)   (num * num * num * num * num * num * num * num * num * num * num * num)


// Function like Macro : FGL_Sine180()
// Used to : calculate Sine value between 0 - FGL_Pi radians.
#define FGL_Sine180(radian)  (radian - (FGL_Cube(radian) / 6.00) + (FGL_Penta(radian) / 120.00) - (FGL_Hepta(radian) / 5040.00) + (FGL_Nona(radian) / 362880.00) - (FGL_Hedeca(radian) / 39916800.00))

// Function like Macro : FGL_Cosine180()
// Used to : calculate Cosine value between 0 - FGL_Pi radians.
#define FGL_Cosine180(radian)  (1.00 - (FGL_Square(radian) / 2.00) + (FGL_Quad(radian) / 24.00) - (FGL_Hexa(radian) / 720.00) + (FGL_Octa(radian) / 40320.00) - (FGL_Deca(radian) / 3628800.00) + (FGL_Dodeca(radian) / 479001600.00))


// Function : FGL_Sin()
// Used to : calculate Sine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Sin value of given angle).
double FGL_Sin(double angle);


// Function : FGL_Cos()
// Used to : calculate Cosine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Cos value of given angle).
double FGL_Cos(double angle);


// Function like Macro : FGL_Tan()
// Used to : calculate Tan value of any degree.
#define FGL_Tan(degree)  (FGL_Sin(degree) / FGL_Cos(degree))
// Function like Macro : FGL_Cot()
// Used to : calculate Cot value of any degree.
#define FGL_Cot(degree)  (FGL_Cos(degree) / FGL_Sin(degree))
// Function like Macro : FGL_Sec()
// Used to : calculate Sec value of any degree.
#define FGL_Sec(degree)  (1.00 / FGL_Cos(degree))
// Function like Macro : FGL_Cosec()
// Used to : calculate Cosec value of any degree.
#define FGL_Cosec(degree)  (1.00 / FGL_Sin(degree))


// Function : FGL_SquareRoot()
// Used to : calculates square root of any number using Bakhshali method.
// Parameters : 
// double num - indicates a number of which square-root to be calculated.
// Returns : double (square-root of given number).
double FGL_SquareRoot(double num);


// Function : FGL_Power()
// Used to : calculates any integer power of any number.
// Parameters : 
// double num - indicates a number of which power to be calculated.
// int deg - indicates which power of number to be calculated.
// Returns : double (given power of given number).
double FGL_Power(double num, int deg);


// Function : FGL_Factorial()
// Used to : calculates factorial of given number.
// Parameters : 
// int num - indicates a number of which factorial to be calculated.
// Returns : long long int (factorial of given number).
long long int FGL_Factorial(int num);


// Function : FGL_Ceil()
// Used to : calculates ceil integer value of any number.
// Parameters : 
// double num - indicates a number of which ceil to be calculated.
// Returns : int (ceil integer of given number).
int FGL_Ceil(double num);


// Function : FGL_Floor()
// Used to : calculates floor integer value of any number.
// Parameters : 
// double num - indicates a number of which floor to be calculated.
// Returns : int (floor integer of given number).
int FGL_Floor(double num);


// Function : FGL_CeilBase()
// Used to : calculates ceil integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which ceil to be calculated of given base.
// double base - indicates a base.
// Returns : int (ceil integer of given number with given base).
int FGL_CeilBase(double num, double base);


// Function : FGL_Floor()
// Used to : calculates floor integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which floor to be calculated of given base.
// double base - indicates a base.
// Returns : int (floor integer of given number with given base).
int FGL_FloorBase(double num, double base);



/*********************** Shapes **********************************/


// Function : FGL_Circle()
// Used to : draw 2D circle of given radius bound with start and end angle. 
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Circle.
// float radius - indicates radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Circle(FGL_Point* center, float radius, float sAngle, float eAngle);


// Function : FGL_Ellipse()
// Used to : draw 2D ellipse of given two radius bound with start and end angle.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Ellipse.
// float xRadius - indicates Xaxis radius of circle to be drawn. 
// float yRadius - indicates Yaxis radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Ellipse(FGL_Point* center, float xRadius, float yRadius, float sAngle, float eAngle);


// Function : FGL_Sphere()
// Used to : draw sphere of given radius.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Sphere.
// FGL_Color* color1 - Pointer to object of color which indicates one color in sphere.
// FGL_Color* color2 - Pointer to object of color which indicates another color in sphere.
// float radius - indicates Xaxis radius of circle to be drawn. 
// Returns : void (nothing).
void FGL_Sphere(FGL_Point* center, FGL_Color* color1, FGL_Color* color2, float radius);



/*********************** Curves **********************************/


// Function like Macro : FGL_Bezier3() 
// Used to : to calculate current coordinate for Bezier curve of 3 points.
#define FGL_Bezier3(u, p0, p1, p2, axis) (p0.##axis * (1.0f - u) * (1.0f - u)) + (p1.##axis * 2.0f * (1.0f - u) * u) + (p2.##axis * u * u)
// Function like Macro : FGL_Bezier4() 
// Used to : to calculate current coordinate for Bezier curve of 4 points.
#define FGL_Bezier4(u, p0, p1, p2, p3, axis) (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 3.0f * (1.0f - u) * u * u) + (p3.##axis * u * u * u)
// Function like Macro : FGL_Bezier5() 
// Used to : to calculate current coordinate for Bezier curve of 5 points.
#define FGL_Bezier5(u, p0, p1, p2, p3, p4, axis)  (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p3.##axis * 4.0f * (1.0f - u) * u * u * u) + (p4.##axis * u * u * u * u);


// Function : FGL_BezierCurve3()
// Used to : draw bezier curve of 3 points.
// Parameters : 
// FGL_3Points* curve - Pointer to object of 3 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve3(FGL_3Points* curve);


// Function : FGL_BezierCurve4()
// Used to : draw bezier curve of 4 points.
// Parameters : 
// FGL_4Points* curve - Pointer to object of 4 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve4(FGL_4Points* curve);


// Function : FGL_BezierCurve5()
// Used to : draw bezier curve of 5 points.
// Parameters : 
// FGL_5Points* curve - Pointer to object of 5 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve5(FGL_5Points* curve);


// Function : FGL_BezierShape33()
// Used to : draw a shape enclosed in between 2 bezier curves of 3 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_3Points* curve2 - Pointer to object of 3 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape33(FGL_3Points* curve1, FGL_3Points* curve2, FGL_Color* c);


// Function : FGL_BezierShape44()
// Used to : draw a shape enclosed in between 2 bezier curves of 4 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape44(FGL_4Points* curve1, FGL_4Points* curve2, FGL_Color* c);


// Function : FGL_BezierShape55()
// Used to : draw a shape enclosed in between 2 bezier curves of 5 points.
// Parameters : 
// FGL_5Points* curve1 - Pointer to object of 5 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape55(FGL_5Points* curve1, FGL_5Points* curve2, FGL_Color* c);


// Function : FGL_BezierShape34()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 4 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape34(FGL_3Points* curve1, FGL_4Points* curve2, FGL_Color* c);


// Function : FGL_BezierShape35()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 5 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape35(FGL_3Points* curve1, FGL_5Points* curve2, FGL_Color* c);


// Function : FGL_BezierShape45()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 4 points and other of 5 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape45(FGL_4Points* curve1, FGL_5Points* curve2, FGL_Color* c);

#endif