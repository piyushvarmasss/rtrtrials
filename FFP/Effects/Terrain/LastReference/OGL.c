
// Common Header Files
#include<stdio.h>			// File IO
#include<stdlib.h>			// exit()
#include <windows.h>		// WIN32 SDK
#include<math.h>
#include "OGL.h"			// Icon


// OpenGL Header Files
#include<gl/GL.h>			// C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl
#include<GL/glu.h>

// Macros
#define PNV_WIN_WIDTH 800
#define PNV_WIN_HEIGHT 600


#define BITMAP_ID 0x4D42
#define MAP_X 32
#define MAP_Z 32
#define MAP_SCALE 20.0f
#define PI 3.14159

// Link with OpenGL Library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


// Global Variable Declarations
FILE* PNV_gFILE = NULL;

HWND PNV_ghwnd = NULL;
BOOL PNV_gbActive = FALSE;
DWORD PNV_dwStyle = 0;
WINDOWPLACEMENT PNV_wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL PNV_gbFullscreen = FALSE;



// OpenGL related Global Variables
HDC PNV_ghdc = NULL;					// Global Handle Device Context  (used when any function do not have window handle)
HGLRC PNV_ghrc = NULL;					// Global Handle Rendering Context	(used for render the window from overall program)


float angle = 0.0f;
float radians = 0.0f;
float waterHeight = 154.0f;
BOOL waterDir = TRUE;

int mouseX, mouseY;
float cameraX, cameraY, cameraZ;
float lookX, lookY, lookZ;

BITMAPINFOHEADER bitmapInfoHeader;
BITMAPINFOHEADER landInfo;
BITMAPINFOHEADER waterInfo;

unsigned char* imageData;
unsigned char* landTexture;
unsigned char* waterTexture;
unsigned int land;
unsigned int water;

float terrain[MAP_X][MAP_Z][3];

float rot = 0.0f;

// Entry-Point Function
int WINAPI WinMain(HINSTANCE PNV_hInstance, HINSTANCE PNV_hPrevInstance, LPSTR PNV_lpszCmdLine, int PNV_iCmdShow)
{
	// Function Declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local Variable Declarations
	WNDCLASSEX PNV_wndclass;
	HWND PNV_hwnd;
	MSG PNV_msg;
	TCHAR PNV_szAppName[] = TEXT("PNVWindow");
	BOOL PNV_bDone = FALSE;
	int PNV_iResult = 0;

	int PNV_SW = GetSystemMetrics(SM_CXSCREEN);
	int PNV_SH = GetSystemMetrics(SM_CYSCREEN);
	int PNV_xCoordinate = ((PNV_SW / 2) - (PNV_WIN_WIDTH / 2));
	int PNV_yCoordinate = ((PNV_SH / 2) - (PNV_WIN_HEIGHT / 2));
	
	// Code
	PNV_gFILE = fopen("log.txt", "w");
	if (PNV_gFILE == NULL)
	{
		MessageBox(NULL, TEXT("Program cannot open log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(PNV_gFILE, "Program started successfully.\n");

	// WNDCLASSEX Initilization 

	PNV_wndclass.cbSize = sizeof(WNDCLASSEX);
	PNV_wndclass.style = CS_HREDRAW | CS_VREDRAW;
	PNV_wndclass.cbClsExtra = 0;
	PNV_wndclass.cbWndExtra = 0;
	PNV_wndclass.lpfnWndProc = WndProc;
	PNV_wndclass.hInstance = PNV_hInstance;
	PNV_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	PNV_wndclass.hIcon = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));
	PNV_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	PNV_wndclass.lpszClassName = PNV_szAppName;
	PNV_wndclass.lpszMenuName = NULL;
	PNV_wndclass.hIconSm = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));


	// Register WNDCLASSEX
	RegisterClassEx(&PNV_wndclass);


	// Create Window								// glutCreateWindow
	PNV_hwnd = CreateWindowEx(WS_EX_APPWINDOW,			// to above of taskbar for fullscreen
						PNV_szAppName,
						TEXT("Piyush Niranjan Varma"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						PNV_xCoordinate,				// glutWindowPosition 1st Parameter
						PNV_yCoordinate,				// glutWindowPosition 2nd Parameter
						PNV_WIN_WIDTH,					// glutWindowSize 1st Parameter
						PNV_WIN_HEIGHT,					// glutWindowSize 2nd Parameter
						NULL,
						NULL,
						PNV_hInstance,
						NULL);

	PNV_ghwnd = PNV_hwnd;

	// Initialization
	PNV_iResult = initialize();
	if (PNV_iResult != 0)
	{
		MessageBox(PNV_hwnd, TEXT("initialize() Failed!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(PNV_hwnd);
	}

	// Show The Window
	ShowWindow(PNV_hwnd, PNV_iCmdShow);
	SetForegroundWindow(PNV_hwnd);
	SetFocus(PNV_hwnd);

	// Game Loop					// glutMainLoop()
	while (PNV_bDone == FALSE)
	{
		if (PeekMessage(&PNV_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (PNV_msg.message == WM_QUIT)
				PNV_bDone = TRUE;
			else
			{
				TranslateMessage(&PNV_msg);
				DispatchMessage(&PNV_msg);
			}
		}
		else
		{
			if (PNV_gbActive == TRUE)
			{
				// Render
				display();

				// Update
				update();
			}
		}
	}

	// Uninitialization
	uninitialize();						// glutCloseFunc(uninitialize)

	return((int)PNV_msg.wParam);
}


// CALLBACK Function
LRESULT CALLBACK WndProc(HWND PNV_hwnd, UINT PNV_iMsg, WPARAM PNV_wParam, LPARAM PNV_lParam)
{
	// Function Declarations
	void ToggleFullscreen( void );
	void resize(int, int);

	// Code
	switch (PNV_iMsg)
	{
		case WM_SETFOCUS:
			PNV_gbActive = TRUE;
			break;

		case WM_KILLFOCUS:
			PNV_gbActive = FALSE;
			break;

		case WM_SIZE:
			resize(LOWORD(PNV_lParam), HIWORD(PNV_lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_KEYDOWN:
			switch (LOWORD(PNV_wParam))
			{
			case VK_ESCAPE:
				fprintf(PNV_gFILE, "Program ended successfully.\n");
				fclose(PNV_gFILE);
				PNV_gFILE = NULL;
				DestroyWindow(PNV_hwnd);
				break;
			}
			break;

		case WM_CHAR:
			switch (LOWORD(PNV_wParam))
			{
			case 'F':
			case 'f':
				if (PNV_gbFullscreen == FALSE)
				{
					ToggleFullscreen();
					PNV_gbFullscreen = TRUE;
					fprintf(PNV_gFILE, "Program entered Fullscreen.\n");
				}
				else
				{
					ToggleFullscreen();
					PNV_gbFullscreen = FALSE;
					fprintf(PNV_gFILE, "Program ended Fullscreen.\n");
				}
				break;
			}
			break;

		case WM_RBUTTONDOWN:									// glutMouseFunc(mouse)
			DestroyWindow(PNV_hwnd);
			break;

		case WM_CLOSE:
			DestroyWindow(PNV_hwnd);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return(DefWindowProc(PNV_hwnd, PNV_iMsg, PNV_wParam, PNV_lParam));
}


void ToggleFullscreen(void)
{
	// Local Variable Declarations
	MONITORINFO PNV_mi = { sizeof(MONITORINFO) };

	// Code
	if (PNV_gbFullscreen == FALSE)
	{
		PNV_dwStyle = GetWindowLong(PNV_ghwnd, GWL_STYLE);

		if (PNV_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(PNV_ghwnd, &PNV_wpPrev) && GetMonitorInfo(MonitorFromWindow(PNV_ghwnd, MONITORINFOF_PRIMARY), &PNV_mi))
			{
				SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(PNV_ghwnd, HWND_TOP, PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.top, PNV_mi.rcMonitor.right - PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.bottom - PNV_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				// HWND_TOP ~ WS_OVERLAPPED, rc ~ RECT, SWP_FRAMECHANGED ~ WM_NCCALCSIZE msg
			}
		}

		ShowCursor(FALSE);
	}
	else {
		SetWindowPlacement(PNV_ghwnd, &PNV_wpPrev);
		SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(PNV_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		// SetWindowPos has greater priority than SetWindowPlacement and SetWindowStyle for Z-Order
		ShowCursor(TRUE);
	}
}


int initialize(void)
{
	// Function Declarations
	void InitializeTerrain();
	unsigned char* LoadBitmapFile(char* filename, BITMAPINFOHEADER * bitmapInfoHeader);

	// Local Variables
	PIXELFORMATDESCRIPTOR PNV_pfd;
	int PNV_iPixelFormatIndex = 0;

	// Code

	// Initialize pfd
	ZeroMemory(&PNV_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	PNV_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	PNV_pfd.nVersion = 1;
	PNV_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	PNV_pfd.iPixelType = PFD_TYPE_RGBA;
	PNV_pfd.cColorBits = 32;
	PNV_pfd.cRedBits = 8;
	PNV_pfd.cGreenBits = 8;
	PNV_pfd.cBlueBits = 8;
	PNV_pfd.cAlphaBits = 8;
	PNV_pfd.cDepthBits = 32;

	// Get the DC (in OpenGL Global Variables)
	PNV_ghdc = GetDC(PNV_ghwnd);

	if (PNV_ghdc == NULL)
	{
		fprintf(PNV_gFILE, "GetDC Failed!\n");
		return(-1);
	}

	// Initialize iPixelFormatIndex
	PNV_iPixelFormatIndex = ChoosePixelFormat(PNV_ghdc, &PNV_pfd);				// Must be non Zero Positive

	if (PNV_iPixelFormatIndex == 0)
	{
		fprintf(PNV_gFILE, "ChoosePixelFormat function failed!\n");
		return(-2);
	}

	// Set Obtained Pixel Format
	if (SetPixelFormat(PNV_ghdc, PNV_iPixelFormatIndex, &PNV_pfd) == FALSE)
	{
		fprintf(PNV_gFILE, "SetPixelFormat funtion failed!\n");
		return(-3);
	}

	// Tell WGL (Bridging API from Window to OpenGL i.e. Windows Graphics Library) to give OpenGL compatible context from this device context
	PNV_ghrc = wglCreateContext(PNV_ghdc);

	if (PNV_ghrc == NULL)
	{
		fprintf(PNV_gFILE, "wglCreateContext function failed!\n");
		return(-4);
	}

	// Make Rendering Context Current
	if (wglMakeCurrent(PNV_ghdc, PNV_ghrc) == FALSE)
	{
		fprintf(PNV_gFILE, "wglMakeCurrent function failed!\n");
		return(-5);
	}

	// Enabling Depth
	glShadeModel(GL_SMOOTH);			// Make Shading Smooth
	glClearDepth(1.0f);					// Clears Depth Buffer in FrameBuffer (make all bits 1)
	glEnable(GL_DEPTH_TEST);			// Enables Rasterizer's Depth Test (1 of the 8 tests)
	glDepthFunc(GL_LEQUAL);				// Depth Comparing/Testing Function (GL_LEQUAL - Less than or equal to 1.0f given in glClearDepth(1.0f))
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   // When Depth Enabled, if corner things are circular then they seems elliptical it is artifact of depth enable.   
	// So to make correction of perspective depth nicest.

	imageData = LoadBitmapFile("terrain2.bmp", &bitmapInfoHeader);
	// initialize the terrain data and load the textures
	InitializeTerrain();
	LoadTextures();

	// Set the Clear Color of Window to Blue (OpenGL Function)
	// Here OpenGL starts
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);						

	// Warm Up Resize
	resize(PNV_WIN_WIDTH, PNV_WIN_HEIGHT);

	return(0);
}


void resize(int PNV_width, int PNV_height)
{
	// Code
	if (PNV_height <= 0)
		PNV_height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)PNV_width, (GLsizei)PNV_height);					// Like Binacular (Part of window to view)

	gluPerspective(45.0f, (GLfloat)PNV_width / (GLfloat)PNV_height, 0.1f, 100.0f);
}


void display(void)
{
	// Code
	void Render(void);

	// Gives Color to Window (given form glClearColor in initialize) 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -5.0f);

	glRotatef(rot, 0.0f, 1.0f, 0.0f);

	glTranslatef(0.0f, -1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	Render();

	SwapBuffers(PNV_ghdc);
}


void update(void)
{
	// Code
	rot += 0.1f;
}


void uninitialize(void)
{
		// Function Declarations
		void ToggleFullScreen(void);

		// Code
		// If Application is exitting in full-screen
		if (PNV_gbFullscreen == TRUE)
		{
			ToggleFullscreen();
			PNV_gbFullscreen = FALSE;
		}

		// Make the hdc as current DC
		if (wglGetCurrentContext() == PNV_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}

		// Delete Rendering Context
		if (PNV_ghrc)
		{
			wglDeleteContext(PNV_ghrc);
			PNV_ghrc = NULL;
		}

		// Release the hdc
		if (PNV_ghdc)
		{
			ReleaseDC(PNV_ghwnd, PNV_ghdc);
			PNV_ghdc = NULL;
		}

		// Destroy Window
		if (PNV_ghwnd)
		{
			DestroyWindow(PNV_ghwnd);
			PNV_ghwnd = NULL;
		}

		// Close the log file
		if (PNV_gFILE)
		{
			fprintf(PNV_gFILE, "Program ended successfully.\n");
			fclose(PNV_gFILE);
			PNV_gFILE = NULL;
		}

}


void InitializeTerrain()
{
	// loop through all of the heightfield points, calculating
	// the coordinates for each point
	for (int z = 0; z < MAP_Z; z++)
	{
		for (int x = 0; x < MAP_X; x++)
		{
			terrain[x][z][0] = (float)x * MAP_SCALE;
			terrain[x][z][1] = (float)imageData[(z * MAP_Z + x) * 3];
			terrain[x][z][2] = -1.0f * (float)z * MAP_SCALE;
		}
	}
}

BOOL LoadTextures() {

	// load the land-texture data
	landTexture = LoadBitmapFile("green.bmp", &landInfo);
	if (!landTexture)
		return FALSE;

	// load the water-texture data
	waterTexture = LoadBitmapFile("water.bmp", &waterInfo);
	if (!waterTexture)
		return FALSE;
	
	// generate the land texture as a mipmap
	glGenTextures(1, &land);
	glBindTexture(GL_TEXTURE_2D, land);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, landInfo.biWidth, landInfo.biHeight,GL_RGB, GL_UNSIGNED_BYTE, landTexture);
	
	// generate the water texture as a mipmap
	glGenTextures(1, &water);
	glBindTexture(GL_TEXTURE_2D, water);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, waterInfo.biWidth, waterInfo.biHeight, GL_RGB, GL_UNSIGNED_BYTE, waterTexture);
	return TRUE;
}

void Render() {

	// set the current texture to the land texture
	//glBindTexture(GL_TEXTURE_2D, land);
	// we are going to loop through all of our terrain�s data points,
	// but we only want to draw one triangle strip for each set along the x axis.
	for (int z = 0; z < MAP_Z - 1; z++)
	{
		glBegin(GL_TRIANGLE_STRIP);
		glColor3f(1.0f, 1.0f, 1.0f);
		for (int x = 0; x < MAP_X - 1; x++)
		{
			// for each vertex, we calculate the grayscale shade color,
			// we set the texture coordinate, and we draw the vertex.

			/*
				the vertices are drawn in this order:
				0 ---> 1
				/
				/
				|/
				2 ---> 3
			*/
			// draw vertex 0
			//glColor3f(terrain[x][z][1] / 255.0f, terrain[x][z][1] / 255.0f, terrain[x][z][1] / 255.0f);
			//glTexCoord2f(0.0f, 0.0f);
			glVertex3f(terrain[x][z][0], terrain[x][z][1], terrain[x][z][2]);
			// draw vertex 1
			//glTexCoord2f(1.0f, 0.0f);
			//glColor3f(terrain[x + 1][z][1] / 255.0f, terrain[x + 1][z][1] / 255.0f, terrain[x + 1][z][1] / 255.0f);
			glVertex3f(terrain[x + 1][z][0], terrain[x + 1][z][1], terrain[x + 1][z][2]);
			// draw vertex 2
			//glTexCoord2f(0.0f, 1.0f);
			//glColor3f(terrain[x][z + 1][1] / 255.0f, terrain[x][z + 1][1] / 255.0f, terrain[x][z + 1][1] / 255.0f);
			glVertex3f(terrain[x][z + 1][0], terrain[x][z + 1][1], terrain[x][z + 1][2]);
			// draw vertex 3
			//glColor3f(terrain[x + 1][z + 1][1] / 255.0f, terrain[x + 1][z + 1][1] / 255.0f, terrain[x + 1][z + 1][1] / 255.0f);
			//glTexCoord2f(1.0f, 1.0f);
			glVertex3f(terrain[x + 1][z + 1][0], terrain[x + 1][z + 1][1], terrain[x + 1][z + 1][2]);
		}
		glEnd();
	}

/*
	
	// enable blending
	glEnable(GL_BLEND);
	// enable read-only depth buffer
	glDepthMask(GL_FALSE);
	// set the blend function to what we use for transparency
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0.5f, 0.5f, 1.0f, 0.7f); // set color to a transparent blue
	glBindTexture(GL_TEXTURE_2D, water); // set texture to the water texture
	// draw water as one large quad surface
	glBegin(GL_QUADS);
	//glTexCoord2f(0.0f, 0.0f); // lower-left corner
	glVertex3f(terrain[0][0][0], waterHeight, terrain[0][0][2]);
	//glTexCoord2f(10.0f, 0.0f); // lower-right corner
	glVertex3f(terrain[MAP_X - 1][0][0], waterHeight, terrain[MAP_X - 1][0][2]);
	//glTexCoord2f(10.0f, 10.0f); // upper-right corner
	glVertex3f(terrain[MAP_X - 1][MAP_Z - 1][0], waterHeight,terrain[MAP_X - 1][MAP_Z - 1][2]);
	//glTexCoord2f(0.0f, 10.0f); // upper-left corner
	glVertex3f(terrain[0][MAP_Z - 1][0], waterHeight, terrain[0][MAP_Z - 1][2]);
	glEnd();
	// set back to normal depth buffer mode (writeable)
	glDepthMask(GL_TRUE);
	// disable blending
	glDisable(GL_BLEND);
	// animate the water
	if (waterHeight > 155.0f)
		waterDir = FALSE;
	else if (waterHeight < 154.0f)
		waterDir = TRUE;

	if (waterDir)
		waterHeight += 0.01f;
	else
		waterHeight -= 0.01f;

*/
}

unsigned char* LoadBitmapFile(char* filename, BITMAPINFOHEADER* bitmapInfoHeader)
{
	FILE* filePtr; // the file pointer
	BITMAPFILEHEADER bitmapFileHeader; // bitmap file header
	unsigned char* bitmapImage; // bitmap image data
	int imageIdx = 0; // image index counter
	unsigned char tempRGB; // swap variable
	// open filename in �read binary� mode

	filePtr = fopen(filename, "rb");
	if (filePtr == NULL)
		return NULL;

	// read the bitmap file header
	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);
	// verify that this is a bitmap by checking for the universal bitmap id
	if (bitmapFileHeader.bfType != BITMAP_ID)
	{
		fclose(filePtr);
		fprintf(PNV_gFILE, "LoadBitmapFile() failed!\n");
		return NULL;
	}
	// read the bitmap information header
	fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);
	// move file pointer to beginning of bitmap data
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);
	// allocate enough memory for the bitmap image data
	bitmapImage = (unsigned char*)malloc(bitmapInfoHeader->biSizeImage);
	// verify memory allocation
	if (!bitmapImage)
	{
		free(bitmapImage);
		fclose(filePtr);
		fprintf(PNV_gFILE, "LoadBitmapFile() failed!\n");
		return NULL;
	}
	// read in the bitmap image data
	fread(bitmapImage, 1, bitmapInfoHeader->biSizeImage, filePtr);
	// make sure bitmap image data was read
	if (bitmapImage == NULL)
	{
		fclose(filePtr);
		fprintf(PNV_gFILE, "LoadBitmapFile() failed!\n");
		return NULL;
	}
	// swap the R and B values to get RGB since the bitmap color format is in BGR
	for (imageIdx = 0; imageIdx < bitmapInfoHeader->biSizeImage; imageIdx += 3)
	{
		tempRGB = bitmapImage[imageIdx];
		bitmapImage[imageIdx] = bitmapImage[imageIdx + 2];
		bitmapImage[imageIdx + 2] = tempRGB;
	}
	// close the file and return the bitmap image data
	fclose(filePtr);
	return bitmapImage;
}