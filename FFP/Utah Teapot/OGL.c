	
// Common Header Files
#include<stdio.h>			// File IO
#include<stdlib.h>			// exit()
#include <windows.h>		// WIN32 SDK
#include "OGL.h"			// Icon

// OpenGL Header Files
#include<gl/GL.h>			// C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl
#include<GL/glu.h>

#include "Model.h"

// Macros
#define PNV_WIN_WIDTH 800
#define PNV_WIN_HEIGHT 600

// Link with OpenGL Library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


// Global Variable Declarations
FILE* PNV_gFILE = NULL;

HWND PNV_ghwnd = NULL;
BOOL PNV_gbActive = FALSE;
DWORD PNV_dwStyle = 0;
WINDOWPLACEMENT PNV_wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL PNV_gbFullscreen = FALSE;



// OpenGL related Global Variables
HDC PNV_ghdc = NULL;					// Global Handle Device Context  (used when any function do not have window handle)
HGLRC PNV_ghrc = NULL;					// Global Handle Rendering Context	(used for render the window from overall program)

// Variable Light
BOOL PNV_bLight = FALSE;
BOOL PNV_bTexture = FALSE;
BOOL PNV_bAnimate = FALSE;
BOOL PNV_bColor = FALSE;

GLfloat PNV_lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat PNV_lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat PNV_lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat PNV_lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f};

GLfloat PNV_materialAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat PNV_materialDiffuse[] = {0.5f, 0.2f, 0.7f, 1.0f};
GLfloat PNV_materialSpecular[] = {0.7f, 0.7f, 0.7f, 1.0f};
GLfloat PNV_materialShininess[] = {128.0f};

GLuint PNV_texture_marble = 0;



// For Animation
GLfloat angle = 0.0f;



// Entry-Point Function
int WINAPI WinMain(HINSTANCE PNV_hInstance, HINSTANCE PNV_hPrevInstance, LPSTR PNV_lpszCmdLine, int PNV_iCmdShow)
{
	// Function Declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local Variable Declarations
	WNDCLASSEX PNV_wndclass;
	HWND PNV_hwnd;
	MSG PNV_msg;
	TCHAR PNV_szAppName[] = TEXT("PNVWindow");
	BOOL PNV_bDone = FALSE;
	int PNV_iResult = 0;

	int PNV_SW = GetSystemMetrics(SM_CXSCREEN);
	int PNV_SH = GetSystemMetrics(SM_CYSCREEN);
	int PNV_xCoordinate = ((PNV_SW / 2) - (PNV_WIN_WIDTH / 2));
	int PNV_yCoordinate = ((PNV_SH / 2) - (PNV_WIN_HEIGHT / 2));
	
	// Code
	PNV_gFILE = fopen("log.txt", "w");
	if (PNV_gFILE == NULL)
	{
		MessageBox(NULL, TEXT("Program cannot open log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(PNV_gFILE, "Program started successfully.\n");

	// WNDCLASSEX Initilization 

	PNV_wndclass.cbSize = sizeof(WNDCLASSEX);
	PNV_wndclass.style = CS_HREDRAW | CS_VREDRAW;
	PNV_wndclass.cbClsExtra = 0;
	PNV_wndclass.cbWndExtra = 0;
	PNV_wndclass.lpfnWndProc = WndProc;
	PNV_wndclass.hInstance = PNV_hInstance;
	PNV_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	PNV_wndclass.hIcon = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));
	PNV_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	PNV_wndclass.lpszClassName = PNV_szAppName;
	PNV_wndclass.lpszMenuName = NULL;
	PNV_wndclass.hIconSm = LoadIcon(PNV_hInstance, MAKEINTRESOURCE(PNVICON));


	// Register WNDCLASSEX
	RegisterClassEx(&PNV_wndclass);


	// Create Window								// glutCreateWindow
	PNV_hwnd = CreateWindowEx(WS_EX_APPWINDOW,			// to above of taskbar for fullscreen
						PNV_szAppName,
						TEXT("Piyush Niranjan Varma"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						PNV_xCoordinate,				// glutWindowPosition 1st Parameter
						PNV_yCoordinate,				// glutWindowPosition 2nd Parameter
						PNV_WIN_WIDTH,					// glutWindowSize 1st Parameter
						PNV_WIN_HEIGHT,					// glutWindowSize 2nd Parameter
						NULL,
						NULL,
						PNV_hInstance,
						NULL);

	PNV_ghwnd = PNV_hwnd;

	// Initialization
	PNV_iResult = initialize();
	if (PNV_iResult != 0)
	{
		MessageBox(PNV_hwnd, TEXT("initialize() Failed!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(PNV_hwnd);
	}

	// Show The Window
	ShowWindow(PNV_hwnd, PNV_iCmdShow);
	SetForegroundWindow(PNV_hwnd);
	SetFocus(PNV_hwnd);

	// Game Loop					// glutMainLoop()
	while (PNV_bDone == FALSE)
	{
		if (PeekMessage(&PNV_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (PNV_msg.message == WM_QUIT)
				PNV_bDone = TRUE;
			else
			{
				TranslateMessage(&PNV_msg);
				DispatchMessage(&PNV_msg);
			}
		}
		else
		{
			if (PNV_gbActive == TRUE)
			{
				// Render
				display();

				// Update
				if (PNV_bAnimate == TRUE) {
				
					update();
				}
			}
		}
	}

	// Uninitialization
	uninitialize();						// glutCloseFunc(uninitialize)

	return((int)PNV_msg.wParam);
}


// CALLBACK Function
LRESULT CALLBACK WndProc(HWND PNV_hwnd, UINT PNV_iMsg, WPARAM PNV_wParam, LPARAM PNV_lParam)
{
	// Function Declarations
	void ToggleFullscreen( void );
	void resize(int, int);

	// Code
	switch (PNV_iMsg)
	{
		case WM_SETFOCUS:
			PNV_gbActive = TRUE;
			break;

		case WM_KILLFOCUS:
			PNV_gbActive = FALSE;
			break;

		case WM_SIZE:
			resize(LOWORD(PNV_lParam), HIWORD(PNV_lParam));
			break;

		case WM_ERASEBKGND:
			return(0);

		case WM_KEYDOWN:
			switch (LOWORD(PNV_wParam))
			{
			case VK_ESCAPE:
				fprintf(PNV_gFILE, "Program ended successfully.\n");
				fclose(PNV_gFILE);
				PNV_gFILE = NULL;
				DestroyWindow(PNV_hwnd);
				break;
			}
			break;

		case WM_CHAR:
			switch (LOWORD(PNV_wParam))
			{
				case 'F':
				case 'f':
					if (PNV_gbFullscreen == FALSE)
					{
						ToggleFullscreen();
						PNV_gbFullscreen = TRUE;
						fprintf(PNV_gFILE, "Program entered Fullscreen.\n");
					}
					else
					{
						ToggleFullscreen();
						PNV_gbFullscreen = FALSE;
						fprintf(PNV_gFILE, "Program ended Fullscreen.\n");
					}
					break;

				case 'L':
				case 'l':
					if (PNV_bLight == FALSE) {

						PNV_bLight = TRUE;
					}else{

						PNV_bLight = FALSE;
					}
					break;

				case 'T':
				case 't':
					if (PNV_bTexture == FALSE) {

						PNV_bTexture = TRUE;
					}
					else {

						PNV_bTexture = FALSE;
					}
					break;

				case 'A':
				case 'a':
					if (PNV_bAnimate == FALSE) {

						PNV_bAnimate = TRUE;
					}
					else {

						PNV_bAnimate = FALSE;
					}
					break;

				case 'C':
				case 'c':
					if (PNV_bColor == FALSE) {

						PNV_bColor = TRUE;
					}
					else {

						PNV_bColor = FALSE;
					}
					break;

				default:
					break;
			}
			break;

		case WM_RBUTTONDOWN:									// glutMouseFunc(mouse)
			DestroyWindow(PNV_hwnd);
			break;

		case WM_CLOSE:
			DestroyWindow(PNV_hwnd);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		default:
			break;
	}

	return(DefWindowProc(PNV_hwnd, PNV_iMsg, PNV_wParam, PNV_lParam));
}


void ToggleFullscreen(void)
{
	// Local Variable Declarations
	MONITORINFO PNV_mi = { sizeof(MONITORINFO) };

	// Code
	if (PNV_gbFullscreen == FALSE)
	{
		PNV_dwStyle = GetWindowLong(PNV_ghwnd, GWL_STYLE);

		if (PNV_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(PNV_ghwnd, &PNV_wpPrev) && GetMonitorInfo(MonitorFromWindow(PNV_ghwnd, MONITORINFOF_PRIMARY), &PNV_mi))
			{
				SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(PNV_ghwnd, HWND_TOP, PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.top, PNV_mi.rcMonitor.right - PNV_mi.rcMonitor.left, PNV_mi.rcMonitor.bottom - PNV_mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				// HWND_TOP ~ WS_OVERLAPPED, rc ~ RECT, SWP_FRAMECHANGED ~ WM_NCCALCSIZE msg
			}
		}

		ShowCursor(FALSE);
	}
	else {
		SetWindowPlacement(PNV_ghwnd, &PNV_wpPrev);
		SetWindowLong(PNV_ghwnd, GWL_STYLE, PNV_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(PNV_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		// SetWindowPos has greater priority than SetWindowPlacement and SetWindowStyle for Z-Order
		ShowCursor(TRUE);
	}
}


int initialize(void)
{
	// Function Declarations
	void resize(int, int);
	BOOL loadGLTexture(GLuint*, TCHAR[]);

	// Local Variables
	PIXELFORMATDESCRIPTOR PNV_pfd;
	int PNV_iPixelFormatIndex = 0;
	BOOL bResult = FALSE;

	// Code

	// Initialize pfd
	ZeroMemory(&PNV_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	PNV_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	PNV_pfd.nVersion = 1;
	PNV_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	PNV_pfd.iPixelType = PFD_TYPE_RGBA;
	PNV_pfd.cColorBits = 32;
	PNV_pfd.cRedBits = 8;
	PNV_pfd.cGreenBits = 8;
	PNV_pfd.cBlueBits = 8;
	PNV_pfd.cAlphaBits = 8;
	PNV_pfd.cDepthBits = 32;

	// Get the DC (in OpenGL Global Variables)
	PNV_ghdc = GetDC(PNV_ghwnd);

	if (PNV_ghdc == NULL)
	{
		fprintf(PNV_gFILE, "GetDC Failed!\n");
		return(-1);
	}

	// Initialize iPixelFormatIndex
	PNV_iPixelFormatIndex = ChoosePixelFormat(PNV_ghdc, &PNV_pfd);				// Must be non Zero Positive

	if (PNV_iPixelFormatIndex == 0)
	{
		fprintf(PNV_gFILE, "ChoosePixelFormat function failed!\n");
		return(-2);
	}

	// Set Obtained Pixel Format
	if (SetPixelFormat(PNV_ghdc, PNV_iPixelFormatIndex, &PNV_pfd) == FALSE)
	{
		fprintf(PNV_gFILE, "SetPixelFormat funtion failed!\n");
		return(-3);
	}

	// Tell WGL (Bridging API from Window to OpenGL i.e. Windows Graphics Library) to give OpenGL compatible context from this device context
	PNV_ghrc = wglCreateContext(PNV_ghdc);

	if (PNV_ghrc == NULL)
	{
		fprintf(PNV_gFILE, "wglCreateContext function failed!\n");
		return(-4);
	}

	// Make Rendering Context Current
	if (wglMakeCurrent(PNV_ghdc, PNV_ghrc) == FALSE)
	{
		fprintf(PNV_gFILE, "wglMakeCurrent function failed!\n");
		return(-5);
	}

	// Enabling Depth
	glShadeModel(GL_SMOOTH);			// Make Shading Smooth
	glClearDepth(1.0f);					// Clears Depth Buffer in FrameBuffer (make all bits 1)
	glEnable(GL_DEPTH_TEST);			// Enables Rasterizer's Depth Test (1 of the 8 tests)
	glDepthFunc(GL_LEQUAL);				// Depth Comparing/Testing Function (GL_LEQUAL - Less than or equal to 1.0f given in glClearDepth(1.0f))
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   // When Depth Enabled, if corner things are circular then they seems elliptical it is artifact of depth enable.   
	// So to make correction of perspective depth nicest.

	// Set the Clear Color of Window to Blue (OpenGL Function)
	// Here OpenGL starts
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);						

	// Light Related initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT, PNV_lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, PNV_lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, PNV_lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, PNV_lightPosition);
	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT, GL_AMBIENT, PNV_materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, PNV_materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, PNV_materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, PNV_materialShininess);

	bResult = loadGLTexture(&PNV_texture_marble, MAKEINTRESOURCE(PNVIDBITMAP_MARBLE));
	if (bResult == FALSE) {

		fprintf(PNV_gFILE, "Loading of marble texture failed!\n");
		return(-6);
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Warm Up Resize
	resize(PNV_WIN_WIDTH, PNV_WIN_HEIGHT);

	return(0);
}


BOOL loadGLTexture(GLuint* texture, TCHAR imageResourceID[]) {

	// Local Variable declarations
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	// Load the image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap == NULL) {

		fprintf(PNV_gFILE, "LoadImage() failed!\n");
		return FALSE;
	}

	// Get image data
	GetObject(hBitmap, sizeof(BITMAP), &bmp);

	// Create OpenGL texture
	glGenTextures(1, texture);

	// Bind to the generated texture
	glBindTexture(GL_TEXTURE_2D, *texture);

	// Pixel store memory of 4 i.e. r,g,b,a .
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	// Set Texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Create Multiple Mipmap images  3/GL_RGB
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, (void*)bmp.bmBits);

	// Unbinding texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete Image resource object
	DeleteObject(hBitmap);
	hBitmap = NULL;

	return TRUE;
}


void resize(int PNV_width, int PNV_height)
{
	// Code
	if (PNV_height <= 0)
		PNV_height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)PNV_width, (GLsizei)PNV_height);					// Like Binacular (Part of window to view)

	gluPerspective(45.0f, (GLfloat)PNV_width / (GLfloat)PNV_height, 0.1f, 100.0f);
}


void display(void)
{
	// Local Variable Declarations
	int i, j;
	int vi, ni, ti;

	// Code
	
	// Gives Color to Window (given form glClearColor in initialize) 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -1.5f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	// Toggle Lighting
	if (PNV_bLight == TRUE) {

		glEnable(GL_LIGHTING);
	}
	else {

		glDisable(GL_LIGHTING);
	}

	// Toggle Texture
	if (PNV_bTexture == TRUE) {

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, PNV_texture_marble);
	}
	else {

		glDisable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}


	// Model Geometry
	glBegin(GL_TRIANGLES);

		for (i = 0; i < sizeof(face_indicies) / sizeof(face_indicies[0]); i++) {

			for (j = 0; j < 3; j++) {
			
				vi = face_indicies[i][j];
				ni = face_indicies[i][j + 3];
				ti = face_indicies[i][j + 6];

				glNormal3f(normals[ni][0], normals[ni][1], normals[ni][2]);
				glTexCoord2f(textures[ti][0], textures[ti][1]);

				if (PNV_bColor == TRUE) {

					//glColor3f(0.5f, 0.2f, 0.7f);
					glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
				}

				glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
			}
		}

	glEnd();

	SwapBuffers(PNV_ghdc);
}


void update(void)
{
	// Code
	angle = angle + 0.05f;
	if (angle >= 360.0f) {

		angle = angle - 360.0f;
	}
}


void uninitialize(void)
{
		// Function Declarations
		void ToggleFullScreen(void);

		// Code
		// If Application is exitting in full-screen
		if (PNV_gbFullscreen == TRUE)
		{
			ToggleFullscreen();
			PNV_gbFullscreen = FALSE;
		}

		// Make the hdc as current DC
		if (wglGetCurrentContext() == PNV_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}

		// Delete Rendering Context
		if (PNV_ghrc)
		{
			wglDeleteContext(PNV_ghrc);
			PNV_ghrc = NULL;
		}

		// Release the hdc
		if (PNV_ghdc)
		{
			ReleaseDC(PNV_ghwnd, PNV_ghdc);
			PNV_ghdc = NULL;
		}

		// Destroy Window
		if (PNV_ghwnd)
		{
			DestroyWindow(PNV_ghwnd);
			PNV_ghwnd = NULL;
		}

		if (PNV_texture_marble) {

			glDeleteTextures(1, &PNV_texture_marble);
			PNV_texture_marble = 0;
		}

		// Close the log file
		if (PNV_gFILE)
		{
			fprintf(PNV_gFILE, "Program ended successfully.\n");
			fclose(PNV_gFILE);
			PNV_gFILE = NULL;
		}
}

