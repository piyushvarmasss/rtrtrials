
// FGL_Math.h is a math library contains almost every math function used by Frustum Group. 

#define FGL_Pi 3.14159265359

#define FGL_Radian(degree)  ((degree * FGL_Pi) / 180.00)

#define FGL_Square(num)   (num * num)
#define FGL_Cube(num)     (num * num * num)
#define FGL_Quad(num)     (num * num * num * num)
#define FGL_Penta(num)    (num * num * num * num * num)
#define FGL_Hexa(num)     (num * num * num * num * num * num)
#define FGL_Hepta(num)    (num * num * num * num * num * num * num)
#define FGL_Octa(num)     (num * num * num * num * num * num * num * num)
#define FGL_Nona(num)     (num * num * num * num * num * num * num * num * num)
#define FGL_Deca(num)     (num * num * num * num * num * num * num * num * num * num)
#define FGL_Hedeca(num)   (num * num * num * num * num * num * num * num * num * num * num)
#define FGL_Dodeca(num)   (num * num * num * num * num * num * num * num * num * num * num * num)

#define FGL_Sine180(radian)  (radian - (FGL_Cube(radian) / 6.00) + (FGL_Penta(radian) / 120.00) - (FGL_Hepta(radian) / 5040.00) + (FGL_Nona(radian) / 362880.00) - (FGL_Hedeca(radian) / 39916800.00))

#define FGL_Cosine180(radian)  (1.00 - (FGL_Square(radian) / 2.00) + (FGL_Quad(radian) / 24.00) - (FGL_Hexa(radian) / 720.00) + (FGL_Octa(radian) / 40320.00) - (FGL_Deca(radian) / 3628800.00) + (FGL_Dodeca(radian) / 479001600.00))

double FGL_Sin(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Sine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Sine180(angle));
	}
}

double FGL_Cos(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Cosine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Cosine180(angle));
	}
}

#define FGL_Tan(degree)  (FGL_Sin(degree) / FGL_Cos(degree))
#define FGL_Cot(degree)  (FGL_Cos(degree) / FGL_Sin(degree))
#define FGL_Sec(degree)  (1.00 / FGL_Cos(degree))
#define FGL_Cosec(degree)  (1.00 / FGL_Sin(degree))


double FGL_SquareRoot(double num) {

	double temp = num / 2.0;
	double a, b;

	int itr = 4;
	while (--itr) {

		a = (num - FGL_Square(temp)) / (2.0 * temp);
		b = (temp + a);
		temp = b - (FGL_Square(a) / (2 * b));
	}

	return temp;
}


double FGL_Power(double num, int deg) {

	if (deg == 0) {

		if (num == 0) {

			return 0;
		}

		return 1;
	}
	else if (deg == 1) {

		return num;
	}
	else {

		double ret = num;

		while (--deg) {

			ret *= num;
		}

		return (deg < 0 ? (1.0 / ret) : ret);
	}
}


long long int FGL_Factorial(int num) {

	long long int ret = num;
	while (--num > 1)
		ret *= num;

	return ret;
}


int FGL_Ceil(double num) {

	if (num > (int)num)
		num = (int)num + 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}


int FGL_Floor(double num) {

	if (num < (int)num)
		num = (int)num - 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}


int FGL_CeilBase(double num, double base) {

	num = FGL_Ceil(num / base);
	return FGL_Ceil(num * base);
}


int FGL_FloorBase(double num, double base) {

	num = FGL_Floor(num / base);
	return FGL_Floor(num * base);
}

