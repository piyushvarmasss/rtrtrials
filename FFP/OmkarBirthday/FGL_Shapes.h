
// FGL_Shapes.h header file contains functions of shapes used by Frustum Group. 

void FGL_Circle(FGL_Point* center, float radius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (radius * FGL_Cos(sAngle)), center->y + (radius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}

void FGL_Ellipse(FGL_Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (xRadius * FGL_Cos(sAngle)), center->y + (yRadius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}


void FGL_Sphere(FGL_Point* center, FGL_Color* color1, FGL_Color* color2, float radius) {

	int i = 0;
	float angle = 0.0f;
	float z = center->z;

	while (angle <= 90.0f) {
	
		glBegin(GL_POLYGON);

			if(i % 2)
				glColor4f(color1->r, color1->g, color1->b, color1->a);
			else
				glColor4f(color2->r, color2->g, color2->b, color2->a);

			center->z = z + (FGL_Cos(angle) * radius);
			FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

			center->z = z - (FGL_Cos(angle) * radius);
			FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

		glEnd();
	
		angle += 0.1f;
		i++;
	}
}