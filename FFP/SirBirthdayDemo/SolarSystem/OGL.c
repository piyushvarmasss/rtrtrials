//Common headers
#include <windows.h> //For WIN32
#include <stdio.h>	//For FILE IO
#include <stdlib.h> //For exit 
#include <math.h>
#include <MMSystem.h>

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>
#include "OGL.h"


//MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with OpenGL Library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "Winmm.lib")

/*********************** Basic **********************************/

// Function : FGL_ColorToFloat() 
// Used to : convert String of hexadecimal color into equivalent color values into given pointer to FGL_Color object. 
// Parameters : 
// char color[] - String of 6 hexadecimal characters indicates color code.
// FGL_Color* c - Pointer to object of color which stores the respective color code string.
// Returns : void (nothing).
void FGL_ColorToFloat(char color[], FGL_Color* c) {

	for (int i = 0; i < 3; i++) {

		int num = 0;

		if (color[i * 2] >= 'A' && color[i * 2] <= 'F')
			num += 16 * ((color[i * 2] - 'A') + 10);
		else if (color[i * 2] >= 'a' && color[i * 2] <= 'f')
			num += 16 * ((color[i * 2] - 'a') + 10);
		else
			num += 16 * (color[i * 2] - 48);

		if (color[(i * 2) + 1] >= 'A' && color[(i * 2) + 1] <= 'F')
			num += ((color[(i * 2) + 1] - 'A') + 10);
		else if (color[(i * 2) + 1] >= 'a' && color[(i * 2) + 1] <= 'f')
			num += ((color[(i * 2) + 1] - 'a') + 10);
		else
			num += (color[(i * 2) + 1] - 48);

		if (i == 0)
			c->r = ((float)num / 255.0f);
		else if (i == 1)
			c->g = ((float)num / 255.0f);
		else
			c->b = ((float)num / 255.0f);
	}

	c->a = 1.0f;
}

// Function : FGL_PointAssign() 
// Used to : assigns given float values of x, y and z in respective co-ordinates into given pointer to FGL_Point object.
// Parameters : 
// FGL_Point* point - Pointer to object of point which stores the given coordinate values.
// float x - indicates given x coordinate and stored in given pointer to FGL_Point. 
// float y - indicates given y coordinate and stored in given pointer to FGL_Point. 
// float z - indicates given z coordinate and stored in given pointer to FGL_Point. 
// Returns : void (nothing). 
void FGL_PointAssign(FGL_Point* point, float x, float y, float z) {
	point->x = x;
	point->y = y;
	point->z = z;
}

// Function : FGL_ColorAssign() 
// Used to : assigns given float values of red, green and blue in respective members into given pointer to FGL_Color object.
// Parameters : 
// FGL_Color* color - Pointer to object of color which stores the given color values.
// float r - indicates given red color and stored in given pointer to FGL_Color. 
// float g - indicates given green color and stored in given pointer to FGL_Color. 
// float b - indicates given blue color and stored in given pointer to FGL_Color.  
// float a - indicates given alpha color and stored in given pointer to FGL_Color. 
// // Returns : void (nothing). 
void FGL_ColorAssign(FGL_Color* color, float r, float g, float b, float a) {
	color->r = r;
	color->g = g;
	color->b = b;
	color->a = a;
}



/*********************** Mathematics **********************************/

// Function : FGL_Sin()
// Used to : calculate Sine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Sin value of given angle).
double FGL_Sin(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Sine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Sine180(angle));
	}
}

// Function : FGL_Cos()
// Used to : calculate Cosine value of any angle in degree.
// Parameters : 
// double angle - indicates angle in degree.
// Returns : double (Cos value of given angle).
double FGL_Cos(double angle) {

	if ((((int)angle) / 180) % 2 == 0) {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return FGL_Cosine180(angle);
	}
	else {

		angle = angle - ((((int)angle) / 180) * 180);
		angle = FGL_Radian(angle);
		return (-1.00 * FGL_Cosine180(angle));
	}
}

// Function : FGL_SquareRoot()
// Used to : calculates square root of any number using Bakhshali method.
// Parameters : 
// double num - indicates a number of which square-root to be calculated.
// Returns : double (square-root of given number).
double FGL_SquareRoot(double num) {

	double temp = num / 2.0;
	double a, b;

	int itr = 4;
	while (--itr) {

		a = (num - FGL_Square(temp)) / (2.0 * temp);
		b = (temp + a);
		temp = b - (FGL_Square(a) / (2 * b));
	}

	return temp;
}

// Function : FGL_Power()
// Used to : calculates any integer power of any number.
// Parameters : 
// double num - indicates a number of which power to be calculated.
// int deg - indicates which power of number to be calculated.
// Returns : double (given power of given number).
double FGL_Power(double num, int deg) {

	if (deg == 0) {

		if (num == 0) {

			return 0;
		}

		return 1;
	}
	else if (deg == 1) {

		return num;
	}
	else {

		double ret = num;

		while (--deg) {

			ret *= num;
		}

		return (deg < 0 ? (1.0 / ret) : ret);
	}
}

// Function : FGL_Factorial()
// Used to : calculates factorial of given number.
// Parameters : 
// int num - indicates a number of which factorial to be calculated.
// Returns : long long int (factorial of given number).
long long int FGL_Factorial(int num) {

	long long int ret = num;
	while (--num > 1)
		ret *= num;

	return ret;
}

// Function : FGL_Ceil()
// Used to : calculates ceil integer value of any number.
// Parameters : 
// double num - indicates a number of which ceil to be calculated.
// Returns : int (ceil integer of given number).
int FGL_Ceil(double num) {

	if (num > (int)num)
		num = (int)num + 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}

// Function : FGL_Floor()
// Used to : calculates floor integer value of any number.
// Parameters : 
// double num - indicates a number of which floor to be calculated.
// Returns : int (floor integer of given number).
int FGL_Floor(double num) {

	if (num < (int)num)
		num = (int)num - 1;
	else if (num == (int)num)
		num = (int)num;

	return (int)num;
}

// Function : FGL_CeilBase()
// Used to : calculates ceil integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which ceil to be calculated of given base.
// double base - indicates a base.
// Returns : int (ceil integer of given number with given base).
int FGL_CeilBase(double num, double base) {

	num = FGL_Ceil(num / base);
	return FGL_Ceil(num * base);
}

// Function : FGL_Floor()
// Used to : calculates floor integer value of any number with given base value.
// Parameters : 
// double num - indicates a number of which floor to be calculated of given base.
// double base - indicates a base.
// Returns : int (floor integer of given number with given base).
int FGL_FloorBase(double num, double base) {

	num = FGL_Floor(num / base);
	return FGL_Floor(num * base);
}



/*********************** Shapes **********************************/

// Function : FGL_Circle()
// Used to : draw 2D circle of given radius bound with start and end angle. 
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Circle.
// float radius - indicates radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Circle(FGL_Point* center, float radius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (radius * FGL_Cos(sAngle)), center->y + (radius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}

// Function : FGL_Ellipse()
// Used to : draw 2D ellipse of given two radius bound with start and end angle.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Ellipse.
// float xRadius - indicates Xaxis radius of circle to be drawn. 
// float yRadius - indicates Yaxis radius of circle to be drawn. 
// float sAngle - indicates start angle in degree. 
// float eAngle - indicates end angle in degree. 
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_Ellipse(FGL_Point* center, float xRadius, float yRadius, float sAngle, float eAngle) {

	while (sAngle <= eAngle) {

		glVertex3f(center->x + (xRadius * FGL_Cos(sAngle)), center->y + (yRadius * FGL_Sin(sAngle)), center->z);
		sAngle += 1.0f;
	}
}

// Function : FGL_Sphere()
// Used to : draw sphere of given radius.
// Parameters : 
// FGL_Point* center - Pointer to object of point which indicates co-ordinate of center of Sphere.
// FGL_Color* color1 - Pointer to object of color which indicates one color in sphere.
// FGL_Color* color2 - Pointer to object of color which indicates another color in sphere.
// float radius - indicates Xaxis radius of circle to be drawn. 
// Returns : void (nothing).
void FGL_Sphere(FGL_Point* center, FGL_Color* color1, FGL_Color* color2, float radius) {

	int i = 0;
	float angle = 0.0f;
	float z = center->z;

	while (angle <= 90.0f) {

		glBegin(GL_POLYGON);

		if (i % 2)
			glColor4f(color1->r, color1->g, color1->b, color1->a);
		else
			glColor4f(color2->r, color2->g, color2->b, color2->a);

		center->z = z + (FGL_Cos(angle) * radius);
		FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

		glEnd();

		glBegin(GL_POLYGON);

		if (i % 2)
			glColor4f(color1->r, color1->g, color1->b, color1->a);
		else
			glColor4f(color2->r, color2->g, color2->b, color2->a);

		center->z = z - (FGL_Cos(angle) * radius);
		FGL_Circle(center, FGL_Sin(angle) * radius, 0.0f, 360.0f);

		glEnd();

		angle += 0.2f;
		i++;
	}
}



/*********************** Curves **********************************/

// Function : FGL_BezierCurve3()
// Used to : draw bezier curve of 3 points.
// Parameters : 
// FGL_3Points* curve - Pointer to object of 3 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve3(FGL_3Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, x);
		y = FGL_Bezier3(u, curve->p0, curve->p1, curve->p2, y);
		z = FGL_Bezier3(u, curve->p0, curve->p2, curve->p2, z);
		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierCurve4()
// Used to : draw bezier curve of 4 points.
// Parameters : 
// FGL_4Points* curve - Pointer to object of 4 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd();
void FGL_BezierCurve4(FGL_4Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f) {
		x = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, x);
		y = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, y);
		z = FGL_Bezier4(u, curve->p0, curve->p1, curve->p2, curve->p3, z);
		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierCurve5()
// Used to : draw bezier curve of 5 points.
// Parameters : 
// FGL_5Points* curve - Pointer to object of 5 points used to draw the curve.
// Returns : void (nothing).
// Note : must call inside glBegin(____); and glEnd(); 
void FGL_BezierCurve5(FGL_5Points* curve) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	for (float u = 0.0f; u <= 1.0f; u += 0.01f)
	{
		x = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, x);
		y = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, y);
		z = FGL_Bezier5(u, curve->p0, curve->p1, curve->p2, curve->p3, curve->p4, z);

		glVertex3f(x, y, z);
	}
}

// Function : FGL_BezierShape33()
// Used to : draw a shape enclosed in between 2 bezier curves of 3 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_3Points* curve2 - Pointer to object of 3 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape33(FGL_3Points* curve1, FGL_3Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, x);
		y = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, y);
		z = FGL_Bezier3(u, curve2->p0, curve2->p1, curve2->p2, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape44()
// Used to : draw a shape enclosed in between 2 bezier curves of 4 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape44(FGL_4Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape55()
// Used to : draw a shape enclosed in between 2 bezier curves of 5 points.
// Parameters : 
// FGL_5Points* curve1 - Pointer to object of 5 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape55(FGL_5Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, x);
		y = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, y);
		z = FGL_Bezier5(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, curve1->p4, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape34()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 4 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_4Points* curve2 - Pointer to object of 4 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape34(FGL_3Points* curve1, FGL_4Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, x);
		y = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, y);
		z = FGL_Bezier4(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape35()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 3 points and other of 5 points.
// Parameters : 
// FGL_3Points* curve1 - Pointer to object of 3 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape35(FGL_3Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, x);
		y = FGL_Bezier3(u, curve1->p0, curve1->p1, curve1->p2, y);
		z = FGL_Bezier3(u, curve1->p0, curve1->p2, curve1->p2, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}

// Function : FGL_BezierShape45()
// Used to : draw a shape enclosed in between 2 bezier curves of one of 4 points and other of 5 points.
// Parameters : 
// FGL_4Points* curve1 - Pointer to object of 4 points indicate the curve1.
// FGL_5Points* curve2 - Pointer to object of 5 points indicate the curve2.
// FGL_Color* c - Pointer to object of color which indicates one color in drawn shape.
// Returns : void (nothing).
void FGL_BezierShape45(FGL_4Points* curve1, FGL_5Points* curve2, FGL_Color* c) {

	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;

	glBegin(GL_QUAD_STRIP);
	glColor4f(c->r, c->g, c->b, c->a);
	for (float u = 0.0f; u <= 1.0f; u += 0.0001f) {
		x = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, x);
		y = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, y);
		z = FGL_Bezier4(u, curve1->p0, curve1->p1, curve1->p2, curve1->p3, z);
		glVertex3f(x, y, z);

		x = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, x);
		y = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, y);
		z = FGL_Bezier5(u, curve2->p0, curve2->p1, curve2->p2, curve2->p3, curve2->p4, z);
		glVertex3f(x, y, z);
	}
	glEnd();
}



//Global function prototype (Visible to OS)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFILE = NULL;
BOOL gbActive = FALSE;
HWND ghwnd = NULL;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; // NULLL kiva 0 la kasa set karaycha tar hi best way ahe
BOOL gbFullscreen = FALSE;

extern GLfloat chair_angle;  // for chair (screen six)

//OpenGL related global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL; // RENDERING CONTEXT(Awasta)

GLUquadric* quadric = NULL;


// Solar system
GLfloat SunAngle = 0.0f;
GLfloat Galaxy_Move = 0.0f;
GLfloat Planet_Day = 0.0f;
GLfloat Mercury_Year = 0.0f;
GLfloat Venus_Year = 0.0f;
GLfloat Earth_Year = 0.0f;
GLfloat Mars_Year = 0.0f;
GLfloat Jupiter_Year = 0.0f;
GLfloat Saturn_Year = 0.0f;
GLfloat Uranus_Year = 0.0f;
GLfloat Neptune_Year = 0.0f;

// Texture Object/variable
GLuint galaxy_texture = 0;
GLuint mercury_texture = 0;
GLuint venus_texture = 0;
GLuint earth_texture = 0;
GLuint moon_texture = 0;
GLuint mars_texture = 0;
GLuint jupiter_texture = 0;
GLuint saturn_texture = 0;
GLuint uranus_texture = 0;
GLuint neptune_texture = 0;
GLfloat galaxy_angle = 0.0f;
GLfloat SolarSystemY = 0.0f;
GLfloat SolarSystemZ = 2.0f;


//Entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{	
	//Function declarations
	int	initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void ToggleFullscreen(void);

	//Local variables
	WNDCLASSEX wndclass; //structure with 12 members
	HWND hwnd; //Handle of window
	MSG msg; //structure with 6 members
	TCHAR szAppName[] = TEXT("Windows");
	int iResult = 0;
	BOOL bDone = FALSE;
	int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// code
	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{
		MessageBox(NULL, TEXT("Log file cannot be opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFILE, "Program started successfully.\n");

	//now assign 12 members of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	//Register WNDCLASSEX to OS
	RegisterClassEx(&wndclass);

	//Create Window with 11 parameters in Memory
	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("Solar System"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(ScreenWidth - WIN_WIDTH) / 2,
		(ScreenHeight - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,   // Handle of parent window
		NULL,   
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize() failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	//Show Window
	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd); // Z-order la top la asnar
	SetFocus(hwnd); //internally WM_SETFOCUS WndProc la pathavta aaplya kadun
	ToggleFullscreen();

	//Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Render
			if (gbActive == TRUE)
			{
				//Render
				display();

				//Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);
}

//Callback which OS is going to interact with (Window Procedure)
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function declaration
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;
	
	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	//case WM_ERASEBKGND:
	//	return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;

		default:
			break;
		}
		break;
	
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//local variable declarations
	MONITORINFO mi = { sizeof(MONITORINFO) };
	
	//code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(
					ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED // WM_NCCALCSIZE
				);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//Function declarations
	void resize(int, int);
	BOOL loadGLTexture(GLuint*, TCHAR[]);

	//Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	BOOL bResult;

	//STEP-1
	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;   //2 raise to 32
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	//STEP-2 Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() Failed\n");
		return(-1);
	}

	//STEP-3 OS kade jaa ani me dilelya PFD la tuza kade aslelya PFD la match kar ani saglyaat javal cha asnarya cha index de
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFILE, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//STEP-4 Set obtained pixelformatdescriptor
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFILE, "SetPixelFormat() Failed.\n");
		return(-3);
	}

	//STEP-5 Tell Wndows Graphic Library (WGL)/Bridging Libraries to give me OpenGL compatible DC from this DC
	//Create OpenGL context from DC
	ghrc = wglCreateContext(ghdc); // jyanchi survat wgl ne honar te bridging api's
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wglCreateContext() Failed\n");
		return(-4);
	}
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Enabling Depth
	glShadeModel(GL_SMOOTH); // Optional Beautification
	// Compulsory
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Optional Beautification

	// initialize quadric
	quadric = gluNewQuadric();

	bResult = loadGLTexture(&galaxy_texture, MAKEINTRESOURCE(GALAXY));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for galaxy texture Failed\n");
		return(-6);
	}

	bResult = loadGLTexture(&mercury_texture, MAKEINTRESOURCE(MERCURY));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for mercury texture Failed\n");
		return(-7);
	}

	bResult = loadGLTexture(&venus_texture, MAKEINTRESOURCE(VENUS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for venus texture Failed\n");
		return(-8);
	}

	bResult = loadGLTexture(&earth_texture, MAKEINTRESOURCE(EARTH));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for earth texture Failed\n");
		return(-9);
	}

	bResult = loadGLTexture(&moon_texture, MAKEINTRESOURCE(MOON));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for moon texture Failed\n");
		return(-10);
	}

	bResult = loadGLTexture(&mars_texture, MAKEINTRESOURCE(MARS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for mars texture Failed\n");
		return(-11);
	}

	bResult = loadGLTexture(&jupiter_texture, MAKEINTRESOURCE(JUPITER));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for jupiter texture Failed\n");
		return(-12);
	}

	bResult = loadGLTexture(&saturn_texture, MAKEINTRESOURCE(SATURN));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for saturn texture Failed\n");
		return(-13);
	}

	bResult = loadGLTexture(&uranus_texture, MAKEINTRESOURCE(URANUS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for uranus texture Failed\n");
		return(-14);
	}

	bResult = loadGLTexture(&neptune_texture, MAKEINTRESOURCE(NEPTUNE));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for nuptune texture Failed\n");
		return(-15);
	}

	glEnable(GL_TEXTURE_2D);

	//Set the clear color of window to blue
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //Here OpenGL starts
	resize(WIN_WIDTH, WIN_HEIGHT);

	return (0);
}

BOOL loadGLTexture(GLuint* texture, TCHAR imageResourceId[])
{
	//Local variables
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//Load the image
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),
		imageResourceId,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION
	);

	if (hBitmap == NULL)
	{
		fprintf(gpFILE, "LoadImage Failed.\n");
		return (FALSE);
	}

	//Get image data
	GetObject(
		hBitmap,
		sizeof(BITMAP),
		&bmp
	);

	//Create OpenGL Texture
	glGenTextures(1, texture);

	//Bind to the texture
	glBindTexture(GL_TEXTURE_2D, *texture);

	//Load and unpack
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	//Set texture paramters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	//Create multiple mip map images
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, (void*)bmp.bmBits); //Instead of 3 GL_RGB

	//Unbind
	glBindTexture(GL_TEXTURE_2D, 0);

	//Delete hBitmap
	DeleteObject(hBitmap);
	hBitmap = NULL;

	return (TRUE);
}

void resize(int width, int height)
{
	//Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // Binocular 0-0
}

void display(void)
{
	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // glclearcolor ya color ni majha buffer clear kr


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	void ScreenFour(void); // Astrology Screen : By Piyush

	glTranslatef(0.0f, 0.0f, -30.0f);

	glPushMatrix();
		ScreenFour();
	glPopMatrix();

	SwapBuffers(ghdc);
}



void ScreenFour(void)
{
	void PNV_SolarSystem(void);
	glRotatef(-10.0f, 0.0f, 0.0f, 1.0f);
	glRotatef(20.0f, 1.0f, 0.0f, 0.0f);
	PNV_SolarSystem(); // solar system call
}

void update(void)
{
	//Code 

	SunAngle += 0.01f;
	Planet_Day += 1.0f;
	Mercury_Year += 0.6f;
	Venus_Year += 0.52f;
	Earth_Year += 0.44f;
	Mars_Year += 0.4f;
	Jupiter_Year += 0.28f;
	Saturn_Year += 0.2f;
	Uranus_Year += 0.12f;
	Neptune_Year += 0.04f;

	Galaxy_Move += 0.1f;
}

void uninitialize(void)
{	
	//Function declaration
	void ToggleFullscreen(void);
	
	//If application is exiting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Code
	//Make the hdc as current dc
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	//Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	//Destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	//CLose the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "Program ended successfully.");
		fclose(gpFILE);
		gpFILE = NULL;
	}
}


void PNV_SolarSystem(void)
{
	// Variable declarations
	FGL_Point p = { 0.0f, 0.0f, 0.0f };
	FGL_Color c1 = { 1.0f, 1.0f, 0.0f, 1.0f };
	FGL_Color c2 = { 1.0f, 0.5f, 0.0f, 1.0f };



	glPushMatrix();

	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glRotatef(Galaxy_Move, 0.0f, 1.0f, 0.0f);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, galaxy_texture);

	gluSphere(quadric, 40.00, 50, 50);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	glPushMatrix();

	////////////////////////////////////////////////////////// Sun
	glPushMatrix();

	glRotatef(SunAngle, 0.0f, 1.0f, 0.0f);
	FGL_Sphere(&p, &c1, &c2, 1.0f);

	glPopMatrix();

	///////////////////////////////////////////////////////// Mercury
	glPushMatrix();

	// Mercury Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 2.0f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Mercury_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(2.0f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Mercury
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, mercury_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.2, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	////////////////////////////////////////////////////////////////// Venus
	glPushMatrix();

	// Venus Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 2.8f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Venus_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(2.8f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Venus
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, venus_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.25, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	//////////////////////////////////////////////////////////// Earth & Moon
	glPushMatrix();

	// Earth Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 4.0f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Earth_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(4.0f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Earth
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, earth_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.3, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	// Draw thw moon
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, moon_texture);

	glTranslatef(0.5f, 0.0f, 0.0f);
	gluSphere(quadric, 0.1, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	///////////////////////////////////////////////////////// Mars
	glPushMatrix();

	// Mars Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 5.0f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Mars_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(5.0f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Mars
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, mars_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.28, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	////////////////////////////////////////////////// Jupiter
	glPushMatrix();

	// Jupiter Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 7.2f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Jupiter_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(7.2f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Jupiter
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, jupiter_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.58, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	////////////////////////////////////////////////////// Saturn
	glPushMatrix();

	// Saturn Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 9.0f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Saturn_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(9.0f, 0.0f, 0.0f);

	// Saturn Rings
	glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
	glLineWidth(1.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(0.39f, 0.33f, 0.29f, 1.0f);
	FGL_PointAssign(&p, 0.0f, 0.0f, 0.0f);

	FGL_Circle(&p, 0.525f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.55f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.575f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.6f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.625f, 0.0f, 360.0f);

	FGL_Circle(&p, 0.7f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.725f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.75f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.775f, 0.0f, 360.0f);
	FGL_Circle(&p, 0.8f, 0.0f, 360.0f);

	glEnd();

	glRotatef(-60.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Saturn
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, saturn_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.42, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	////////////////////////////////////////////////////// Uranus
	glPushMatrix();

	// Uranus Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 10.5f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Uranus_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(10.5f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Uranus
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, uranus_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.45, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();


	///////////////////////////////////////////////////////// Neptune
	glPushMatrix();

	// Neptune Orbit
	glLineWidth(1.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	FGL_Circle(&p, 12.0f, 0.0f, 360.0f);

	glEnd();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef(Neptune_Year, 0.0f, 1.0f, 0.0f);
	glTranslatef(12.0f, 0.0f, 0.0f);
	glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

	// Draw the Neptune
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, neptune_texture);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 0.4, 15, 15);

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();

	glPopMatrix();
}

