#pragma once

#define MYICON 101

#define GALAXY 106

#define MERCURY 107

#define VENUS 108

#define EARTH 109

#define MOON 110

#define MARS 111

#define JUPITER 112

#define SATURN 113

#define URANUS 114

#define NEPTUNE 115


/*********************** Basic **********************************/

// Structure For Vertex of OpenGL
typedef struct FGL_Point {
	float x;
	float y;
	float z;
} FGL_Point;

// Structure For Color of OpenGL
typedef struct FGL_Color {
	float r;
	float g;
	float b;
	float a;
} FGL_Color;

// Structure of 3 Vertex to draw 3 points curve
typedef struct FGL_3Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
} FGL_3Points;

// Structure of 4 Vertex to draw 4 points curve
typedef struct FGL_4Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
} FGL_4Points;

// Structure of 5 Vertex to draw 5 points curve
typedef struct FGL_5Points {
	FGL_Point p0;
	FGL_Point p1;
	FGL_Point p2;
	FGL_Point p3;
	FGL_Point p4;
} FGL_5Points;



// FGL_ColorToFloat converts String of hexadecimal color into equivalent color values into given pointer to FGL_Color object. 
void FGL_ColorToFloat(char color[], FGL_Color* c);

// FGL_PointAssign assigns given float values of x, y and z in respective co-ordinates into given pointer to FGL_Point object.  
void FGL_PointAssign(FGL_Point* point, float x, float y, float z);

// FGL_ColorAssign assigns given float values of red, green and blue in respective members into given pointer to FGL_Color object. 
void FGL_ColorAssign(FGL_Color* color, float r, float g, float b, float a);



/*********************** Mathematics **********************************/

// Value of Pi
#define FGL_Pi 3.14159265359

// Function like macro to convert degree into radian
#define FGL_Radian(degree)  ((degree * FGL_Pi) / 180.00)

// Function like macro to calculate powers of number hard-codedly.
#define FGL_Square(num)   (num * num)
#define FGL_Cube(num)     (num * num * num)
#define FGL_Quad(num)     (num * num * num * num)
#define FGL_Penta(num)    (num * num * num * num * num)
#define FGL_Hexa(num)     (num * num * num * num * num * num)
#define FGL_Hepta(num)    (num * num * num * num * num * num * num)
#define FGL_Octa(num)     (num * num * num * num * num * num * num * num)
#define FGL_Nona(num)     (num * num * num * num * num * num * num * num * num)
#define FGL_Deca(num)     (num * num * num * num * num * num * num * num * num * num)
#define FGL_Hedeca(num)   (num * num * num * num * num * num * num * num * num * num * num)
#define FGL_Dodeca(num)   (num * num * num * num * num * num * num * num * num * num * num * num)

// Function like macro to calculate Sine value between 0-180 degrees.
#define FGL_Sine180(radian)  (radian - (FGL_Cube(radian) / 6.00) + (FGL_Penta(radian) / 120.00) - (FGL_Hepta(radian) / 5040.00) + (FGL_Nona(radian) / 362880.00) - (FGL_Hedeca(radian) / 39916800.00))

// Function like macro to calculate Cosine value between 0-180 degrees.
#define FGL_Cosine180(radian)  (1.00 - (FGL_Square(radian) / 2.00) + (FGL_Quad(radian) / 24.00) - (FGL_Hexa(radian) / 720.00) + (FGL_Octa(radian) / 40320.00) - (FGL_Deca(radian) / 3628800.00) + (FGL_Dodeca(radian) / 479001600.00))

// FGL_Sin function calculates Sine value of any angle.
double FGL_Sin(double angle);

// FGL_Cos function calculates Cosine value of any angle.
double FGL_Cos(double angle);

// Other remaining T-functions 
#define FGL_Tan(degree)  (FGL_Sin(degree) / FGL_Cos(degree))
#define FGL_Cot(degree)  (FGL_Cos(degree) / FGL_Sin(degree))
#define FGL_Sec(degree)  (1.00 / FGL_Cos(degree))
#define FGL_Cosec(degree)  (1.00 / FGL_Sin(degree))

// FGL_SquareRoot gives square root of any number using Bakhshali method.
double FGL_SquareRoot(double num);

// FGL_Power gives any integer power of any number.
double FGL_Power(double num, int deg);

// FGL_Factorial gives factorial of any integer number.
long long int FGL_Factorial(int num);

// FGL_Ceil gives ceil integer value of any number.
int FGL_Ceil(double num);

// FGL_Floor gives floor integer value of any number.
int FGL_Floor(double num);

// FGL_CeilBase gives ceil integer value of any number with given base value.
int FGL_CeilBase(double num, double base);

// FGL_FloorBase gives floor integer value of any number with given base value.
int FGL_FloorBase(double num, double base);



/*********************** Shapes **********************************/

// FGL_Circle gives circle of given radius bound with start and end angle. (must call inside glBegin(____);)
void FGL_Circle(FGL_Point* center, float radius, float sAngle, float eAngle);

// FGL_Ellipse gives ellipse of given two radius bound with start and end angle. (must call inside glBegin(____);)
void FGL_Ellipse(FGL_Point* center, float xRadius, float yRadius, float sAngle, float eAngle);

// FGL_Sphere gives sphere of given radius.
void FGL_Sphere(FGL_Point* center, FGL_Color* color1, FGL_Color* color2, float radius);



/*********************** Curves **********************************/

// Function like macro for Bezier curve of 3, 4 and 5 points.
#define FGL_Bezier3(u, p0, p1, p2, axis) (p0.##axis * (1.0f - u) * (1.0f - u)) + (p1.##axis * 2.0f * (1.0f - u) * u) + (p2.##axis * u * u)
#define FGL_Bezier4(u, p0, p1, p2, p3, axis) (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 3.0f * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 3.0f * (1.0f - u) * u * u) + (p3.##axis * u * u * u)
#define FGL_Bezier5(u, p0, p1, p2, p3, p4, axis)  (p0.##axis * (1.0f - u) * (1.0f - u) * (1.0f - u) * (1.0f - u)) + (p1.##axis * 4.0f * (1.0f - u) * (1.0f - u) * (1.0f - u) * u) + (p2.##axis * 6.0f * (1.0f - u) * (1.0f - u) * u * u) + (p3.##axis * 4.0f * (1.0f - u) * u * u * u) + (p4.##axis * u * u * u * u);

// FGL_BezierCurve3 gives bezier curve of 3 points. (must call inside glBegin(____);)
void FGL_BezierCurve3(FGL_3Points* curve);

// FGL_BezierCurve4 gives bezier curve of 4 points. (must call inside glBegin(____);)
void FGL_BezierCurve4(FGL_4Points* curve);

// FGL_BezierCurve5 gives bezier curve of 5 points. (must call inside glBegin(____);)
void FGL_BezierCurve5(FGL_5Points* curve);

// FGL_BezierShape33 draws a shape enclosed in between 2 bezier curves of 3 points.
void FGL_BezierShape33(FGL_3Points* curve1, FGL_3Points* curve2, FGL_Color* c);

// FGL_BezierShape44 draws a shape enclosed in between 2 bezier curves of 4 points.
void FGL_BezierShape44(FGL_4Points* curve1, FGL_4Points* curve2, FGL_Color* c);

// FGL_BezierShape55 draws a shape enclosed in between 2 bezier curves of 5 points.
void FGL_BezierShape55(FGL_5Points* curve1, FGL_5Points* curve2, FGL_Color* c);

// FGL_BezierShape34 draws a shape enclosed in between 2 bezier curves of one of 3 points and other of 4 points.
void FGL_BezierShape34(FGL_3Points* curve1, FGL_4Points* curve2, FGL_Color* c);

// FGL_BezierShape35 draws a shape enclosed in between 2 bezier curves of one of 3 points and other of 5 points.
void FGL_BezierShape35(FGL_3Points* curve1, FGL_5Points* curve2, FGL_Color* c);

// FGL_BezierShape45 draws a shape enclosed in between 2 bezier curves of one of 4 points and other of 5 points.
void FGL_BezierShape45(FGL_4Points* curve1, FGL_5Points* curve2, FGL_Color* c);

