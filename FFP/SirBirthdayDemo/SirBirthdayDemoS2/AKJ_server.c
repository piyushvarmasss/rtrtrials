#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

GLfloat cAngle_AKJ = 0.0f;

//void AKJ_people(void)
//{
//	// function declaration
//	void AKJ_human(void);
//
//	// code
//	glPushMatrix(); // start 1
//		glTranslatef(-0.5f, -0.15f, 0.3f);
//		glScalef(0.8f, 0.8f, 1.0f);
//		AKJ_human();
//	glPopMatrix(); // end 1
//
//	glPushMatrix(); // start 2
//		glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
//
//		glPushMatrix(); // start 3
//			glTranslatef(-0.47f, -0.20f, -0.5f);
//			glScalef(0.7f, 0.7f, 1.0f);
//			AKJ_human();
//		glPopMatrix(); // end 3
//	glPopMatrix(); // end 2
//
//}
//
//void AKJ_human(void)
//{
//	// function declarations
//	void AKJ_head();
//	void AKJ_upperBody(void);
//	void AKJ_hand(void);
//	void AKJ_lowerBody(void);
//	void AKJ_legPartition(void);
//	void AKJ_shoes(void);
//	
//	// code
//
//	glPushMatrix();
//		AKJ_head();
//		AKJ_upperBody();
//		AKJ_hand();
//		AKJ_lowerBody();
//		AKJ_legPartition();
//		AKJ_shoes();
//	glPopMatrix();
//
//}
//
//void AKJ_head(void)
//{
//	// variable declarations
//	FGL_Point center;
//	FGL_Color myColor;
//
//	// code
//	FGL_PointAssign(&center, -0.30f, 0.48f, 0.0f);
//
//	glBegin(GL_TRIANGLE_FAN);
//	FGL_ColorToFloat("F9EAD8", &myColor);
//	glColor4f(myColor.r, myColor.g, myColor.b, myColor.a);
//	FGL_Ellipse(&center, 0.06f, 0.13f, 0.0f, 360.0f);
//	glEnd();
//}
//
//void AKJ_upperBody(void)
//{
//	// variable declaration
//	FGL_3Points curve_3pts;
//	FGL_4Points curve_4pts;
//	FGL_Color myColor;
//
//	// code
//	// assign points to FGL_3Points
//	FGL_PointAssign(&curve_3pts.p0, -0.35f, 0.4f, 0.0f);
//	FGL_PointAssign(&curve_3pts.p1, -0.4f, 0.0f, 0.0f);
//	FGL_PointAssign(&curve_3pts.p2, -0.35f, -0.2f, 0.0f);
//
//	// assign points to FGL_4Points
//	FGL_PointAssign(&curve_4pts.p0, -0.25f, 0.35f, 0.0f);
//	FGL_PointAssign(&curve_4pts.p1, -0.15f, 0.25f, 0.0f);
//	FGL_PointAssign(&curve_4pts.p2, -0.18f, 0.0f, 0.0f);
//	FGL_PointAssign(&curve_4pts.p3, -0.20f, -0.18f, 0.0f);
//
//	// paint color - white for now
//	FGL_ColorToFloat("F3F2F3", &myColor);
//
//	// draw shape
//	FGL_BezierShape34(&curve_3pts, &curve_4pts, &myColor);
//}
//
//void AKJ_hand(void)
//{
//	// variable declaration
//	FGL_3Points curve_3pts_bottom;
//	FGL_3Points curve_3pts_top;
//	FGL_Color myColor;
//
//	// code
//	// assign points to FGL_3Points
//	FGL_PointAssign(&curve_3pts_bottom.p0, -0.32f, 0.2f, 0.0f);
//	FGL_PointAssign(&curve_3pts_bottom.p1, -0.22f, -0.25f, 0.0f);
//	FGL_PointAssign(&curve_3pts_bottom.p2, -0.07f, 0.2f, 0.0f);
//
//	// assign points to FGL_3Points
//	FGL_PointAssign(&curve_3pts_top.p0, -0.25f, 0.17f, 0.0f);
//	FGL_PointAssign(&curve_3pts_top.p1, -0.215f, -0.10f, 0.0f);
//	FGL_PointAssign(&curve_3pts_top.p2, -0.13f, 0.17f, 0.0f);
//
//	// paint color 
//	FGL_ColorToFloat("F9EAD8", &myColor);
//
//	// draw shape
//	FGL_BezierShape33(&curve_3pts_bottom, &curve_3pts_top, &myColor);
//
//}
//
//void AKJ_lowerBody(void)
//{
//	// variable declaration
//	FGL_4Points curve_4pts_bottom, curve_4pts_top;
//	FGL_Color myColor;
//
//	// code
//	// assign points to curve_4pts_bottom
//	FGL_PointAssign(&curve_4pts_bottom.p0, -0.34f, -0.21f, 0.0f);
//	FGL_PointAssign(&curve_4pts_bottom.p1, -0.40f, -0.71f, 0.0f);
//	FGL_PointAssign(&curve_4pts_bottom.p2, 0.00f, -0.10f, 0.0f);
//	FGL_PointAssign(&curve_4pts_bottom.p3, 0.00f, -0.78f, 0.0f);
//
//	// assign points to curve_4pts_top
//	FGL_PointAssign(&curve_4pts_top.p0, -0.20f, -0.18f, 0.0f);
//	FGL_PointAssign(&curve_4pts_top.p1, 0.10f, -0.15f, 0.0f);
//	FGL_PointAssign(&curve_4pts_top.p2, 0.15f, -0.46f, 0.0f);
//	FGL_PointAssign(&curve_4pts_top.p3, 0.15f, -0.75f, 0.0f);
//
//	// paint color - white for now
//	FGL_ColorToFloat("F3F2F3", &myColor);
//
//	// draw shape
//	FGL_BezierShape44(&curve_4pts_bottom, &curve_4pts_top, &myColor);
//
//}
//
//void AKJ_legPartition(void)
//{
//	// variable declaration
//	FGL_3Points curve_3pts;
//
//	// code 
//	FGL_PointAssign(&curve_3pts.p0, -0.15f, -0.30f, 0.0f);
//	FGL_PointAssign(&curve_3pts.p1, 0.10f, -0.35f, 0.0f);
//	FGL_PointAssign(&curve_3pts.p2, 0.08f, -0.75f, 0.0f);
//
//
//	glColor3f(0.0f, 0.0f, 0.0f);
//	glBegin(GL_LINE_STRIP);
//	FGL_BezierCurve3(&curve_3pts);
//	glEnd();
//}
//
//void AKJ_shoes(void)
//{
//	// code
//	glBegin(GL_QUADS);
//	glVertex3f(0.13f, -0.75f, 0.0f);
//	glVertex3f(0.08f, -0.77f, 0.0f);
//	glVertex3f(0.08f, -0.83f, 0.0f);
//	glVertex3f(0.13f, -0.82f, 0.0f);
//	glEnd();
//
//	glBegin(GL_POLYGON);
//	glVertex3f(0.13f, -0.80f, 0.0f);
//	glVertex3f(0.08f, -0.83f, 0.0f);
//	glVertex3f(0.08f, -0.87f, 0.0f);
//	glVertex3f(0.18f, -0.86f, 0.0f);
//	glVertex3f(0.18f, -0.82f, 0.0f);
//	glEnd();
//
//
//	glBegin(GL_QUADS);
//	glVertex3f(0.07f, -0.75f, 0.0f);
//	glVertex3f(0.02f, -0.77f, 0.0f);
//	glVertex3f(0.02f, -0.83f, 0.0f);
//	glVertex3f(0.07f, -0.82f, 0.0f);
//	glEnd();
//
//	glBegin(GL_POLYGON);
//	glVertex3f(0.07f, -0.80f, 0.0f);
//	glVertex3f(0.02f, -0.83f, 0.0f);
//	glVertex3f(0.02f, -0.87f, 0.0f);
//	glVertex3f(0.12f, -0.86f, 0.0f);
//	glVertex3f(0.12f, -0.82f, 0.0f);
//	glEnd();
//
//
//}
//
//void AKJ_benches(void)
//{
//	// function declarations
//	void AKJ_bench(void);
//	void AKJ_benchLeg(void);
//
//	// code
//	glPushMatrix();
//		AKJ_bench();
//
//		glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
//		glScalef(0.8f, 0.8f, 0.1f);
//		glTranslatef(-0.05f, -0.1f, 0.5f);
//		glPushMatrix();
//			AKJ_bench();
//		glPopMatrix();
//	glPopMatrix();
//
//}
//
//void AKJ_bench(void)
//{
//	// function declaration
//	void AKJ_backRest(void);
//	void AKJ_benchSeat(void);
//	void AKJ_benchLeg(void);
//
//	// code
//
//	//glRotatef(cAngle_AKJ, 1.0f, 0.0f, 0.0f);
//	//glRotatef(cAngle_AKJ, 0.0f, 1.0f, 0.0f);
//	//glRotatef(cAngle_AKJ, 0.0f, 0.0f, 1.0f);
//
//	glPushMatrix(); // start 1 
//		glScalef(0.15f, 0.25f, 1.0f);
//		glTranslatef(-5.0f, -1.0f, -2.7f);
//
//		glPushMatrix(); // start 2
//		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
//
//			glPushMatrix(); // start 2
//				AKJ_backRest();
//
//				glTranslatef(0.0f, -1.1f, 0.0f);
//
//				glPushMatrix(); // start 3
//					AKJ_benchSeat();
//
//					glPushMatrix(); // start 4
//						AKJ_benchLeg();
//
//					glPopMatrix(); // end 4
//				glPopMatrix(); // end 3
//			glPopMatrix(); // end 2
//		glPopMatrix(); // end 2
//	glPopMatrix(); // end 1
//
//}
//
//void AKJ_benchLeg(void)
//{
//	// function declaration
//	void AKJ_Cube(void);
//
//	// code
//	glPushMatrix(); // start 1
//		glScalef(0.1f, 0.8f, 0.1f);
//		glTranslatef(-30.0f, -1.0f, 0.0f);
//
//		glPushMatrix(); // start 2
//			AKJ_Cube();
//
//			glTranslatef(8.5f, 0.0f, 0.0f);
//
//			glPushMatrix(); // start 3
//				AKJ_Cube();
//
//			glPopMatrix(); // end 3
//		glPopMatrix(); // end 2
//	glPopMatrix(); // end 1
//}
//
//void AKJ_benchSeat(void)
//{
//	// function declaration
//	void AKJ_Cube(void);
//
//	// code
//	glPushMatrix(); // start 1
//		glScalef(1.0f, 0.1f, 1.0f);
//
//		glPushMatrix(); // start 2
//			AKJ_Cube();
//
//			glTranslatef(-2.1f, 0.0f, 0.0f);
//			glPushMatrix(); // start 3
//				AKJ_Cube();
//
//				glTranslatef(4.2f, 0.0f, 0.0f);
//
//				glPushMatrix(); // start 4
//					AKJ_Cube();
//				glPopMatrix(); // end 4
//			glPopMatrix(); // end 3
//		glPopMatrix(); // end 2
//	glPopMatrix(); // end 1
//
//}
//
//void AKJ_backRest(void)
//{
//	// function declaration
//	void AKJ_Cube(void);
//
//	// code 
//	glPushMatrix(); // start 1
//		glScalef(1.0f, 1.0f, 0.1f);
//		glTranslatef(0.0f, 0.0f, -12.0f);
//
//		glPushMatrix(); // start 2
//			AKJ_Cube();
//
//			glTranslatef(-2.1f, 0.0f, 0.0f);
//			glPushMatrix(); // start 3
//				AKJ_Cube();
//
//				glTranslatef(4.2f, 0.0f, 0.0f);
//				glPushMatrix(); // start 4
//					AKJ_Cube();
//
//				glPopMatrix(); // end 4
//			glPopMatrix(); // end 3
//		glPopMatrix(); // end 2
//	glPopMatrix(); // end 1
//}
//
//void AKJ_Cube(void)
//{
//	// variable declarations
//	FGL_Color myColor;
//
//	// code
//	FGL_ColorToFloat("7FB3D5", &myColor);
//	glColor4f(myColor.r, myColor.g, myColor.b, myColor.a);
//
//	//glColor4f(0.51f, 0.64f, 0.77f);
//	glBegin(GL_QUADS);
//
//	// Front face 
//	glVertex3f(1.0f, 1.0f, 1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, 1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom
//
//	// right face
//	glVertex3f(1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(1.0f, 1.0f, 1.0f); // left top
//	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom
//
//	// back face
//	glVertex3f(-1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom
//	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom
//
//	// left face
//	glVertex3f(-1.0f, 1.0f, 1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom
//	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom
//
//	// top face
//	glVertex3f(1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom
//
//	// bottom face
//	glVertex3f(1.0f, -1.0f, -1.0f); // right top
//	glVertex3f(-1.0f, -1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom
//
//	glEnd();
//
//}

void AKJ_people(void)
{
	// function declaration
	void AKJ_human(void);

	// code
	glPushMatrix(); // start 1
	glTranslatef(-0.5f, -0.15f, -0.3f);
	glScalef(0.8f, 0.8f, 1.0f);
	AKJ_human();
	glPopMatrix(); // end 1

	glPushMatrix(); // start 2
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix(); // start 3
	glTranslatef(-0.45f, -0.19f, -0.65f);
	glScalef(0.55f, 0.55f, 1.0f);
	AKJ_human();
	glPopMatrix(); // end 3
	glPopMatrix(); // end 2

}

void AKJ_human(void)
{
	// function declarations
	void AKJ_head();
	void AKJ_upperBody(void);
	void AKJ_hand(void);
	void AKJ_lowerBody(void);
	void AKJ_legPartition(void);
	void AKJ_shoes(void);

	// code

	glPushMatrix();
	AKJ_head();
	AKJ_upperBody();
	AKJ_hand();
	AKJ_lowerBody();
	AKJ_legPartition();
	AKJ_shoes();
	glPopMatrix();

}

void AKJ_head(void)
{
	// variable declarations
	FGL_Point center;
	FGL_Color myColor;

	// code
	FGL_PointAssign(&center, -0.30f, 0.48f, 0.0f);

	glBegin(GL_TRIANGLE_FAN);
	FGL_ColorToFloat("F9EAD8", &myColor);
	glColor4f(myColor.r, myColor.g, myColor.b, myColor.a);
	FGL_Ellipse(&center, 0.07f, 0.14f, 0.0f, 360.0f);
	glEnd();
}

void AKJ_upperBody(void)
{
	// variable declaration
	FGL_3Points curve_3pts;
	FGL_4Points curve_4pts;
	FGL_Color myColor;

	// code
	// assign points to FGL_3Points
	FGL_PointAssign(&curve_3pts.p0, -0.35f, 0.4f, 0.0f);
	FGL_PointAssign(&curve_3pts.p1, -0.4f, 0.0f, 0.0f);
	FGL_PointAssign(&curve_3pts.p2, -0.35f, -0.2f, 0.0f);

	// assign points to FGL_4Points
	FGL_PointAssign(&curve_4pts.p0, -0.25f, 0.35f, 0.0f);
	FGL_PointAssign(&curve_4pts.p1, -0.15f, 0.25f, 0.0f);
	FGL_PointAssign(&curve_4pts.p2, -0.18f, 0.0f, 0.0f);
	FGL_PointAssign(&curve_4pts.p3, -0.20f, -0.18f, 0.0f);

	// paint color - white for now
	FGL_ColorToFloat("F3F2F3", &myColor);

	// draw shape
	FGL_BezierShape34(&curve_3pts, &curve_4pts, &myColor);
}

void AKJ_hand(void)
{
	// variable declaration
	FGL_3Points curve_3pts_bottom;
	FGL_3Points curve_3pts_top;
	FGL_Color myColor;

	// code
	// assign points to FGL_3Points
	FGL_PointAssign(&curve_3pts_bottom.p0, -0.32f, 0.2f, 0.0f);
	FGL_PointAssign(&curve_3pts_bottom.p1, -0.22f, -0.25f, 0.0f);
	FGL_PointAssign(&curve_3pts_bottom.p2, -0.07f, 0.2f, 0.0f);

	// assign points to FGL_3Points
	FGL_PointAssign(&curve_3pts_top.p0, -0.25f, 0.17f, 0.0f);
	FGL_PointAssign(&curve_3pts_top.p1, -0.215f, -0.10f, 0.0f);
	FGL_PointAssign(&curve_3pts_top.p2, -0.13f, 0.17f, 0.0f);

	// paint color 
	FGL_ColorToFloat("F9EAD8", &myColor);

	// draw shape
	FGL_BezierShape33(&curve_3pts_bottom, &curve_3pts_top, &myColor);

}

void AKJ_lowerBody(void)
{
	// variable declaration
	FGL_4Points curve_4pts_bottom, curve_4pts_top;
	FGL_Color myColor;

	// code
	// assign points to curve_4pts_bottom
	FGL_PointAssign(&curve_4pts_bottom.p0, -0.34f, -0.21f, 0.0f);
	FGL_PointAssign(&curve_4pts_bottom.p1, -0.40f, -0.71f, 0.0f);
	FGL_PointAssign(&curve_4pts_bottom.p2, 0.00f, -0.10f, 0.0f);
	FGL_PointAssign(&curve_4pts_bottom.p3, 0.00f, -0.78f, 0.0f);

	// assign points to curve_4pts_top
	FGL_PointAssign(&curve_4pts_top.p0, -0.20f, -0.18f, 0.0f);
	FGL_PointAssign(&curve_4pts_top.p1, 0.10f, -0.15f, 0.0f);
	FGL_PointAssign(&curve_4pts_top.p2, 0.15f, -0.46f, 0.0f);
	FGL_PointAssign(&curve_4pts_top.p3, 0.15f, -0.75f, 0.0f);

	// paint color - white for now
	FGL_ColorToFloat("F3F2F3", &myColor);

	// draw shape
	FGL_BezierShape44(&curve_4pts_bottom, &curve_4pts_top, &myColor);

}

void AKJ_legPartition(void)
{
	// variable declaration
	FGL_3Points curve_3pts;

	// code 
	FGL_PointAssign(&curve_3pts.p0, -0.15f, -0.30f, 0.0f);
	FGL_PointAssign(&curve_3pts.p1, 0.10f, -0.35f, 0.0f);
	FGL_PointAssign(&curve_3pts.p2, 0.08f, -0.75f, 0.0f);


	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);
	FGL_BezierCurve3(&curve_3pts);
	glEnd();
}

void AKJ_shoes(void)
{
	// code
	glBegin(GL_QUADS);
	glVertex3f(0.13f, -0.75f, 0.0f);
	glVertex3f(0.08f, -0.77f, 0.0f);
	glVertex3f(0.08f, -0.83f, 0.0f);
	glVertex3f(0.13f, -0.82f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.13f, -0.80f, 0.0f);
	glVertex3f(0.08f, -0.83f, 0.0f);
	glVertex3f(0.08f, -0.87f, 0.0f);
	glVertex3f(0.18f, -0.86f, 0.0f);
	glVertex3f(0.18f, -0.82f, 0.0f);
	glEnd();


	glBegin(GL_QUADS);
	glVertex3f(0.07f, -0.75f, 0.0f);
	glVertex3f(0.02f, -0.77f, 0.0f);
	glVertex3f(0.02f, -0.83f, 0.0f);
	glVertex3f(0.07f, -0.82f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.07f, -0.80f, 0.0f);
	glVertex3f(0.02f, -0.83f, 0.0f);
	glVertex3f(0.02f, -0.87f, 0.0f);
	glVertex3f(0.12f, -0.86f, 0.0f);
	glVertex3f(0.12f, -0.82f, 0.0f);
	glEnd();


}

void AKJ_benches(void)
{
	// function declarations
	void AKJ_bench(void);
	void AKJ_benchLeg(void);

	// code
	glPushMatrix();
	AKJ_bench();

	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glScalef(0.8f, 0.8f, 0.1f);
	glTranslatef(-0.05f, -0.1f, 0.8f);
	glPushMatrix();
	AKJ_bench();
	glPopMatrix();
	glPopMatrix();

}

void AKJ_bench(void)
{
	// function declaration
	void AKJ_backRest(void);
	void AKJ_benchSeat(void);
	void AKJ_benchLeg(void);

	// code

	//glRotatef(cAngle_AKJ, 1.0f, 0.0f, 0.0f);
	//glRotatef(cAngle_AKJ, 0.0f, 1.0f, 0.0f);
	//glRotatef(cAngle_AKJ, 0.0f, 0.0f, 1.0f);

	glPushMatrix(); // start 1 
	glScalef(0.15f, 0.25f, 1.0f);
	glTranslatef(-5.0f, -1.0f, -2.7f);

	glPushMatrix(); // start 2
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix(); // start 2
	AKJ_backRest();

	glTranslatef(0.0f, -1.1f, 0.0f);

	glPushMatrix(); // start 3
	AKJ_benchSeat();

	glPushMatrix(); // start 4
	AKJ_benchLeg();

	glPopMatrix(); // end 4
	glPopMatrix(); // end 3
	glPopMatrix(); // end 2
	glPopMatrix(); // end 2
	glPopMatrix(); // end 1

}

void AKJ_benchLeg(void)
{
	// function declaration
	void AKJ_Cube(void);

	// code
	glPushMatrix(); // start 1
	glScalef(0.1f, 0.8f, 0.1f);
	glTranslatef(-30.0f, -1.0f, 0.0f);

	glPushMatrix(); // start 2
	AKJ_Cube();

	glTranslatef(8.5f, 0.0f, 0.0f);

	glPushMatrix(); // start 3
	AKJ_Cube();

	glPopMatrix(); // end 3
	glPopMatrix(); // end 2
	glPopMatrix(); // end 1
}

void AKJ_benchSeat(void)
{
	// function declaration
	void AKJ_Cube(void);

	// code
	glPushMatrix(); // start 1
	glScalef(1.0f, 0.1f, 1.0f);

	glPushMatrix(); // start 2
	AKJ_Cube();

	glTranslatef(-2.1f, 0.0f, 0.0f);
	glPushMatrix(); // start 3
	AKJ_Cube();

	glTranslatef(4.2f, 0.0f, 0.0f);

	glPushMatrix(); // start 4
	AKJ_Cube();
	glPopMatrix(); // end 4
	glPopMatrix(); // end 3
	glPopMatrix(); // end 2
	glPopMatrix(); // end 1

}

void AKJ_backRest(void)
{
	// function declaration
	void AKJ_Cube(void);

	// code 
	glPushMatrix(); // start 1
	glScalef(1.0f, 1.0f, 0.1f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glPushMatrix(); // start 2
	AKJ_Cube();

	glTranslatef(-2.1f, 0.0f, 0.0f);
	glPushMatrix(); // start 3
	AKJ_Cube();

	glTranslatef(4.2f, 0.0f, 0.0f);
	glPushMatrix(); // start 4
	AKJ_Cube();

	glPopMatrix(); // end 4
	glPopMatrix(); // end 3
	glPopMatrix(); // end 2
	glPopMatrix(); // end 1
}

void AKJ_Cube(void)
{
	// variable declarations
	FGL_Color myColor;

	// code
	FGL_ColorToFloat("316B92", &myColor);
	//FGL_ColorToFloat("7FB3D5", &myColor);
	glColor4f(myColor.r, myColor.g, myColor.b, myColor.a);

	//glColor4f(0.51f, 0.64f, 0.77f);
	glBegin(GL_QUADS);

	// Front face 
	glVertex3f(1.0f, 1.0f, 1.0f); // right top
	glVertex3f(-1.0f, 1.0f, 1.0f); // left top
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	// right face
	glVertex3f(1.0f, 1.0f, -1.0f); // right top
	glVertex3f(1.0f, 1.0f, 1.0f); // left top
	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom

	// back face
	glVertex3f(-1.0f, 1.0f, -1.0f); // right top
	glVertex3f(1.0f, 1.0f, -1.0f); // left top
	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom
	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom

	// left face
	glVertex3f(-1.0f, 1.0f, 1.0f); // right top
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom

	// top face
	glVertex3f(1.0f, 1.0f, -1.0f); // right top
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom

	// bottom face
	glVertex3f(1.0f, -1.0f, -1.0f); // right top
	glVertex3f(-1.0f, -1.0f, -1.0f); // left top
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	glEnd();

}