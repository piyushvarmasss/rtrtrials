#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

// global variables
GLfloat AMA_CurtainTranslateDist = 0.0f;
GLboolean AMA_CurtainOpenFlag = FALSE;
GLfloat KernelAlpha = 0.0f;

extern HDC ghdc;

void theatreStage(void)
{
	// variable declarations
	FGL_Color stageColor;
//FGL_ColorToFloat("87CEEB", &stageColor);

	glBegin(GL_QUADS);
		
		// front face of base
		//glColor4f(stageColor.r, stageColor.g, stageColor.b, stageColor.a);
	//glColor4f(0.03f,0.18f,0.72f,1.0f);
	glColor4f(0.03f, 0.18f, 0.42f, 1.0f);

		glVertex3f(3.75f, -1.5f, 0.0f);
		glVertex3f(-3.75f, -1.5f, 0.0f);
		glVertex3f(-3.75f, -2.25f, 0.0f);
		glVertex3f(3.75f, -2.25f, 0.0f);

		// top face of base
		//glColor4f(stageColor.r-0.1f, stageColor.g - 0.1f, stageColor.b - 0.1f, stageColor.a);
		glColor4f(0.03f, 0.18f, 0.72f, 1.0f);
		glVertex3f(2.25f, -0.75f, 0.0f);
		glVertex3f(-2.25f, -0.75f, 0.0f);
		glVertex3f(-3.75f, -1.5f, 0.0f);
		glVertex3f(3.75f, -1.5f, 0.0f);

		// right side wall
		//glColor4f(stageColor.r, stageColor.g, stageColor.b, stageColor.a);
		glColor4f(0.03f, 0.18f, 0.5f, 1.0f);
		glVertex3f(3.75f, 2.1f, 0.0f);
		glVertex3f(2.25f, 1.5f, 0.0f);
		glVertex3f(2.25f, -0.75f, 0.0f);
		glVertex3f(3.75f, -1.5f, 0.0f);

		// left side wall
		glColor4f(0.03f, 0.18f, 0.5f, 1.0f);
		glVertex3f(-3.75f, 2.1f, 0.0f);
		glVertex3f(-2.25f, 1.5f, 0.0f);
		glVertex3f(-2.25f, -0.75f, 0.0f);
		glVertex3f(-3.75f, -1.5f, 0.0f);

		// inner top face of the stage
		//glColor4f(stageColor.r - 0.1f, stageColor.g - 0.1f, stageColor.b - 0.1f, stageColor.a);
		glColor4f(0.03f, 0.18f, 0.72f, 1.0f);
		glVertex3f(3.75f, 2.1f, 0.0f);
		glVertex3f(-3.75f, 2.1f, 0.0f);
		glVertex3f(-2.25f, 1.5f, 0.0f);
		glVertex3f(2.25f, 1.5f, 0.0f);

		// back face of the stage
		//glColor4f(stageColor.r - 0.2f, stageColor.g - 0.2f, stageColor.b - 0.2f, stageColor.a);
		glColor4f(0.03f, 0.18f, 0.42f, 1.0f);
		glVertex3f(2.25f, 1.5f, 0.0f);
		glVertex3f(-2.25f, 1.5f, 0.0f);
		glVertex3f(-2.25f, -0.75f, 0.0f);
		glVertex3f(2.25f, -0.75f, 0.0f);

	glEnd();
	

}

void theatreCurtain(void)
{
	// variable declarations
	FGL_Color CurtainColor1;
	FGL_Color CurtainColor2;
	FGL_Point center;

	// code
	FGL_ColorToFloat("e10600", &CurtainColor1);
	FGL_ColorToFloat("890800", &CurtainColor2);

	glBegin(GL_QUADS);
		/*
			glColor4f(myColor2.r, myColor2.g, myColor2.b, myColor2.a);
			glVertex3f(3.75f, 2.1f, 0.0f);
			glColor4f(myColor1.r, myColor1.g, myColor1.b, myColor1.a);
			glVertex3f(0.01f, 2.1f, 0.0f);
			glVertex3f(0.01f, -1.48f, 0.0f);
			glColor4f(myColor2.r, myColor2.g, myColor2.b, myColor2.a);
			glVertex3f(3.75f, -1.48f, 0.0f);

			glColor4f(myColor2.r, myColor2.g, myColor2.b, myColor2.a);
			glVertex3f(-3.75f, 2.1f, 0.0f);
			glColor4f(myColor1.r, myColor1.g, myColor1.b, myColor1.a);
			glVertex3f(-0.01f, 2.1f, 0.0f);
			glVertex3f(-0.01f, -1.48f, 0.0f);
			glColor4f(myColor2.r, myColor2.g, myColor2.b, myColor2.a);
			glVertex3f(-3.75f, -1.48f, 0.0f);
		*/

		for (int i = 1; i <= 15; ++i)
		{
			glColor4f(CurtainColor1.r - ((i - 1) * 0.05), CurtainColor1.g - ((i - 1) * 0.05), CurtainColor1.b - ((i - 1) * 0.05), CurtainColor1.a);
			//glTranslatef(AMA_CurtainTranslateDist, 0.0f, 0.0f);
			glVertex3f(0.01f + AMA_CurtainTranslateDist + ((i - 1) * 0.25f), 2.1f, 0.0f);
			glVertex3f(0.01f + AMA_CurtainTranslateDist + ((i - 1) * 0.25f), -1.48f, 0.0f);
			glVertex3f(0.01f + AMA_CurtainTranslateDist + (i * 0.25f), -1.48f, 0.0f);
			glVertex3f(0.01f + AMA_CurtainTranslateDist + (i * 0.25f), 2.1f, 0.0f);

			//glTranslatef((-1.0f) * AMA_CurtainTranslateDist, 0.0f, 0.0f);
			glVertex3f(-0.01f - AMA_CurtainTranslateDist - ((i - 1) * 0.25f), 2.1f, 0.0f);
			glVertex3f(-0.01f - AMA_CurtainTranslateDist - ((i - 1) * 0.25f), -1.48f, 0.0f);
			glVertex3f(-0.01f - AMA_CurtainTranslateDist - (i * 0.25f), -1.48f, 0.0f);
			glVertex3f(-0.01f - AMA_CurtainTranslateDist - (i * 0.25f), 2.1f, 0.0f);
		}


	glEnd();


	//FGL_ColorToFloat("e52100", &CurtainColor2);
	//glColor4f(CurtainColor2.r, CurtainColor2.g, CurtainColor2.b, CurtainColor2.a);
	glColor4f(0.4f, 0.0f, 0.0f, 1.0f);
	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, 0.65f, 2.25f, 0.0f);
		FGL_Circle(&center, 0.7f, 180.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, -0.65f, 2.25f, 0.0f);
		FGL_Circle(&center, 0.7f, 180.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, -2.4f, 2.65f, 0.0f);
		FGL_Circle(&center, 1.5f, 180.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, 2.4f, 2.65f, 0.0f);
		FGL_Circle(&center, 1.5f, 180.0f, 360.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, -4.3f, 1.3f, 0.0f);
		FGL_Circle(&center, 1.5f, 270.0f, 450.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, 4.3f, 1.3f, 0.0f);
		FGL_Circle(&center, 1.5f, 90.0f, 270.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, -5.8f, -1.5f, 0.0f);
		FGL_Circle(&center, 2.8f, 360.0f, 405.0f);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
		FGL_PointAssign(&center, 5.8f, -1.5f, 0.0f);
		FGL_Circle(&center, 2.8f, 135.0f, 180.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
		glVertex3f(-3.0f, -1.5f, 0.0f);
		glVertex3f(-4.0f, 1.0f, 0.0f);
		glVertex3f(-4.0f, -1.5f, 0.0f);

		glVertex3f(3.0f, -1.5f, 0.0f);
		glVertex3f(4.0f, 1.0f, 0.0f);
		glVertex3f(4.0f, -1.5f, 0.0f);
	glEnd();
}

void Kernel(void)
{
	// For texture

	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 1.0f, 1.0f, KernelAlpha);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	char str1[] = "\"KERNEL\"";

	glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
	DeleteObject(hFont);
}