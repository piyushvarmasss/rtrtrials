// Header file guard used to : prevent inclusion of the header multiple times.
#ifndef _AMA_SERVER_H 
#define _AMA_SERVER_H 

// function declarations
void theatreStage(void);
void theatreCurtain(void);
void Kernel(void);

// global variable declarations
extern GLfloat AMA_CurtainTranslateDist;
extern GLboolean AMA_CurtainOpenFlag;

#endif	/* _AMA_SERVER_H */