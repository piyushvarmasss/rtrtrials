// function declarations
void PNV_SolarSystem(void);
void Director_Chair(void);
void AstroText(void);
void FirstScreenOfFont(void);
void SecondScreenOfFont(void);
void LastCredits(void);
void SpecialThanks(void);
void IgnitedBy(void);
void TheEnd(void);