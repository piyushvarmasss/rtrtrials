#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

//void aks_myObject(void)
//{
//	// function delcarations
//	void aks_room(void);
//	void AKJ_door(void);
//
//	// variable declarations
//	FGL_Point center;
//	FGL_Color myColor;
//
//	// code
//	FGL_PointAssign(&center, 0.0f, 0.0f, 0.0f);
//	
//	glTranslatef(0.0f, 0.0f, -4.0f);
//	glScalef(2.2f, 1.25f, 1.0f);
//	glPushMatrix();
//		aks_room();
//		AKJ_door();
//	glPopMatrix();
//}
//
//void aks_room(void)
//{
//	//base floor
//	glColor4f(1.0f, 1.0f, 0.8f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(1.0f, -1.0f, -1.0f);
//	glVertex3f(-1.0f, -1.0f, -1.0f);
//	glVertex3f(-1.0f, -1.0f, 1.0f);
//	glVertex3f(1.0f, -1.0f, 1.0f);
//	glEnd();
//
//	// tiles
//	GLfloat x = 0.0f;
//	while (x < 1.0f)
//	{
//		//glLineWidth(0.3f);
//		glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(x, -0.99f, 1.0f);
//		glVertex3f(x, -0.99f, -1.0f);
//		glEnd();
//
//		x = x + 0.2f;
//	}	
//
//	x = 0.0f;
//	while (x > -1.0f)
//	{
//		//glLineWidth(0.3f);
//		glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(x, -0.99f, 1.0f);
//		glVertex3f(x, -0.99f, -1.0f);
//		glEnd();
//
//		x = x - 0.2f;
//	}
//
//
//	GLfloat z = 1.0f;
//	
//
//	while (z > 0.0f)
//	{
//		glLineWidth(0.3f);
//		glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(1, -1, z);
//		glVertex3f(-1, -1, z);
//		glEnd();
//
//		z = z - 0.2f;
//	}
//
//	z = 1.0f;
//	
//	while (z > 0.0f)
//	{
//		glLineWidth(0.3f);
//		glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//		glVertex3f(1.0f, -1.0f, -z);
//		glVertex3f(-1.0f, -1.0f, -z);
//		glEnd();
//
//		z = z - 0.2f;
//
//	}
//
//	// right Wall
//	glColor4f(0.6f, 0.8f, 1.0f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(1.0f, 1.0f, 1.0f);
//	glVertex3f(1.0f, 1.0f, -1.0f);
//	glVertex3f(1.0f, -1.0f, -1.0f);
//	glVertex3f(1.0f, -1.0f, 1.0f);
//	glEnd();
//
//	// left Wall
//	glColor4f(0.6f, 0.8f, 1.0f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(-1.0f, 1.0f, -1.0f);
//	glVertex3f(-1.0f, 1.0f, 1.0f);
//	glVertex3f(-1.0f, -1.0f, 1.0f);
//	glVertex3f(-1.0f, -1.0f, -1.0f);
//	glEnd();
//
//	// top 
//	glColor4f(0.9f, 1.0f, 1.0f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(1.0f, 1.0f, -1.0f);
//	glVertex3f(-1.0f, 1.0f, -1.0f);
//	glVertex3f(-1.0f, 1.0f, 1.0f);
//	glVertex3f(1.0f, 1.0f, 1.0f);
//	glEnd();
//
//	// Operation theater wall
//
//	glColor4f(0.6f, 0.9f, 1.0f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(1.0f, 1.0f, -1.0f);
//	glVertex3f(-1.0f, 1.0f, -1.0f);
//	glVertex3f(-1.0f, -1.0f, -1.0f);
//	glVertex3f(1.0f, -1.0f, -1.0f);
//	glEnd();
//
//	/*glColor3f(0.0f, 0.0f, 0.0f);
//	glBegin(GL_QUADS);
//	glVertex3f(-0.5f, 0.3f, -1.0f);
//	glVertex3f(-0.5f, -0.3f, -1.0f);
//	glVertex3f(0.5f, -0.3f, -1.0f);
//	glVertex3f(0.5f, 0.3f, -1.0f);
//	glEnd();*/
//}
//
//void AKJ_light(void)
//{
//
//}
//
//void AKJ_door(void)
//{
//	// function declaration
//	void AKJ_doorFrame(void);
//	void AKJ_doorInnerPart(void);
//	void AKJ_doorKnob(void);
//
//	// code
//	glTranslatef(1.0f, -0.2f, 0.8f);
//	glScalef(0.5f, 0.5f, 0.5f);
//	glPushMatrix();
//		glRotatef(92.0f, 0.0f, 1.0f, 0.0f);
//
//		AKJ_doorFrame();
//		AKJ_doorInnerPart();
//		AKJ_doorKnob();
//	glPopMatrix();
//
//}
//
//void AKJ_doorKnob(void)
//{
//	// variable declarations
//	FGL_Point center;
//	FGL_Color myColor;
//
//	// code
//	FGL_PointAssign(&center, 0.6f, 0.0f, 0.0f);
//	FGL_ColorToFloat("316B92", &myColor);
//
//	FGL_Sphere(&center, &myColor, &myColor, 0.05f);
//}
//
//void AKJ_doorFrame(void)
//{
//	// function declaration
//	void AKJ_DoorCube(FGL_Color*);
//
//	// variable declarations
//	FGL_Color myColor;
//
//	// code
//
//	glPushMatrix();
//	glScalef(0.85f, 1.6f, 0.02f);
//	FGL_ColorToFloat("316B92", &myColor);
//	glPushMatrix();
//	AKJ_DoorCube(&myColor);
//	glPopMatrix();
//	glPopMatrix();
//}
//
//void AKJ_doorInnerPart(void)
//{
//	// function declaration
//	void AKJ_Cube(void);
//
//	// code
//	glPushMatrix();
//	glScalef(0.8f, 1.5f, 0.02f);
//		glPushMatrix();
//			AKJ_Cube();
//		glPopMatrix();
//	glPopMatrix();
//}
//
//void AKJ_DoorCube(FGL_Color* myColor)
//{
//	// variable declarations
//	//FGL_Color myColor;
//
//	// code
//	//FGL_ColorToFloat("7FB3D5", &myColor);
//	glColor4f(myColor->r, myColor->g, myColor->b, myColor->a);
//
//	//glColor4f(0.51f, 0.64f, 0.77f);
//	glBegin(GL_QUADS);
//
//	// Front face 
//	glVertex3f(1.0f, 1.0f, 1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, 1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom
//
//	// right face
//	glVertex3f(1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(1.0f, 1.0f, 1.0f); // left top
//	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom
//
//	// back face
//	glVertex3f(-1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom
//	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom
//
//	// left face
//	glVertex3f(-1.0f, 1.0f, 1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom
//	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom
//
//	// top face
//	glVertex3f(1.0f, 1.0f, -1.0f); // right top
//	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom
//
//	// bottom face
//	glVertex3f(1.0f, -1.0f, -1.0f); // right top
//	glVertex3f(-1.0f, -1.0f, -1.0f); // left top
//	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
//	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom
//
//	glEnd();
//
//}

extern GLuint texture_ganpati;

void aks_myObject(void)
{
	// function delcarations
	void aks_room(void);
	void AKJ_door(void);

	// variable declarations
	FGL_Point center;
	FGL_Color myColor;


	// code
	FGL_PointAssign(&center, 0.0f, 0.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, -4.0f);
	glScalef(2.2f, 1.25f, 1.0f);
	glPushMatrix();
	aks_room();
	AKJ_door();
	glPopMatrix();
}

void aks_room(void)
{
	//base floor
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	glBegin(GL_QUADS);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();


	// tiles
	GLfloat x = 0.0f;
	while (x < 1.0f)
	{
		//glLineWidth(0.3f);
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		glBegin(GL_LINE_STRIP);
			glVertex3f(x, -0.99f, 1.0f);
			glVertex3f(x, -0.99f, -1.0f);
		glEnd();

		x = x + 0.2f;
	}

	x = 0.0f;
	while (x > -1.0f)
	{
		//glLineWidth(0.3f);
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		glBegin(GL_LINE_STRIP);
		glVertex3f(x, -0.99f, 1.0f);
		glVertex3f(x, -0.99f, -1.0f);
		glEnd();

		x = x - 0.2f;
	}

	GLfloat z = 1.0f;

	while (z > -0.8f)
	{
		glLineWidth(0.3f);
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		glBegin(GL_LINE_STRIP);
			glVertex3f(1.0f, -0.99f, z);
			glVertex3f(-1.0f, -0.99f, z);
		glEnd();

		z = z - 0.2f;
	}


	// right Wall
	glColor4f(0.6f, 0.8f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glEnd();

	// left Wall
	glColor4f(0.6f, 0.8f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glEnd();

	// top 
	glColor4f(0.9f, 1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glEnd();

	// Operation theater wall
	glColor4f(0.6f, 0.9f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();


	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture_ganpati);
	//glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.25f, 0.4f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.25f, -0.4f, -1.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.25f, -0.4f, -1.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.25f, 0.4f, -1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void AKJ_light(void)
{

}

void AKJ_door(void)
{
	// function declaration
	void AKJ_doorFrame(void);
	void AKJ_doorInnerPart(void);
	void AKJ_doorKnob(void);

	// code
	glTranslatef(1.0f, -0.2f, 0.8f);
	glScalef(0.5f, 0.5f, 0.5f);
	glPushMatrix();
		glRotatef(92.0f, 0.0f, 1.0f, 0.0f);

		AKJ_doorFrame();
		//AKJ_doorInnerPart();
		AKJ_doorKnob();
	glPopMatrix();

}

void AKJ_doorKnob(void)
{
	// variable declarations
	FGL_Point center;
	FGL_Color myColor;

	// code
	FGL_PointAssign(&center, 0.6f, 0.0f, 0.0f);
	FGL_ColorToFloat("316B92", &myColor);
	//FGL_ColorToFloat("7FB3D5", &myColor);

	FGL_Sphere(&center, &myColor, &myColor, 0.05f);
}

void AKJ_doorFrame(void)
{
	// function declaration
	void AKJ_DoorCube(FGL_Color*);

	// variable declarations
	FGL_Color myColor;

	// code

	glPushMatrix();
		glScalef(0.85f, 1.6f, 0.02f);
		FGL_ColorToFloat("316B92", &myColor);
		glPushMatrix();
			AKJ_DoorCube(&myColor);
		glPopMatrix();
	glPopMatrix();
}

void AKJ_doorInnerPart(void)
{
	// function declaration
	void AKJ_Cube(void);

	// code
	glPushMatrix();
		glScalef(0.8f, 1.5f, 0.02f);
	glPushMatrix();
		AKJ_Cube();
	glPopMatrix();
	glPopMatrix();
}

void AKJ_DoorCube(FGL_Color* myColor)
{
	// variable declarations
	//FGL_Color myColor;

	// code
	//FGL_ColorToFloat("7FB3D5", &myColor);
	glColor4f(myColor->r, myColor->g, myColor->b, myColor->a);

	//glColor4f(0.51f, 0.64f, 0.77f);
	glBegin(GL_QUADS);

	// Front face 
	glVertex3f(1.0f, 1.0f, 1.0f); // right top
	glVertex3f(-1.0f, 1.0f, 1.0f); // left top
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	// right face
	glVertex3f(1.0f, 1.0f, -1.0f); // right top
	glVertex3f(1.0f, 1.0f, 1.0f); // left top
	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom

	// back face
	glVertex3f(-1.0f, 1.0f, -1.0f); // right top
	glVertex3f(1.0f, 1.0f, -1.0f); // left top
	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom
	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom

	// left face
	glVertex3f(-1.0f, 1.0f, 1.0f); // right top
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom
	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom

	// top face
	glVertex3f(1.0f, 1.0f, -1.0f); // right top
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top
	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom

	// bottom face
	glVertex3f(1.0f, -1.0f, -1.0f); // right top
	glVertex3f(-1.0f, -1.0f, -1.0f); // left top
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	// Inner  Colored
	FGL_ColorToFloat("7FB3D5", myColor);
	glColor4f(myColor->r, myColor->g, myColor->b, myColor->a);
	glVertex3f(-0.8f, 0.8f, -1.01f); // right top
	glVertex3f(0.8f, 0.8f, -1.01f); // left top
	glVertex3f(0.8f, -0.8f, -1.01f); // left bottom
	glVertex3f(-0.8f, -0.8f, -1.01f); // right bottom


	glEnd();

}