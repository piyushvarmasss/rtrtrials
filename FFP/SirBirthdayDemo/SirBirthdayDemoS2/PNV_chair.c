#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"
#include "OGL.h"

GLfloat chair_angle = 0.0f;
extern GLuint texture_wood;

void Director_Chair(void) {

	// Function Declarations
	FGL_Color c;
	FGL_Point p;
	glEnable(GL_TEXTURE_2D);
	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(chair_angle, 0.0f, 1.0f, 0.0f);

	///////////////////////////////////////////// Left Back Leg
	glBindTexture(GL_TEXTURE_2D, texture_wood);
	glBegin(GL_QUADS);
	
		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		
		// Bottom
		glVertex3f(-1.0f, -1.5f, -1.0f);
		glVertex3f(-1.0f, -1.5f, -0.8f);
		glVertex3f(-0.8f, -1.5f, -0.8f);
		glVertex3f(-0.8f, -1.5f, -1.0f);

		// Top
		glVertex3f(-1.0f, 0.2f, -1.0f);
		glVertex3f(-1.0f, 0.2f, -0.8f);
		glVertex3f(-0.8f, 0.2f, -0.8f);
		glVertex3f(-0.8f, 0.2f, -1.0f);

		FGL_ColorToFloat("765827", &c);		// Medium
		glColor3f(c.r, c.g, c.b);
		// Right
		glVertex3f(-0.8f, -1.5f, -1.0f);
		glVertex3f(-0.8f, -1.5f, -0.8f);
		glVertex3f(-0.8f, 0.2f, -0.8f);
		glVertex3f(-0.8f, 0.2f, -1.0f);

		// Left
		glVertex3f(-1.0f, -1.5f, -1.0f);
		glVertex3f(-1.0f, -1.5f, -0.8f);
		glVertex3f(-1.0f, 0.2f, -0.8f);
		glVertex3f(-1.0f, 0.2f, -1.0f);

		// Front
		glVertex3f(-0.8f, -1.5f, -0.8f);
		glVertex3f(-0.8f, 0.2f, -0.8f);
		glVertex3f(-1.0f, 0.2f, -0.8f);
		glVertex3f(-1.0f, -1.5f, -0.8f);

		// Back
		glVertex3f(-0.8f, -1.5f, -1.0f);
		glVertex3f(-0.8f, 0.2f, -1.0f);
		glVertex3f(-1.0f, 0.2f, -1.0f);
		glVertex3f(-1.0f, -1.5f, -1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);

	//////////////////////////////////////////////// Left Front Leg 
	glBegin(GL_QUADS);

		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		// Bottom
		glVertex3f(-1.0f, -1.5f, 1.0f);
		glVertex3f(-1.0f, -1.5f, 0.8f);
		glVertex3f(-0.8f, -1.5f, 0.8f);
		glVertex3f(-0.8f, -1.5f, 1.0f);

		// Top
		glVertex3f(-1.0f, -0.5f, 1.0f);
		glVertex3f(-1.0f, -0.5f, 0.8f);
		glVertex3f(-0.8f, -0.5f, 0.8f);
		glVertex3f(-0.8f, -0.5f, 1.0f);

		FGL_ColorToFloat("765827", &c);		// Medium
		glColor3f(c.r, c.g, c.b);
		// Right
		glVertex3f(-0.8f, -1.5f, 1.0f);
		glVertex3f(-0.8f, -1.5f, 0.8f);
		glVertex3f(-0.8f, -0.5f, 0.8f);
		glVertex3f(-0.8f, -0.5f, 1.0f);

		// Left
		glVertex3f(-1.0f, -1.5f, 1.0f);
		glVertex3f(-1.0f, -1.5f, 0.8f);
		glVertex3f(-1.0f, -0.5f, 0.8f);
		glVertex3f(-1.0f, -0.5f, 1.0f);

		// Front
		glVertex3f(-0.8f, -1.5f, 0.8f);
		glVertex3f(-0.8f, -0.5f, 0.8f);
		glVertex3f(-1.0f, -0.5f, 0.8f);
		glVertex3f(-1.0f, -1.5f, 0.8f);

		// Back
		glVertex3f(-0.8f, -1.5f, 1.0f);
		glVertex3f(-0.8f, -0.5f, 1.0f);
		glVertex3f(-1.0f, -0.5f, 1.0f);
		glVertex3f(-1.0f, -1.5f, 1.0f);

	glEnd();


	//////////////////////////////////////////////// Right Back Leg
	glBegin(GL_QUADS);

		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		// Bottom
		glVertex3f(1.0f, -1.5f, -1.0f);
		glVertex3f(1.0f, -1.5f, -0.8f);
		glVertex3f(0.8f, -1.5f, -0.8f);
		glVertex3f(0.8f, -1.5f, -1.0f);

		// Top
		glVertex3f(1.0f, 0.2f, -1.0f);
		glVertex3f(1.0f, 0.2f, -0.8f);
		glVertex3f(0.8f, 0.2f, -0.8f);
		glVertex3f(0.8f, 0.2f, -1.0f);

		FGL_ColorToFloat("765827", &c);		// Medium
		glColor3f(c.r, c.g, c.b);
		// Right
		glVertex3f(0.8f, -1.5f, -1.0f);
		glVertex3f(0.8f, -1.5f, -0.8f);
		glVertex3f(0.8f, 0.2f, -0.8f);
		glVertex3f(0.8f, 0.2f, -1.0f);

		// Left
		glVertex3f(1.0f, -1.5f, -1.0f);
		glVertex3f(1.0f, -1.5f, -0.8f);
		glVertex3f(1.0f, 0.2f, -0.8f);
		glVertex3f(1.0f, 0.2f, -1.0f);

		// Front
		glVertex3f(0.8f, -1.5f, -0.8f);
		glVertex3f(0.8f, 0.2f, -0.8f);
		glVertex3f(1.0f, 0.2f, -0.8f);
		glVertex3f(1.0f, -1.5f, -0.8f);

		// Back
		glVertex3f(0.8f, -1.5f, -1.0f);
		glVertex3f(0.8f, 0.2f, -1.0f);
		glVertex3f(1.0f, 0.2f, -1.0f);
		glVertex3f(1.0f, -1.5f, -1.0f);

	glEnd();



	//////////////////////////////////////////// Right Front Leg
	glBegin(GL_QUADS);

		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		// Bottom
		glVertex3f(1.0f, -1.5f, 1.0f);
		glVertex3f(1.0f, -1.5f, 0.8f);
		glVertex3f(0.8f, -1.5f, 0.8f);
		glVertex3f(0.8f, -1.5f, 1.0f);

		// Top
		glVertex3f(1.0f, -0.5f, 1.0f);
		glVertex3f(1.0f, -0.5f, 0.8f);
		glVertex3f(0.8f, -0.5f, 0.8f);
		glVertex3f(0.8f, -0.5f, 1.0f);

		FGL_ColorToFloat("765827", &c);		// Medium
		glColor3f(c.r, c.g, c.b);
		// Right
		glVertex3f(0.8f, -1.5f, 1.0f);
		glVertex3f(0.8f, -1.5f, 0.8f);
		glVertex3f(0.8f, -0.5f, 0.8f);
		glVertex3f(0.8f, -0.5f, 1.0f);

		// Left
		glVertex3f(1.0f, -1.5f, 1.0f);
		glVertex3f(1.0f, -1.5f, 0.8f);
		glVertex3f(1.0f, -0.5f, 0.8f);
		glVertex3f(1.0f, -0.5f, 1.0f);

		// Front
		glVertex3f(0.8f, -1.5f, 0.8f);
		glVertex3f(0.8f, -0.5f, 0.8f);
		glVertex3f(1.0f, -0.5f, 0.8f);
		glVertex3f(1.0f, -1.5f, 0.8f);

		// Back
		glVertex3f(0.8f, -1.5f, 1.0f);
		glVertex3f(0.8f, -0.5f, 1.0f);
		glVertex3f(1.0f, -0.5f, 1.0f);
		glVertex3f(1.0f, -1.5f, 1.0f);

	glEnd();


	/////////////////////////////////////////// Sit 
	glBegin(GL_QUADS);

		FGL_ColorToFloat("A47E3B", &c);		// Light
		glColor3f(c.r, c.g, c.b);
		// Bottom
		glVertex3f(-1.2f, -0.5f, -1.2f);
		glVertex3f(1.2f, -0.5f, -1.2f);
		glVertex3f(1.2f, -0.5f, 1.2f);
		glVertex3f(-1.2f, -0.5f, 1.2f);

		// Top
		glVertex3f(-1.2f, -0.4f, -1.2f);
		glVertex3f(1.2f, -0.4f, -1.2f);
		glVertex3f(1.2f, -0.4f, 1.2f);
		glVertex3f(-1.2f, -0.4f, 1.2f);

		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		// Right
		glVertex3f(1.2f, -0.4f, -1.2f);
		glVertex3f(1.2f, -0.4f, 1.2f);
		glVertex3f(1.2f, -0.5f, 1.2f);
		glVertex3f(1.2f, -0.5f, -1.2f);

		// Left
		glVertex3f(-1.2f, -0.4f, -1.2f);
		glVertex3f(-1.2f, -0.4f, 1.2f);
		glVertex3f(-1.2f, -0.5f, 1.2f);
		glVertex3f(-1.2f, -0.5f, -1.2f);

		// Front
		glVertex3f(1.2f, -0.4f, 1.2f);
		glVertex3f(-1.2f, -0.4f, 1.2f);
		glVertex3f(-1.2f, -0.5f, 1.2f);
		glVertex3f(1.2f, -0.5f, 1.2f);

		// Back
		glVertex3f(1.2f, -0.4f, -1.2f);
		glVertex3f(-1.2f, -0.4f, -1.2f);
		glVertex3f(-1.2f, -0.5f, -1.2f);
		glVertex3f(1.2f, -0.5f, -1.2f);

	glEnd();


	/////////////////////////////////////////////////////////// Back
	glBegin(GL_QUADS);

		FGL_ColorToFloat("65451F", &c);		// Dark
		glColor3f(c.r, c.g, c.b);
		// Bottom
		glVertex3f(-1.2f, 0.2f, -1.0f);
		glVertex3f(1.2f, 0.2f, -1.0f);
		glVertex3f(1.2f, 0.2f, -0.8f);
		glVertex3f(-1.2f, 0.2f, -0.8f);

		// Top
		glVertex3f(-1.2f, 1.5f, -1.0f);
		glVertex3f(1.2f, 1.5f, -1.0f);
		glVertex3f(1.2f, 1.5f, -0.8f);
		glVertex3f(-1.2f, 1.5f, -0.8f);

		// Right
		glVertex3f(1.2f, 1.5f, -1.0f);
		glVertex3f(1.2f, 0.2f, -1.0f);
		glVertex3f(1.2f, 0.2f, -0.8f);
		glVertex3f(1.2f, 1.5f, -0.8f);

		// Left
		glVertex3f(-1.2f, 1.5f, -1.0f);
		glVertex3f(-1.2f, 0.2f, -1.0f);
		glVertex3f(-1.2f, 0.2f, -0.8f);
		glVertex3f(-1.2f, 1.5f, -0.8f);

		FGL_ColorToFloat("A47E3B", &c);		// Light
		glColor3f(c.r, c.g, c.b);
		// Front
		glVertex3f(-1.2f, 1.5f, -0.8f);
		glVertex3f(-1.2f, 0.2f, -0.8f);
		glVertex3f(1.2f, 0.2f, -0.8f);
		glVertex3f(1.2f, 1.5f, -0.8f);

		// Back
		glVertex3f(-1.2f, 1.5f, -1.0f);
		glVertex3f(-1.2f, 0.2f, -1.0f);
		glVertex3f(1.2f, 0.2f, -1.0f);
		glVertex3f(1.2f, 1.5f, -1.0f);

	glEnd();

	glPushMatrix();
		///////////////////////////////////////// Spot-Light
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
		FGL_PointAssign(&p, 0.0f, 0.0f, 1.5f);
		glBegin(GL_TRIANGLE_FAN);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(0.0f, 0.0f, 1.5f);

			glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
			FGL_Circle(&p, 2.5f, 0.0f, 360.0f);

		glEnd();
	glPopMatrix();
}

