#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"


extern HDC ghdc;
extern GLuint heart_texture;


void MedicineText(void)
{
	// For texture

	HFONT hFont = CreateFont(
		20,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	char str1[] = "Message Loop";

	glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
	DeleteObject(hFont);
}

void MediText(void)
{
	// For texture

	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	char str1[] = "MEDI";

	glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
	DeleteObject(hFont);
}

void Text(char*  strText, FGL_Color *c)
{
	// For texture

	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(c->r, c->g, c->b, c->a);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	/*char *str1[] = strText;*/

	glCallLists(strlen(strText), GL_UNSIGNED_BYTE, strText);
	DeleteObject(hFont);
}

void Mkk_OperationRoom(void)
{
	glBegin(GL_QUADS);
		glColor4f(0.0f, 0.0f, 1.0f,1.0f);
			glVertex3f(-1.0f, -0.40f, 0.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
			glVertex3f(1.0f, -0.40f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
			glVertex3f(-1.0f, -0.20f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -0.20f, 0.0f);
	glEnd();

}

void ReportMeasure(void)
{
	// Font Rendering
	


	
	glBegin(GL_QUADS); // Report display stand
		glColor4f(0.670f, 0.670f, 0.670f, 1.0f);
		glVertex3f(-0.45f, 0.0f, 0.0f);
		glVertex3f(-0.50f, 0.0f, 0.0f);
		glVertex3f(-0.50f, -0.580f, 0.0f);
		glVertex3f(-0.45f, -0.580f, 0.0f);		
	glEnd();


	glBegin(GL_QUADS); // stand base
		//glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
			glVertex3f(-0.520f, -0.58f, 0.0f);
			glVertex3f(-0.520f, -0.60f, 0.0f);
			glVertex3f(-0.43f, -0.60f, 0.0f);
			glVertex3f(-0.43f, -0.58f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Display outer
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(-0.33f, 0.25f, 0.0f);
			glVertex3f(-0.62f, 0.25f, 0.0f);
			glVertex3f(-0.62f, -0.02f, 0.0f);
			glVertex3f(-0.33f, -0.02f, 0.0f);

	glEnd();

	glBegin(GL_QUADS); // Display outer
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
			glVertex3f(-0.35f, 0.23f, 0.0f);
			glVertex3f(-0.60f, 0.23f, 0.0f);
			glVertex3f(-0.60f, 0.0f, 0.0f);
			glVertex3f(-0.35f, 0.0f, 0.0f);
	glEnd();
	
	glLineWidth(1.0f);
	glBegin(GL_LINE_STRIP); // Line
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glVertex3f(-0.60f,0.12f,0.0f);
		glVertex3f(-0.55f,0.12f,0.0f);
		glVertex3f(-0.52f,0.03f,0.0f);
		glVertex3f(-0.45f,0.21f,0.0f);
		glVertex3f(-0.40f,0.12f,0.0f);
		glVertex3f(-0.35f,0.12f,0.0f);

	glEnd();

	
}

void Bed(void)
{
	glBegin(GL_QUADS);
		glColor4f(0.49f, 0.49f, 0.49f,1.0f); // Big Rectngle
			glVertex3f(-0.44f, -0.26f, 0.0f);
			glVertex3f(-0.44f, -0.34f, 0.0f);
			glVertex3f(0.42f, -0.34f, 0.0f);
			glVertex3f(0.42f, -0.26f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //Bed left side
		glColor4f(1.0f,1.0f,1.0f,1.0f);
			glVertex3f(-0.44f,-0.26f,0.0f);
			glVertex3f(-0.46f,-0.26f,0.0f);
			glVertex3f(-0.46f,-0.58f,0.0f);
			glVertex3f(-0.44f,-0.58f,0.0f);
	glEnd();

	glBegin(GL_QUADS); //Bed left side
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(0.42f, -0.26f, 0.0f);
			glVertex3f(0.44f, -0.26f, 0.0f);
			glVertex3f(0.44f, -0.58f, 0.0f);
			glVertex3f(0.42f, -0.58f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Top Corner
	glColor4f(1.0f,1.0f,1.0f,1.0f);
		glVertex3f(-0.46f,-0.26f,0.0f);
		glVertex3f(-0.46f,-0.24f,0.0f);
		glVertex3f(0.44f,-0.24f,0.0f);
		glVertex3f(0.44f,-0.26f,0.0f);
	glEnd();

	// Bed shidi
	glBegin(GL_QUADS);
		glColor4f(1.0f,1.0f,1.0f,1.0f);
			glVertex3f(-0.24f,-0.3350f,0.0f);
			glVertex3f(-0.24f,-0.35f,0.0f);
			glVertex3f(0.26f,-0.35f,0.0f);
			glVertex3f(0.26f,-0.3350f,0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(-0.24f, -0.3850f, 0.0f);
			glVertex3f(-0.24f, -0.40f, 0.0f);
			glVertex3f(0.26f, -0.40f, 0.0f);
			glVertex3f(0.26f, -0.3850f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(-0.24f, -0.4350f, 0.0f);
			glVertex3f(-0.24f, -0.45f, 0.0f);
			glVertex3f(0.26f, -0.45f, 0.0f);
			glVertex3f(0.26f, -0.4350f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Shidi left side
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glVertex3f(-0.24f, -0.24f, 0.0f);
			glVertex3f(-0.26f, -0.24f, 0.0f);
			glVertex3f(-0.26f, -0.45f, 0.0f);
			glVertex3f(-0.24f, -0.45f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Shidi right side
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glVertex3f(0.26f, -0.24f, 0.0f);
		glVertex3f(0.28f, -0.24f, 0.0f);
		glVertex3f(0.28f, -0.45f, 0.0f);
		glVertex3f(0.26f, -0.45f, 0.0f);
	glEnd();

}


void BoyFace(void)
{
	FGL_Point p;

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.0f, 0.0f, 0.0f);
	FGL_Ellipse(&p, 0.32f, 0.18f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Hairs
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, .0f, 1.0f);
	glVertex3f(-0.13f, 0.17f, 0.0f);
	glVertex3f(-0.13f, -0.17f, 0.0f);
	glVertex3f(-0.24f, -0.17f, 0.0f);
	glVertex3f(-0.24f, 0.17f, 0.0f);

	glEnd();

	//small fron curve
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0, 0.0f, 1.0f);
	FGL_3Points pt1;
	FGL_PointAssign(&pt1.p0, -0.13f, 0.17f, 0.0f);
	FGL_PointAssign(&pt1.p1, -0.19f, 0.25f, 0.0f);
	FGL_PointAssign(&pt1.p2, -0.24f, 0.17f, 0.0f);//H
	FGL_BezierCurve3(&pt1);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0, 0.0f, 1.0f);
	FGL_3Points pt2;
	FGL_PointAssign(&pt2.p0, -0.24f, 0.17f, 0.0f);
	FGL_PointAssign(&pt2.p1, -0.47f, 0.0f, 0.0f);
	FGL_PointAssign(&pt2.p2, -0.24f, -0.17f, 0.0f);//H
	FGL_BezierCurve3(&pt2);
	glEnd();

	//Ear
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.68f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, -0.02f, -0.17f, 0.0f);
	FGL_Circle(&p, 0.03f, 0.0f, 360.0f);
	glEnd();

	//triangle
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, .0f, 1.0f);
	glVertex3f(-0.13f, -0.17f, 0.0f);
	glVertex3f(-0.13f, -0.11f, 0.0f);
	glVertex3f(-0.0f, -0.17f, 0.0f);
	glEnd();


	//Eyebrow
	glLineWidth(1.5f);
	glBegin(GL_LINE_STRIP);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	FGL_3Points pt3;
	FGL_PointAssign(&pt3.p0, -0.05f, 0.18f, 0.0f);
	FGL_PointAssign(&pt3.p1, -0.07f, 0.14f, 0.0f);
	FGL_PointAssign(&pt3.p2, -0.05f, 0.09f, 0.0f);//H
	FGL_BezierCurve3(&pt3);
	glEnd();

	//Eye
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_PointAssign(&p, 0.0f, 0.14f, 0.0f);
	FGL_Ellipse(&p, 0.02f, 0.03f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Black Eye
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
	FGL_PointAssign(&p, 0.0f, 0.15f, 0.0f);
	FGL_Circle(&p, 0.01f, 0.0f, 360.0f);
	glEnd();

	//nose
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	glVertex3f(0.05f, 0.15f, 0.0f);
	glVertex3f(0.095f, 0.22f, 0.0f);
	glVertex3f(0.14f, 0.15f, 0.0f);
	glEnd();

	//mouth
	glLineWidth(1.5f);
	glBegin(GL_LINE_STRIP);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_3Points pt4;
	FGL_PointAssign(&pt4.p0, 0.19f, 0.15f, 0.0f);
	FGL_PointAssign(&pt4.p1, 0.21f, 0.11f, 0.0f);
	FGL_PointAssign(&pt4.p2, 0.19f, 0.07f, 0.0f);//H
	FGL_BezierCurve3(&pt4);
	glEnd();

	//neck
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color

	glVertex3f(0.33f, -0.02f, 0.0f);
	glVertex3f(0.5f, -0.06f, 0.0f);
	glVertex3f(0.4f, -0.15f, 0.0f);
	glVertex3f(0.05f, -0.0f, 0.0f);
	glEnd();

}

void SleepBoy(void)
{
	FGL_Point p;

	//face
	glScalef(0.5f, 0.5f, 0.0f);
	BoyFace();

	//face

	//glRotatef(-20.0f, 0.0f, 1.0f, 0.0f);
	/*glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.0f, 0.0f, 0.0f);
	FGL_Ellipse(&p, 0.32f, 0.18f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Hairs
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, .0f, 1.0f);
	glVertex3f(-0.13f, 0.17f, 0.0f);
	glVertex3f(-0.13f, -0.17f, 0.0f);
	glVertex3f(-0.24f, -0.17f, 0.0f);
	glVertex3f(-0.24f, 0.17f, 0.0f);

	glEnd();

	//small fron curve
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0, 0.0f, 1.0f);
	FGL_3Points pt1;
	FGL_PointAssign(&pt1.p0, -0.13f, 0.17f, 0.0f);
	FGL_PointAssign(&pt1.p1, -0.19f, 0.25f, 0.0f);
	FGL_PointAssign(&pt1.p2, -0.24f, 0.17f, 0.0f);//H
	FGL_BezierCurve3(&pt1);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0, 0.0f, 1.0f);
	FGL_3Points pt2;
	FGL_PointAssign(&pt2.p0, -0.24f, 0.17f, 0.0f);
	FGL_PointAssign(&pt2.p1, -0.47f, 0.0f, 0.0f);
	FGL_PointAssign(&pt2.p2, -0.24f, -0.17f, 0.0f);//H
	FGL_BezierCurve3(&pt2);
	glEnd();

	//Ear
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.68f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, -0.02f, -0.17f, 0.0f);
	FGL_Circle(&p, 0.03f, 0.0f, 360.0f);
	glEnd();

	//triangle
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, .0f, 1.0f);
	glVertex3f(-0.13f, -0.17f, 0.0f);
	glVertex3f(-0.13f, -0.11f, 0.0f);
	glVertex3f(-0.0f, -0.17f, 0.0f);
	glEnd();


	//Eyebrow
	glLineWidth(1.5f);
	glBegin(GL_LINE_STRIP);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	FGL_3Points pt3;
	FGL_PointAssign(&pt3.p0, -0.05f, 0.18f, 0.0f);
	FGL_PointAssign(&pt3.p1, -0.07f, 0.14f, 0.0f);
	FGL_PointAssign(&pt3.p2, -0.05f, 0.09f, 0.0f);//H
	FGL_BezierCurve3(&pt3);
	glEnd();

	//Eye
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_PointAssign(&p, 0.0f, 0.14f, 0.0f);
	FGL_Ellipse(&p, 0.02f, 0.03f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Black Eye
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
	FGL_PointAssign(&p, 0.0f, 0.15f, 0.0f);
	FGL_Circle(&p, 0.01f, 0.0f, 360.0f);
	glEnd();

	//nose
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	glVertex3f(0.05f, 0.15f, 0.0f);
	glVertex3f(0.095f, 0.22f, 0.0f);
	glVertex3f(0.14f, 0.15f, 0.0f);
	glEnd();

	//mouth
	glLineWidth(1.5f);
	glBegin(GL_LINE_STRIP);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_3Points pt4;
	FGL_PointAssign(&pt4.p0, 0.19f, 0.15f, 0.0f);
	FGL_PointAssign(&pt4.p1, 0.21f, 0.11f, 0.0f);
	FGL_PointAssign(&pt4.p2, 0.19f, 0.07f, 0.0f);//H
	FGL_BezierCurve3(&pt4);
	glEnd();

	//neck
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color

	glVertex3f(0.33f, -0.02f, 0.0f);
	glVertex3f(0.5f, -0.06f, 0.0f);
	glVertex3f(0.4f, -0.15f, 0.0f);
	glVertex3f(0.05f, -0.0f, 0.0f);
	glEnd();*/



	//Blanket

	/*glBegin(GL_POLYGON);
	glColor4f(0.43f, 0.81f, 0.93f, 1.0f);
	glVertex3f(0.05f, -0.2f, 0.0f);
	glVertex3f(0.54f, -0.015f, 0.0f);
	glVertex3f(1.6f, -0.06f, 0.0f);
	glVertex3f(1.3f, -0.34f, 0.0f);
	glEnd();*/

	glBegin(GL_POLYGON);
	glColor4f(0.43f, 0.81f, 0.93f, 1.0f);
	glVertex3f(0.34f, -0.4f, 0.0f);
	glVertex3f(0.34f, 0.05f, 0.0f);
	glVertex3f(2.4f, 0.05f, 0.0f);
	glVertex3f(2.4f, -0.4f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	FGL_4Points pa;
	glColor4f(0.43f, 0.81f, 0.93f, 1.0f);
	FGL_PointAssign(&pa.p0, 0.34f, 0.05f, 0.0f);
	FGL_PointAssign(&pa.p1, 1.0f, 0.45f, 0.0f);
	FGL_PointAssign(&pa.p2, 1.5f, 0.35f, 0.0f);
	FGL_PointAssign(&pa.p3, 2.4f, 0.05f, 0.0f);
	FGL_BezierCurve4(&pa);
	glEnd();
}

//Watch
void clock(void)
{
	// code
	//Outer Box
	glBegin(GL_QUADS);
	glColor3f(0.8f, 0.4f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glEnd();

	//Inner 
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, 0.4f, 0.0f);
	glVertex3f(0.4f, -0.4f, 0.0f);
	glVertex3f(-0.4f, -0.4f, 0.0f);
	glEnd();


	//12 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.39f, 0.0f);
	glVertex3f(0.01f, 0.33f, 0.0f);
	glVertex3f(-0.01f, 0.33f, 0.0f);
	glEnd();

	//3 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.33f, 0.01f, 0.0f);
	glVertex3f(0.39f, 0.01f, 0.0f);
	glVertex3f(0.39f, -0.01f, 0.0f);
	glVertex3f(0.33f, -0.01f, 0.0f);
	glEnd();



	//6 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.33f, 0.0f);
	glVertex3f(0.01f, -0.39f, 0.0f);
	glVertex3f(-0.01f, -0.39f, 0.0f);
	glEnd();

	//9 o'clock
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.39f, 0.01f, 0.0f);
	glVertex3f(-0.33f, 0.01f, 0.0f);
	glVertex3f(-0.33f, -0.01f, 0.0f);
	glVertex3f(-0.39f, -0.01f, 0.0f);



	//glVertex3f(-0.39f, 0.0f, 0.0f);
	//glVertex3f(-0.35f, 0.0f, 0.0f);
	glEnd();

	//Minute kata
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.01f, 0.0f);
	glVertex3f(0.32f, 0.0f, 0.0f);
	glVertex3f(-0.05f, -0.01f, 0.0f);
	glEnd();

	//Hr kata
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.01f, 0.0f);
	glVertex3f(-0.15f, -0.25f, 0.0f);
	glVertex3f(-0.05f, -0.01f, 0.0f);
	glEnd();


}

void Heart(void)
{
	//Variable declarations
	FGL_3Points curve1, curve2;
	FGL_Color c = {1.0f, 0.0f, 0.0f, 1.0f};
	
	//Draw Heart

	//left side
	FGL_PointAssign(&(curve1.p0), -0.2f, 0.1f, 0.0f);
	FGL_PointAssign(&(curve1.p1), -0.7f, -0.2f, 0.0f);
	FGL_PointAssign(&(curve1.p2), 0.1f, -0.4f, 0.0f);

	FGL_PointAssign(&(curve2.p0), -0.2f, 0.1f, 0.0f);
	FGL_PointAssign(&(curve2.p1), -0.07f, -0.1f, 0.0f);
	FGL_PointAssign(&(curve2.p2), 0.1f, -0.4f, 0.0f);

	FGL_BezierShape33(&curve1, &curve2, &c);

	//Right Upper
	FGL_ColorAssign(&c, 1.0f, 1.0f, 1.0f, 1.0f);

	FGL_PointAssign(&(curve1.p0), -0.10f, -0.07f, 0.0f);
	FGL_PointAssign(&(curve1.p1), -0.10f, 0.35f, 0.0f);
	FGL_PointAssign(&(curve1.p2), 0.15f, -0.1f, 0.0f);

	FGL_PointAssign(&(curve2.p0), 0.15f, -0.1f, 0.0f);
	FGL_PointAssign(&(curve2.p1), 0.15f, 0.3f, 0.0f);
	FGL_PointAssign(&(curve2.p2), -0.05f, -0.07f, 0.0f);


	FGL_BezierShape33(&curve1, &curve2, &c);

	FGL_4Points f1,f2; //Right side

	FGL_ColorAssign(&c, 1.0f, 0.0f, 0.0f, 1.0f);


	FGL_PointAssign(&(f1.p0), -0.2f, 0.1f, 0.0f);
	FGL_PointAssign(&(f1.p1), -0.05f, -0.07f, 0.0f);
	FGL_PointAssign(&(f1.p2), 0.05f, 0.1f, 0.0f);
	FGL_PointAssign(&(f1.p3), 0.2f, -0.1f, 0.0f);

	FGL_PointAssign(&(f2.p0), 0.1f, -0.4f, 0.0f);
	FGL_PointAssign(&(f2.p1), 0.12f, -0.25f, 0.0f);
	FGL_PointAssign(&(f2.p2), 0.25f, -0.25f, 0.0f);
	FGL_PointAssign(&(f2.p3), 0.2f, -0.1f, 0.0f);

	FGL_BezierShape44(&f1, &f2, &c);

}

void Operation_theatre(void)
{
	
	// partition

	glBegin(GL_LINES); // right handle
	glLineWidth(5);
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.92f, 0.99f);
	glVertex2f(0.71f, 0.99f);
	glVertex2f(0.92f, 0.99f);
	glVertex2f(0.95f, 0.95f);
	glVertex2f(0.71f, 0.99f);
	glVertex2f(0.68f, 0.95f);
	glEnd();

	glBegin(GL_POLYGON); // right
	glColor3f(0.7f, 1.0f, 0.9f);
	glVertex2f(0.95f, 0.95f);
	glVertex2f(0.68f, 0.95f);
	glVertex2f(0.68f, 0.25f);
	glVertex2f(0.95f, 0.25f);
	glEnd();

	glBegin(GL_LINES); // mid handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.63f, 0.99f);
	glVertex2f(0.42f, 0.99f);
	glVertex2f(0.63f, 0.99f);
	glVertex2f(0.66f, 0.95f);
	glVertex2f(0.42f, 0.99f);
	glVertex2f(0.39f, 0.95f);
	glEnd();

	glBegin(GL_POLYGON); // mid
	glColor3f(0.7f, 1.0f, 0.9f);
	glVertex2f(0.66f, 0.95f);
	glVertex2f(0.39f, 0.95f);
	glVertex2f(0.39f, 0.25f);
	glVertex2f(0.66f, 0.25f);
	glEnd();

	glBegin(GL_POLYGON); // left
	glColor3f(0.7f, 1.0f, 0.9f);
	glVertex2f(0.37f, 0.95f);
	glVertex2f(0.1f, 0.95f);
	glVertex2f(0.1f, 0.25f);
	glVertex2f(0.37f, 0.25f);
	glEnd();

	glBegin(GL_LINES); // left handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.13f, 0.99f);
	glVertex2f(0.34f, 0.99f);
	glVertex2f(0.34f, 0.99f);
	glVertex2f(0.37f, 0.95f);
	glVertex2f(0.13f, 0.99f);
	glVertex2f(0.1f, 0.95f);
	glEnd();

	glBegin(GL_POLYGON); // upper handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.98f, 0.95f);
	glVertex2f(0.057f, 0.95f);
	glVertex2f(0.057f, 0.93f);
	glVertex2f(0.98f, 0.93f);
	glEnd();

	// right stand
	glBegin(GL_POLYGON); // right handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.95f, 0.25f);
	glVertex2f(0.93f, 0.25f);
	glVertex2f(0.93f, 0.14f);
	glVertex2f(0.95f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // left handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.7f, 0.25f);
	glVertex2f(0.68f, 0.25f);
	glVertex2f(0.68f, 0.14f);
	glVertex2f(0.7f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // mid
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.95f, 0.2f);
	glVertex2f(0.7f, 0.2f);
	glVertex2f(0.7f, 0.18f);
	glVertex2f(0.95f, 0.18f);
	glEnd();

	// mid stand
	glBegin(GL_POLYGON); // right handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.66f, 0.25f);
	glVertex2f(0.64f, 0.25f);
	glVertex2f(0.64f, 0.14f);
	glVertex2f(0.66f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // left handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.41f, 0.25f);
	glVertex2f(0.39f, 0.25f);
	glVertex2f(0.39f, 0.14f);
	glVertex2f(0.41f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // mid
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.66f, 0.2f);
	glVertex2f(0.39f, 0.2f);
	glVertex2f(0.39f, 0.18f);
	glVertex2f(0.66f, 0.18f);
	glEnd();

	// left stand
	glBegin(GL_POLYGON); // right handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.37f, 0.25f);
	glVertex2f(0.35f, 0.25f);
	glVertex2f(0.35f, 0.14f);
	glVertex2f(0.37f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // left handle
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.12f, 0.25f);
	glVertex2f(0.1f, 0.25f);
	glVertex2f(0.1f, 0.14f);
	glVertex2f(0.12f, 0.14f);
	glEnd();

	glBegin(GL_POLYGON); // mid
	glColor3f(0.8f, 0.9f, 1.0f);
	glVertex2f(0.37f, 0.2f);
	glVertex2f(0.1f, 0.2f);
	glVertex2f(0.1f, 0.18f);
	glVertex2f(0.37f, 0.18f);
	glEnd();

	/*glBegin(GL_POLYGON); // right wheel
	glColor3f(0.5f, 1.0f, 0.9f);
	glVertex2f(0.37f, 0.14f);
	glVertex2f(0.35f, 0.14f);

	glVertex2f(0.35f, 0.12f);
	glVertex2f(0.37f, 0.12f);
	glEnd();*/

}

void drawClinic(void)
{
	//FGL_Point p[3];
	FGL_3Points p;

	// Floor
	glBegin(GL_POLYGON);
	glColor3f(0.51f, 0.64f, 0.77f);
	glVertex3f(2.0f, -0.20f, 0.0f);
	glVertex3f(-2.0f, -0.20f, 0.0f);
	glVertex3f(-2.0f, -1.0f, 0.0f);
	glVertex3f(2.0f, -1.0f, 0.0f);
	glEnd();

	// Horizontal Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(2.0f, -0.2f, 0.0f);
	glVertex3f(-2.0f, -0.2f, 0.0f);
	glVertex3f(-2.0f, -0.22f, 0.0f);
	glVertex3f(2.0f, -0.22f, 0.0f);
	glEnd();

	// Horizontal Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(2.0f, -0.4f, 0.0f);
	glVertex3f(-2.0f, -0.4f, 0.0f);
	glVertex3f(-2.0f, -0.42f, 0.0f);
	glVertex3f(2.0f, -0.42f, 0.0f);
	glEnd();

	// Horizontal Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(2.0f, -0.6f, 0.0f);
	glVertex3f(-2.0f, -0.6f, 0.0f);
	glVertex3f(-2.0f, -0.62f, 0.0f);
	glVertex3f(2.0f, -0.62f, 0.0f);
	glEnd();

	// Horizontal Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(2.0f, -0.8f, 0.0f);
	glVertex3f(-2.0f, -0.8f, 0.0f);
	glVertex3f(-2.0f, -0.82f, 0.0f);
	glVertex3f(2.0f, -0.82f, 0.0f);
	glEnd();


	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-1.62f, -0.0f, 0.0f);
	glVertex3f(-1.6f, -0.0f, 0.0f);
	glVertex3f(-1.90f, -1.0f, 0.0f);
	glVertex3f(-1.92f, -1.0f, 0.0f);
	glEnd();

	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-1.32f, -0.0f, 0.0f);
	glVertex3f(-1.3f, -0.0f, 0.0f);
	glVertex3f(-1.60f, -1.0f, 0.0f);
	glVertex3f(-1.62f, -1.0f, 0.0f);
	glEnd();

	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-0.92f, -0.0f, 0.0f);
	glVertex3f(-0.9f, -0.0f, 0.0f);
	glVertex3f(-1.20f, -1.0f, 0.0f);
	glVertex3f(-1.22f, -1.0f, 0.0f);
	glEnd();



	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-0.52f, -0.0f, 0.0f);
	glVertex3f(-0.5f, -0.0f, 0.0f);
	glVertex3f(-0.72f, -1.0f, 0.0f);
	glVertex3f(-0.74f, -1.0f, 0.0f);
	glEnd();


	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-0.12f, -0.0f, 0.0f);
	glVertex3f(-0.1f, -0.0f, 0.0f);
	glVertex3f(-0.22f, -1.0f, 0.0f);
	glVertex3f(-0.24f, -1.0f, 0.0f);
	glEnd();


	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(0.32f, -0.0f, 0.0f);
	glVertex3f(0.3f, -0.0f, 0.0f);
	glVertex3f(0.22f, -1.0f, 0.0f);
	glVertex3f(0.24f, -1.0f, 0.0f);
	glEnd();


	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(0.72f, -0.0f, 0.0f);
	glVertex3f(0.7f, -0.0f, 0.0f);
	glVertex3f(0.62f, -1.0f, 0.0f);
	glVertex3f(0.64f, -1.0f, 0.0f);
	glEnd();


	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(1.12f, -0.0f, 0.0f);
	glVertex3f(1.1f, -0.0f, 0.0f);
	glVertex3f(1.04f, -1.0f, 0.0f);
	glVertex3f(1.06f, -1.0f, 0.0f);
	glEnd();

	// Vertical Floor Line 
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(1.52f, -0.0f, 0.0f);
	glVertex3f(1.5f, -0.0f, 0.0f);
	glVertex3f(1.44f, -1.0f, 0.0f);
	glVertex3f(1.46f, -1.0f, 0.0f);
	glEnd();


	// back Wall  
	glBegin(GL_POLYGON);
	glColor3f(0.85f, 0.93f, 0.95f);
	glVertex3f(2.0f, 1.0f, 0.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);
	glVertex3f(-2.0f, 0.0f, 0.0f);
	glVertex3f(2.0f, 0.0f, 0.0f);
	glEnd();


	// back Wall  
	glBegin(GL_POLYGON);
	glColor3f(0.43f, 0.54f, 0.63f);
	glVertex3f(-1.0f, 0.6f, 0.0f);
	glVertex3f(-1.4f, 0.6f, 0.0f);
	glVertex3f(-1.4f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glEnd();

	// Bulbs
	// Center Bulb
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.4f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glEnd();


	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glVertex3f(0.0f, 0.68f, 0.0f);
	glEnd();

	// Trapezon
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.03f, 0.69f, 0.0f);
	glVertex3f(-0.03f, 0.69f, 0.0f);
	glVertex3f(-0.08f, 0.59f, 0.0f);
	glVertex3f(0.08f, 0.59f, 0.0f);
	glEnd();

	// Right Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, 0.03f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, 0.09f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, 0.08f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();

	// Left Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, -0.03f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, -0.09f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, -0.08f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();

	// Left Bulb
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.5f, 1.4f, 0.0f);
	glVertex3f(-0.5f, 0.7f, 0.0f);
	glEnd();

	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.5f, 0.7f, 0.0f);
	glVertex3f(-0.5f, 0.68f, 0.0f);
	glEnd();

	// Trapezon
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.47f, 0.69f, 0.0f);
	glVertex3f(-0.53f, 0.69f, 0.0f);
	glVertex3f(-0.58f, 0.59f, 0.0f);
	glVertex3f(-0.42f, 0.59f, 0.0f);
	glEnd();

	// Right Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, -0.47f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, -0.41f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, -0.42f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();

	// Left Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, -0.53f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, -0.59f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, -0.58f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();


	// Right Bulb
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, 1.4f, 0.0f);
	glVertex3f(0.5f, 0.7f, 0.0f);
	glEnd();

	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.7f, 0.0f);
	glVertex3f(0.5f, 0.68f, 0.0f);
	glEnd();


	// Trapezon
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.47f, 0.69f, 0.0f);
	glVertex3f(0.53f, 0.69f, 0.0f);
	glVertex3f(0.58f, 0.59f, 0.0f);
	glVertex3f(0.42f, 0.59f, 0.0f);
	glEnd();

	// Right Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, 0.47f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, 0.41f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, 0.42f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();

	// Left Curve
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p.p0, 0.53f, 0.69f, 0.0f);
	FGL_PointAssign(&p.p1, 0.59f, 0.64f, 0.0f);
	FGL_PointAssign(&p.p2, 0.58f, 0.59f, 0.0f);
	FGL_BezierCurve3(&p);
	glEnd();


}

void Door(void)
{
	glBegin(GL_POLYGON); // Door chaukat
	glColor3f(0.6f, 0.9f, 0.9f);
	glVertex2f(-0.05f, 1.0f);
	glVertex2f(-1.0f, 1.0f);
	glVertex2f(-1.0f, 0.1f);
	glVertex2f(-0.05f, 0.1f);
	glEnd();

	glColor3f(0.8f, 1.0f, 0.9f);

	glBegin(GL_POLYGON); // Right door
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-0.1f, 0.95f);
	glVertex2f(-0.52f, 0.95f);
	glVertex2f(-0.52f, 0.1f);
	glVertex2f(-0.1f, 0.1f);
	glEnd();

	glBegin(GL_POLYGON); // Left door
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-0.53f, 0.95f);
	glVertex2f(-0.95f, 0.95f);
	glVertex2f(-0.95f, 0.1f);
	glVertex2f(-0.53f, 0.1f);
	glEnd();

	glBegin(GL_POLYGON); // Right glass
	glColor3f(0.5f, 1.0f, 1.0f);
	glVertex2f(-0.2f, 0.9f);
	glVertex2f(-0.45f, 0.9f);
	glVertex2f(-0.45f, 0.45f);
	glVertex2f(-0.2f, 0.45f);
	glEnd();

	glBegin(GL_POLYGON); // Left glass
	glColor3f(0.5f, 1.0f, 1.0f);
	glVertex2f(-0.6f, 0.9f);
	glVertex2f(-0.85f, 0.9f);
	glVertex2f(-0.85f, 0.45f);
	glVertex2f(-0.6f, 0.45f);
	glEnd();

	glBegin(GL_POLYGON); // Door left handle
	glColor3f(0.7f, 0.8f, 0.9f);
	glVertex2f(-0.55f, 0.67f);
	glVertex2f(-0.58f, 0.67f);
	glVertex2f(-0.58f, 0.4f);
	glVertex2f(-0.55f, 0.4f);
	glEnd();

	glBegin(GL_POLYGON); // Door right handle
	glColor3f(0.7f, 0.8f, 0.9f);
	glVertex2f(-0.47f, 0.67f);
	glVertex2f(-0.5f, 0.67f);
	glVertex2f(-0.5f, 0.4f);
	glVertex2f(-0.47f, 0.4f);
	glEnd();
}

void Clinic3D(void)
{

		
	glBegin(GL_QUADS);
			// Front Wall
		glColor3f(0.85f, 0.93f, 0.95f);
			glVertex3f(1.60f, 1.30f, -0.6f);
			glVertex3f(-1.60f, 1.30f, -0.6f);
			glVertex3f(-1.60f, -0.50f, -0.6f);
			glVertex3f(1.60f, -0.50f, -0.6f);

		// right side Wall
		glColor3f(0.80f, 0.88f, 0.90f);
			glVertex3f(2.0f, 1.30f, 0.0f);
			glVertex3f(1.60f, 1.30f, -0.6f);
			glVertex3f(1.60f, -0.50f, -0.6f);
			glVertex3f(2.0f, -1.0f, 0.0f);


		//Left side wall
			glColor3f(0.80f, 0.88f, 0.90f);
				glVertex3f(-2.0f, 1.30f, 0.0f);
				glVertex3f(-1.60f, 1.30f, -0.6f);
				glVertex3f(-1.60f, -0.50f, -0.6f);
				glVertex3f(-2.0f, -1.0f, 0.0f);


		// Floor
		glColor3f(0.71f, 0.84f, 0.97f);
			glVertex3f(1.60f, -0.50f, -0.6f);
			glVertex3f(-1.60f, -0.50f, -0.6f);
			glVertex3f(-2.0f, -1.0f, 0.0f);
			glVertex3f(2.0f, -1.0f, 0.0f);
	glEnd();



}

void Thoughtcloud(void)
{
	// variable declarations
	FGL_Point center;
	//FGL_Color myColor;

	// code
	FGL_PointAssign(&center, 0.0f, 0.0f, 0.0f);

	// FGL_ColorToFloat("fefbe6", &myColor);

	glColor4f(1.4f, 1.4f, 1.4f, 1.4f);

	glBegin(GL_TRIANGLE_FAN);
	FGL_Circle(&center, 0.1f, 0.0f, 360.0f);
	glEnd();

	FGL_PointAssign(&center, 0.3f, 0.3f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	FGL_Circle(&center, 0.2f, 0.0f, 360.0f);
	glEnd();

	FGL_PointAssign(&center, 1.1f, 1.1f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	FGL_Circle(&center, 0.8f, 0.0f, 360.0f);
	glEnd();

	glPushMatrix();
		//////// Heart Texture
		glBindTexture(GL_TEXTURE_2D, heart_texture);
		glTranslatef(1.1f, 1.1f, 0.0f);
		glScalef(0.5f, 0.5f, 0.4f);
		glBegin(GL_QUADS);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();
}

void Doctor(void)
{
	//Face	
	FGL_Point p;

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.0f, 0.35f, 0.0f);
	FGL_Ellipse(&p, 0.19f, 0.25f, 0.0f, 360.0f);//radius,angles
	glEnd();


	//Cap
	glBegin(GL_QUADS);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(-0.14f, 0.48f, 0.0f);
	glVertex3f(0.14f, 0.48f, 0.0f);
	glVertex3f(0.14f, 0.60f, 0.0f);
	glVertex3f(-0.14f, 0.60f, 0.0f);
	glEnd();

	//left kan hair
	glBegin(GL_POLYGON);
	glVertex3f(-0.14f, 0.48f, 0.0f);
	glVertex3f(-0.24f, 0.35f, 0.0f);
	glVertex3f(-0.14f, 0.54f, 0.0f);
	glEnd();

	//right kan hairs
	glBegin(GL_POLYGON);
	glVertex3f(0.14f, 0.48f, 0.0f);
	glVertex3f(0.24f, 0.35f, 0.0f);
	glVertex3f(0.14f, 0.54f, 0.0f);
	glEnd();
	//khalch curve
	glBegin(GL_POLYGON);
	FGL_3Points pa;
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	FGL_PointAssign(&pa.p0, -0.14f, 0.60f, 0.0f);
	FGL_PointAssign(&pa.p1, 0.02f, 0.70f, 0.0f);
	FGL_PointAssign(&pa.p2, 0.14f, 0.60f, 0.0f);
	FGL_BezierCurve3(&pa);
	glEnd();

	//left curve
	glBegin(GL_POLYGON);
	FGL_3Points pz;
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	FGL_PointAssign(&pz.p0, -0.14f, 0.60f, 0.0f);
	FGL_PointAssign(&pz.p1, -0.20f, 0.50f, 0.0f);
	FGL_PointAssign(&pz.p2, -0.14f, 0.50f, 0.0f);
	//FGL_BezierCurve3(&pz);
	glEnd();

	//Eyebrow
	glLineWidth(1.8f);
	glBegin(GL_LINES);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
	glVertex3f(-0.13, 0.42f, 0.0f);
	glVertex3f(-0.03f, 0.42f, 0.0f);
	glVertex3f(0.03f, 0.42f, 0.0f);
	glVertex3f(0.14f, 0.42f, 0.0f);
	glEnd();

	//Left Eye
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, -0.07f, 0.35f, 0.0f);
	FGL_Ellipse(&p, 0.03f, 0.02f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Lahan Eye
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, -0.07f, 0.34f, 0.0f);
	FGL_Ellipse(&p, 0.024f, 0.015f, 0.0f, 360.0f);//radius,angles
	glEnd();


	//Right Eye
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.08f, 0.35f, 0.0f);
	FGL_Ellipse(&p, 0.03f, 0.02f, 0.0f, 360.0f);//radius,angles
	glEnd();
	//Lahan eye
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.08f, 0.34f, 0.0f);
	FGL_Ellipse(&p, 0.024f, 0.015f, 0.0f, 360.0f);//radius,angles
	glEnd();




	//Left Ear
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, -0.2f, 0.37f, 0.0f);
	FGL_Ellipse(&p, 0.03f, 0.06f, 0.0f, 360.0f);//radius,angles
	glEnd();

	//Right Ear
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_PointAssign(&p, 0.2f, 0.37f, 0.0f);
	FGL_Ellipse(&p, 0.03f, 0.06f, 0.0f, 360.0f);//radius,angles
	glEnd();



	//Mouth 

	glBegin(GL_QUADS);
	glColor4f(0.0f, 0.0f, 0.9f, 1.0f);
	glVertex3f(-0.15f, 0.32f, 0.0f);
	glVertex3f(0.15f, 0.32f, 0.0f);
	glVertex3f(0.15f, 0.18f, 0.0f);
	glVertex3f(-0.15f, 0.18f, 0.0f);
	glEnd();

	//Khalch curve
	glBegin(GL_POLYGON);
	FGL_3Points pm;
	glColor4f(0.0f, 0.0f, 0.9f, 1.0f);
	FGL_PointAssign(&pm.p0, -0.15f, 0.18f, 0.0f);
	FGL_PointAssign(&pm.p1, 0.0f, 0.12f, 0.0f);
	FGL_PointAssign(&pm.p2, 0.15f, 0.18f, 0.0f);
	FGL_BezierCurve3(&pm);
	glEnd();

	//lINES
	glBegin(GL_LINES);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	glVertex3f(-0.15f, 0.32f, 0.0f);
	glVertex3f(-0.19f, 0.45f, 0.0f);

	glVertex3f(0.15f, 0.32f, 0.0f);
	glVertex3f(0.19f, 0.45f, 0.0f);

	glEnd();

	//Right Hand

	//Kurta
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);//A

	glVertex3f(-0.1f, 0.15f, 0.0f);//B
	glVertex3f(0.1f, 0.15f, 0.0f);//C
	glVertex3f(0.3f, 0.1f, 0.0f);//D
	glVertex3f(0.3f, -0.9f, 0.0f);//E
	glVertex3f(-0.3f, -0.9f, 0.0f);//F
	glEnd();

	// Right Sholder
	//Kurta
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	glVertex3f(0.3f, 0.1f, 0.0f);//D
	glVertex3f(0.3f, -0.15f, 0.0f);//E
	glVertex3f(0.4f, -0.2f, 0.0f);//F
	glVertex3f(0.4f, 0.08f, 0.0f);//F
	glEnd();

	// Kurta Right Side Curve

	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.30f) * (1.0f - u) * (1.0f - u)) + ((0.35f) * 2.0f * (1.0f - u) * u) + ((0.30f) * u * u);	// x1	x2	x3
		float y = ((0.10f) * (1.0f - u) * (1.0f - u)) + ((-0.40f) * 2.0f * (1.0f - u) * u) + ((-0.90f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Base POLYGON
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(0.30f, 0.20f, 0.0f);		//A
	glVertex3f(0.40f, -0.20f, 0.0f);	//B
	glVertex3f(0.52f, -0.17f, 0.0f);	//C
	glVertex3f(0.40f, 0.22f, 0.0f);		//D
	glEnd();

	// Right Hand Left Side Curve Filled

	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.30f) * (1.0f - u) * (1.0f - u)) + ((0.30f) * 2.0f * (1.0f - u) * u) + ((0.40f) * u * u);	// x1	x2	x3
		float y = ((0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.13f) * 2.0f * (1.0f - u) * u) + ((-0.20f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Left Side Curve Outline

	glBegin(GL_LINE_STRIP);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.30f) * (1.0f - u) * (1.0f - u)) + ((0.30f) * 2.0f * (1.0f - u) * u) + ((0.40f) * u * u);	// x1	x2	x3
		float y = ((0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.13f) * 2.0f * (1.0f - u) * u) + ((-0.20f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Right Side Curve Filled
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.40f) * (1.0f - u) * (1.0f - u)) + ((0.48f) * 2.0f * (1.0f - u) * u) + ((0.52f) * u * u);	// x1	x2	x3
		float y = ((-0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.22f) * 2.0f * (1.0f - u) * u) + ((-0.17f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Right Side Curve Outline

	glBegin(GL_LINE_STRIP);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.40f) * (1.0f - u) * (1.0f - u)) + ((0.48f) * 2.0f * (1.0f - u) * u) + ((0.52f) * u * u);	// x1	x2	x3
		float y = ((-0.20f) * (1.0f - u) * (1.0f - u)) + ((-0.22f) * 2.0f * (1.0f - u) * u) + ((-0.17f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Bottom Curve Filled
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.40f) * (1.0f - u) * (1.0f - u)) + ((0.54f) * 2.0f * (1.0f - u) * u) + ((0.52f) * u * u);	// x1	x2	x3
		float y = ((0.22f) * (1.0f - u) * (1.0f - u)) + ((-0.10f) * 2.0f * (1.0f - u) * u) + ((-0.17f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	// Right Hand Bottom Curve Outline
	glBegin(GL_LINE_STRIP);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((0.40f) * (1.0f - u) * (1.0f - u)) + ((0.54f) * 2.0f * (1.0f - u) * u) + ((0.52f) * u * u);	// x1	x2	x3
		float y = ((0.22f) * (1.0f - u) * (1.0f - u)) + ((-0.10f) * 2.0f * (1.0f - u) * u) + ((-0.17f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();


	//Right Finger
	//Injection
	glBegin(GL_POLYGON);
	glColor4f(0.66f, 0.78f, 1.008f, 1.0f);
	glVertex3f(0.31f, 0.20f, 0.0f);
	glVertex3f(0.31f, 0.35f, 0.0f);
	glVertex3f(0.35f, 0.35f, 0.0f);
	glVertex3f(0.35f, 0.20f, 0.0f);
	glEnd();

	//injection top
	glLineWidth(0.2f);
	glBegin(GL_LINES);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	glVertex3f(0.32f, 0.35f, 0.0f);
	glVertex3f(0.32f, 0.46f, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	glVertex3f(0.32f, 0.28f, 0.0f);
	glVertex3f(0.37f, 0.28f, 0.0f);
	glVertex3f(0.37f, 0.20f, 0.0f);
	glVertex3f(0.35f, 0.20f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	FGL_3Points pM;
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	//	FGL_PointAssign(&pM.p0, 0.32f, 0.28f, 0.0f);
	FGL_PointAssign(&pM.p0, 0.3f, 0.2f, 0.0f);
	FGL_PointAssign(&pM.p1, 0.39f, 0.35f, 0.0f);
	FGL_PointAssign(&pM.p2, 0.4, 0.2f, 0.0f);
	FGL_BezierCurve3(&pM);


	glEnd();

	// Left Hand Center Base	
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(-0.30f, 0.1f, 0.0f);//A
	glVertex3f(-0.10f, -0.1f, 0.0f);//B
	glVertex3f(-0.13f, -0.43f, 0.0f);//C
	glVertex3f(-0.55f, -0.40f, 0.0f);//D
	glEnd();

	//left Hand Bottom Curve1 Filled
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((-0.55f) * (1.0f - u) * (1.0f - u)) + ((-0.30f) * 2.0f * (1.0f - u) * u) + ((-0.13f) * u * u);	// x1	x2	x3
		float y = ((-0.40f) * (1.0f - u) * (1.0f - u)) + ((-0.47f) * 2.0f * (1.0f - u) * u) + ((-0.43f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//left Hand Bottom Curve1 Outline
	glBegin(GL_LINE_STRIP);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((-0.55f) * (1.0f - u) * (1.0f - u)) + ((-0.30f) * 2.0f * (1.0f - u) * u) + ((-0.13f) * u * u);	// x1	x2	x3
		float y = ((-0.40f) * (1.0f - u) * (1.0f - u)) + ((-0.47f) * 2.0f * (1.0f - u) * u) + ((-0.43f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//left Hand Bottom Curve2
	glBegin(GL_LINE_STRIP);
	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((-0.32f) * (1.0f - u) * (1.0f - u)) + ((-0.32f) * 2.0f * (1.0f - u) * u) + ((-0.08f) * u * u);	// x1	x2	x3
		float y = ((-0.25f) * (1.0f - u) * (1.0f - u)) + ((-0.30f) * 2.0f * (1.0f - u) * u) + ((-0.33f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//left Hand SIDE Curve2
	glBegin(GL_POLYGON);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);

	for (float u = 0.0f; u <= 1.0f; u += 0.0001f)
	{
		float x = ((-0.30f) * (1.0f - u) * (1.0f - u)) + ((-0.52f) * 2.0f * (1.0f - u) * u) + ((-0.55f) * u * u);	// x1	x2	x3
		float y = ((0.1f) * (1.0f - u) * (1.0f - u)) + ((-0.20f) * 2.0f * (1.0f - u) * u) + ((-0.4f) * u * u);	// y1	y2  y3
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	//Left hand fingers
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	glVertex3f(-0.08f, -0.33f, 0.0f);
	glVertex3f(-0.0f, -0.34f, 0.0f);
	glVertex3f(-0.02f, -0.43f, 0.0f);
	glVertex3f(-0.14f, -0.43f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	FGL_3Points pK;
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_PointAssign(&pK.p0, -0.0f, -0.34f, 0.0f);
	FGL_PointAssign(&pK.p1, 0.04f, -0.40f, 0.0f);
	FGL_PointAssign(&pK.p2, -0.02f, -0.43f, 0.0f);
	FGL_BezierCurve3(&pK);
	glEnd();

	//Finger curves

	glBegin(GL_LINE_STRIP);
	FGL_3Points pO;
	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
	FGL_PointAssign(&pO.p0, -0.1f, -0.36f, 0.0f);
	FGL_PointAssign(&pO.p1, -0.04f, -0.33f, 0.0f);
	FGL_PointAssign(&pO.p2, 0.002f, -0.37f, 0.0f);
	FGL_BezierCurve3(&pO);
	glEnd();

	glBegin(GL_LINE_STRIP);
	FGL_3Points pT;
	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
	FGL_PointAssign(&pT.p0, -0.11f, -0.39f, 0.0f);
	FGL_PointAssign(&pT.p1, -0.06f, -0.37f, 0.0f);
	FGL_PointAssign(&pT.p2, -0.0015f, -0.40f, 0.0f);
	FGL_BezierCurve3(&pT);
	glEnd();

	glBegin(GL_LINE_STRIP);
	FGL_3Points pF;
	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
	FGL_PointAssign(&pF.p0, -0.13f, -0.42f, 0.0f);
	FGL_PointAssign(&pF.p1, -0.06f, -0.40f, 0.0f);
	FGL_PointAssign(&pF.p2, -0.015f, -0.43f, 0.0f);
	FGL_BezierCurve3(&pF);
	glEnd();

	//Neck
	glBegin(GL_TRIANGLES);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	glVertex3f(-0.1f, 0.15f, 0.0f);//B
	glVertex3f(0.1f, 0.15f, 0.0f);//C
	glVertex3f(0.0f, -0.05f, 0.0f);//C
	glEnd();

	//Legs
	//Left
	glBegin(GL_QUADS);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(-0.22f, -0.9f, 0.0f);
	glVertex3f(-0.06f, -0.9f, 0.0f);
	glVertex3f(-0.06f, -1.27f, 0.0f);
	glVertex3f(-0.22f, -1.27f, 0.0f);
	glEnd();
	//Right
	glBegin(GL_QUADS);
	glColor4f(0.0f, 0.6f, 0.7f, 1.0f);
	glVertex3f(0.22f, -0.9f, 0.0f);
	glVertex3f(0.06f, -0.9f, 0.0f);
	glVertex3f(0.06f, -1.27f, 0.0f);
	glVertex3f(0.22f, -1.27f, 0.0f);
	glEnd();

	//Left shoes
	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.3f, 0.1f, 1.0f);
	glVertex3f(-0.22f, -1.27f, 0.0f);
	glVertex3f(-0.06f, -1.27f, 0.0f);
	glVertex3f(-0.06f, -1.38f, 0.0f);
	glVertex3f(-0.35f, -1.38f, 0.0f);
	glVertex3f(-0.35f, -1.35f, 0.0f);
	glEnd();

	//Right Shoes
	glBegin(GL_POLYGON);
	glColor4f(0.6f, 0.3f, 0.1f, 1.0f);
	glVertex3f(0.22f, -1.27f, 0.0f);
	glVertex3f(0.35f, -1.35f, 0.0f);
	glVertex3f(0.35f, -1.38f, 0.0f);
	glVertex3f(0.06f, -1.38f, 0.0f);
	glVertex3f(0.06f, -1.27f, 0.0f);
	glEnd();

}
