#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

GLfloat SunAngle = 0.0f;
GLfloat Galaxy_Move = 0.0f;

// Solar system
GLfloat Planet_Day = 0.0f;
GLfloat Mercury_Year = 0.0f;
GLfloat Venus_Year = 0.0f;
GLfloat Earth_Year = 0.0f;
GLfloat Mars_Year = 0.0f;
GLfloat Jupiter_Year = 0.0f;
GLfloat Saturn_Year = 0.0f;
GLfloat Uranus_Year = 0.0f;
GLfloat Neptune_Year = 0.0f;


extern GLUquadric* quadric;
extern HDC ghdc;
extern GLuint logoTexture;
extern GLuint matrixTexture;
extern GLuint galaxy_texture;
extern GLuint mercury_texture;
extern GLuint venus_texture;
extern GLuint earth_texture;
extern GLuint moon_texture;
extern GLuint mars_texture;
extern GLuint jupiter_texture;
extern GLuint saturn_texture;
extern GLuint uranus_texture;
extern GLuint neptune_texture;
extern int scene;

BOOL loadGLTexture(GLuint*, TCHAR[]);

void PNV_AstrologyText(void)
{
	// For texture

	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	char str1[] = "ASTROLOGY";

	glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
	DeleteObject(hFont);
}

void AstroText(void)
{
	// For texture

	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(0.0f, 0.8f, -1.0f);
	glListBase(1000);
	char str1[] = "ASTRO";

	glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
	DeleteObject(hFont);
}

//void PNV_SolarSystem(void)
//{
//	// Variable declarations
//	FGL_Point p = { 0.0f, 0.0f, 0.0f };
//	FGL_Color c1 = { 1.0f, 1.0f, 0.0f, 1.0f };
//	FGL_Color c2 = { 1.0f, 0.5f, 0.0f, 1.0f };
//
//	// View Transformation
//	gluLookAt(0.0f, 5.0f, 0.0f, 0.0f, 0.0f, -20.0f, -0.75f, 6.0f, 0.0f);
//	glTranslatef(0.0f, 0.0f, -20.0f);
//
//	////////////////////////////////////////////////////////// Sun
//	glRotatef(SunAngle, 0.0f, 1.0f, 0.0f);
//	FGL_Sphere(&p, &c1, &c2, 0.8f);
//	
//
//	///////////////////////////////////////////////////////// Mercury
//	glPushMatrix();
//
//		// Mercury Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 2.0f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Mercury_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(2.0f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Mercury
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glColor3f(0.75f, 0.75f, 0.75f);
//		gluSphere(quadric, 0.2, 15, 15);
//
//	glPopMatrix();
//
//
//	////////////////////////////////////////////////////////////////// Venus
//	glPushMatrix();
//
//		// Venus Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 2.8f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Venus_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(2.8f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Venus
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		FGL_ColorToFloat("FFCD4B", &c1);
//		glColor3f(c1.r, c1.g, c1.b);
//		gluSphere(quadric, 0.25, 15, 15);
//
//	glPopMatrix();
//
//
//	//////////////////////////////////////////////////////////// Earth & Moon
//	glPushMatrix();
//
//		// Earth Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 4.0f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Earth_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(4.0f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Earth
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glColor3f(0.4f, 0.9f, 1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		gluSphere(quadric, 0.3, 15, 15);
//
//		// Draw thw moon
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glTranslatef(0.5f, 0.0f, 0.0f);
//		glColor3f(1.0f, 1.0f, 1.0f);
//		gluSphere(quadric, 0.1, 15, 15);
//
//	glPopMatrix();
//
//
//	///////////////////////////////////////////////////////// Mars
//	glPushMatrix();
//
//		// Mars Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 5.0f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Mars_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(5.0f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Mars
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		FGL_ColorToFloat("B2533E", &c1);
//		glColor3f(c1.r, c1.g, c1.b);
//		gluSphere(quadric, 0.28, 15, 15);
//
//	glPopMatrix();
//
//
//	////////////////////////////////////////////////// Jupiter
//	glPushMatrix();
//
//		// Jupiter Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 7.2f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Jupiter_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(7.2f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Jupiter
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glColor3f(0.61f, 0.53f, 0.45f);
//		gluSphere(quadric, 0.58, 15, 15);
//
//	glPopMatrix();
//
//
//	////////////////////////////////////////////////////// Saturn
//	glPushMatrix();
//
//		// Saturn Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 9.0f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Saturn_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(9.0f, 0.0f, 0.0f);
//
//		// Saturn Rings
//		glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
//		glLineWidth(2.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(0.39f, 0.33f, 0.29f, 1.0f);
//			FGL_PointAssign(&p, 0.0f, 0.0f, 0.0f);
//
//			FGL_Circle(&p, 0.525f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.55f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.575f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.6f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.625f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.65f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.675f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.7f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.725f, 0.0f, 360.0f);
//			FGL_Circle(&p, 0.75f, 0.0f, 360.0f);
//
//		glEnd();
//
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Saturn
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glColor3f(0.81f, 0.73f, 0.64f);
//		gluSphere(quadric, 0.42, 15, 15);
//
//	glPopMatrix();
//
//
//	////////////////////////////////////////////////////// Uranus
//	glPushMatrix();
//
//		// Uranus Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 10.5f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Uranus_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(10.5f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Uranus
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glColor3f(0.03f, 0.46f, 0.6f);
//		gluSphere(quadric, 0.45, 15, 15);
//
//	glPopMatrix();
//
//
//	///////////////////////////////////////////////////////// Neptune
//	glPushMatrix();
//
//		// Neptune Orbit
//		glLineWidth(1.0f);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glBegin(GL_LINE_STRIP);
//
//			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//			FGL_Circle(&p, 12.0f, 0.0f, 360.0f);
//
//		glEnd();
//		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//
//		glRotatef(Neptune_Year, 0.0f, 1.0f, 0.0f);
//		glTranslatef(12.0f, 0.0f, 0.0f);
//		glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);
//
//		// Draw the Neptune
//		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//		glColor3f(0.45f, 0.59f, 0.92f);
//		gluSphere(quadric, 0.4, 15, 15);
//
//	glPopMatrix();
//
//
//}

void PNV_SolarSystem(void)
{
	// Variable declarations
	FGL_Point p = { 0.0f, 0.0f, 0.0f };
	FGL_Color c1 = { 1.0f, 1.0f, 0.0f, 1.0f };
	FGL_Color c2 = { 1.0f, 0.5f, 0.0f, 1.0f };

/*	// Font Rendering
	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(-1.2f, 1.5f, 0.0f);
	glListBase(1000);
	char str[] = "ASTROLOGY";

	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
*/
	if (scene == 6) {

			glPushMatrix();

				glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
				glRotatef(Galaxy_Move, 0.0f, 1.0f, 0.0f);
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				gluQuadricTexture(quadric, GL_TRUE);
				glBindTexture(GL_TEXTURE_2D, galaxy_texture);

				gluSphere(quadric, 40.00, 50, 50);

				glBindTexture(GL_TEXTURE_2D, 0);

			glPopMatrix();
	}

	glPushMatrix();

		////////////////////////////////////////////////////////// Sun
		glPushMatrix();

			glRotatef(SunAngle, 0.0f, 1.0f, 0.0f);
			FGL_Sphere(&p, &c1, &c2, 1.0f);

		glPopMatrix();

		///////////////////////////////////////////////////////// Mercury
		glPushMatrix();

			// Mercury Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 2.0f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Mercury_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(2.0f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Mercury
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, mercury_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.2, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		////////////////////////////////////////////////////////////////// Venus
		glPushMatrix();

			// Venus Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 2.8f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Venus_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(2.8f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Venus
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, venus_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.25, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		//////////////////////////////////////////////////////////// Earth & Moon
		glPushMatrix();

			// Earth Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 4.0f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Earth_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(4.0f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Earth
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, earth_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.3, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

			// Draw thw moon
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, moon_texture);

			glTranslatef(0.5f, 0.0f, 0.0f);
			gluSphere(quadric, 0.1, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		///////////////////////////////////////////////////////// Mars
		glPushMatrix();

			// Mars Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 5.0f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Mars_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(5.0f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Mars
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, mars_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.28, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		////////////////////////////////////////////////// Jupiter
		glPushMatrix();

			// Jupiter Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 7.2f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Jupiter_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(7.2f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Jupiter
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, jupiter_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.58, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		////////////////////////////////////////////////////// Saturn
		glPushMatrix();

			// Saturn Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 9.0f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Saturn_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(9.0f, 0.0f, 0.0f);

			// Saturn Rings
			glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
			glLineWidth(1.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(0.39f, 0.33f, 0.29f, 1.0f);
			FGL_PointAssign(&p, 0.0f, 0.0f, 0.0f);

			FGL_Circle(&p, 0.525f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.55f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.575f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.6f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.625f, 0.0f, 360.0f);

			FGL_Circle(&p, 0.7f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.725f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.75f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.775f, 0.0f, 360.0f);
			FGL_Circle(&p, 0.8f, 0.0f, 360.0f);

			glEnd();

			glRotatef(-60.0f, 1.0f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Saturn
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, saturn_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.42, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		////////////////////////////////////////////////////// Uranus
		glPushMatrix();

			// Uranus Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 10.5f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Uranus_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(10.5f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Uranus
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, uranus_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.45, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();


		///////////////////////////////////////////////////////// Neptune
		glPushMatrix();

			// Neptune Orbit
			glLineWidth(1.0f);
			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			glBegin(GL_LINE_STRIP);

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			FGL_Circle(&p, 12.0f, 0.0f, 360.0f);

			glEnd();
			glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

			glRotatef(Neptune_Year, 0.0f, 1.0f, 0.0f);
			glTranslatef(12.0f, 0.0f, 0.0f);
			glRotatef(Planet_Day, 0.0f, 1.0f, 0.0f);

			// Draw the Neptune
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			gluQuadricTexture(quadric, GL_TRUE);
			glBindTexture(GL_TEXTURE_2D, neptune_texture);

			glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
			gluSphere(quadric, 0.4, 15, 15);

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();

	glPopMatrix();
}

void FirstScreenOfFont() {


	// Funtion Declarations
	void Font(char[], int, float, float, float);

	//glColor4f(1.0f, 1.0f, 1.0f, FirstScreenFontAlpha);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("ASTROMEDICOMP'S", 120, -2.2f, 0.0f, 0.0f);
}

void SecondScreenOfFont() {

	// Funtion Declarations
	void Font(char[], int, float, float, float);

	// Code

	// Font
	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("Frustum", 100, -1.35f, -0.5f, 0.0f);
	Font("Group", 100, 0.3f, -0.5f, 0.0f);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("Presents", 100, -0.8f, -1.0f, 0.0f);

	// Texture
	glBindTexture(GL_TEXTURE_2D, logoTexture);
	glTranslatef(0.0f, 0.8f, 0.0f);
	glBegin(GL_QUADS);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.1f, 0.8f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.1f, 0.8f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.1f, -0.8f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.1f, -0.8f, 0.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void LastCredits() {

	// Function Declarations
	void Font(char[], int, float, float, float);

	// Code
	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("Group Members", 50, -0.7f, 1.8f, 0.0f);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("Aditya Jhaver", 60, -0.72f, 1.5f, 0.0f);
	Font("Arif Sayyed", 60, -0.62f, 1.2f, 0.0f);
	Font("Ashish Ambade", 60, -0.82f, 0.9f, 0.0f);
	Font("Kalyani Magdum", 60, -0.88f, 0.6f, 0.0f);
	Font("Mangesh Kachare", 60, -0.93f, 0.3f, 0.0f);
	Font("Omkar Khot", 60, -0.68f, 0.0f, 0.0f);
	Font("Piyush Varma", 60, -0.75f, -0.3f, 0.0f);
	Font("Pratik Lavhale", 60, -0.76f, -0.6f, 0.0f);
	Font("Rupali Sonne", 60, -0.7f, -0.9f, 0.0f);

	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("Group Leader", 50, -0.6f, -1.4f, 0.0f);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("Devendra Rajendra Ghadge", 60, -1.4f, -1.7f, 0.0f);
}

void SpecialThanks() {
	// Function Declarations
	void Font(char[], int, float, float, float);

	// Code
	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("Special Thanks", 50, -0.7f, 1.0f, 0.0f);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("Pradnya Vijay Gokhale", 70, -1.35f, 0.6f, 0.0f);
	Font("and", 60, -0.25f, 0.32f, 0.0f);
	Font("Dr. Rama Vijay Gokhale", 70, -1.42f, 0.0f, 0.0f);
}

void IgnitedBy() {
	// Function Declarations
	void Font(char[], int, float, float, float);

	// Code
	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("Ignited By", 50, -0.5f, 1.0f, 0.0f);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	Font("Dr. Vijay Dattatray Gokhale", 80, -1.8f, 0.6f, 0.0f);
}

void TheEnd() {

	// Function Declarations
	void Font(char[], int, float, float, float);

	// Code
	glColor4f(0.0f, 0.8f, 0.0f, 1.0f);
	Font("The End", 100, -0.8f, 0.0f, 0.0f);
}

void Font(char str[], int height, float x, float y, float z) {

	// Font Rendering
	HFONT hFont = CreateFont(
		height,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(x, y, z);
	glListBase(1000);
	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
	DeleteObject(hFont);
}

