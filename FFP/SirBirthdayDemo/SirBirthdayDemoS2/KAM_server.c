#include <windows.h> //For WIN32

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>

#include "common_server.h"

GLfloat matrixTranslate = 0.0f;
GLfloat computerSceneAngle = 180.0f;
GLfloat Fan_Angle = 0.0f;
GLfloat Lines_Time = 0.0f;

GLfloat BulletX = 0.0f;
GLfloat BulletZ = 0.0f;
GLfloat BulletAlpha = 0.0f;
GLfloat redBulletY = 0.0f;
GLfloat blueBulletY = 0.0f;
GLfloat MothaBachanAlpha = 0.05f;

extern GLuint matrixTexture;
extern HDC ghdc;
extern int scene;

void ComputerText()
{
	// Font Rendering
	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(-1.0f, 1.5f, 0.0f);
	glListBase(1000);
	char str[] = "COMPUTER";

	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
}

void CompText(void)
{
	// Font Rendering
	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(-1.0f, 1.5f, 0.0f);
	glListBase(1000);
	char str[] = "COMP";

	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
}


void Computer_Scene() {

	// Function Declarations
	void Computer_Table(void);
	void ChotaBachan(void);
	void PC(void);
	void GPU(void);

	// Variable Declarations
	FGL_Point p;

/*
	// Font Rendering
	HFONT hFont = CreateFont(
		100,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"impact.ttf"
	);
	SelectObject(ghdc, hFont);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	wglUseFontBitmaps(ghdc, 0, 255, 1000);

	glRasterPos3f(-1.0f, 1.5f, 0.0f);
	glListBase(1000);
	char str[] = "COMPUTER";

	glCallLists(strlen(str), GL_UNSIGNED_BYTE, str);
*/

	// Initial Transformations
	glTranslatef(0.0f, 0.0f, -5.0f);


	/*
			glPushMatrix();

				glBindTexture(GL_TEXTURE_2D, matrixTexture);


				glScalef(10.0f, 2.01f, 10.0f);
				glTranslatef(0.0f, 0.0f, 0.0f);
				glBegin(GL_QUADS);

					glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
					// Front Face
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(1.0f, 5.0f, 1.0f);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(-1.0f, 5.0f, 1.0f);

					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, 1.0f);

					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, 1.0f);

					// Right Face
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, 5.0f, 1.0f);

					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, 1.0f);

					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, -1.0f);

					// Back Face
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, -1.0f);

					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, -1.0f);

					// Left Face
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, 5.0f, 1.0f);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(-1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, -1.0f);

					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, 1.0f);

					// Top Face
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(-1.0f, 5.0f, -1.0f);

					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(-1.0f, 5.0f, 1.0f);

					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(1.0f, 5.0f, 1.0f);

					// Bottom Face
					//glTexCoord2f(0.0f, 0.0f);
					glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, -1.0f);

					//glTexCoord2f(1.0f, 0.0f);
					glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, -1.0f);

					//glTexCoord2f(1.0f, 1.0f);
					glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
					glVertex3f(-1.0f, -1.0f, 1.0f);

					//glTexCoord2f(0.0f, 1.0f);
					glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
					glVertex3f(1.0f, -1.0f, 1.0f);

				glEnd();

				glBindTexture(GL_TEXTURE_2D, 0);

			glPopMatrix();
	*/

	///////////////////////// Matrix  

	if (scene == 7) {

		glPushMatrix();

			glBindTexture(GL_TEXTURE_2D, matrixTexture);
			glTranslatef(0.0f, matrixTranslate, 0.0f);
			glBegin(GL_QUADS);

			glColor4f(1.0f, 1.0f, 1.0f, 0.4f);
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(11.0f, 10.0f, -5.0f);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-11.0f, 10.0f, -5.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-11.0f, -10.0f, -5.0f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(11.0f, -10.0f, -5.0f);

			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(11.0f, 10.0f, -5.0f);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-11.0f, 10.0f, -5.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-11.0f, 30.0f, -5.0f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(11.0f, 30.0f, -5.0f);

			glEnd();

			glBindTexture(GL_TEXTURE_2D, 0);

		glPopMatrix();
	}


	glRotatef(computerSceneAngle, 0.0f, 1.0f, 0.0f);

	///////////////////////// Light Circle
	glPushMatrix();

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	FGL_PointAssign(&p, 0.0f, 0.0f, 2.0f);

	glBegin(GL_TRIANGLE_FAN);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 2.0f);

	glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
	FGL_Ellipse(&p, 5.0f, 3.0f, 0.0f, 360.0f);

	glEnd();

	glPopMatrix();


	////////////////////// Computer Table
	glPushMatrix();

	Computer_Table();

	glPopMatrix();

	////////////////////// PC
	glPushMatrix();

	PC();

	glScalef(0.5f, 0.5f, 1.0f);
	glTranslatef(2.8f, 1.1f, 0.54f - MothaBachanAlpha);
	ChotaBachan();

	glPopMatrix();

	////////////////////// GPU
	glPushMatrix();

	glTranslatef(-0.3f, 0.0f, 0.0f);
	GPU();

	glScalef(0.3f, 0.3f, 1.0f);
	glTranslatef(-3.0f, 1.2f, 0.5f);
	ChotaBachan();

	glPopMatrix();

	/////////////////////// Lines
	glPushMatrix();

	FGL_Point upperLine1 = { -1.0f, 0.7f, 0.5f };
	FGL_Point lowerLine1 = { -1.0f, 0.0f, 0.5f };
	int i = (int)Lines_Time;
	while (i) {

		glBegin(GL_LINES);

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		glVertex3f(upperLine1.x, upperLine1.y, upperLine1.z);
		glVertex3f(upperLine1.x + (2 * 0.0145f), upperLine1.y + (2 * 0.0045f), upperLine1.z);

		glVertex3f(lowerLine1.x, lowerLine1.y, lowerLine1.z);
		glVertex3f(lowerLine1.x + (2 * 0.0145f), lowerLine1.y - (2 * 0.0015f), lowerLine1.z);

		glEnd();

		i--;
		upperLine1.x += (4 * 0.0145f);
		upperLine1.y += (4 * 0.0045f);
		lowerLine1.x += (4 * 0.0145f);
		lowerLine1.y -= (4 * 0.0015f);
	}

	glPopMatrix();


	/////////////////////// Bullet Circles
	glPushMatrix();

	FGL_PointAssign(&p, -1.0f, 0.35f, 0.5f);

	glTranslatef(BulletX, redBulletY, BulletZ);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(1.0f, 0.0f, 0.0f, BulletAlpha);
	FGL_Circle(&p, 0.05f, 0.0f, 360.0f);

	glEnd();

	glTranslatef(0.0f, -redBulletY, 0.0f);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(0.0f, 1.0f, 0.0f, BulletAlpha);
	FGL_Circle(&p, 0.05f, 0.0f, 360.0f);

	glEnd();

	glTranslatef(0.0f, blueBulletY, 0.0f);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(0.0f, 0.0f, 1.0f, BulletAlpha);
	FGL_Circle(&p, 0.05f, 0.0f, 360.0f);

	glEnd();

	glPopMatrix();
}

void Computer_Table() {

	// Variable Declarations
	FGL_Color c;

	///////////////////////////////////////////// Left Back Leg
	glBegin(GL_QUADS);

	FGL_ColorToFloat("65451F", &c);		// Dark
	glColor3f(c.r, c.g, c.b);

	// Bottom
	glVertex3f(-3.0f, -2.0f, -1.0f);
	glVertex3f(-3.0f, -2.0f, -0.8f);
	glVertex3f(-2.8f, -2.0f, -0.8f);
	glVertex3f(-2.8f, -2.0f, -1.0f);

	// Top
	glVertex3f(-3.0f, -0.5f, -1.0f);
	glVertex3f(-3.0f, -0.5f, -0.8f);
	glVertex3f(-2.8f, -0.5f, -0.8f);
	glVertex3f(-2.8f, -0.5f, -1.0f);

	FGL_ColorToFloat("765827", &c);		// Medium
	glColor3f(c.r, c.g, c.b);
	// Right
	glVertex3f(-2.8f, -2.0f, -1.0f);
	glVertex3f(-2.8f, -2.0f, -0.8f);
	glVertex3f(-2.8f, -0.5f, -0.8f);
	glVertex3f(-2.8f, -0.5f, -1.0f);

	// Left
	glVertex3f(-3.0f, -2.0f, -1.0f);
	glVertex3f(-3.0f, -2.0f, -0.8f);
	glVertex3f(-3.0f, -0.5f, -0.8f);
	glVertex3f(-3.0f, -0.5f, -1.0f);

	// Front
	glVertex3f(-2.8f, -2.0f, -0.8f);
	glVertex3f(-2.8f, -0.5f, -0.8f);
	glVertex3f(-3.0f, -0.5f, -0.8f);
	glVertex3f(-3.0f, -2.0f, -0.8f);

	// Back
	glVertex3f(-2.8f, -2.0f, -1.0f);
	glVertex3f(-2.8f, -0.5f, -1.0f);
	glVertex3f(-3.0f, -0.5f, -1.0f);
	glVertex3f(-3.0f, -2.0f, -1.0f);

	glEnd();



	//////////////////////////////////////////////// Left Front Leg 
	glBegin(GL_QUADS);

	FGL_ColorToFloat("65451F", &c);		// Dark
	glColor3f(c.r, c.g, c.b);
	// Bottom
	glVertex3f(-3.0f, -2.0f, 1.0f);
	glVertex3f(-3.0f, -2.0f, 0.8f);
	glVertex3f(-2.8f, -2.0f, 0.8f);
	glVertex3f(-2.8f, -2.0f, 1.0f);

	// Top
	glVertex3f(-3.0f, -0.5f, 1.0f);
	glVertex3f(-3.0f, -0.5f, 0.8f);
	glVertex3f(-2.8f, -0.5f, 0.8f);
	glVertex3f(-2.8f, -0.5f, 1.0f);

	FGL_ColorToFloat("765827", &c);		// Medium
	glColor3f(c.r, c.g, c.b);
	// Right
	glVertex3f(-2.8f, -2.0f, 1.0f);
	glVertex3f(-2.8f, -2.0f, 0.8f);
	glVertex3f(-2.8f, -0.5f, 0.8f);
	glVertex3f(-2.8f, -0.5f, 1.0f);

	// Left
	glVertex3f(-3.0f, -2.0f, 1.0f);
	glVertex3f(-3.0f, -2.0f, 0.8f);
	glVertex3f(-3.0f, -0.5f, 0.8f);
	glVertex3f(-3.0f, -0.5f, 1.0f);

	// Front
	glVertex3f(-2.8f, -2.0f, 0.8f);
	glVertex3f(-2.8f, -0.5f, 0.8f);
	glVertex3f(-3.0f, -0.5f, 0.8f);
	glVertex3f(-3.0f, -2.0f, 0.8f);

	// Back
	glVertex3f(-2.8f, -2.0f, 1.0f);
	glVertex3f(-2.8f, -0.5f, 1.0f);
	glVertex3f(-3.0f, -0.5f, 1.0f);
	glVertex3f(-3.0f, -2.0f, 1.0f);

	glEnd();


	//////////////////////////////////////////////// Right Back Leg
	glBegin(GL_QUADS);

	FGL_ColorToFloat("65451F", &c);		// Dark
	glColor3f(c.r, c.g, c.b);
	// Bottom
	glVertex3f(3.0f, -2.0f, -1.0f);
	glVertex3f(3.0f, -2.0f, -0.8f);
	glVertex3f(2.8f, -2.0f, -0.8f);
	glVertex3f(2.8f, -2.0f, -1.0f);

	// Top
	glVertex3f(3.0f, -0.5f, -1.0f);
	glVertex3f(3.0f, -0.5f, -0.8f);
	glVertex3f(2.8f, -0.5f, -0.8f);
	glVertex3f(2.8f, -0.5f, -1.0f);

	FGL_ColorToFloat("765827", &c);		// Medium
	glColor3f(c.r, c.g, c.b);
	// Right
	glVertex3f(2.8f, -2.0f, -1.0f);
	glVertex3f(2.8f, -2.0f, -0.8f);
	glVertex3f(2.8f, -0.5f, -0.8f);
	glVertex3f(2.8f, -0.5f, -1.0f);

	// Left
	glVertex3f(3.0f, -2.0f, -1.0f);
	glVertex3f(3.0f, -2.0f, -0.8f);
	glVertex3f(3.0f, -0.5f, -0.8f);
	glVertex3f(3.0f, -0.5f, -1.0f);

	// Front
	glVertex3f(2.8f, -2.0f, -0.8f);
	glVertex3f(2.8f, -0.5f, -0.8f);
	glVertex3f(3.0f, -0.5f, -0.8f);
	glVertex3f(3.0f, -2.0f, -0.8f);

	// Back
	glVertex3f(2.8f, -2.0f, -1.0f);
	glVertex3f(2.8f, -0.5f, -1.0f);
	glVertex3f(3.0f, -0.5f, -1.0f);
	glVertex3f(3.0f, -2.0f, -1.0f);

	glEnd();



	//////////////////////////////////////////// Right Front Leg
	glBegin(GL_QUADS);

	FGL_ColorToFloat("65451F", &c);		// Dark
	glColor3f(c.r, c.g, c.b);
	// Bottom
	glVertex3f(3.0f, -2.0f, 1.0f);
	glVertex3f(3.0f, -2.0f, 0.8f);
	glVertex3f(2.8f, -2.0f, 0.8f);
	glVertex3f(2.8f, -2.0f, 1.0f);

	// Top
	glVertex3f(3.0f, -0.5f, 1.0f);
	glVertex3f(3.0f, -0.5f, 0.8f);
	glVertex3f(2.8f, -0.5f, 0.8f);
	glVertex3f(2.8f, -0.5f, 1.0f);

	FGL_ColorToFloat("765827", &c);		// Medium
	glColor3f(c.r, c.g, c.b);
	// Right
	glVertex3f(2.8f, -2.0f, 1.0f);
	glVertex3f(2.8f, -2.0f, 0.8f);
	glVertex3f(2.8f, -0.5f, 0.8f);
	glVertex3f(2.8f, -0.5f, 1.0f);

	// Left
	glVertex3f(3.0f, -2.0f, 1.0f);
	glVertex3f(3.0f, -2.0f, 0.8f);
	glVertex3f(3.0f, -0.5f, 0.8f);
	glVertex3f(3.0f, -0.5f, 1.0f);

	// Front
	glVertex3f(2.8f, -2.0f, 0.8f);
	glVertex3f(2.8f, -0.5f, 0.8f);
	glVertex3f(3.0f, -0.5f, 0.8f);
	glVertex3f(3.0f, -2.0f, 0.8f);

	// Back
	glVertex3f(2.8f, -2.0f, 1.0f);
	glVertex3f(2.8f, -0.5f, 1.0f);
	glVertex3f(3.0f, -0.5f, 1.0f);
	glVertex3f(3.0f, -2.0f, 1.0f);

	glEnd();


	/////////////////////////////////////////// Sit 
	glBegin(GL_QUADS);

	FGL_ColorToFloat("A47E3B", &c);		// Light
	glColor3f(c.r, c.g, c.b);
	// Bottom
	glVertex3f(-3.2f, -0.5f, -1.2f);
	glVertex3f(3.2f, -0.5f, -1.2f);
	glVertex3f(3.2f, -0.5f, 1.2f);
	glVertex3f(-3.2f, -0.5f, 1.2f);

	// Top
	glVertex3f(-3.2f, -0.4f, -1.2f);
	glVertex3f(3.2f, -0.4f, -1.2f);
	glVertex3f(3.2f, -0.4f, 1.2f);
	glVertex3f(-3.2f, -0.4f, 1.2f);

	FGL_ColorToFloat("65451F", &c);		// Dark
	glColor3f(c.r, c.g, c.b);
	// Right
	glVertex3f(3.2f, -0.4f, -1.2f);
	glVertex3f(3.2f, -0.4f, 1.2f);
	glVertex3f(3.2f, -0.5f, 1.2f);
	glVertex3f(3.2f, -0.5f, -1.2f);

	// Left
	glVertex3f(-3.2f, -0.4f, -1.2f);
	glVertex3f(-3.2f, -0.4f, 1.2f);
	glVertex3f(-3.2f, -0.5f, 1.2f);
	glVertex3f(-3.2f, -0.5f, -1.2f);

	// Front
	glVertex3f(3.2f, -0.4f, 1.2f);
	glVertex3f(-3.2f, -0.4f, 1.2f);
	glVertex3f(-3.2f, -0.5f, 1.2f);
	glVertex3f(3.2f, -0.5f, 1.2f);

	// Back
	glVertex3f(3.2f, -0.4f, -1.2f);
	glVertex3f(-3.2f, -0.4f, -1.2f);
	glVertex3f(-3.2f, -0.5f, -1.2f);
	glVertex3f(3.2f, -0.5f, -1.2f);

	glEnd();
}

void ChotaBachan(void)
{

	FGL_Color c;

	FGL_Point p;
	FGL_PointAssign(&p, 0.05f, 0.66f, 0.0f);//center of circle
	//neck
	//glBegin(GL_POLYGON);
	//glVertex3f(-0.08f, 0.40f, 0.0f);//A
	//glVertex3f(0.18f, 0.40f, 0.0f);//A
	//glVertex3f(0.18f, 0.45f, 0.0f);//A
	//glVertex3f(-0.08f, 0.45f, 0.0f);//A
	//glEnd();

	//Shirt
	FGL_Color myColor;
	FGL_ColorToFloat("fefbe6", &myColor);

	glColor4f(0.9f, 0.2f, 0.0f, 1.0f);
	glBegin(GL_POLYGON);
	glVertex3f(-0.25f, 0.35f, 0.0f);//A
	glVertex3f(-0.1f, 0.40f, 0.0f);//A
	glVertex3f(0.20f, 0.40f, 0.0f);//A
	glVertex3f(0.35f, 0.35f, 0.0f);	//B
	glVertex3f(0.35f, -0.36f, 0.0f);//C
	glVertex3f(-0.25f, -0.36f, 0.0f);//D
	glEnd();



	//Head / face
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);	//Skin Color
	FGL_Ellipse(&p, 0.22f, 0.29f, 0.0f, 360.0f);//radius,angles
	glEnd();
	//Ear
	FGL_PointAssign(&p, -0.18f, 0.64f, 0.0f);
	glBegin(GL_POLYGON);
	FGL_Circle(&p, 0.05f, 0.0f, 360.0f);
	glEnd();

	//Hairs
	glBegin(GL_POLYGON);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	//khalch curve
	FGL_3Points pk;
	FGL_PointAssign(&pk.p0, -0.18f, 0.85f, 0.0f);
	FGL_PointAssign(&pk.p1, 0.09f, 0.65f, 0.0f);
	FGL_PointAssign(&pk.p2, 0.31f, 0.90f, 0.0f);
	FGL_BezierCurve3(&pk);
	glEnd();

	//vrch curve
	glBegin(GL_POLYGON);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	FGL_3Points ph;
	FGL_PointAssign(&ph.p0, -0.18f, 0.85f, 0.0f);
	FGL_PointAssign(&ph.p1, 0.09f, 1.05f, 0.0f);
	FGL_PointAssign(&ph.p2, 0.32f, 0.89f, 0.0f);

	FGL_BezierCurve3(&ph);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	glVertex3f(-0.18f, 0.85f, 0.0f);
	glVertex3f(-0.18f, 0.65f, 0.0f);
	glVertex3f(-0.08f, 0.80f, 0.0f);
	glEnd();


	//Eyebrow
	glBegin(GL_LINES);
	glLineWidth(0.25f);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	glVertex3f(-0.04f, 0.71f, 0.0f);
	glVertex3f(0.03f, 0.71f, 0.0f);
	glVertex3f(0.14f, 0.71f, 0.0f);
	glVertex3f(0.21f, 0.71f, 0.0f);
	glEnd();

	//Eyes right
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_PointAssign(&p, 0.01f, 0.62f, 0.0f);
	FGL_Circle(&p, 0.03f, 0.0f, 360.0f);
	glEnd();

	//Black Eye right
	glBegin(GL_POLYGON);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	FGL_PointAssign(&p, 0.02f, 0.62f, 0.0f);
	FGL_Circle(&p, 0.01f, 0.0f, 360.0f);
	glEnd();


	//left eye
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_PointAssign(&p, 0.18f, 0.62f, 0.0f);
	FGL_Circle(&p, 0.03f, 0.0f, 360.0f);
	glEnd();


	//Black eye left
	glBegin(GL_POLYGON);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	FGL_PointAssign(&p, 0.19f, 0.62f, 0.0f);
	FGL_Circle(&p, 0.01f, 0.0f, 360.0f);
	glEnd();

	//Nak
	glBegin(GL_LINES);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	glVertex3f(0.1f, 0.60f, 0.0f);
	glVertex3f(0.13f, 0.54f, 0.0f);
	glVertex3f(0.13f, 0.54f, 0.0f);
	glVertex3f(0.1f, 0.54f, 0.0f);
	glEnd();

	//Mouth
	glBegin(GL_POLYGON);
	glColor4f(1.0f, 1.0f, 0.8f, 1.0f);
	FGL_3Points ps;
	FGL_PointAssign(&ps.p0, 0.08f, 0.50f, 0.0f);
	FGL_PointAssign(&ps.p1, 0.12f, 0.44f, 0.0f);
	FGL_PointAssign(&ps.p2, 0.16f, 0.50f, 0.0f);//H
	FGL_BezierCurve3(&ps);
	glEnd();

	//lEFT Hands
	//SLeave
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.3f, 0.0f, 1.0f);
	glVertex3f(-0.38f, 0.25f, 0.0f);
	glVertex3f(-0.20f, 0.30f, 0.0f);
	glVertex3f(-0.1f, 0.05f, 0.0f);
	glVertex3f(-0.30f, -0.05f, 0.0f);

	//glColor4f(0.0f, 0.0f, 0.5f, 1.0f);
	FGL_3Points pt;
	FGL_PointAssign(&pt.p0, -0.38f, 0.25f, 0.0f);
	FGL_PointAssign(&pt.p1, -0.28f, 0.42f, 0.0f);
	FGL_PointAssign(&pt.p2, -0.20f, 0.30f, 0.0f);//H

	FGL_BezierCurve3(&pt);

	glEnd();

	//Kopra
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);
	glVertex3f(-0.28f, -0.019f, 0.0f);	//1
	glVertex3f(-0.15f, 0.03f, 0.0f);//2
	glVertex3f(-0.12f, -0.04f, 0.0f);//3
	glVertex3f(-0.25f, -0.15f, 0.0f);//4
	glEnd();

	//Hat
	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);
	glVertex3f(-0.25f, -0.15f, 0.0f);//4
	glVertex3f(-0.12f, -0.04f, 0.0f);//3
	glVertex3f(0.138f, -0.115f, 0.0f);//2
	glVertex3f(0.14f, -0.19f, 0.0f);//1

	glEnd();

	//trigger
	glLineWidth(1.9f);
	glBegin(GL_LINE_STRIP);
	glColor4f(0.5f, 0.7f, 0.8f, 1.0f);
	FGL_3Points pp;
	FGL_PointAssign(&pp.p0, 0.16f, -0.12f, 0.0f);
	FGL_PointAssign(&pp.p1, 0.24f, -0.18f, 0.0f);
	FGL_PointAssign(&pp.p2, 0.28f, -0.06f, 0.0f);//H

	FGL_BezierCurve3(&pp);

	glEnd();

	//Bot 
	glBegin(GL_QUADS);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);
	glVertex3f(0.138f, -0.115f, 0.0f);//1
	glVertex3f(0.22f, -0.09f, 0.0f);//2
	glVertex3f(0.22f, -0.1f, 0.0f);//3
	glVertex3f(0.14f, -0.15f, 0.0f);//4
	glEnd();

	// Panja
	FGL_PointAssign(&p, 0.14f, -0.16f, 0.0f);
	glBegin(GL_POLYGON);

	FGL_Circle(&p, 0.05f, 0.0f, 360.0f);

	glEnd();

	/*	//panja
		glBegin(GL_QUADS);
		glVertex3f(0.14f, -0.15f, 0.0f);//4
		glVertex3f(0.18f, -0.12f, 0.0f);
		glVertex3f(0.18f, -0.19f, 0.0f);
		glVertex3f(0.14f, -0.19f, 0.0f);
		glEnd();
	*/

	//botach curve
	glBegin(GL_POLYGON);
	FGL_3Points pt2;
	//	FGL_Point p;
	FGL_PointAssign(&pt2.p0, 0.14, -0.15f, 0.0f);//H
	FGL_PointAssign(&pt2.p1, 0.19f, -0.1f, 0.0f);
	FGL_PointAssign(&pt2.p2, 0.22f, -0.08f, 0.0f);
	FGL_BezierCurve3(&pt2);
	glEnd();


	//Right Hand
	//sleaves
	glBegin(GL_TRIANGLES);
	glColor4f(0.9f, 0.3f, 0.0f, 1.0f);
	glVertex3f(0.35f, 0.35f, 0.0f);
	glVertex3f(0.5, 0.18f, 0.0f);
	glVertex3f(0.35f, 0.1f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.9f, 0.3f, 0.0f, 1.0f);
	FGL_3Points pt1;
	FGL_PointAssign(&pt1.p0, 0.35f, 0.35f, 0.0f);
	FGL_PointAssign(&pt1.p1, 0.5f, 0.25f, 0.0f);
	FGL_PointAssign(&pt1.p2, 0.5f, 0.18f, 0.0f);//H
	FGL_BezierCurve3(&pt1);
	glEnd();

	//hat
	glBegin(GL_QUADS);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);
	glVertex3f(0.35f, 0.12f, 0.0f);
	glVertex3f(0.48f, 0.18f, 0.0f);
	glVertex3f(0.63f, -0.1f, 0.0f);
	glVertex3f(0.48f, -0.1f, 0.0f);
	glEnd();

	//seat bhag
	glBegin(GL_POLYGON);
	glColor4f(0.4f, 0.7f, 0.9f, 1.0f);
	glVertex3f(-0.25, -0.35, 0.0f);//D
	glVertex3f(0.35, -0.35, 0.0f);//C
	glVertex3f(0.35, -0.52f, 0.0f);//E
	glVertex3f(-0.25, -0.52f, 0.0f);//F
	glEnd();

	//Left-Leg
	glBegin(GL_POLYGON);
	glColor4f(0.4f, 0.7f, 0.9f, 1.0f);
	glVertex3f(-0.25f, -0.52f, 0.0f);  //F          
	glVertex3f(0.08f, -0.52f, 0.0f);//G
	glVertex3f(-0.04f, -1.1f, 0.0f);//H
	glVertex3f(-0.22f, -1.1f, 0.0f);//I
	glEnd();

	//Right Leg
	glBegin(GL_POLYGON);
	glColor4f(0.4f, 0.7f, 0.9f, 1.0f);
	glVertex3f(0.06f, -0.52f, 0.0f);//G
	glVertex3f(0.35f, -0.52f, 0.0f);//T
	glVertex3f(0.33f, -1.1f, 0.0f);//X
	glVertex3f(0.15f, -1.1f, 0.0f);//Y
	glEnd();

	//Left Shoes
	FGL_ColorToFloat("3D0C11", &c);
	glBegin(GL_POLYGON);
	glColor4f(c.r, c.g, c.b, 1.0f);
	glVertex3f(-0.22f, -1.1f, 0.0f);//I
	glVertex3f(-0.04f, -1.1f, 0.0f);//H
	glVertex3f(0.05f, -1.2f, 0.0f);//P
	glVertex3f(-0.25f, -1.2f, 0.0f);//Q
	glEnd();

	//Right Shoes
	glBegin(GL_POLYGON);
	glVertex3f(0.15f, -1.1f, 0.0f);//Y
	glVertex3f(0.33f, -1.1f, 0.0f);//X
	glVertex3f(0.45f, -1.2f, 0.0f);//P
	glVertex3f(0.12f, -1.2f, 0.0f);//Q
	glEnd();

	//Banduk
	glPushMatrix();
	glTranslatef(0.09f, -0.05f, 0.0f);
	glColor4f(0.5f, 0.7f, 0.8f, 1.0f);
	glBegin(GL_POLYGON);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.05f, -0.05f);
	glVertex2f(0.35f, -0.05f);
	glVertex2f(0.35f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.2f, 0.2f, 0.0f, 1.0f);
	glVertex2f(0.35f, 0.0f);
	glVertex2f(0.35f, -0.08f);
	glVertex2f(0.6f, -0.08f);
	glVertex2f(0.6f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.5f, 0.7f, 0.8f, 1.0f);
	glVertex2f(0.5f, 0.0f);
	glVertex2f(0.5f, -0.02f);
	glVertex2f(0.8f, -0.02f);
	glVertex2f(0.8f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor4f(0.5f, 0.7f, 0.8f, 1.0f);
	glVertex2f(0.76f, 0.01f);
	glVertex2f(0.75f, 0.0f);
	glVertex2f(0.78f, 0.0f);
	glVertex2f(0.775f, 0.01f);
	glEnd();

	glPopMatrix();

	//Right hand bota
	FGL_PointAssign(&p, 0.59f, -0.1f, 0.0f);
	glColor4f(0.9f, 0.7f, 0.5f, 1.0f);
	glBegin(GL_POLYGON);
	FGL_Circle(&p, 0.07f, 180.0f, 360.0f);
	glEnd();


}

void PC() {

	//////////////////////////////////////////// Base
	glBegin(GL_QUADS);

	glColor4f(0.2f, 0.2f, 0.2f, 1.0f);

	// Bottom
	glVertex3f(1.0f, -0.4f, 0.5f);
	glVertex3f(1.0f, -0.4f, -0.5f);
	glVertex3f(2.0f, -0.4f, -0.5f);
	glVertex3f(2.0f, -0.4f, 0.5f);

	// Top
	glVertex3f(1.0f, -0.35f, 0.5f);
	glVertex3f(1.0f, -0.35f, -0.5f);
	glVertex3f(2.0f, -0.35f, -0.5f);
	glVertex3f(2.0f, -0.35f, 0.5f);

	// Right
	glVertex3f(2.0f, -0.4f, 0.5f);
	glVertex3f(2.0f, -0.4f, -0.5f);
	glVertex3f(2.0f, -0.35f, -0.5f);
	glVertex3f(2.0f, -0.35f, 0.5f);

	// Left
	glVertex3f(1.0f, -0.4f, 0.5f);
	glVertex3f(1.0f, -0.4f, -0.5f);
	glVertex3f(1.0f, -0.35f, -0.5f);
	glVertex3f(1.0f, -0.35f, 0.5f);

	// Front
	glVertex3f(1.0f, -0.4f, -0.5f);
	glVertex3f(1.0f, -0.35f, -0.5f);
	glVertex3f(2.0f, -0.35f, -0.5f);
	glVertex3f(2.0f, -0.4f, -0.5f);

	// Rear
	glVertex3f(1.0f, -0.4f, 0.5f);
	glVertex3f(1.0f, -0.35f, 0.5f);
	glVertex3f(2.0f, -0.35f, 0.5f);
	glVertex3f(2.0f, -0.4f, 0.5f);

	glEnd();


	////////////////////////////////////////////// Rod
	glBegin(GL_QUADS);

	glColor4f(0.4f, 0.4f, 0.4f, 1.0f);

	// Bottom
	glVertex3f(1.4f, -0.35f, -0.2f);
	glVertex3f(1.4f, -0.35f, -0.4f);
	glVertex3f(1.6f, -0.35f, -0.4f);
	glVertex3f(1.6f, -0.35f, -0.2f);

	// Top
	glVertex3f(1.4f, 0.5f, -0.2f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.2f);

	// Right
	glVertex3f(1.6f, -0.35f, -0.2f);
	glVertex3f(1.6f, -0.35f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.2f);

	// Left
	glVertex3f(1.4f, -0.35f, -0.2f);
	glVertex3f(1.4f, -0.35f, -0.4f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.4f, 0.5f, -0.2f);

	// Front
	glVertex3f(1.6f, -0.35f, -0.2f);
	glVertex3f(1.4f, -0.35f, -0.2f);
	glVertex3f(1.4f, 0.5f, -0.2f);
	glVertex3f(1.6f, 0.5f, -0.2f);

	// Back
	glVertex3f(1.6f, -0.35f, -0.4f);
	glVertex3f(1.4f, -0.35f, -0.4f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.4f);

	glEnd();


	//////////////////////////////////////////// Support
	glBegin(GL_QUADS);

	glColor4f(0.4f, 0.4f, 0.4f, 1.0f);

	// Bottom
	glVertex3f(1.4f, 0.5f, -0.1f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.5f, -0.1f);

	// Top
	glVertex3f(1.4f, 0.6f, -0.1f);
	glVertex3f(1.4f, 0.6f, -0.4f);
	glVertex3f(1.6f, 0.6f, -0.4f);
	glVertex3f(1.6f, 0.6f, -0.1f);

	// Right
	glVertex3f(1.6f, 0.5f, -0.1f);
	glVertex3f(1.6f, 0.5f, -0.4f);
	glVertex3f(1.6f, 0.6f, -0.4f);
	glVertex3f(1.6f, 0.6f, -0.1f);

	// Left
	glVertex3f(1.4f, 0.5f, -0.1f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.4f, 0.6f, -0.4f);
	glVertex3f(1.4f, 0.6f, -0.1f);

	// Front
	glVertex3f(1.6f, 0.5f, -0.1f);
	glVertex3f(1.4f, 0.5f, -0.1f);
	glVertex3f(1.4f, 0.6f, -0.1f);
	glVertex3f(1.6f, 0.6f, -0.1f);

	// Back
	glVertex3f(1.6f, 0.5f, -0.4f);
	glVertex3f(1.4f, 0.5f, -0.4f);
	glVertex3f(1.4f, 0.6f, -0.4f);
	glVertex3f(1.6f, 0.6f, -0.4f);

	glEnd();


	/////////////////////////////////////// Screen Frustum
	glBegin(GL_QUADS);

	glColor4f(0.2f, 0.2f, 0.2f, 1.0f);

	// Bottom
	glVertex3f(1.3f, 0.4f, -0.1f);
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(1.7f, 0.4f, -0.1f);

	// Top
	glVertex3f(1.3f, 0.7f, -0.1f);
	glVertex3f(0.5f, 1.2f, 0.4f);
	glVertex3f(2.5f, 1.2f, 0.4f);
	glVertex3f(1.7f, 0.7f, -0.1f);

	// Right
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(1.7f, 0.4f, -0.1f);
	glVertex3f(1.7f, 0.7f, -0.1f);
	glVertex3f(2.5f, 1.2f, 0.4f);

	// Left
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(1.3f, 0.4f, -0.1f);
	glVertex3f(1.3f, 0.7f, -0.1f);
	glVertex3f(0.5f, 1.2f, 0.4f);

	// Front
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(0.5f, 1.2f, 0.4f);
	glVertex3f(2.5f, 1.2f, 0.4f);

	// Back
	glVertex3f(1.7f, 0.4f, -0.1f);
	glVertex3f(1.3f, 0.4f, -0.1f);
	glVertex3f(1.3f, 0.7f, -0.1f);
	glVertex3f(1.7f, 0.7f, -0.1f);

	glEnd();


	////////////////////////////////////////// Screen
	glBegin(GL_QUADS);

	glColor4f(0.1f, 0.1f, 0.1f, 1.0f);

	// Bottom
	glVertex3f(0.5f, -0.2f, 0.5f);
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(2.5f, -0.2f, 0.5f);

	// Top
	glVertex3f(0.5f, 1.2f, 0.5f);
	glVertex3f(0.5f, 1.2f, 0.4f);
	glVertex3f(2.5f, 1.2f, 0.4f);
	glVertex3f(2.5f, 1.2f, 0.5f);

	// Right
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(2.5f, -0.2f, 0.5f);
	glVertex3f(2.5f, 1.2f, 0.5f);
	glVertex3f(2.5f, 1.2f, 0.4f);

	// Left
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(0.5f, -0.2f, 0.5f);
	glVertex3f(0.5f, 1.2f, 0.5f);
	glVertex3f(0.5f, 1.2f, 0.4f);

	// Front
	glVertex3f(2.5f, -0.2f, 0.5f);
	glVertex3f(0.5f, -0.2f, 0.5f);
	glVertex3f(0.5f, 1.2f, 0.5f);
	glVertex3f(2.5f, 1.2f, 0.5f);

	// Back
	glVertex3f(2.5f, -0.2f, 0.4f);
	glVertex3f(0.5f, -0.2f, 0.4f);
	glVertex3f(0.5f, 1.2f, 0.4f);
	glVertex3f(2.5f, 1.2f, 0.4f);

	// Screen Inner
	glColor4f(0.05f, 0.05f, 0.05f, 1.0f);
	glVertex3f(2.4f, -0.1f, 0.501f);
	glVertex3f(0.6f, -0.1f, 0.501f);
	glVertex3f(0.6f, 1.1f, 0.501f);
	glVertex3f(2.4f, 1.1f, 0.501f);

	glEnd();
}

void GPU() {

	// Local Variables
	FGL_Color c;
	FGL_Point p;

	//////////////////////////////////////////// Base
	glBegin(GL_QUADS);

	glColor4f(0.4f, 0.4f, 0.4f, 1.0f);

	// Bottom
	glVertex3f(-0.8f, -0.4f, 0.8f);
	glVertex3f(-0.8f, -0.4f, 0.0f);
	glVertex3f(-2.2f, -0.4f, 0.0f);
	glVertex3f(-2.2f, -0.4f, 0.8f);

	// Top
	glVertex3f(-0.8f, 0.0f, 0.8f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glVertex3f(-2.2f, 0.0f, 0.0f);
	glVertex3f(-2.2f, 0.0f, 0.8f);

	// Right
	glVertex3f(-2.2f, -0.4f, 0.8f);
	glVertex3f(-2.2f, -0.4f, 0.0f);
	glVertex3f(-2.2f, 0.0f, 0.0f);
	glVertex3f(-2.2f, 0.0f, 0.8f);

	// Left
	glVertex3f(-0.8f, -0.4f, 0.8f);
	glVertex3f(-0.8f, -0.4f, 0.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glVertex3f(-0.8f, 0.0f, 0.8f);

	// Rear
	glColor4f(0.2f, 0.2f, 0.2f, 1.0f);

	glVertex3f(-0.8f, -0.4f, 0.0f);
	glVertex3f(-0.8f, 0.0f, 0.0f);
	glVertex3f(-2.2f, 0.0f, 0.0f);
	glVertex3f(-2.2f, -0.4f, 0.0f);

	// Front
	glVertex3f(-0.8f, -0.4f, 0.8f);
	glVertex3f(-0.8f, 0.0f, 0.8f);
	glVertex3f(-2.2f, 0.0f, 0.8f);
	glVertex3f(-2.2f, -0.4f, 0.8f);


	FGL_ColorToFloat("76b900", &c);
	glColor4f(c.r, c.g, c.b, c.a);
	glVertex3f(-0.85f, -0.38f, 0.801f);
	glVertex3f(-0.85f, -0.02f, 0.801f);
	glVertex3f(-2.15f, -0.02f, 0.801f);
	glVertex3f(-2.15f, -0.38f, 0.801f);

	glEnd();

	// Left Fan
	FGL_PointAssign(&p, -1.8f, -0.2f, 0.802f);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(0.1f, 0.1f, 0.1f, 1.0f);
	FGL_Circle(&p, 0.17f, 0.0f, 360.0f);

	glEnd();

	glPushMatrix();

	glTranslatef(-1.8f, -0.2f, 0.01f);
	glRotatef(Fan_Angle, 0.0f, 0.0f, 1.0f);

	glBegin(GL_TRIANGLES);

	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.15f, 0.8f);
	glVertex3f(0.1f, 0.15f, 0.8f);

	glVertex3f(0.15f, -0.1f, 0.8f);
	glVertex3f(0.15f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.0f, 0.8f);

	glVertex3f(0.0f, 0.0f, 0.8f);
	glVertex3f(0.0f, -0.15f, 0.8f);
	glVertex3f(-0.1f, -0.15f, 0.8f);

	glVertex3f(-0.15f, 0.1f, 0.8f);
	glVertex3f(-0.15f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.0f, 0.8f);

	glEnd();

	glPopMatrix();


	// Right Fan
	FGL_PointAssign(&p, -1.2f, -0.2f, 0.802f);
	glBegin(GL_TRIANGLE_FAN);

	glColor4f(0.1f, 0.1f, 0.1f, 1.0f);
	FGL_Circle(&p, 0.17f, 0.0f, 360.0f);

	glEnd();

	glPushMatrix();

	glTranslatef(-1.2f, -0.2f, 0.01f);
	glRotatef(Fan_Angle, 0.0f, 0.0f, 1.0f);

	glBegin(GL_TRIANGLES);

	glColor4f(0.5f, 0.5f, 0.5f, 1.0f);

	glVertex3f(0.0f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.13f, 0.8f);
	glVertex3f(0.1f, 0.13f, 0.8f);

	glVertex3f(0.13f, -0.1f, 0.8f);
	glVertex3f(0.13f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.0f, 0.8f);

	glVertex3f(0.0f, 0.0f, 0.8f);
	glVertex3f(0.0f, -0.13f, 0.8f);
	glVertex3f(-0.1f, -0.13f, 0.8f);

	glVertex3f(-0.13f, 0.1f, 0.8f);
	glVertex3f(-0.13f, 0.0f, 0.8f);
	glVertex3f(0.0f, 0.0f, 0.8f);

	glEnd();

	glPopMatrix();
}

