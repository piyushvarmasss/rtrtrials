//Common headers
#include <windows.h> //For WIN32
#include <stdio.h>	//For FILE IO
#include <stdlib.h> //For exit 
#include <math.h>
#include <MMSystem.h>

//OpenGL Header Files
#include <gl/GL.h>
#include <gl/GLU.h>
#include "OGL.h"

#include "common_server.h"
#include "MKK_server.h"
#include "AMA_server.h"
#include "PNV_server.h"
#include "AKJ_server.h"
#include "AKS_server.h"


//MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with OpenGL Library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "Winmm.lib")

//Global function prototype (Visible to OS)
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFILE = NULL;
BOOL gbActive = FALSE;
HWND ghwnd = NULL;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; // NULLL kiva 0 la kasa set karaycha tar hi best way ahe
BOOL gbFullscreen = FALSE;

extern GLfloat chair_angle;  // for chair (screen six)

//OpenGL related global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL; // RENDERING CONTEXT(Awasta)

// Theatre Scene
extern GLfloat KernelAlpha;

// Global variable for solar system 
extern GLfloat SunAngle;
extern GLfloat Planet_Day;
extern GLfloat Mercury_Year;
extern GLfloat Venus_Year;
extern GLfloat Earth_Year;
extern GLfloat Mars_Year;
extern GLfloat Jupiter_Year;
extern GLfloat Saturn_Year;
extern GLfloat Uranus_Year;
extern GLfloat Neptune_Year;

GLUquadric* quadric = NULL;

// Global variable for Computer Scene
extern GLfloat computerSceneAngle;
extern GLfloat matrixTranslate;
//extern GLfloat angle;
extern GLfloat Fan_Angle;
extern GLfloat Lines_Time;

extern GLfloat BulletX;
extern GLfloat BulletZ;
extern GLfloat BulletAlpha;
extern GLfloat redBulletY;
extern GLfloat blueBulletY;
extern GLfloat MothaBachanAlpha;
extern GLfloat Galaxy_Move;
extern GLfloat SunAngle;
extern GLfloat Planet_Day;
extern GLfloat Mercury_Year;
extern GLfloat Venus_Year;
extern GLfloat Earth_Year;
extern GLfloat Mars_Year;
extern GLfloat Jupiter_Year;
extern GLfloat Saturn_Year;
extern GLfloat Uranus_Year;
extern GLfloat Neptune_Year;

// Fade In Fade Out
int scene = 1;
int fade = 0;
float alpha = 1.0f;
/*
int fadeIn = 0;
int fadeOut = 0;
*/




// Screen Six Transition
float screensix_Astro = -3.55f;
float screensix_Comp = 1.50f;
float screensix_Medi = -0.70f;
float screensix_timer = 0.0f;
float letterY = 1.20f;
float letterX = 1.20f;
float letterZ = 1.20f;
float letterW = 1.07f;

SYSTEMTIME previousTime;

// Texture Object/variable
GLuint texture_ganpati = 0;
GLuint galaxy_texture = 0;
GLuint heart_texture = 0;
GLuint mercury_texture = 0;
GLuint venus_texture = 0;
GLuint earth_texture = 0;
GLuint moon_texture = 0;
GLuint mars_texture = 0;
GLuint jupiter_texture = 0;
GLuint saturn_texture = 0;
GLuint uranus_texture = 0;
GLuint neptune_texture = 0;
GLuint texture_wood = 0;
GLuint texture_galaxy = 0;
GLuint logoTexture = 0;
GLuint matrixTexture = 0;
GLfloat galaxy_angle = 0.0f;
GLfloat FontDropWait = 0.0f;

GLfloat ScreenTheatreZ = 0.0f;
GLfloat KernelZ = -5.1f;
GLfloat DoctorX = -0.45f;
GLfloat DoctorY = -0.43f;
GLfloat DoctorZ = -0.35f;
GLfloat GanpatiSceneZ = 0.0f;
GLfloat SolarSystemY = 0.0f;
GLfloat SolarSystemZ = 2.0f;

GLfloat HBDAlpha = 0.0f;

int hour = 0;
int day = 0;
int year = 0;
int mRevolve = 0;
int SixScreenSceneFlag = 0;
int ChairFlag = 0;


//Entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{	
	//Function declarations
	int	initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
	void ToggleFullscreen(void);

	//Local variables
	WNDCLASSEX wndclass; //structure with 12 members
	HWND hwnd; //Handle of window
	MSG msg; //structure with 6 members
	TCHAR szAppName[] = TEXT("Windows");
	int iResult = 0;
	BOOL bDone = FALSE;
	int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// code
	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{
		MessageBox(NULL, TEXT("Log file cannot be opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFILE, "Program started successfully.\n");

	//now assign 12 members of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	//Register WNDCLASSEX to OS
	RegisterClassEx(&wndclass);

	//Create Window with 11 parameters in Memory
	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sir Birthday Demo"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		ScreenWidth / 2 - WIN_WIDTH / 2,
		ScreenHeight / 2 - WIN_HEIGHT / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,   // Handle of parent window
		NULL,   
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize() failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	//Show Window
	ShowWindow(hwnd, iCmdShow);
	
	SetForegroundWindow(hwnd); // Z-order la top la asnar
	SetFocus(hwnd); //internally WM_SETFOCUS WndProc la pathavta aaplya kadun
	ToggleFullscreen();

	//Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Render
			if (gbActive == TRUE)
			{
				//Render
				display();

				//Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);
}

//Callback which OS is going to interact with (Window Procedure)
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function declaration
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;
	
	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	//case WM_ERASEBKGND:
	//	return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;

		case 'O':
		case 'o':
			AMA_CurtainOpenFlag = TRUE;
			break;

		// Transition fade out
		case 'i':  
		case 'I':
			{
				switch (scene)
				{
					case 1:    //FirstScreenOfFont();
						scene = 2;
						break;

					case 2:     //SecondScreenOfFont();
						scene = 3;
						break;

					case 3:  // Stage Screen 
						scene = 4;
						break;

					case 4: // Medical Scene
						scene = 5;
						break;

					case 5: // Outside operation theater
						scene = 6;
						break;

					case 6:  // Solar System
						scene = 7;
						break;

					case 7: // Computer Scene
						scene = 8;
						screensix_timer = 0.0f;
						break;

					case 8:   // MultiViewport screens
						scene = 9;
						break;

					case 9:   //Last Credits
						scene = 10;
						break;

					case 10:   // Special Thanks
						scene = 11;
						break;

					case 11:   // The End
						scene = 12;
						break;

					default:
						break;
					}
			}

		case 't':  // Transition
		case 'T':
			if (fade)
				fade = 0;
			else
				fade = 1;
			break;

		case 's':  
		case 'S':
			SixScreenSceneFlag++;
			break;
	
		case 'c':
		case 'C':
			ChairFlag = 1;
			break;

		default:
			break;
		}
		break;
	
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//local variable declarations
	MONITORINFO mi = { sizeof(MONITORINFO) };
	
	//code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(
					ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED // WM_NCCALCSIZE
				);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//Function declarations
	void resize(int, int);
	BOOL loadGLTexture(GLuint*, TCHAR[]);

	//Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	BOOL bResult;

	//STEP-1
	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;   //2 raise to 32
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	//STEP-2 Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() Failed\n");
		return(-1);
	}

	//STEP-3 OS kade jaa ani me dilelya PFD la tuza kade aslelya PFD la match kar ani saglyaat javal cha asnarya cha index de
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFILE, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//STEP-4 Set obtained pixelformatdescriptor
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFILE, "SetPixelFormat() Failed.\n");
		return(-3);
	}

	//STEP-5 Tell Wndows Graphic Library (WGL)/Bridging Libraries to give me OpenGL compatible DC from this DC
	//Create OpenGL context from DC
	ghrc = wglCreateContext(ghdc); // jyanchi survat wgl ne honar te bridging api's
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wglCreateContext() Failed\n");
		return(-4);
	}
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	PlaySound(MAKEINTRESOURCE(MUSIC), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);

	// Enabling Depth
	glShadeModel(GL_SMOOTH); // Optional Beautification
	// Compulsory
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Optional Beautification

	//Set the clear color of window to blue
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //Here OpenGL starts

	// initialize quadric
	quadric = gluNewQuadric();

	bResult = loadGLTexture(&texture_galaxy, MAKEINTRESOURCE(GALAXYTEXTURE));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for galaxy texture Failed\n");
		return(-6);
	}

	bResult = loadGLTexture(&logoTexture, MAKEINTRESOURCE(LOGO));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for logo texture Failed\n");
		return(-7);
	}

	bResult = loadGLTexture(&matrixTexture, MAKEINTRESOURCE(MATRIX));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() failed to load matrix texture.\n");
		return(-8);
	}


	bResult = loadGLTexture(&galaxy_texture, MAKEINTRESOURCE(GALAXY));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for galaxy texture Failed\n");
		return(-9);
	}

	bResult = loadGLTexture(&mercury_texture, MAKEINTRESOURCE(MERCURY));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for mercury texture Failed\n");
		return(-10);
	}

	bResult = loadGLTexture(&venus_texture, MAKEINTRESOURCE(VENUS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for venus texture Failed\n");
		return(-11);
	}

	bResult = loadGLTexture(&earth_texture, MAKEINTRESOURCE(EARTH));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for earth texture Failed\n");
		return(-12);
	}

	bResult = loadGLTexture(&moon_texture, MAKEINTRESOURCE(MOON));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for moon texture Failed\n");
		return(-13);
	}

	bResult = loadGLTexture(&mars_texture, MAKEINTRESOURCE(MARS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for mars texture Failed\n");
		return(-14);
	}

	bResult = loadGLTexture(&jupiter_texture, MAKEINTRESOURCE(JUPITER));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for jupiter texture Failed\n");
		return(-15);
	}

	bResult = loadGLTexture(&saturn_texture, MAKEINTRESOURCE(SATURN));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for saturn texture Failed\n");
		return(-16);
	}

	bResult = loadGLTexture(&uranus_texture, MAKEINTRESOURCE(URANUS));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for uranus texture Failed\n");
		return(-17);
	}

	bResult = loadGLTexture(&neptune_texture, MAKEINTRESOURCE(NEPTUNE));
	if (bResult == FALSE) {

		fprintf(gpFILE, "loadGLTexture() for nuptune texture Failed\n");
		return(-18);
	}

	bResult = loadGLTexture(&texture_ganpati, MAKEINTRESOURCE(MY_GANPATI_BITMAP));
	if (bResult == FALSE)
	{
		fprintf(gpFILE, "Loading of Ganpati Bappa Texture failed !!!\n");
		return(-19);
	}

	bResult = loadGLTexture(&heart_texture, MAKEINTRESOURCE(HEART));
	if (bResult == FALSE)
	{
		fprintf(gpFILE, "Loading of Heart Texture failed !!!\n");
		return(-20);
	}

	glEnable(GL_TEXTURE_2D);
	resize(WIN_WIDTH, WIN_HEIGHT);

	return (0);
}

BOOL loadGLTexture(GLuint* texture, TCHAR imageResourceId[])
{
	//Local variables
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	//Load the image
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),
		imageResourceId,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION
	);

	if (hBitmap == NULL)
	{
		fprintf(gpFILE, "LoadImage Failed.\n");
		return (FALSE);
	}

	//Get image data
	GetObject(
		hBitmap,
		sizeof(BITMAP),
		&bmp
	);

	//Create OpenGL Texture
	glGenTextures(1, texture);

	//Bind to the texture
	glBindTexture(GL_TEXTURE_2D, *texture);

	//Load and unpack
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	//Set texture paramters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	//Create multiple mip map images
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, (void*)bmp.bmBits); //Instead of 3 GL_RGB

	//Unbind
	glBindTexture(GL_TEXTURE_2D, 0);

	//Delete hBitmap
	DeleteObject(hBitmap);
	hBitmap = NULL;

	return (TRUE);
}

void resize(int width, int height)
{
	//Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // Binocular 0-0
}

void display(void)
{
	//Code
	void drawQuad(void); // For fading effect

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // glclearcolor ya color ni majha buffer clear kr


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	void ScreenTwo (void); // Medical Scene  : Omkar & Mangesh
	void ScreenFive(void); // Computer scene : By kalyani & Rupali
	void ScreenOne(void); // stage Screen : By Ashish & Pratik
	void ScreenFour(void); // Astrology Screen : By Piyush
	void ScreenThree(void); // Prayers Medical : By Adity & Arif
	void ScreenSix(void); //

	//glTranslatef(0.0f, 0.0f, -4.0f);

	if (scene == 1)
	{
		glPushMatrix();
			glTranslatef(0.0f, 0.0f, -5.0f);
			FirstScreenOfFont();
		glPopMatrix();
	}

	else if (scene == 2)
	{
		glPushMatrix();
			glTranslatef(0.0f, 0.0f, -5.0f);
			SecondScreenOfFont();
		glPopMatrix();
	}

	else if (scene == 3)
	{
		glPushMatrix();
			ScreenOne();
		glPopMatrix();

		glPushMatrix();

			glTranslatef(-0.25f, 0.1f, -1.25f);
			glScalef(0.0f, 0.0f, 0.0f);
			Kernel();

		glPopMatrix();
	}

	else if (scene == 4)
	{
		/*
		glPushMatrix();
			glTranslatef(0.31f, 0.6f, -2.1f);
			glScalef(0.0f, 0.0f, 0.0f);
			MedicineText();
		glPopMatrix();
		*/

		glPushMatrix();
			glTranslatef(DoctorX, DoctorY, DoctorZ);
			ScreenTwo();
		glPopMatrix();

	}

	else if (scene == 5)
	{
		glPushMatrix();
			ScreenThree();
		glPopMatrix();

	}

	else if (scene == 6)
	{
		glPushMatrix();
			ScreenFour();
		glPopMatrix();

	}

	else if (scene == 7)
	{
		glPushMatrix();
			ScreenFive();
		glPopMatrix();

	}
	else if (scene == 8)
	{
		glPushMatrix();
		// View Transformation
		ScreenSix();
		glPopMatrix();
	}

	else if (scene == 9)
	{
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -5.0f);
		LastCredits();
		glPopMatrix();

	}

	else if (scene == 10)
	{
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -5.0f);
		SpecialThanks();
		glPopMatrix();

	}

	else if (scene == 11)
	{
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -5.0f);
		IgnitedBy();
		glPopMatrix();
	}

	else if (scene == 12)
	{
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -5.0f);
		TheEnd();
		glPopMatrix();

	}
	
	// end of display 
	glPushMatrix();
	drawQuad();
	glPopMatrix();

	//ScreenOne();
	//ScreenTwo();  // Medical Scene
	//ScreenThree();  // outside operation theaters Scene
	//ScreenFour(); // Solar System
	//ScreenFive(); // Computer screen


	SwapBuffers(ghdc);
}

void ScreenOne(void)
{
	gluLookAt(0.0f, 0.0f, ScreenTheatreZ, 0.0f, 0.0f, -5.0f, 0.0f, 1.0f, ScreenTheatreZ);
	glTranslatef(0.0f, 0.0f, -5.0f);

	glPushMatrix(); 

		theatreStage();
		theatreCurtain();

	glPopMatrix();
}

void ScreenTwo(void) {
	//glTranslatef(0.0f, 0.0f, -2.2f);


	glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.2f);
		//glScalef(2.5f, 2.5f, 2.5f);
		Clinic3D(); //Clinic Room
	glPopMatrix();


	glPushMatrix();
		glTranslatef(-0.85f, 0.50f, -0.0f);
		glScalef(0.15f, 0.15f, 0.0f);
		clock();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.25f,-0.5f,-0.0f);
		glScalef(0.70f,0.75f,0.0f);
		Door();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(0.25f, -0.60f, 0.0f);
		glScalef(1.0f, 0.85f, 1.0f);
		Operation_theatre(); // Partision
	glPopMatrix();

	glPushMatrix();
		glTranslated(-0.15f,-0.30f,0.0f);
		glScalef(0.40f,0.40f,0.0f);
		Doctor();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.30f, -0.25f, 0.0f);
		glScalef(1.15f, 1.0f, 1.0f);
		Bed();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-0.60f, -0.30f, 0.0f);
		glScalef(1.0f,0.70f,1.0f);
		ReportMeasure();
	glPopMatrix();

	glPushMatrix();
	    glTranslatef(-0.55f, -0.46f, 0.0f);
		glScalef(0.60f, 0.50f, 1.0f);
		SleepBoy();
	glPopMatrix();

	glPushMatrix();
		glScalef(0.40f,0.40f,1.0f);
		Thoughtcloud();
	glPopMatrix();
}

void ScreenThree(void)
{
	// Outside operation theater scene
	//glTranslatef(0.0f, 0.0f, -4.0f);
	gluLookAt(0.0f, 0.0f, GanpatiSceneZ, 0.0f, 0.0f, -4.0f, 0.0f, 1.0f, GanpatiSceneZ);
	glPushMatrix();

		aks_myObject();

		glPushMatrix();
		AKJ_benches();

		glPushMatrix();
		glTranslatef(0.0f, 0.0f, 0.1f);
		AKJ_people();
		glPopMatrix(); // pop for AKJ_human();

		glPopMatrix(); // pop for AKJ_benches();

	glPopMatrix(); // pop for aks_myObject();

}

void ScreenFour(void)
{
	gluLookAt(0.0f, SolarSystemY, SolarSystemZ, 0.0f, 0.0f, 0.0f, 0.0f, SolarSystemY + 1.0f, SolarSystemZ);
	glRotatef(-8.0f, 0.0f, 0.0f, 1.0f);
	PNV_SolarSystem(); // solar system call
}

void ScreenFive(void)
{
	glTranslatef(0.0f, 0.0f, -5.0f);

	glPushMatrix();
		glScalef(1.0f, 1.0f, 1.0f);
		Computer_Scene();
	glPopMatrix();
}

void ScreenSix(void)
{
	// Function Declaration
	void ScreenSixMiddle(void);

	// Variable Declarations
	FGL_Color c = {1.0f, 1.0f, 1.0f, 1.0f};

	glPushMatrix();

		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(Galaxy_Move, 0.0f, 1.0f, 0.0f);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		gluQuadricTexture(quadric, GL_TRUE);
		glBindTexture(GL_TEXTURE_2D, galaxy_texture);

		gluSphere(quadric, 40.00, 50, 50);

		glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();

	// Code

	glPushMatrix();

		glTranslatef(-0.4f, 0.1f, -1.0f);
		glScalef(0.0f, 0.0f, 0.0f);
		if (SixScreenSceneFlag >= 6) {

			HFONT hFont = CreateFont(
				100,
				0,
				0,
				0,
				FW_BOLD,
				FALSE,
				FALSE,
				FALSE,
				ANSI_CHARSET,
				OUT_DEFAULT_PRECIS,
				CLIP_DEFAULT_PRECIS,
				DEFAULT_QUALITY,
				DEFAULT_PITCH | FF_DONTCARE,
				"impact.ttf"
			);
			SelectObject(ghdc, hFont);

			glColor4f(1.0f, 1.0f, 1.0f, HBDAlpha);
			wglUseFontBitmaps(ghdc, 0, 255, 1000);

			glRasterPos3f(0.0f, 0.8f, -1.0f);
			glListBase(1000);
			char str1[] = "HAPPY BIRTHDAY SIR";

			glCallLists(strlen(str1), GL_UNSIGNED_BYTE, str1);
			DeleteObject(hFont);
		}

	glPopMatrix();

	// Solar System
	if (SixScreenSceneFlag >= 1 && SixScreenSceneFlag < 6) {

		glPushMatrix();
			glTranslatef(-8.8f, 2.5f, -20.0f);
			glScalef(0.3f, 0.4f, 0.4f);
			glRotatef(20.0f, 1.0f, 0.0f, 0.0f);
			PNV_SolarSystem();
		glPopMatrix();
	}

	// Medical  
	if (SixScreenSceneFlag >= 2 && SixScreenSceneFlag < 5) {

		glPushMatrix();
		glTranslatef(0.70f, 1.20f, -5.0f);
		glScalef(1.20f, 1.20f, 0.20f);
		ScreenSixMiddle();
		glPopMatrix();
	}

	// Computer
	if (SixScreenSceneFlag == 3 && SixScreenSceneFlag < 4) {

		glPushMatrix();
			glTranslatef(3.10f, 0.65f, -5.0f);
			glScalef(0.3f, 0.3f, 0.3f);
			glRotatef(8.0f, 1.0f, 0.0f, 0.0f);
			Computer_Scene();
		glPopMatrix();
	}

	// ASTROLOGY
	if (SixScreenSceneFlag >= 1) {

		glPushMatrix();
		glTranslatef(screensix_Astro, 1.20f, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		AstroText();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-2.15f, letterZ, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("L", &c);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.95f, letterX, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("O", &c);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.65f, letterY, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("G", &c);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-1.35f, letterW, -5.0f);
		glScalef(0.30f, 0.50f, 0.50f);
		Text("Y", &c);
		glPopMatrix();
	}

	// MEDICAL
	if (SixScreenSceneFlag >= 2) {

		glPushMatrix();
		glTranslatef(screensix_Medi, 1.20f, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		MediText();
		glPopMatrix();

		FGL_ColorAssign(&c, 1.0f, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
		glTranslatef(0.35f, letterZ, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("C", &c);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0.67f, letterY, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("A", &c);
		glPopMatrix();


		glPushMatrix();
		glTranslatef(0.95f, letterX, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("L", &c);
		glPopMatrix();
	}

	// COMPUTER
	if (SixScreenSceneFlag >= 3) {

		glPushMatrix();
		glTranslatef(screensix_Comp, 0.9f, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		CompText();
		glPopMatrix();

		FGL_ColorAssign(&c, 0.0f, 0.8f, 0.0f, 1.0f);
		glPushMatrix();
		glTranslatef(2.73f, letterX, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("U", &c);
		glPopMatrix();


		glPushMatrix();
		glTranslatef(3.0f, letterZ, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("T", &c);
		glPopMatrix();


		glPushMatrix();
		glTranslatef(3.25f, letterY, -5.0f);
		glScalef(0.10f, 0.30f, 0.40f);
		Text("E", &c);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(3.6f, letterW, -5.0f);
		glScalef(0.30f, 0.50f, 0.50f);
		Text("R", &c);
		glPopMatrix();
	}
	 

	// Director Chair
	glPushMatrix();
		glTranslatef(0.0f, -1.0f, -5.0f);
		glScalef(0.30f, 0.45f, 0.60f);
		Director_Chair();
	glPopMatrix();	
}

void ScreenSixMiddle(void)
{

	glTranslatef(0.0f, 0.0f, -2.20f);


	glPushMatrix();
		glPushMatrix();
		glTranslated(-0.15f, -0.30f, 0.0f);
		glScalef(0.40f, 0.40f, 0.0f);
		Doctor();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-0.30f, -0.25f, 0.0f);
		glScalef(1.15f, 1.0f, 1.0f);
		Bed();
		glPopMatrix();



		glPushMatrix();
		glTranslatef(-0.60f, -0.30f, 0.0f);
		glScalef(1.0f, 0.70f, 1.0f);
		ReportMeasure();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(-0.55f, -0.46f, 0.0f);
			glScalef(0.60f, 0.50f, 1.0f);
			SleepBoy();
		glPopMatrix();
	glPopMatrix();

}

void drawQuad(void)
{
	//code
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -0.1f);
	glScalef(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
		glColor4f(0.0f, 0.0f, 0.0f, alpha); // Width Right Rectangle : Black
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();
	glDisable(GL_BLEND);

}

void update(void)
{
	//Code 

	// variable declaration for Scene fadeIn fadeOut
	static SYSTEMTIME currentTime;

	// Code
	GetLocalTime(&currentTime);		// get the current local time

	// get the time difference between the time of the previous call of the update() and current call of the update() as below :
	ULONGLONG timeDiff = ((ULONGLONG)currentTime.wSecond * 1000 + currentTime.wMilliseconds) -
		((ULONGLONG)previousTime.wSecond * 1000 + previousTime.wMilliseconds);

	if (fade == 0)
	{
		if (alpha > 0.0f)
		{
			switch (scene) {
				case 1:
					alpha -= 0.0015f;
					break;
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					alpha -= 0.003f;
					break;
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
					alpha -= 0.001f;
					break;
			}
		}
	}
	if (fade == 1)
	{
		if (alpha < 1.0f)
		{
			switch (scene) {
				case 2:
					alpha += 0.004f;
					break;
				case 6:
					alpha += 0.006f;
					break;
				case 1:
				case 3:
				case 4:
				case 5:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
					alpha += 0.003f;
					break;
			}
		}
	}

	if (SixScreenSceneFlag >= 6) {

		if (HBDAlpha < 1.0f)
			HBDAlpha += 0.005f;
	}

	if (timeDiff > 2)	// this value '2' here can be changed to adjust the required speed of animation (for e.g, if you want to increase the speed, then decrease this value or else vice-versa)
	{

		// For theater curtains
		//Code
		if (AMA_CurtainOpenFlag == TRUE)
		{
			AMA_CurtainTranslateDist = AMA_CurtainTranslateDist + 0.003f;
			if (AMA_CurtainTranslateDist >= 2.6f)
			{
				AMA_CurtainTranslateDist = 2.6f;
				if (KernelAlpha < 1.0f)
					KernelAlpha += 0.005f;
				else if (ScreenTheatreZ > -1.0f) 
					ScreenTheatreZ -= 0.001f;
			}
		}

		if (scene == 4) {
			
			if (DoctorX < 0.0f) {
			
				DoctorX += 0.000045f;
				DoctorY += 0.000043f;
				DoctorZ -= 0.000185f;
			}
			else {
			
				DoctorX = 0.0f;
				DoctorY = 0.0f;
				DoctorZ = -2.2f;
			}
		}

		if (scene == 5) {
			
			if(GanpatiSceneZ > -3.5f)
				GanpatiSceneZ -= 0.003f;
		}

		// Animation for Solar System
		if (SixScreenSceneFlag == 1) {

			Galaxy_Move += 0.02f;
		}
		else {

			Galaxy_Move += 0.008f;
		}

		SunAngle += 0.01f;

		Planet_Day += 1.0f;

		Mercury_Year += 0.6f;
		Venus_Year += 0.52f;
		Earth_Year += 0.44f;
		Mars_Year += 0.4f;
		Jupiter_Year += 0.28f;
		Saturn_Year += 0.2f;
		Uranus_Year += 0.12f;
		Neptune_Year += 0.04f;

		if (scene == 6) {

			if (SolarSystemZ < 25.0f) {

				SolarSystemY += (5.0f * 0.0005f);
				SolarSystemZ += (5.0f * 0.0023f);
			}
		}


		// Animation for Computer Screen
		Fan_Angle += 3.5f;

		if (scene == 7)
		{
			matrixTranslate -= 0.001f;

			if(computerSceneAngle > 0.0f)
				computerSceneAngle -= 0.03f;


			if (screensix_timer == 0)
			{

				if (Lines_Time < 25.001f) {

					Lines_Time += 0.005f;
				}
				else if (BulletX <= 1.55f) {

					BulletAlpha += 0.01f;
					BulletX += 0.00145f;
					redBulletY += 0.0004f;
					blueBulletY -= 0.0004f;
					BulletZ -= 0.0001f;
				}
				else {

					BulletAlpha = 0.0f;

					if (MothaBachanAlpha > 0.0f)
						MothaBachanAlpha -= 0.00005f;
				}

			}
			else
			{
				if (Lines_Time < 25.001f) {

					Lines_Time += 0.05f;
				}
				else if (BulletX <= 1.55f) {

					BulletAlpha += 0.01f;
					BulletX += 0.0145f;
					redBulletY += 0.004f;
					blueBulletY -= 0.004f;
					BulletZ -= 0.001f;
				}
				else {

					BulletAlpha = 0.0f;

					if (MothaBachanAlpha > 0.0f)
						MothaBachanAlpha -= 0.005f;
				}
			}
		}

		previousTime = currentTime;		// set the current time to the previous time
	}

	galaxy_angle += 0.1f;

	if (SixScreenSceneFlag >= 3) {

		if (FontDropWait > 1.0f) {

			if (screensix_timer > 1.7f)
			{
				if (screensix_Astro <= -1.93f)
				{
					screensix_Astro += 0.01f;
				}

				if (screensix_Comp >= 0.6f)
				{
					screensix_Comp -= 0.006f;
				}

				if (screensix_Medi < -0.48f)
				{
					screensix_Medi += 0.0018f;
				}

			}

			if (screensix_timer > 0.2f)
			{
				letterY -= 0.04f;
			}

			if (screensix_timer > 0.3f)
			{
				letterX -= 0.03f;
			}

			if (screensix_timer > 0.5f)
			{
				letterZ -= 0.03f;
			}

			if (screensix_timer > 0.55f)
			{
				letterW -= 0.02f;
			}
		}

		FontDropWait += 0.055f;
		screensix_timer += 0.005f;
	}
	
	if (ChairFlag == 1) {

		chair_angle += 1.0f;
	}
}

void uninitialize(void)
{	
	//Function declaration
	void ToggleFullscreen(void);
	
	//If application is exiting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Code
	//Make the hdc as current dc
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	//Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	//Destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// uninitialize texture

	if (texture_wood)
	{
		glDeleteTextures(1, &texture_wood);
		texture_wood = 0;
	}

	//CLose the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "Program ended successfully.");
		fclose(gpFILE);
		gpFILE = NULL;
	}
}
